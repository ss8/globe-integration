package com.ss8.globe.util;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class DateUtil {

    private static final String DATE_TIMEZONE_FORMAT_ELASTIC = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static String convertDateToUTCString(final LocalDateTime date) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(DATE_TIMEZONE_FORMAT_ELASTIC);
        if (date != null) {
            return date.toString(fmt);
        }
        return null;
    }

    public static DateTime convertUTCStringToDate(final String dateString) {
        if(dateString != null) {
            return ISODateTimeFormat.dateTimeParser().parseDateTime(dateString);
        }
        return null;
    }
}
