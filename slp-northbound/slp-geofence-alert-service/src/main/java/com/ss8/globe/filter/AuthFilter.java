package com.ss8.globe.filter;

import com.ss8.globe.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthFilter extends OncePerRequestFilter {

    @Value("${geofencealert.api.hashkey:5cc4007c7b90607828a259142ea805a0}")
    private String localHashKey;

    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String hashKey = request.getHeader("hash-key");

        if(!localHashKey.equals(hashKey)) {
            //throw new UnauthorizedException("Unauthorized access to api");
            resolver.resolveException(request, response, null, new UnauthorizedException("Unauthorized access to api"));
        }

        filterChain.doFilter(request, response);
    }
}
