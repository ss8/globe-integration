package com.ss8.globe.dto;

import org.joda.time.DateTime;

public class DocEventESDTO {

    private String eventId;
    private DateTime eventDate;
    private boolean isEnterEvent;

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public DateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(DateTime eventDate) {
        this.eventDate = eventDate;
    }

    public boolean isEnterEvent() {
        return isEnterEvent;
    }

    public void setEnterEvent(boolean enterEvent) {
        isEnterEvent = enterEvent;
    }
}
