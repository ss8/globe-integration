package com.ss8.globe.controller;

import com.ss8.globe.dto.AlertDTO;
import com.ss8.globe.service.AlertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/geofencealert/alert")
public class AlertController {

    @Autowired
    private AlertService alertService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAlert(@RequestBody AlertDTO alertDTO) {
        alertService.createAlert(alertDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @DeleteMapping(value = "/{alert_id}")
    public ResponseEntity deleteAlert(@PathVariable("alert_id") Long alertID) {
        alertService.deleteAlert(alertID);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
