package com.ss8.globe.schedule;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.globe.dto.DocEventESDTO;
import com.ss8.globe.dto.GeofenceUpdateDTO;
import com.ss8.globe.model.Alert;
import com.ss8.globe.model.AlertData;
import com.ss8.globe.model.AlertLastRun;
import com.ss8.globe.service.AlertLastRunService;
import com.ss8.globe.service.AlertService;
import com.ss8.globe.service.AreaService;
import com.ss8.globe.service.ElasticService;
import com.ss8.globe.service.EventService;
import org.elasticsearch.common.geo.GeoPoint;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Component
public class GeoFencingAlertScheduler {
    private static final Logger LOG = LoggerFactory.getLogger(GeoFencingAlertScheduler.class);
    @Autowired
    private AlertService alertService;
    @Autowired
    private AreaService areaService;
    @Autowired
    private ElasticService elasticService;
    @Autowired
    private EventService eventService;
    @Autowired
    private AlertLastRunService alertLastRunService;
    //@Lazy
    @Autowired
    private RestTemplate restTemplate;

    @Value("${geofence.updateapi.clientname:inteldev-156-140}")
    private String geofenceUpdateClientName;
    @Value("${geofence.updateapi.hashkey:de747f001f62e6b6ead44f69e91344ac}")
    private String geofenceUpdateHashKey;
    @Value("${geofence.updateapi.url:http://localhost:80/intellego/geofencing/document/{document_id}/geofences}")
    private String geofenceUpdateApiUrl;
    @Value("${threadpool.max.size:10}")
    private int MAX_THREAD_POOL_SIZE;
    @Value("${globe.slp.geofencealert.cronjob.lookback.max.mins:15}")
    private int maxLookBackMins;

    @Scheduled(cron = "${globe.slp.geofencealert.cron:0 0/15 * * * *}")
    public void monitorAlert() {

        LOG.info("Started Alert Monitor Job at : " + new Date());
        long start = System.currentTimeMillis();
        List<Alert> alerts = alertService.getAllAlerts();

        if(null == alerts || alerts.size() == 0) {
            LOG.info("No alerts to monitor. Exiting...");
            return;
        }
        AlertLastRun alertLastRun = alertLastRunService.getLastRun(1);
        Map<String, String> lastRunData = new HashMap<>();
        LocalDateTime currTime = LocalDateTime.now(DateTimeZone.UTC);
        LocalDateTime lastRunTime = null; //currTime.minusMinutes(maxLookBackMins);

        if(null != alertLastRun && null != alertLastRun.getLastRunDate()) {
            lastRunTime = alertLastRun.getLastRunDate();
        }

        // Set last run time to fetch data from elastic to max look back time if greater than max lookback time
        if(null == lastRunTime || Period.fieldDifference(lastRunTime, currTime).getMinutes() > maxLookBackMins) {
            lastRunTime = currTime.minusMinutes(maxLookBackMins);
        }
        if(null != alertLastRun && null != alertLastRun.getLastRunData()) {
            LOG.debug("Found last run data : " + alertLastRun.getLastRunData());
            try {
                lastRunData = new ObjectMapper().readValue(alertLastRun.getLastRunData()
                        , new TypeReference<Map<String, String>>() {});
            } catch (JsonProcessingException e) {
                LOG.error(e.getMessage(), e);
            }
        }

        for(Alert alert : alerts) {
            Map<String, String> updatedData = performAlertTask(alert, lastRunTime, currTime, lastRunData);
            for(String key : updatedData.keySet()) {
                lastRunData.put(key, updatedData.get(key));
            }
        }

        // Saving the current status in the alert_last_run table to reference in next cycle
        try {
            if(alertLastRun == null) alertLastRun = new AlertLastRun();
            alertLastRun.setLastRunId(1);
            alertLastRun.setLastRunDate(currTime);
            alertLastRun.setLastRunData(new ObjectMapper().writeValueAsString(lastRunData));
            alertLastRunService.saveLastRun(1, currTime, new ObjectMapper().writeValueAsString(lastRunData));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        long totalTimeTaken = (System.currentTimeMillis() - start)/1000;
        LOG.info("Finished Alert Monitor Job at : " + new Date());
        LOG.info(" Total Time taken (in seconds): "  + totalTimeTaken);
    }

    private Map<String, String> performAlertTask(Alert alert, LocalDateTime lastRunTime, LocalDateTime currTime, Map<String, String> lastRunData) {

        List<Long> interceptIDs = getAlertDataIds(alert, AlertData.TYPE_INTERCEPT);
        List<Long> areaIDs = getAlertDataIds(alert, AlertData.TYPE_GEO_FENCING_AREA);

        Map<String, String> updatedData = new HashMap<>();

        for(Long areaID : areaIDs) {
            for(Long interceptID : interceptIDs) {

                List<GeoPoint> location = areaService.getAreaGeoPoints(areaID);

                List<DocEventESDTO> enterEvents = eventService.
                        getEventsEnter(location, lastRunTime, currTime, interceptID);
                List<DocEventESDTO> exitEvents = eventService.
                        getEventsExit(location, lastRunTime, currTime, interceptID);


                List<DocEventESDTO> allEventsSorted = eventService.getAllEventsSorted(enterEvents, exitEvents);

                Map<String, String> updated = computeAlertsAndSend(allEventsSorted, alert.getName(), lastRunData, interceptID, areaID);

                for(String key : updated.keySet()) {
                    updatedData.put(key, updated.get(key));
                }
            }
        }

        return updatedData;
    }

    private Map<String, String> computeAlertsAndSend(List<DocEventESDTO> allEventsSorted, String alertName, Map<String, String> lastRunData, Long interceptID, Long areaID) {

        String key = interceptID + ":" + areaID;
        Map<String, String> updatedData = new HashMap<>();
        updatedData.put(key, lastRunData.get(key));
        for(DocEventESDTO event : allEventsSorted) {
            String currState = event.isEnterEvent() ? "ENTER" : "EXIT";
            String prevState = updatedData.get(key);

            if(isStateChanged(currState, prevState)) {
                sendGeoFenceAlert(event.getEventId(), alertName);
                updatedData.put(key, currState);
            }
        }

        return updatedData;
    }

    private void sendGeoFenceAlert(String eventId, String alertName) {

        Map<String, String> params = new HashMap<>();
        params.put("document_id", eventId);

        List<String> geofenceValues = new ArrayList<>();
        geofenceValues.add(alertName);
        geofenceValues.add(eventId);

        GeofenceUpdateDTO geofenceUpdate = new GeofenceUpdateDTO();
        geofenceUpdate.setClientname(this.geofenceUpdateClientName);
        //geofenceUpdate.setHashkey(this.geofenceUpdateHashKey);
        geofenceUpdate.setValue(geofenceValues);

        HttpHeaders headers = new HttpHeaders();
        headers.add("hash-key", this.geofenceUpdateHashKey);

        HttpEntity<GeofenceUpdateDTO> entity = new HttpEntity<>(geofenceUpdate, headers);
        LOG.debug(String.format("URL : %1$s  --  eventID : %2$s  --  alertName: %3$s" ,this.geofenceUpdateApiUrl, eventId, alertName));
        try {
            ResponseEntity<String> resp = restTemplate.exchange(this.geofenceUpdateApiUrl, HttpMethod.PUT, entity, String.class, params);
            LOG.debug(resp.toString());
        } catch (Exception e) {
            LOG.error(String.format("Unable to send request to geofence update api: [eventId = %1$s , alertName = %2$s ]",eventId ,alertName ), e);
        }
    }

    private boolean isStateChanged(String currState, String prevState) {
        boolean isStateChanged = false;

        if(null == prevState && currState.equals("ENTER")) {
            isStateChanged = true;
        } else if(null != prevState && !currState.equals(prevState)) {
            isStateChanged = true;
        }

        return isStateChanged;
    }

    private List<Long> getAlertDataIds(Alert alert, int type) {

        if(null == alert.getData()) return null;

        List<Long> ids = new ArrayList<>();

        for(AlertData alertData : alert.getData()) {

            if(alertData.getType() == type) {
                ids.add(alertData.getTypeId());
            }
        }

        return ids;
    }

}
