package com.ss8.globe.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.globe.dto.AreaGeometryDTO;
import com.ss8.globe.model.Area;
import com.ss8.globe.repository.AreaRepository;
import org.elasticsearch.common.geo.GeoPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AreaService {

    @Autowired
    private AreaRepository areaRepository;

    private Logger LOGGER = LoggerFactory.getLogger(AreaService.class);

    @Cacheable(cacheNames = "areaCache", key = "#areaID")
    public List<List<Double>> getAreaLocation(long areaID) {

        Area area = areaRepository.findById(areaID).orElse(null);

        if(null == area) return null;

        List<List<Double>> locationCoordinates = null;

        ObjectMapper mapper = new ObjectMapper();
        try {
            if(null != area.getGeometry() && area.getGeometry().length() > 0) {
                List<AreaGeometryDTO> geometryList = mapper.readValue(area.getGeometry()
                        , new TypeReference<List<AreaGeometryDTO>>() {});

                if(null != geometryList && geometryList.size() > 0) {
                    for(AreaGeometryDTO geometryDTO : geometryList) {

                        if(null != geometryDTO.getGeometry()
                                && null != geometryDTO.getGeometry().getCoordinates()
                                && geometryDTO.getGeometry().getCoordinates().size() > 0) {

                            if(null == locationCoordinates) locationCoordinates = new ArrayList<>();

                            locationCoordinates.addAll(geometryDTO.getGeometry().getCoordinates().get(0));
                        }
                    }
                }

            }

        } catch (JsonProcessingException e) {
            LOGGER.warn(e.getMessage());
        }

        return locationCoordinates;
    }

    public List<GeoPoint> getAreaGeoPoints(long areaID) {
        List<List<Double>> location = getAreaLocation(areaID);

        List<GeoPoint> points = new ArrayList<>();

        if(null != location && location.size() > 0) {
            for(List<Double> geoPoint : location) {
                points.add(new GeoPoint(geoPoint.get(0), geoPoint.get(1)));
            }
        } else {
            removeAreaFromCache(areaID);
        }

        return points;
    }

    @CacheEvict(cacheNames = "areaCache", key = "#areaID")
    public void removeAreaFromCache(long areaID) {

    }
}
