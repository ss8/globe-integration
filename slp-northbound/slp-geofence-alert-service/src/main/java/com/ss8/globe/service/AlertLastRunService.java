package com.ss8.globe.service;

import com.ss8.globe.model.AlertLastRun;
import org.joda.time.LocalDateTime;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AlertLastRunService {

    @Cacheable(cacheNames = "lastRunCache", key = "#id")
    public AlertLastRun getLastRun(int id) {
        AlertLastRun alertLastRun = new AlertLastRun();
        alertLastRun.setLastRunId(1);
        return alertLastRun;
    }

    @CachePut(cacheNames = "lastRunCache", key = "#alertLastRun.lastRunId")
    public AlertLastRun saveLastRun(AlertLastRun alertLastRun) {
        return alertLastRun;
    }

    @CachePut(cacheNames = "lastRunCache", key = "#id")
    public AlertLastRun saveLastRun(int id, LocalDateTime time, String data) {
        AlertLastRun alertLastRun = new AlertLastRun();
        alertLastRun.setLastRunId(id);
        alertLastRun.setLastRunData(data);
        alertLastRun.setLastRunDate(time);
        return alertLastRun;
    }
}
