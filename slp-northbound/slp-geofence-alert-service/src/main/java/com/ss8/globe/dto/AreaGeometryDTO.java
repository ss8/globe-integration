package com.ss8.globe.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AreaGeometryDTO {

    private GeometryCoordinates geometry;

    public GeometryCoordinates getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryCoordinates geometry) {
        this.geometry = geometry;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class GeometryCoordinates {
        private List<List<List<Double>>> coordinates;

        public List<List<List<Double>>> getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(List<List<List<Double>>> coordinates) {
            this.coordinates = coordinates;
        }
    }
}
