package com.ss8.globe.service;

import com.ss8.globe.dto.DocEventESDTO;
import com.ss8.globe.util.DateUtil;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ExistsQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class EventService {
    private static final Logger LOG = LoggerFactory.getLogger(EventService.class);
    @Autowired
    private ElasticService elasticService;
    @Value("${elastic.index.name:intellego}")
    private String indexName = "intellego";
    private String eventLocationFieldName = "eventLocations";
    private String eventDateFieldName = "eventDate";
    private String interceptFieldName = "intercept";
    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public List<DocEventESDTO> getAllEventsSorted(List<DocEventESDTO> enterEvents, List<DocEventESDTO> exitEvents) {

        Set<String> enterEventIDSet = enterEvents.stream().map(e -> e.getEventId()).collect(Collectors.toSet());
        Set<String> exitEventIDSet = exitEvents.stream().map(e -> e.getEventId()).collect(Collectors.toSet());

        Set<String> allEventIDs = new HashSet<>();
        allEventIDs.addAll(enterEventIDSet);
        allEventIDs.addAll(exitEventIDSet);

        List<DocEventESDTO> events = new ArrayList<>();

        if(allEventIDs.size() > 0) {
            BoolQueryBuilder query = buildElasticFetchEventQuery(allEventIDs);
            FieldSortBuilder sortBuilder = new FieldSortBuilder(eventDateFieldName).order(SortOrder.ASC);
            SearchResponse response = null;
            try {
                response = elasticService.queryElastic(indexName, query, sortBuilder);

                SearchHits searchHits = response.getHits();

                if(null != searchHits && searchHits.getHits().length > 0) {

                    for (SearchHit searchHit : searchHits) {
                        DocEventESDTO event = new DocEventESDTO();
                        event.setEventId(searchHit.getId());

                        if(enterEventIDSet.contains(event.getEventId())) {
                            event.setEnterEvent(true);
                        } else {
                            event.setEnterEvent(false);
                        }
                        events.add(event);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }

        return events;
    }

    public List<DocEventESDTO> getEventsEnter(List<GeoPoint> geoPoints, LocalDateTime lastRun, LocalDateTime curr, Long intercept) {

        BoolQueryBuilder query = buildElasticFetchEventGeoPointQuery(geoPoints, lastRun, curr, intercept, true);
        FieldSortBuilder sortBuilder = new FieldSortBuilder(eventDateFieldName).order(SortOrder.ASC);

        SearchResponse response = null;
        List<DocEventESDTO> events = new ArrayList<>();
        try {
            response = elasticService.queryElastic(indexName, query, sortBuilder);
            SearchHits searchHits = response.getHits();

            if(null != searchHits && searchHits.getHits().length > 0) {

                for (SearchHit searchHit : searchHits) {
                    if(isNumeric(searchHit.getId())) {
                        DocEventESDTO event = new DocEventESDTO();
                        event.setEventId(searchHit.getId());
                        event.setEnterEvent(true);
                        events.add(event);
                    }
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return events;
    }

    public List<DocEventESDTO> getEventsExit(List<GeoPoint> geoPoints, LocalDateTime lastRun, LocalDateTime curr, Long intercept) {

        BoolQueryBuilder query = buildElasticFetchEventGeoPointQuery(geoPoints, lastRun, curr, intercept, false);
        FieldSortBuilder sortBuilder = new FieldSortBuilder(eventDateFieldName).order(SortOrder.ASC);

        SearchResponse response = null;
        List<DocEventESDTO> events = new ArrayList<>();
        try {
            response = elasticService.queryElastic(indexName, query, sortBuilder);
            SearchHits searchHits = response.getHits();

            if(null != searchHits && searchHits.getHits().length > 0) {

                for (SearchHit searchHit : searchHits) {
                    if(isNumeric(searchHit.getId())) {
                        DocEventESDTO event = new DocEventESDTO();
                        event.setEventId(searchHit.getId());
                        event.setEnterEvent(false);
                        events.add(event);
                    }
                }
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return events;
    }

    private BoolQueryBuilder buildElasticFetchEventGeoPointQuery(List<GeoPoint> geoPoints, LocalDateTime lastRun, LocalDateTime curr, Long intercept, boolean isEnter) {
        String currTime = DateUtil.convertDateToUTCString(curr);
        String lastRunTime = DateUtil.convertDateToUTCString(lastRun);

        BoolQueryBuilder query = new BoolQueryBuilder()
                .must(new TermsQueryBuilder(interceptFieldName, Arrays.asList(new Long[] {intercept})))
                .must(new ExistsQueryBuilder(eventLocationFieldName));

        if(isEnter) {
            query.must(QueryBuilders.geoPolygonQuery(eventLocationFieldName, geoPoints));
        } else {
            query.mustNot(QueryBuilders.geoPolygonQuery(eventLocationFieldName, geoPoints));
        }

        if(null == lastRunTime) {
            query.must(new RangeQueryBuilder(eventDateFieldName).lte(currTime));
        } else {
            query.must(new RangeQueryBuilder(eventDateFieldName).lte(currTime).gte(lastRunTime));
        }

        return query;
    }

    private BoolQueryBuilder buildElasticFetchEventQuery(Set<String> eventIds) {
        BoolQueryBuilder query = new BoolQueryBuilder()
                .must(new TermsQueryBuilder("_id", eventIds));;

        return query;
    }

    private boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }
}
