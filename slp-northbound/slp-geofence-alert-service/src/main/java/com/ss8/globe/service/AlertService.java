package com.ss8.globe.service;

import com.ss8.globe.dto.AlertDTO;
import com.ss8.globe.dto.AreaDTO;
import com.ss8.globe.dto.InterceptDTO;
import com.ss8.globe.model.Alert;
import com.ss8.globe.model.AlertData;
import com.ss8.globe.repository.AlertRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;

@Service
public class AlertService {

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private AreaService areaService;

    public void createAlert(AlertDTO alertDTO) {

        validateAlertDTO(alertDTO);
        Alert alert = convertDTOToModel(alertDTO);
        alertRepository.save(alert);
    }

    public List<Alert> getAllAlerts() {
        return alertRepository.findAll();
    }

    private Alert convertDTOToModel(AlertDTO alertDTO) {

        Alert alert = new Alert();

        alert.setAlertId(alertDTO.getAlertID());
        alert.setName(alertDTO.getName());
        alert.setDescription(alertDTO.getDescription());
        alert.setData(new HashSet<AlertData>());
        alert.setCreateDate(LocalDateTime.now());
        alert.setUpdateDate(LocalDateTime.now());

        for(InterceptDTO intercept : alertDTO.getIntercepts()) {
            AlertData alertData = new AlertData();
            alertData.setType(AlertData.TYPE_INTERCEPT);
            alertData.setTypeId(intercept.getId());
            alertData.setTypeName(intercept.getName());
            alertData.setCreateDate(LocalDateTime.now());
            alertData.setUpdateDate(LocalDateTime.now());

            alertData.setAlert(alert);

            alert.getData().add(alertData);
        }

        for(AreaDTO area : alertDTO.getAreas()) {
            AlertData alertData = new AlertData();
            alertData.setType(AlertData.TYPE_GEO_FENCING_AREA);
            alertData.setTypeId(area.getId());
            alertData.setTypeName(area.getName());
            alertData.setCreateDate(LocalDateTime.now());
            alertData.setUpdateDate(LocalDateTime.now());

            alertData.setAlert(alert);

            alert.getData().add(alertData);
        }

        return alert;
    }

    private void validateAlertDTO(AlertDTO alertDTO) {

        if(null == alertDTO.getAlertID() || alertDTO.getAlertID() == 0)
            throw new IllegalArgumentException("Invalid alertID");

        if(null == alertDTO.getIntercepts() || alertDTO.getIntercepts().size() == 0)
            throw new IllegalArgumentException("No intercepts provided");

        if(null == alertDTO.getAreas() || alertDTO.getAreas().size() == 0)
            throw new IllegalArgumentException("No areas provided");

        for(InterceptDTO intercept : alertDTO.getIntercepts()) {
            if(null == intercept.getId() || intercept.getId() == 0)
                throw new IllegalArgumentException("Invalid intercept id");
        }

        for(AreaDTO area : alertDTO.getAreas()) {
            if(null == area.getId() || area.getId() == 0)
                throw new IllegalArgumentException("Invalid area id");
        }
    }

    public void deleteAlert(Long alertID) {
        alertRepository.deleteById(alertID);
    }
}
