package com.ss8.globe.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AlertDTO {

    private Long alertID;
    private String name;
    private String description;
    private List<InterceptDTO> intercepts;
    private List<AreaDTO> areas;

    public Long getAlertID() {
        return alertID;
    }

    public void setAlertID(Long alertID) {
        this.alertID = alertID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<InterceptDTO> getIntercepts() {
        return intercepts;
    }

    public void setIntercepts(List<InterceptDTO> intercepts) {
        this.intercepts = intercepts;
    }

    public List<AreaDTO> getAreas() {
        return areas;
    }

    public void setAreas(List<AreaDTO> areas) {
        this.areas = areas;
    }
}
