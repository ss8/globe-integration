package com.ss8.globe.test;

import com.ss8.globe.dto.AlertDTO;
import com.ss8.globe.dto.AreaDTO;
import com.ss8.globe.dto.InterceptDTO;
import com.ss8.globe.model.Alert;
import com.ss8.globe.model.AlertData;
import com.ss8.globe.repository.AlertRepository;
import com.ss8.globe.repository.AreaRepository;
import com.ss8.globe.service.AlertService;
import com.ss8.globe.service.AreaService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;

@RunWith(SpringRunner.class)
public class AlertServiceTest {

    @TestConfiguration
    static class AlertServiceTestConfig {

        @Bean
        public AlertService alertService() {
            return new AlertService();
        }
        @Bean
        public AreaService areaService() {
            return new AreaService();
        }
    }

    @Autowired
    private AlertService alertService;

    @MockBean
    private AlertRepository alertRepository;
    @MockBean
    private AreaRepository areaRepository;

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Before
    public void setUp() {
        Mockito.when(alertRepository.save(Mockito.any())).thenReturn(getAlert());
    }

    @Test(expected = Test.None.class)
    public void whenValidAlertDTO_thenAlertCreated() {
        alertService.createAlert(getAlertDTO());
    }

    @Test
    public void whenAlertIDNull_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid alertID");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setAlertID(null);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenAlertIDZero_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid alertID");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setAlertID(0L);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenNoIntercepts_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("No intercepts provided");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setIntercepts(null);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenEmptyInterceptList_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("No intercepts provided");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setIntercepts(new ArrayList<>());

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenNullInterceptID_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid intercept id");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.getIntercepts().get(0).setId(null);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenZeroInterceptID_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid intercept id");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.getIntercepts().get(0).setId(0L);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenNoAreas_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("No areas provided");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setAreas(null);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenEmptyAreaList_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("No areas provided");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.setAreas(new ArrayList<>());

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenNullAreaID_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid area id");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.getAreas().get(0).setId(null);

        alertService.createAlert(alertDTO);
    }

    @Test
    public void whenZeroAreaID_thenThrowException() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid area id");

        AlertDTO alertDTO = getAlertDTO();
        alertDTO.getAreas().get(0).setId(0L);

        alertService.createAlert(alertDTO);
    }

    private AlertDTO getAlertDTO() {
        AlertDTO alert = new AlertDTO();
        alert.setAlertID(1L);
        alert.setName("Alert_1");
        alert.setDescription("Alert 1");

        InterceptDTO intercept1 = new InterceptDTO();
        intercept1.setId(1L);
        intercept1.setName("Intercept_1");

        InterceptDTO intercept2 = new InterceptDTO();
        intercept2.setId(2L);
        intercept2.setName("Intercept_2");

        AreaDTO area1 = new AreaDTO();
        area1.setId(1L);
        area1.setName("Area_1");

        AreaDTO area2 = new AreaDTO();
        area2.setId(2L);
        area2.setName("Area_2");

        alert.setIntercepts(new ArrayList<>());
        alert.getIntercepts().add(intercept1);
        alert.getIntercepts().add(intercept2);

        alert.setAreas(new ArrayList<>());
        alert.getAreas().add(area1);
        alert.getAreas().add(area2);

        return alert;
    }

    private Alert getAlert() {
        Alert alert = new Alert();
        alert.setAlertId(1L);
        alert.setName("Alert_1");
        alert.setDescription("Alert 1");

        AlertData intercept1 = new AlertData();
        intercept1.setType(AlertData.TYPE_INTERCEPT);
        intercept1.setTypeId(1L);
        intercept1.setTypeName("Intercept_1");

        AlertData intercept2 = new AlertData();
        intercept2.setType(AlertData.TYPE_INTERCEPT);
        intercept2.setTypeId(2L);
        intercept2.setTypeName("Intercept_2");

        AlertData area1 = new AlertData();
        area1.setType(AlertData.TYPE_GEO_FENCING_AREA);
        area1.setTypeId(1);
        area1.setTypeName("Area_1");

        AlertData area2 = new AlertData();
        area2.setType(AlertData.TYPE_GEO_FENCING_AREA);
        area2.setTypeId(2);
        area2.setTypeName("Area_2");

        alert.setData(new HashSet<>());
        alert.getData().add(intercept1);
        alert.getData().add(intercept2);
        alert.getData().add(area1);
        alert.getData().add(area2);

        return alert;
    }
}
