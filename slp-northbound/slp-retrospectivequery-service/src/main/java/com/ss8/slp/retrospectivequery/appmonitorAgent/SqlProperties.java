package com.ss8.slp.retrospectivequery.appmonitorAgent;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "sqlproperties")
@Component
public class SqlProperties {
	
		private	Map<String, String> dbconfig = new HashMap<String, String>();
		
		public Map<String, String> getDbconfig() {
			return dbconfig;
		}

		public void setDbconfig(Map<String, String> dbconfig) {
			this.dbconfig = dbconfig;
		}

}
