package com.ss8.slp.retrospectivequery.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.ss8.iql.query.dto.EventTypes;
import com.ss8.iql.query.dto.Field;
import com.ss8.iql.query.dto.FieldType;
import com.ss8.iql.query.dto.Fields;
import com.ss8.iql.query.dto.Option;
import com.ss8.iql.query.dto.Options;
import com.ss8.iql.query.dto.QueryBuilderConfig;
import com.ss8.iql.query.dto.SourceType;
import com.ss8.slp.retrospectivequery.dao.IngestDataSourceDAO;
import com.ss8.slp.retrospectivequery.dao.QueryConfigDao;
import com.ss8.slp.retrospectivequery.dto.DataSource;
import com.ss8.slp.retrospectivequery.dto.DataSourceEventDTO;
import com.ss8.slp.retrospectivequery.utils.CommonConstants;

@Service
public class QueryConfigServiceImpl implements QueryConfigService {

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	QueryConfigDao queryConfigDao;

	@Autowired
	IngestDataSourceDAO ingestDatasourceDao;

	@Autowired
	CacheManager cacheManager;

	@Value("${metahub.server.datasources}")
	private String fusionDataSources;

	@Value("${metahub.server.queryConfigFileLocation}")
	private String queryConfigFileLocation;

	@Value("${query_config.field_exclusions:null}")
	private String fieldExclusions;

	@Value("${query_config.event.type.intellego:null}")
	private String eventTypeIntellego;

	@Value("${metahub.intellego.deployment:false}")
	private boolean intellegoDeployment;

	private QueryBuilderConfig queryConfig = null;

	@Override
	public QueryBuilderConfig getQueryConfig() throws IOException {
		this.retrieveLatestQueryConfig();
		return this.queryConfig;
	}

	@Override
	public void flushAndFetchQueryConfig() throws IOException {
		this.evictQueryConfigCacheAtIntervals();
	}

	private synchronized void retrieveLatestQueryConfig() throws IOException {
		/*
		 * Populate the datasources by getting the datasources from the config. Convert
		 * the fusionDataSources to a json value.
		 */
		ObjectMapper mapper = new ObjectMapper();
		TypeFactory typeFactory = mapper.getTypeFactory();
		List<DataSource> dataSourcesList = mapper.readValue(fusionDataSources,
				typeFactory.constructCollectionType(List.class, DataSource.class));
		if (dataSourcesList != null && dataSourcesList.size() > 0) {

			Set<String> fieldExclusionsList = new HashSet<String>();
			if (fieldExclusions != null) {
				for (String excludedField : fieldExclusions.split(",")) {
					fieldExclusionsList.add(excludedField);
				}
			}

			QueryBuilderConfig baseQueryConfig = populateBaseQueryConfig();
			for (DataSource dataSource : dataSourcesList) {
				Map<String, SourceType> sourcesTypesMap = baseQueryConfig.getSourceTypes().getSourceTypesMap();
				SourceType sourceType = sourcesTypesMap.get(CommonConstants.DATA_SOURCE_TYPE);
				if (sourceType.getFieldTypes() != null && sourceType.getFieldTypes().getFieldTypesMap()
						.containsKey(CommonConstants.FIELD_EVENT_TYPE)) {
					FieldType fieldType = sourceType.getFieldTypes().getFieldTypesMap()
							.get(CommonConstants.FIELD_EVENT_TYPE);
					Options typeOptions = new Options();
					List<DataSourceEventDTO> dataSourceEventTypes=null;
					// Check for Intellego deployment is true, if so get the event type values from property.
					if (intellegoDeployment) {
						dataSourceEventTypes = prepareEventTypeForIntellego(eventTypeIntellego);
					} else {
						dataSourceEventTypes = ingestDatasourceDao.getDataSourceEventTypes();
					}
					 
					for (DataSourceEventDTO eventTypesDto : dataSourceEventTypes) {
						Option option = new Option();
						option.setDisplayName(eventTypesDto.getEventType());
						typeOptions.setOptionsMap(Integer.toString(eventTypesDto.getId()), option);
					}
					fieldType.setOptions(typeOptions);
				}
				Fields fields = new Fields();
				sourceType.setFields(fields);
				EventTypes eventTypes = sourceType.getEventTypes();
				for (String source : dataSource.getSources()) {
					LinkedHashMap<String, Field> fieldsMap = this.queryConfigDao
							.getSchemaFieldsForElasticCollection(source);
					HashSet<String> fieldNamesSet = new HashSet<String>();
					for (String field : fieldsMap.keySet()) {
						if (!fieldNamesSet.contains(field) && !fieldExclusionsList.contains(field)) {
						fields.setFieldsMap(field, fieldsMap.get(field));
						eventTypes.getEventTypesMap().get(CommonConstants.EVENT_TYPES_MAP).getFields().add(field);
						fieldNamesSet.add(field);
						}
					}
					eventTypes.getEventTypesMap().get(CommonConstants.EVENT_TYPES_MAP).getSourceNames().add(source);
				}
			}
			this.updateQueryConfigCache(baseQueryConfig);
		}
	}

	private List<DataSourceEventDTO> prepareEventTypeForIntellego(String eventTypeIntellego) {
		List<DataSourceEventDTO> eventList= new LinkedList<>();
		if (eventTypeIntellego != null) {
			int i=0;
			for (String eventType : eventTypeIntellego.split(",")) {
				DataSourceEventDTO dataSourceEventDTO=new DataSourceEventDTO();
				dataSourceEventDTO.setEventType(eventType);
				dataSourceEventDTO.setId(i++);
				eventList.add(dataSourceEventDTO);
			}
		}
		return eventList;
	}

	private void updateQueryConfigCache(final QueryBuilderConfig queryConfig) {
		this.queryConfig = queryConfig;
	}

	private QueryBuilderConfig populateBaseQueryConfig() throws IOException {
		QueryBuilderConfig baseQueryConfig = null;
		File file = new File(queryConfigFileLocation);
		String json = null;
		if (file.exists()) {
			json = FileUtils.readFileToString(file, Charset.defaultCharset());
		} else {
			Resource resource = resourceLoader.getResource("classpath:" + queryConfigFileLocation);
			InputStream in = resource.getInputStream();
			json = IOUtils.toString(in, Charset.defaultCharset());
			in.close();
		}
		ObjectMapper mapper = new ObjectMapper();
		baseQueryConfig = mapper.readValue(json, QueryBuilderConfig.class);
		return baseQueryConfig;
	}

	@Scheduled(fixedRateString = "${my.fixedRate}")
	public synchronized void evictQueryConfigCacheAtIntervals() throws IOException {
		if (cacheManager.getCache("queryConfig") != null) {
			cacheManager.getCache("queryConfig").clear();
		}
		this.retrieveLatestQueryConfig();
	}
}
