package com.ss8.slp.retrospectivequery.exception;

public class ExceededLimitException extends RuntimeException {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExceededLimitException(String exception) {
		    super(exception);
		  }
}
