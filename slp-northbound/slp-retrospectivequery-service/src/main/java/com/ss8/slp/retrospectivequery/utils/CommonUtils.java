package com.ss8.slp.retrospectivequery.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.elasticsearch.search.sort.SortOrder;

import com.ss8.iql.query.dto.Field;
import com.ss8.iql.query.dto.Fields;
import com.ss8.iql.query.dto.QueryBuilderConfig;
import com.ss8.iql.query.dto.SortModel;
import com.ss8.iql.query.dto.SourceType;
import com.ss8.iql.query.dto.TimeRange;
import com.ss8.iql.query.parser.QueryRequest;
import com.ss8.iql.query.parser.QueryRequest.SQLStatement;
import com.ss8.slp.retrospectivequery.dto.GeoPoint;
import com.ss8.slp.retrospectivequery.dto.RetrospectiveQueryResponseDto;

public class CommonUtils {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static RetrospectiveQueryResponseDto<Object> constructEventRecordsFromDataStores(List<Map<String, Object>> data,
			Integer endRow, long totalRows) {
		long lastRow = endRow >= totalRows ? totalRows : -1;
		return new RetrospectiveQueryResponseDto(data, lastRow, new ArrayList<String>());
	}
	
	public static StringBuffer createSqlQueryFromRequest(final QueryRequest queryRequest,
			final HashSet<String> selectedColumns, final TimeRange timeRange, final List<Long> selectedIntercepts,
			final SortModel sortModel, final int startRow, final int endRow, final String calciteTables) throws Exception {
		List<SQLStatement> sqlStmnts = queryRequest.createSQL(selectedColumns, calciteTables);
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(queryRequest.addTimeAndInterceptsCond(sqlStmnts.get(0), timeRange, selectedIntercepts).statement);
		CommonUtils.appendOrderByToSqlQuery(sqlQuery, sortModel);
		CommonUtils.appendOffsetAndSizeToSqlQuery(sqlQuery, startRow, endRow);
		return sqlQuery;
	}
	
	public static void appendOrderByToSqlQuery(final StringBuffer sql, final SortModel sortModel) {
		if (sortModel != null) {
			SortOrder sortOrder = sortModel.getSort().equals("asc") ? SortOrder.ASC : SortOrder.DESC;
			sql.append(" order by " + sortModel.getColId() + " " + sortOrder.name() + " ");
		}
	}
	
	public static void appendOffsetAndSizeToSqlQuery(final StringBuffer sql, final Integer startRow, final Integer endRow) {
		sql.append(" offset " + startRow + " rows fetch first " + endRow + " rows only");
	}
	
	public static void removeNullValueKeysFromDocs(final List<Map<String, Object>> documents, final QueryBuilderConfig queryConfig) {
		HashSet<String> locationFields = CommonUtils.getLocationFieldsFromQueryConfig(queryConfig);
		for (Map<String, Object> object : documents) {
			Set<Entry<String, Object>> setOfEntries = object.entrySet();
			Iterator<Entry<String, Object>> iterator = setOfEntries.iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> entry = iterator.next();
				if (entry.getValue() == null) {
					iterator.remove();
					continue;
				}
				if (locationFields.contains(entry.getKey())) {
					// Parse the value as a list from string.
					String locationValue = (String) entry.getValue();
					Matcher matcher = Pattern.compile("\\((.*?)\\)").matcher(locationValue);
					List<GeoPoint> locationsList = new ArrayList<GeoPoint>();
					int pos = -1;
					while (matcher.find(pos + 1)) {
						pos = matcher.start();
						String[] latLongValue = matcher.group(1).split(",");
						if (latLongValue.length == 2) {
							locationsList.add(new GeoPoint(Double.parseDouble(latLongValue[0]), Double.parseDouble(latLongValue[1])));
						}
					}
					if (locationsList.size() > 0) {
						entry.setValue(locationsList);
					} else {
						iterator.remove();
					}
				} else if (entry.getValue() instanceof String) {
					String value = (String) entry.getValue();
					if (value.trim().isEmpty()) {
						iterator.remove();
					}
				}
			}
		}
	}
	
	public static HashSet<String> getLocationFieldsFromQueryConfig(final QueryBuilderConfig queryConfig) {
		Map<String, SourceType> sourcesTypesMap = queryConfig.getSourceTypes().getSourceTypesMap();
		SourceType sourceType = sourcesTypesMap.get("fusion");
		Fields fields = sourceType.getFields();
		HashSet<String> locationFields = new HashSet<String>();
		for (String field : fields.getFieldsMap().keySet()) {
			 if (CommonConstants.DATA_TYPE_GEO_POINT.equals(fields.getFieldsMap().get(field).getFieldType())
					 || CommonConstants.DATA_TYPE_GEO_SHAPE.equals(fields.getFieldsMap().get(field).getFieldType())) {
				 locationFields.addAll(fields.getFieldsMap().get(field).getSourceNames());
			 }
		}
		return locationFields;	
	}
	
	public static HashSet<String> getSelectedColumnsFromQueryFields(final HashSet<String> excludedFieldTypes, final LinkedHashMap<String, Field> configFields) {
		HashSet<String> selectedColumns = new HashSet<String>();
		for (String field : configFields.keySet()) {
			String fieldType = configFields.get(field).getFieldType();
			if (!excludedFieldTypes.contains(fieldType)) {
				selectedColumns.add(field);
			}
		}
		return selectedColumns;
	}
	
	public static TimeRange getTimeRange(TimeRange timeRange) {
		if(timeRange==null) {
			return timeRange;
		}
		timeRange.setStartTime(millsToLocalDateTime(Long.valueOf(timeRange.getStartTime())));
		timeRange.setEndTime(millsToLocalDateTime(Long.valueOf(timeRange.getEndTime())));
		return timeRange;
	}


	private static String millsToLocalDateTime(long millis) {
		Instant instant = Instant.ofEpochMilli(millis);
		LocalDateTime date = instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
		DateTimeFormatter zonedFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
		return zonedFormatter.format(date);
	}
}
