package com.ss8.slp.retrospectivequery.appmonitorAgent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
public class AppMonitorService {

	@Autowired
	AppMonitorDAO amDao;

	@Autowired
	SqlProperties sqlProperties;

	private String thisServiceName;

	@PostConstruct
	public void initProperties() {
		thisServiceName = sqlProperties.getDbconfig().get("this-service-name").trim();
	}

	@Scheduled(fixedDelayString = "${sqlproperties.dbconfig.healthupdate-interval-in-seconds:120}000")
	private void scheduleMonitor() {

		List<Map<String, Object>> findAll = amDao.getAllRowsFromAppMonitor();

		if (findAll == null || findAll.isEmpty()) {
			System.out.println(
					"Issue with DB Connection!.. Or Table appmonitor is Empty!.. :: Unable to monitor/manage the applications");
		} else {
			for (int i = 0; i < findAll.size(); i++) {
				if (findAll.get(i).get("app_name").toString().trim().equalsIgnoreCase(thisServiceName)) {
					DateTimeFormatter formatter1 = DateTimeFormatter.ISO_DATE_TIME;			

					LocalDateTime timeNow = LocalDateTime.parse(Instant.now().toString(), formatter1);
					AppMonitor am = new AppMonitor();
					am.setAppname(thisServiceName);
					am.setLastcontacted(timeNow);
					int updateAppMonitor = amDao.updateAppMonitor(am);
					if (updateAppMonitor == 0)
						System.out.println(
								"Unable to Update Table appmonitor!.. :: Can't monitor/manage the applications");
				}
			}
		}

	}

}
