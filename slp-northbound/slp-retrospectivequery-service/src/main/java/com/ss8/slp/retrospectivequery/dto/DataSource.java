package com.ss8.slp.retrospectivequery.dto;

import java.util.ArrayList;

public class DataSource {

	@Override
	public String toString() {
		return "DataSource [sourceType=" + sourceType + ", sources=" + sources + "]";
	}

	private String sourceType;

	private ArrayList<String> sources;

	/**
	 * @return the sources
	 */
	public ArrayList<String> getSources() {
		return sources;
	}

	/**
	 * @param sources the sources to set
	 */
	public void setSources(ArrayList<String> sources) {
		this.sources = sources;
	}

	/**
	 * @return the sourceType
	 */
	public String getSourceType() {
		return sourceType;
	}

	/**
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

}
