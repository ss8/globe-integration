package com.ss8.slp.retrospectivequery.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.codehaus.jettison.json.JSONException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.iql.query.dto.QueryBuilderConfig;
import com.ss8.iql.query.dto.TimeRange;
import com.ss8.iql.query.parser.QueryRequest;
import com.ss8.iql.query.parser.QueryRequest.ElasticStatement;
import com.ss8.slp.retrospectivequery.config.ElasticSearchConfig;
import com.ss8.slp.retrospectivequery.utils.CommonConstants;
import com.ss8.slp.retrospectivequery.utils.CommonUtils;
import com.zaxxer.hikari.HikariDataSource;

@Repository("eventsDao")
public class EventsDao extends BaseJdbcDao{

	private static final Logger log = LoggerFactory.getLogger(EventsDao.class);

	private static final int CALCITE_QUERY_RETRY_ATTEMPTS = 10;

	@Autowired
	@Qualifier("jdbcCalciteDBTemplate")
	JdbcTemplate calciteJdbcTemplate;

	@Autowired
	@Qualifier("calciteDataSource")
	private HikariDataSource dataSourceCalcite;
	
	@Autowired
	private ElasticSearchConfig elasticConfig;
	
	@Value("${elastic.searchIndex:metadata}")
	private String elasticIndexName;
	
	@Value("${metahub.intellego.deployment:false}")
	private boolean intellegoDeployment;


	@PostConstruct
	public void init() {
	}

	
	public List<Map<String, Object>> queryForDocsFromCalcite(final String sqlQuery, final QueryBuilderConfig queryConfig) throws Exception {
		List<Map<String, Object>> documents = null;
		// Retry CALCITE_QUERY_RETYR_ATTEMPT times, before throwing exception.
		int retryAttempt = 0;
		while (retryAttempt < CALCITE_QUERY_RETRY_ATTEMPTS) {
			retryAttempt++;
			try {
				log.info("Attempt -> " + (retryAttempt) + " SQL Query ---> " + sqlQuery.toString());
				documents = calciteJdbcTemplate.queryForList(sqlQuery.toString());
				CommonUtils.removeNullValueKeysFromDocs(documents, queryConfig);
				return documents;
			} catch (Exception ex) {
				// Evict current connections only for the first exception.
				// This is to take care of scenario where a dynamic field was added and was missed by the application.
				if (retryAttempt == 1 && dataSourceCalcite.getHikariPoolMXBean() != null) {
					dataSourceCalcite.getHikariPoolMXBean().softEvictConnections();
				}
				ex.printStackTrace();
				if (retryAttempt >= CALCITE_QUERY_RETRY_ATTEMPTS) {
					if (ex instanceof Exception) {
						throw new Exception(ex);
					} else {
						throw new Exception("Unknown exception");
					}
				}
			}
		}
		return documents;
	}
	
	public List<String> getInterceptNames() throws JSONException, IOException {

		ElasticQueryWrapper elasticQueryWrapper = this.elasticConfig.getElasticInstance();
		List<String> interceptNames = new ArrayList<>();
		if (intellegoDeployment) {
			final String[] groupByFields = new String[] { CommonConstants.INTELLEGO_INTERCEPT_NAME };
			SearchResponse searchResponse = elasticQueryWrapper.queryForFacetsFromElastic(null, 0, 10000, groupByFields,
					elasticIndexName);
			ParsedTerms agg = searchResponse.getAggregations().get(groupByFields[0]);
			for (Bucket entry : agg.getBuckets()) {
				if (!interceptNames.contains(entry.getKeyAsString())) {
					interceptNames.add(entry.getKeyAsString());
				}
			}
			return interceptNames;
		}
		return interceptNames;

	}

	public List<String> getDataSourceNames() throws JSONException, IOException {

		ElasticQueryWrapper elasticQueryWrapper = this.elasticConfig.getElasticInstance();
		List<String> datasourceNames = new ArrayList<>();
		if (!intellegoDeployment) {
			final String[] groupByFields = new String[] { CommonConstants.METAHUB_ELASTIC_DATA_SOURCE_NAME };
			SearchResponse searchResponse = elasticQueryWrapper.queryForFacetsFromElastic(null, 0, 10000, groupByFields,
					elasticIndexName);
			ParsedTerms agg = searchResponse.getAggregations().get(groupByFields[0]);
			for (Bucket entry : agg.getBuckets()) {
				if (!datasourceNames.contains(entry.getKeyAsString())) {
					datasourceNames.add(entry.getKeyAsString());
				}
			}
			return datasourceNames;
		}
		return datasourceNames;

	}
	
	public long getEventsCountForElasticQuery(final QueryRequest queryRequest, final TimeRange timeRange,
			final List<Long> selectedIntercepts) throws Exception {
		ElasticQueryWrapper elasticQueryWrapper = this.elasticConfig.getElasticInstance();
		List<ElasticStatement> elasticStmnts = queryRequest.createElasticQuery();
		ElasticStatement elasticQuery = elasticStmnts.get(0);
		elasticQuery = queryRequest.addTimeAndInterceptsCond(elasticQuery, timeRange, selectedIntercepts);
		long count = elasticQueryWrapper.countElasticDocs(elasticQuery.query, elasticIndexName);
		return count;
	}
	
}
