package com.ss8.slp.retrospectivequery.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.iql.query.dto.EventReqAdvancedQueryDto;
import com.ss8.iql.query.dto.SortModel;
import com.ss8.iql.query.dto.TimeRange;
import com.ss8.iql.query.parser.QueryConfig;
import com.ss8.iql.query.parser.QueryRequest;
import com.ss8.slp.retrospectivequery.dao.EventsDao;
import com.ss8.slp.retrospectivequery.dto.RetrospectiveQueryResponseDto;
import com.ss8.slp.retrospectivequery.utils.CommonConstants;
import com.ss8.slp.retrospectivequery.utils.CommonUtils;

@Service
public class RetrospectiveQueryServiceImpl implements RetrospectiveQueryService {

	private static final Logger log = LoggerFactory.getLogger(RetrospectiveQueryService.class);

	@Autowired
	private QueryConfigService queryConfigService;

	@Value("${metahub.server.mapQueryEventsLimit:10000}")
	private int mapQueryEventsLimit;

	@Value("${metahub.calcite.tables:metadata}")
	private String calciteTables;

	@Autowired
	EventsDao eventsDao;

	private final static HashSet<String> COMMON_EXCLUDED_FIELD_TYPES = new HashSet<String>();
	static {
		COMMON_EXCLUDED_FIELD_TYPES.add(CommonConstants.DATA_TYPE_LONG_TEXT);
	}

	@Override
	public RetrospectiveQueryResponseDto<Object> getEventsForRetrospectiveQuery(EventReqAdvancedQueryDto request)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		QueryConfig config = QueryConfig.factory(mapper.writeValueAsString(queryConfigService.getQueryConfig()), true);
		String iql = mapper.writeValueAsString(request.getIql());
		QueryRequest queryRequest = new QueryRequest(config, iql);
		TimeRange timeRange = CommonUtils.getTimeRange(request.getTimeRange());
		List<Map<String, Object>> documents = this.getDocumentsFromCalcite(queryRequest,
				CommonUtils.getSelectedColumnsFromQueryFields(COMMON_EXCLUDED_FIELD_TYPES,
						queryConfigService.getQueryConfig().getSourceTypes().getSourceTypesMap().get("fusion")
								.getFields().getFieldsMap()),
				timeRange, request.getSelectedIntercepts(), request.getSortModel(), request.getStartRow(),
				request.getEndRow());
		long totalCount = eventsDao.getEventsCountForElasticQuery(queryRequest, timeRange,
				request.getSelectedIntercepts());
		return CommonUtils.constructEventRecordsFromDataStores(documents, request.getEndRow(), totalCount);
	}

	@Override
	public List<String> getDataSourceNames() throws JSONException, IOException {

		return eventsDao.getDataSourceNames();

	}

	@Override
	public List<String> getInterceptNames() throws JSONException, IOException {

		return eventsDao.getInterceptNames();

	}
	
	@Override
	public RetrospectiveQueryResponseDto<Object> getEventsForRetrospectiveMapQuery(EventReqAdvancedQueryDto request)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		QueryConfig config = QueryConfig.factory(mapper.writeValueAsString(queryConfigService.getQueryConfig()), true);
		String iql = mapper.writeValueAsString(request.getIql());
		QueryRequest queryRequest = new QueryRequest(config, iql);
		TimeRange timeRange = CommonUtils.getTimeRange(request.getTimeRange());
		List<Map<String, Object>> documents = this.getDocumentsFromCalcite(queryRequest,
				CommonUtils.getSelectedColumnsFromQueryFields(COMMON_EXCLUDED_FIELD_TYPES,
						queryConfigService.getQueryConfig().getSourceTypes().getSourceTypesMap().get("fusion")
								.getFields().getFieldsMap()),
				timeRange, request.getSelectedIntercepts(), request.getSortModel(), 0, this.mapQueryEventsLimit);
		long totalCount = eventsDao.getEventsCountForElasticQuery(queryRequest, timeRange,
				request.getSelectedIntercepts());
		return CommonUtils.constructEventRecordsFromDataStores(documents, this.mapQueryEventsLimit, totalCount);
	}

	private List<Map<String, Object>> getDocumentsFromCalcite(final QueryRequest queryRequest,
			final HashSet<String> selectedColumns, final TimeRange timeRange, final List<Long> selectedIntercepts,
			final SortModel sortModel, final int startRow, final int endRow) throws Exception {
		StringBuffer sqlQuery = CommonUtils.createSqlQueryFromRequest(queryRequest, selectedColumns, timeRange,
				selectedIntercepts, sortModel, startRow, endRow, calciteTables);
		long startQueryTime = System.currentTimeMillis();
		List<Map<String, Object>> documents = eventsDao.queryForDocsFromCalcite(sqlQuery.toString(),
				queryConfigService.getQueryConfig());
		log.info("Total time taken ---> " + (System.currentTimeMillis() - startQueryTime));
		return documents;
	}

}
