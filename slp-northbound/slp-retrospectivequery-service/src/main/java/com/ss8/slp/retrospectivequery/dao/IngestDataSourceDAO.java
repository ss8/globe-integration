package com.ss8.slp.retrospectivequery.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ss8.slp.retrospectivequery.dto.DataSourceEventDTO;

@Repository("ingestDataSourceDao")
public class IngestDataSourceDAO extends BaseJdbcDao {
	private static final String SELECT_QUERY_DATASOURCE_EVENTTYPE = "select id, event_type from datasource_event_types order by id desc";

	public List<DataSourceEventDTO> getDataSourceEventTypes() {
		return (this.jdbcTemplate.query(SELECT_QUERY_DATASOURCE_EVENTTYPE, new DataSourceEventTypeRowMapper()));
	}

	private static final class DataSourceEventTypeRowMapper implements RowMapper<DataSourceEventDTO> {
		public DataSourceEventDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			DataSourceEventDTO dto = new DataSourceEventDTO();
			dto.setId(rs.getInt("id"));
			dto.setEventType(rs.getString("event_type"));
			return dto;
		}
	}

}