package com.ss8.slp.retrospectivequery.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.ss8.iql.query.ElasticQueryWrapper;
import com.ss8.iql.query.dto.ElasticServer;

@Configuration
public class ElasticSearchConfig {

	@Value("${elastic.ip:localhost}")
	private String elasticHost;

	@Value("${elastic.port:12913}")
	private int elasticPort;

	public ElasticQueryWrapper getElasticInstance() {
		ElasticQueryWrapper eqw = ElasticQueryWrapper.getInstance();
		List<ElasticServer> servers = new ArrayList<>(); // list of servers ElasticServer
		ElasticServer methubInstance = new ElasticServer(elasticHost, elasticPort);
		servers.add(methubInstance);
		eqw.RegisterElasticServers(servers); // register the servers
		return eqw;
	}

}