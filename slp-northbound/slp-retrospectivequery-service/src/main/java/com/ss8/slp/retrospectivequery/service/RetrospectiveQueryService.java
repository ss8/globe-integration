package com.ss8.slp.retrospectivequery.service;

import java.io.IOException;
import java.util.List;

import org.codehaus.jettison.json.JSONException;

import com.ss8.iql.query.dto.EventReqAdvancedQueryDto;
import com.ss8.slp.retrospectivequery.dto.RetrospectiveQueryResponseDto;

public interface RetrospectiveQueryService {

	public RetrospectiveQueryResponseDto<Object> getEventsForRetrospectiveQuery(EventReqAdvancedQueryDto request) throws Exception;

	public List<String> getDataSourceNames()throws JSONException,  IOException;
	
	public List<String> getInterceptNames()throws JSONException,  IOException;
	
	public RetrospectiveQueryResponseDto<Object> getEventsForRetrospectiveMapQuery(EventReqAdvancedQueryDto request) throws Exception;

}
