package com.ss8.slp.retrospectivequery.config;

import java.io.File;
import java.io.IOException;

import javax.sql.DataSource;

import org.apache.calcite.util.Sources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ss8.slp.retrospectivequery.utils.PasswordEncrypter;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
public class JdbcConfiguration {

    static final Logger log = LoggerFactory.getLogger(JdbcConfiguration.class);

	@Value("${spring.datasource.driver-class-name}")
	private String driver;

	@Value("${spring.datasource.url:null}")
	private String url;
	
	@Value("${spring.datasource.username::null}")
	private String username;
	
	@Value("${spring.datasource.password::null}")
	private String encodedPassword;
	
	@Value("${spring.datasource.intellegoJdbcUrl:null}")
	private String intellegoJdbcUrl;
	
	@Value("${spring.datasource.intellegoJdbcUserName:null}")
	private String intellegoJdbcUserName;
	
	@Value("${spring.datasource.intellegoJdbcEncodedPassword:null}")
	private String intellegoJdbcEncodedPassword;
	
	@Value("${spring.datasource.hikari.maximumPoolSize:5}")
	private int poolSize;
	
	@Value("${spring.datasource.hikari.maximumCalcitePoolSize:10}")
	private int calcitePoolSize;
	
	@Value("${metahub.cassandra.keyspace:metahub}")
	private String keyspace;
	
	@Value("${metahub.cassandra.contactpoints:127.0.0.1}")
	private String cassandraHosts;
	
	@Value("${metahub.cassandra.port:9142}")
	private int cassandraPort;
	
	@Value("${elastic.searchIndex:metadata}")
	private String searchIndexName;
	
	@Value("${metahub-db.datacenter:datacenter1}")
	private String metahubDbDataCenter;
	
	@Value("${data_storage.metahub.collection:metadata}")
	private String metahubDataStorageCollectionName;
	
	@Value("${data_storage.intellego.collection:document}")
	private String intellegoDataStorageCollectionName;
	
	@Value("${query_config.collections:metadata}")
	private String queryConfigCollections;
	
	@Value("${elastic.ip:localhost}")
	private String elasticHosts;
	
	@Value("${elastic.port:12913}")
	private int elasticPort;
	
	@Value("${metahub.intellego.deployment:false}")
	private boolean intellegoDeployment;
	
	@Value("${metahub.intellego-metahub.deployment:false}")
	private boolean intellegoMetahubDeployment;
	
	@Value("${metahub.server.calciteIntellegoMetahubConfig}")
	private String calciteIntellegoMetahubConfig;
	
	@Value("${metahub.server.calciteMetahubConfig}")
	private String calciteMetahubConfig;
	
	@Value("${intellego.server.calciteIntellegoConfig}")
	private String calciteIntellegoConfig;
	
	@Value("${query_config.field_exclusions: null}")
	private String fieldExclusions;
	
	@Primary
	@Bean(name = "dataSource")
	public DataSource dataSource() {
		
		if (intellegoDeployment) {
			url = intellegoJdbcUrl;
			username = intellegoJdbcUserName;
			encodedPassword = intellegoJdbcEncodedPassword;
		}
		 
		 String password = PasswordEncrypter.decrypt(encodedPassword);
		 log.info("Database configuration: U=" + username + ", PW=" + password + " D=" + driver + "\nURL=" + url); 
		 
	     HikariDataSource dataSource = new HikariDataSource();
	        dataSource.setDriverClassName(driver);
	        dataSource.setJdbcUrl(url);
	        dataSource.setUsername(username);
	        dataSource.setPassword(password);
	        dataSource.addDataSourceProperty("cachePrepStmts", true);
	        dataSource.addDataSourceProperty("prepStmtCacheSize", 25000);
	        dataSource.addDataSourceProperty("prepStmtCacheSqlLimit", 20048);
	        dataSource.addDataSourceProperty("useServerPrepStmts", true);
	        dataSource.addDataSourceProperty("initializationFailFast", true);
	        dataSource.setPoolName("MYSQL_CONNECTION_POOL");
	        dataSource.setMaximumPoolSize(poolSize);
	        return dataSource;
	 }
	
	@Bean(name = "jdbcCommonDBTemplate")
	public JdbcTemplate jdbcTemplate(@Qualifier("dataSource") DataSource dataSourceSFW) {
		return new JdbcTemplate(dataSourceSFW);
	}

	@Bean(name = "jdbcCommonDBNamedTemplate")
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(
			@Qualifier("dataSource") DataSource dataSourceSFW) {
		return new NamedParameterJdbcTemplate(dataSourceSFW);
	}

	@Lazy
	@Bean(name = "calciteDataSource")
	public DataSource calciteDataSource() throws IOException {
		String password = PasswordEncrypter.decrypt(encodedPassword);
		log.info("Calcite Database configuration: U=" + username + ", PW=" + password + " D=" + driver + "\nURL=" + url);
		// Set some system properties for the calcite driver to substitute those property values in the schema model.
		setCalciteSystemProperties();
		HikariConfig config = new HikariConfig();
		config.setDriverClassName("org.apache.calcite.jdbc.Driver");
		File calciteModelFile = null;
		if (intellegoMetahubDeployment) {
			calciteModelFile = new File(calciteIntellegoMetahubConfig);
			if (!calciteModelFile.exists()) {
				calciteModelFile =  new File(getClass().getClassLoader().getResource("calcite-intellego-metahub.json").getFile());
			}
		} else if (intellegoDeployment) {
			calciteModelFile = new File(calciteIntellegoConfig);
			if (!calciteModelFile.exists()) {
				calciteModelFile =  new File(getClass().getClassLoader().getResource("calcite-intellego.json").getFile());
			}
		} else {
			calciteModelFile = new File(calciteMetahubConfig);
			if (!calciteModelFile.exists()) {
				calciteModelFile =  new File(getClass().getClassLoader().getResource("calcite-metahub.json").getFile());
			}
		}
		System.out.println("calciteModelFile -> " + calciteModelFile.getAbsolutePath());
		config.setJdbcUrl("jdbc:calcite:model=" + Sources.of(calciteModelFile).file().getAbsolutePath() + ";lex=MYSQL;fun=spatial");
		config.setUsername(username);
		config.setPassword(password);
		config.setPoolName("CALCITE_CONNECTION_POOL");
		config.setMaximumPoolSize(calcitePoolSize);
		config.setRegisterMbeans(true);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("prepStmtCacheSize", 25000);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 20048);
		config.addDataSourceProperty("useServerPrepStmts", true);
		config.addDataSourceProperty("initializationFailFast", true);
		HikariDataSource dataSource = new HikariDataSource(config);
		return dataSource;
	}
    
    @Lazy
    @Bean(name = "jdbcCalciteDBTemplate") 
    public JdbcTemplate jdbcTemplateCalcite(@Qualifier("calciteDataSource")DataSource dataSourceCalcite) { 
        return new JdbcTemplate(dataSourceCalcite); 
    } 
    
    @Lazy
    @Bean(name = "jdbcCalciteDBNamedTemplate") 
    public NamedParameterJdbcTemplate namedParameterJdbcTemplateCalcite(@Qualifier("calciteDataSource")DataSource dataSourceCalcite){
       return new NamedParameterJdbcTemplate(dataSourceCalcite);
    }
    
    private void setCalciteSystemProperties() {
    	
    	if (intellegoDeployment) {
    		setIntellegoProperties();
    	}else if(intellegoMetahubDeployment) {
    		setMetahubProperties();
    		setIntellegoProperties();
    	}else {
    		setMetahubProperties();
    	}
    	
		String[] elasticHostsList = elasticHosts.split(",");
		StringBuffer elasticHostsWithPort = new StringBuffer();
		for (int index = 0; index < elasticHostsList.length; index++) {
			if (index < elasticHostsList.length - 1) {
				elasticHostsWithPort.append(elasticHostsList[index] + ":" + elasticPort + ",");
			} else {
				elasticHostsWithPort.append(elasticHostsList[index] + ":" + elasticPort);
			}
		}
		System.setProperty("es.hosts", elasticHostsWithPort.toString());
		System.setProperty("es.index_name", searchIndexName);
		System.setProperty("query_config.collections", queryConfigCollections);
		System.setProperty("query_config.field_exclusions", fieldExclusions);
		
	}

    
	private void setIntellegoProperties() {
		
		System.setProperty("intellego-db.jdbc_driver", driver);
		System.setProperty("intellego-db.jdbc_url", intellegoJdbcUrl);
		System.setProperty("intellego-db.jdbc_userName", intellegoJdbcUserName);
		System.setProperty("intellego-db.jdbc_password", intellegoJdbcEncodedPassword);
		System.setProperty("data_storage.intellego.collection", intellegoDataStorageCollectionName);
		
	}

	private void setMetahubProperties() {
		
		System.setProperty("metahub-config-db.jdbc_driver", driver);
		System.setProperty("metahub-config-db.jdbc_url", url);
		System.setProperty("metahub-config-db.jdbc_userName", username);
		System.setProperty("metahub-config-db.jdbc_password", encodedPassword);
		System.setProperty("metahub-db.datacenter", metahubDbDataCenter);
		String[] cassandraHostsList = cassandraHosts.split(",");
		StringBuffer cassandraHostsWithPort = new StringBuffer();
		for (int index = 0; index < cassandraHostsList.length; index++) {
			if (index < cassandraHostsList.length - 1) {
				cassandraHostsWithPort.append(cassandraHostsList[index] + ":" + cassandraPort + ",");
			} else {
				cassandraHostsWithPort.append(cassandraHostsList[index] + ":" + cassandraPort);
			}
		}
		System.setProperty("metahub-db.hosts", cassandraHostsWithPort.toString());
		System.setProperty("metahub-db.keyspace", keyspace);
		System.setProperty("data_storage.metahub.collection", metahubDataStorageCollectionName);
		
	}
}
