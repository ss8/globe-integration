package com.ss8.slp.retrospectivequery.controller;

import java.io.IOException;

import org.codehaus.jettison.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.iql.query.dto.EventReqAdvancedQueryDto;
import com.ss8.slp.retrospectivequery.dto.RetrospectiveQueryResponseDto;
import com.ss8.slp.retrospectivequery.service.RetrospectiveQueryService;

@RestController
@RequestMapping("/retrospective-query")
public class RetrospectiveQueryController {

	@Autowired
	private RetrospectiveQueryService retrospectiveQueryService;

	@PostMapping(value = "/search", produces = "application/json")
	public @ResponseBody RetrospectiveQueryResponseDto<Object> getDataForRetrospectiveQuery(
			@RequestBody EventReqAdvancedQueryDto request) throws IOException, JSONException, Exception {
		return (RetrospectiveQueryResponseDto<Object>) retrospectiveQueryService.getEventsForRetrospectiveQuery(request);
	}

	@GetMapping(value = "/data-sources", produces = "application/json")
	public @ResponseBody RetrospectiveQueryResponseDto<String> getDataSourceNames() throws IOException, JSONException {
		return new RetrospectiveQueryResponseDto<String>(retrospectiveQueryService.getDataSourceNames(), 0L, null);
	}

	@GetMapping(value = "/intercept-names", produces = "application/json")
	public @ResponseBody RetrospectiveQueryResponseDto<String> getInterceptNames() throws IOException, JSONException {
		return new RetrospectiveQueryResponseDto<String>(retrospectiveQueryService.getInterceptNames(), 0L, null);
	}

	@PostMapping(value = "/map-search", produces = "application/json")
	public @ResponseBody RetrospectiveQueryResponseDto<Object> getDataForRetrospectiveMapQuery(
			@RequestBody EventReqAdvancedQueryDto request) throws IOException, JSONException, Exception {
		return (RetrospectiveQueryResponseDto<Object>) retrospectiveQueryService.getEventsForRetrospectiveMapQuery(request);
	}
}
