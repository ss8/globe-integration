package com.ss8.slp.retrospectivequery.appmonitorAgent;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;


@Configuration
@Repository
@PropertySource("classpath:application.properties")
public class AppMonitorDAO {
	
	@Autowired
	SqlProperties sqlProperties;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AppMonitorDAO.class);

	@Autowired
	private Environment env;
	private String datasourceUsername;
	private String datasourcePassword;
	private String datasourceUrl;
	private String datasourceDriverName;
   
	private JdbcTemplate jdbcTemplate;


	public AppMonitorDAO() {
	}
	
	@PostConstruct
	private void init() {
		LOGGER.info("Initializing the DB parameters");
		datasourceUsername = env.getProperty("sqlproperties.dbconfig.controller_username", sqlProperties.getDbconfig().get("controller_username").trim());
		datasourcePassword = env.getProperty("sqlproperties.dbconfig.controller_password", sqlProperties.getDbconfig().get("controller_password").trim());
		datasourceDriverName = env.getProperty("sqlproperties.dbconfig.controller_driver-class-name", sqlProperties.getDbconfig().get("controller_driver-class-name").trim());
		datasourceUrl = env.getProperty("sqlproperties.dbconfig.controller_url", sqlProperties.getDbconfig().get("controller_url").trim());
		if(this.jdbcTemplate==null) {
			this.jdbcTemplate = createJdbcTemplateDb(datasourceUrl, datasourceUsername, datasourcePassword, datasourceDriverName);
		}

	}

	/**
	 * This method creates the JdbcTemplate object and sets up the dB parameters
	 * 
	 * @param
	 * @return JdbcTemplate
	 */
	private JdbcTemplate createJdbcTemplateDb(String datasourceUrl, String datasourceUsername, String datasourcePassword, String datasourceDriverName) {
		LOGGER.info("Creating the JdbcTemplate and setting up the dB parameters");
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(datasourceUrl);
		dataSource.setUsername(datasourceUsername);
		dataSource.setPassword(datasourcePassword);
		dataSource.setDriverClassName(datasourceDriverName);
		return new JdbcTemplate(dataSource);
	}
	
	
	public List<Map<String, Object>> getAllRowsFromAppMonitor() {
		String appMonitorQuery = "SELECT * FROM app_monitor";
		return jdbcTemplate.queryForList(appMonitorQuery);
	}
	
	public int updateAppMonitor(AppMonitor appMonitor) {
		String appMonitorUpdate = "update app_monitor set last_contacted=? where app_name=?";
		return jdbcTemplate.update(appMonitorUpdate, appMonitor.getLastcontacted(), appMonitor.getAppname()); 
	}
		
	
	
		

}
