package com.ss8.slp.retrospectivequery.utils;

public class CommonConstants {
	
	public static String FIELD_EVENT_TYPE = "eventType";
	public static String METAHUB_ELASTIC_DATA_SOURCE_NAME = "datasourceName";
	public static String DATA_SOURCE_TYPE ="fusion";
	public static String EVENT_TYPES_MAP="all";
	public static String INTELLEGO_INTERCEPT_NAME = "interceptName";
	public static String FIELD_EVENT_DATE = "eventDate";
	
	public static final String DATA_TYPE_GEO_POINT = "GeoPoint";
	public static final String DATA_TYPE_GEO_SHAPE = "GeoShape";
	public static final String DATA_TYPE_LONG_TEXT = "LongText";
}
