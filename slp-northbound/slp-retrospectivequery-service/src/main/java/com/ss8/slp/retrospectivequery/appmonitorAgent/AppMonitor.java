package com.ss8.slp.retrospectivequery.appmonitorAgent;

import java.time.LocalDateTime;


public class AppMonitor {

	private int id;

	private String appname;

	private LocalDateTime lastcontacted;
	
	private LocalDateTime lastRestarted;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public LocalDateTime getLastcontacted() {
		return lastcontacted;
	}

	public void setLastcontacted(LocalDateTime lastcontacted) {
		this.lastcontacted = lastcontacted;
	}
	
	public LocalDateTime getLastRestarted() {
		return lastRestarted;
	}

	public void setLastRestarted(LocalDateTime lastRestarted) {
		this.lastRestarted = lastRestarted;
	}
	
	@Override
	public String toString() {
		return "AppMonitor [id=" + id + ", appname=" + appname + ", lastcontacted=" + lastcontacted + ", lastRestarted="
				+ lastRestarted + "]";
	}

}
