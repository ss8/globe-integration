package com.ss8.slp.retrospectivequery.dao;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ss8.iql.query.dto.Field;

@Repository("queryConfigDao")
public class QueryConfigDao extends BaseJdbcDao {

	@PostConstruct
	public void init() {
	}

	private static final class FieldRowMapper implements RowMapper<Field> {
		public Field mapRow(ResultSet rs, int rowNum) throws SQLException {
			Field field =  new Field();
		    field.setFieldType(rs.getString("fieldType"));
		    field.setDisplayName(rs.getString("displayName"));
		    ArrayList<String> sourceNames = new ArrayList<String>(1);
			sourceNames.add(rs.getString("field"));
		    field.setSourceNames(sourceNames);
		    if(null != rs.getString("childType")){
		    field.setChildType(rs.getString("childType"));
		   // field.setChild(true);
		    }
			return field;
		}
	}
	
	public LinkedHashMap<String, Field> getSchemaFieldsForElasticCollection(final String collectionName)
			throws IOException {
		LinkedHashMap<String, Field> fieldsMap = new LinkedHashMap<String, Field>();
		List<Field> fieldList = jdbcTemplate.query(	new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("select field, displayName ,fieldType ,childType from query_fields where collection = ?");
				ps.setString(1, collectionName);
				return ps;
			}
		} , new FieldRowMapper());
		for (Field field : fieldList) {
			
			fieldsMap.put(field.getSourceNames().get(0), field);

		}
		return fieldsMap;
	}

	
}