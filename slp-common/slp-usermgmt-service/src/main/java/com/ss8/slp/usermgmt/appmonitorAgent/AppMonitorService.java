package com.ss8.slp.usermgmt.appmonitorAgent;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class AppMonitorService {
	private static final Logger LOG = LoggerFactory.getLogger(AppMonitorService.class);

	@Autowired
	AppMonitorDAO amDao;

	@Autowired
	ConfigProps cfg;

	private String thisServiceName;

	@PostConstruct
	public void initProperties() {
		thisServiceName = cfg.getThisServiceName().trim();
	}

	@Scheduled(fixedDelayString = "${config-properties.healthupdate-interval-in-seconds:120}000")
	private void scheduleMonitor() {

		List<AppMonitor> findAll = amDao.findAll();

		if (findAll == null || findAll.isEmpty()) {
			LOG.info(
					"Issue with DB Connection!.. Or Table appmonitor is Empty!.. :: Unable to monitor/manage the applications");
		} else {
			for (AppMonitor am : findAll) {
				if (am.getAppname().trim().equalsIgnoreCase(thisServiceName)) {
					DateTimeFormatter formatter1 = DateTimeFormatter.ISO_DATE_TIME;			
					LocalDateTime timeNow = LocalDateTime.parse(Instant.now().toString(), formatter1);
					am.setLastcontacted(timeNow);
					amDao.update(am);
				}
			}
		}

	}

}
