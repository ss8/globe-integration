package com.ss8.slp.usermgmt.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ss8.slp.usermgmt.dao.IRoleDao;
import com.ss8.slp.usermgmt.dao.IUserDao;
import com.ss8.slp.usermgmt.dao.IUserRoles;
import com.ss8.slp.usermgmt.dto.RoleDto;
import com.ss8.slp.usermgmt.dto.UserDto;
import com.ss8.slp.usermgmt.entity.Role;
import com.ss8.slp.usermgmt.entity.User;
import com.ss8.slp.usermgmt.entity.UserRoles;

@Service
public class UserService implements IUserService {
	@Autowired
	IUserDao iUserDao;
	@Autowired
	IRoleDao iRoleDao;

	@Autowired
	EntityManager entityManager;
	@Autowired
	IUserRoles iUserRoles;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Transactional
	public void save(UserDto userDto) {
		try {
			User user = new User();
			if (userDto.getId() != 0)
				user.setUserId(userDto.getId());
			user.setFirstName(userDto.getFirstName());
			user.setLastName(userDto.getLastName());
			String pwdWithoutEncoded = userDto.getPassword();
			String encodedPassword = passwordEncoder.encode(pwdWithoutEncoded);
			user.setPassword(encodedPassword);
			user.setCreatedBy(userDto.getCreatedBy());
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			LocalDateTime ldateTime = LocalDateTime.parse(Instant.now().toString(), formatter1);
			user.setCreatedDate(ldateTime);
			entityManager.persist(user);
			if (userDto.getRoleDtos() != null) {
				List<UserRoles> userRoleList = new ArrayList<>();
				if (user.getUserId() != 0) {
					for (RoleDto role : userDto.getRoleDtos()) {
						UserRoles userRoles = new UserRoles();
						userRoles.setUserId(user.getUserId());
						userRoles.setRoleId(role.getId());
						userRoles.setCreatedby(userDto.getCreatedBy());
						LocalDateTime ldateTime1 = LocalDateTime.parse(Instant.now().toString(), formatter1);
						userRoles.setCreateddate(ldateTime1);
						userRoleList.add(userRoles);
					}
					for (UserRoles userRole : userRoleList) {
						entityManager.persist(userRole);
					}
				}
			}
		} finally {
			entityManager.close();
		}
	}

	@Override
	@Transactional
	public void update(UserDto userDto) {
		try {
			User user = iUserDao.findByuserId(userDto.getId());
			user.setFirstName(userDto.getFirstName());
			user.setLastName(userDto.getLastName());
			if (user.getPassword().equals(userDto.getPassword())) {
				user.setPassword(user.getPassword());
			} else {
				String pwdWithoutEncoded = userDto.getPassword();
				String encodedPassword = passwordEncoder.encode(pwdWithoutEncoded);
				user.setPassword(encodedPassword);
			}
			user.setUpdatedBy(userDto.getUpdatedBy());

			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			LocalDateTime ldateTime2 = LocalDateTime.parse(Instant.now().toString(), formatter1);
			user.setUpdatedDate(ldateTime2);

			entityManager.persist(user);

			List<UserRoles> userRoleList = new ArrayList<>();
			if (userDto.getRoleDtos() != null) {
				if (user.getUserId() != 0) {
					iUserRoles.deleteByuserId(user.getUserId());
					for (RoleDto role : userDto.getRoleDtos()) {
						UserRoles userRoles = new UserRoles();
						if (role.getUserRoleId() != 0)
							userRoles.setId(role.getUserRoleId());
						userRoles.setUserId(user.getUserId());
						if (role.getId() != 0)
							userRoles.setRoleId(role.getId());
						userRoles.setCreatedby(userDto.getCreatedBy());
						LocalDateTime ldateTime3 = LocalDateTime.parse(Instant.now().toString(), formatter1);
						userRoles.setCreateddate(ldateTime3);
						userRoles.setUpdateddate(ldateTime3);
						userRoles.setUpdatedby(userDto.getUpdatedBy());
						userRoleList.add(userRoles);
					}
					for (UserRoles userRole : userRoleList) {
						entityManager.persist(userRole);
					}
				}
			}
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(List<UserDto> users) {
		for (UserDto userDto : users) {
			for (RoleDto roleDto : userDto.getRoleDtos()) {
				iUserRoles.deleteById(roleDto.getUserRoleId());
			}
			iUserDao.deleteById(userDto.getId());
		}

	}

	@Override
	public List<UserDto> findAll() {
		List<UserDto> userDtos = new ArrayList<>();
		List<User> userList = iUserDao.findAll();
		List<Role> roles = iRoleDao.findAll();

		Map<Integer, String> roleMap = new HashMap<Integer, String>();
		for (Role role : roles) {
			roleMap.put(role.getRoleId(), role.getDescription());
		}

		for (User user : userList) {
			List<UserRoles> userRoles = iUserRoles.findByuserId(user.getUserId());
			UserDto userDto = new UserDto();
			userDto.setId(user.getUserId());
			userDto.setFirstName(user.getFirstName());
			userDto.setLastName(user.getLastName());
			userDto.setPassword(user.getPassword());
			if (user.getCreatedBy() != null && user.getCreatedBy() != 0)
				userDto.setCreatedBy(user.getCreatedBy());
			if (user.getCreatedDate() != null)
				userDto.setCreatedDate(user.getCreatedDate());
			if (user.getUpdatedBy() != null && user.getUpdatedBy() != 0)
				userDto.setUpdatedBy(user.getUpdatedBy());
			if (user.getUpdatedDate() != null)
				userDto.setUpdatedDate(user.getUpdatedDate());
			Set<RoleDto> roleDtos = new HashSet<>();
			for (UserRoles userRole : userRoles) {
				RoleDto roleDto = new RoleDto();
				roleDto.setUserRoleId(userRole.getId());
				roleDto.setId(userRole.getRoleId());
				roleDto.setDescription(roleMap.get(userRole.getRoleId()));
				roleDtos.add(roleDto);
			}

			userDto.setRoleDtos(roleDtos);
			userDtos.add(userDto);
		}
		return userDtos;
	}

	@Override
	public List<Role> getRoles() {
		return iRoleDao.findAll();
	}

	@Override
	public User findByUserName(String firstName) {
		return iUserDao.findByfirstName(firstName);
	}

}
