package com.ss8.slp.usermgmt.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ss8.slp.usermgmt.entity.Role;

public interface IRoleDao extends JpaRepository<Role, Integer>{
	

}
