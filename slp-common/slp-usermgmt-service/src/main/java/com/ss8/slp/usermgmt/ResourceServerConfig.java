package com.ss8.slp.usermgmt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

//import com.ss8.oauth.config.CustomLogoutSuccessHandler;

@Configuration
@EnableResourceServer
@PropertySource("classpath:application.properties")
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource_id";

	@Value("${auth.server.check.token}")
	private String checkTokenURL;

	@Value("${auth.client.id}")
	private String clientId;

	@Value("${auth.client.secret}")
	private String clientSecret;

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {
		http.cors().and().anonymous().and().headers().xssProtection().block(true).and().frameOptions().deny().and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers("/**").authenticated();
		
//		http.cors().and().anonymous().and().headers().xssProtection().block(true).and().frameOptions().deny().and()
//		.authorizeRequests().antMatchers("/**").authenticated();
		
		
//		.and()
//		.exceptionHandling().accessDeniedHandler(accessDeniedHandler()).authenticationEntryPoint(authenticationEntryPoint());
	}

	@Primary
	@Bean
	public RemoteTokenServices tokenServices() {
		final RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setCheckTokenEndpointUrl(checkTokenURL);
		tokenService.setClientId(clientId);
		tokenService.setClientSecret(clientSecret);
//        tokenService.setAccessTokenConverter(new CustomAccessTokenConverter());
		return tokenService;
	}
	
	/**
     * This method is used to configure the filters for all the requests
     * @return
     */
//	@Bean
//    public CorsConfigurationSource corsConfigurationSource() {
//        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("*"));
//        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
//        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
//        configuration.setExposedHeaders(Arrays.asList("x-auth-token"));
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", configuration);
//        return source;
//    }

}