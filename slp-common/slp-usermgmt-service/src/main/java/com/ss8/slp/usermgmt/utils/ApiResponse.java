package com.ss8.slp.usermgmt.utils;

import org.springframework.http.HttpStatus;

public class ApiResponse {

	private int status;
	private String message;
	private String result;

	public ApiResponse(HttpStatus status, String message, String result) {
		this.status = status.value();
		this.message = message;
		this.result = result;
	}

	public ApiResponse(HttpStatus status, String message) {
		this.status = status.value();
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
