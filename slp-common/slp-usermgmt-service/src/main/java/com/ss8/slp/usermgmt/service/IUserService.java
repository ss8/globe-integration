package com.ss8.slp.usermgmt.service;

import java.util.List;

import com.ss8.slp.usermgmt.dto.UserDto;
import com.ss8.slp.usermgmt.entity.Role;
import com.ss8.slp.usermgmt.entity.User;
import com.ss8.slp.usermgmt.utils.ApiResponse;

public interface IUserService {

	void save(UserDto user);

	List<UserDto> findAll();


	void update(UserDto user);

	void delete(List<UserDto> users);

	List<Role> getRoles();

	User findByUserName(String firstName);
	
	

}
