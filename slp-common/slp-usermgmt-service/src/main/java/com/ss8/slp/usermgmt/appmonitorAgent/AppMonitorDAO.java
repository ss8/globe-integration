package com.ss8.slp.usermgmt.appmonitorAgent;


import java.util.List;

public interface AppMonitorDAO {
	
	public List<AppMonitor> findAll();
	public AppMonitor update(AppMonitor appMonitor);


}
