package com.ss8.slp.usermgmt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.slp.usermgmt.dto.UserDto;
import com.ss8.slp.usermgmt.entity.Role;
import com.ss8.slp.usermgmt.service.IUserService;
@RestController
@RequestMapping(value = "/roles")
public class RoleController {
	
	@Autowired
	private IUserService iUserService;
	@GetMapping
    public List<Role> listRoles(){
//        log.info(String.format("received rpom.xmlequest to list user %s", authenticationFacadeService.getAuthentication().getPrincipal()));
        return iUserService.getRoles();
    }
}
