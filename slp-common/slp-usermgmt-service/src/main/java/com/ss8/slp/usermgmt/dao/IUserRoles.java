package com.ss8.slp.usermgmt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ss8.slp.usermgmt.entity.UserRoles;

public interface IUserRoles extends JpaRepository<UserRoles, Integer>{

	List<UserRoles> findByuserId(Integer id);

	void deleteByuserId(Integer id);

}
