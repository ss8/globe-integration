package com.ss8.slp.usermgmt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.slp.usermgmt.dto.UserDto;
import com.ss8.slp.usermgmt.entity.User;
import com.ss8.slp.usermgmt.service.IUserService;
import com.ss8.slp.usermgmt.utils.ApiResponse;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private IUserService iUserService;

	public static String MESSAGE = "";
	public static String RESULT = "";

	public static HttpStatus httpStatus = HttpStatus.OK;

	/*
	 * rest api to create user
	 */
	@Secured("ROLE_ADMIN")
	@PostMapping
	public ApiResponse create(@RequestBody UserDto user) {
		User isExistUser = iUserService.findByUserName(user.getFirstName());
		if (isExistUser == null) {
//			splitting the userid from the username.
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String[] splitUser = auth.getName().split("_");
			user.setCreatedBy(Integer.valueOf(splitUser[1]));
			iUserService.save(user);
			httpStatus = HttpStatus.OK;
			MESSAGE = "User has been created Successfully";
			RESULT = "Success";
		} else {
			httpStatus = HttpStatus.CONFLICT;
			MESSAGE = "First Name is already exists";
			RESULT = "failed";
		}
		return new ApiResponse(httpStatus, MESSAGE, RESULT);
	}

	/*
	 * rest api to get all the users
	 */
	@Secured("ROLE_ADMIN")
	@GetMapping
	public List<UserDto> listUser() {
		return iUserService.findAll();
	}

	/*
	 * rest api to update the user
	 */
	@Secured("ROLE_ADMIN")
	@PutMapping
	public ApiResponse update(@RequestBody UserDto user) {

		User isExistUser = iUserService.findByUserName(user.getFirstName());
		if (isExistUser != null) {
			if (isExistUser.getUserId().equals(user.getId())) {
				//splitting the userid from the username.
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				String[] splitUser = auth.getName().split("_");
				user.setUpdatedBy(Integer.valueOf(splitUser[1]));
				iUserService.update(user);
				httpStatus = HttpStatus.OK;
				MESSAGE = "User has been updated Successfully";
				RESULT = "Success";
			} else {
				httpStatus = HttpStatus.CONFLICT;
				MESSAGE = "First Name already exists";
				RESULT = "failed";

			}
		} else {
			httpStatus = HttpStatus.CONFLICT;
			MESSAGE = "First Name can not be change";
			RESULT = "failed";
		}
		return new ApiResponse(httpStatus, MESSAGE, RESULT);
	}

	/*
	 * rest api to delete the user
	 */
	@Secured("ROLE_ADMIN")
	@DeleteMapping
	public ApiResponse delete(@RequestBody List<UserDto> users) {
		iUserService.delete(users);
		return new ApiResponse(HttpStatus.OK, "User has been deleted Successfully");
	}

}
