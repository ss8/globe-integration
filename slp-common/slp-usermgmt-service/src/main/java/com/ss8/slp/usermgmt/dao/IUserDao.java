package com.ss8.slp.usermgmt.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ss8.slp.usermgmt.entity.User;
@Repository
public interface IUserDao extends JpaRepository<User, Integer>{

	User findByuserId(Integer id);

	User findByfirstName(String firstName);
}