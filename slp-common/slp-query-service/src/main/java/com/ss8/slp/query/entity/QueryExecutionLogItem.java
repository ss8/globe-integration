package com.ss8.slp.query.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "quey_execution_log")
@SqlResultSetMapping(name = "queryExecutionLogItemResultMap", entities = @EntityResult(entityClass = com.ss8.slp.query.entity.QueryExecutionLogItem.class, fields = {
		@FieldResult(name = "id", column = "id"), @FieldResult(name = "queryId", column = "query_id"),
		@FieldResult(name = "resultJson", column = "query_result"),
		@FieldResult(name = "executeTime", column = "last_execution_time")

}))

@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "selectquerylog", procedureName = "selectquerylog", resultSetMappings = "queryExecutionLogItemResultMap"),
		@NamedStoredProcedureQuery(name = "deletequerylog", procedureName = "deletequerylog", resultSetMappings = "queryExecutionLogItemResultMap"),
		@NamedStoredProcedureQuery(name = "insertquerylog", procedureName = "insertquerylog", resultSetMappings = "queryExecutionLogItemResultMap") })

public class QueryExecutionLogItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "last_execution_time")
	private LocalDateTime executeTime;
	@Column(name = "query_id")
	private int queryId;
	@Column(name = "query_result")
	private String resultJson;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(LocalDateTime executeTime) {
		this.executeTime = executeTime;
	}

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int queryId) {
		this.queryId = queryId;
	}

	public String getResultJson() {
		return resultJson;
	}

	public void setResultJson(String resultJson) {
		this.resultJson = resultJson;
	}
}
