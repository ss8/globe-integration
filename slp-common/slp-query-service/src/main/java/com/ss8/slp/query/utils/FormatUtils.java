package com.ss8.slp.query.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.ss8.slp.query.entity.QueryItem;
import com.ss8.slp.query.model.QueryItemModel;

@Service
@PropertySource("classpath:application.properties")
public class FormatUtils {
	/**
	 * @param createDate
	 * @param sdfDate
	 */

	public String getDateFormatted(LocalDateTime createDate) {
		if (createDate != null) {
			//System.out.println("String getDateFormatted :: " + createDate + " createDate.toString :: " + createDate.toString());
			return createDate.toString();
		} else
			return null;
	}

	public String getExecuteTimeDateStringFormatted(LocalDateTime createDate) {
		if (createDate != null) {
			//System.out.println("String getExecuteTimeDateStringFormatted :: " + createDate + " createDate.toString :: "+ createDate.toString());
			return createDate.toString();
		} else
			return null;
	}

	public LocalDateTime getExecuteTimeDateFormatted(String executeTime) {
		if (executeTime != null) {
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
			return LocalDateTime.parse(executeTime, formatter1);
		} else
			return null;
	}

	public QueryItemModel convertToModel(QueryItem myObject) {
		QueryItemModel queryItemModelObject = new QueryItemModel();
		// map MyObject fields to MyObjectDTO fields
		queryItemModelObject.setQueryId(myObject.getQueryId());
		queryItemModelObject.setQueryName(myObject.getQueryName());
		queryItemModelObject.setCreateDate(getDateFormatted(myObject.getCreateDate()));
		queryItemModelObject.setUpdateDate(getDateFormatted(myObject.getUpdateDate()));
		queryItemModelObject.setCriteria(myObject.getCriteria());
		queryItemModelObject.setType(myObject.getType());
		queryItemModelObject.setTimeRange(myObject.getTimeRange());
		queryItemModelObject.setQueryDescription(myObject.getDescription());
		queryItemModelObject.setStatus(myObject.getStatus());
		queryItemModelObject.setSubscriberid(myObject.getSubqueryid());
		queryItemModelObject.setQueryOwner(myObject.getQueryOwner());
		queryItemModelObject.setPublicQueryAccess(myObject.getPublicQueryAccess());
		queryItemModelObject.setModifiedBy(myObject.getModifiedBy());
		return queryItemModelObject;

	}

	public static String decode(String url) {

		try {
			String prevURL = "";
			String decodeURL = url;
			while (!prevURL.equals(decodeURL)) {
				prevURL = decodeURL;
				decodeURL = URLDecoder.decode(decodeURL, "UTF-8");
			}
			return decodeURL;
		} catch (UnsupportedEncodingException e) {
			return "Issue while decoding" + e.getMessage();
		}
	}

}