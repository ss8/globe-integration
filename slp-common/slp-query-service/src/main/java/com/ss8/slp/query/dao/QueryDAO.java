package com.ss8.slp.query.dao;

import java.time.LocalDateTime;
import java.util.List;

import com.ss8.slp.query.entity.ConfigItem;
import com.ss8.slp.query.entity.QueryExecutionLogItem;
import com.ss8.slp.query.entity.QueryItem;
import com.ss8.slp.query.model.QueryItemModel;

/**
 * @author : Priyanka (Pb00519606)
 */
public interface QueryDAO {

	List<QueryItem> getqueryById(int queryId);

	List<QueryItem> getQueryByUser(String user);

	List<QueryItem> getAllQueries();

	String deleteArea(int queryId, String userId);

	String updateQuery(QueryItem queryEntity, int queryId);

	boolean isQueryExist(QueryItemModel qryItemObj);

	QueryItem addQeryItem(QueryItem queryItem);

	QueryItem getQueryItem(QueryItem queryItem);

	public String callQidtableindexing_cs(int queryId);

	public void deleteMultipleQureies(List<Integer> ids);

	public int updateConfig(String jsonData);

	public List<ConfigItem> getAllConfig();

	public int insertQueryLog(QueryExecutionLogItem quexlog);

	public QueryExecutionLogItem getQueryExecutionLog(LocalDateTime executionTime);

	public String deleteQueryExecutionLog(LocalDateTime executionTime);

}
