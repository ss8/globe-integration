package com.ss8.slp.query.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "query")
@SqlResultSetMapping(name = "queryItemResultMap", entities = @EntityResult(entityClass = com.ss8.slp.query.entity.QueryItem.class, fields = {
		@FieldResult(name = "queryId", column = "id"), @FieldResult(name = "queryName", column = "name"),
		@FieldResult(name = "createDate", column = "createdate"),
		@FieldResult(name = "updateDate", column = "updatedate"), @FieldResult(name = "criteria", column = "criteria"),
		@FieldResult(name = "type", column = "type"), @FieldResult(name = "timeRange", column = "timerange"),
		@FieldResult(name = "description", column = "description"), @FieldResult(name = "status", column = "status"),
		@FieldResult(name = "subqueryid", column = "parentid"),
		@FieldResult(name = "queryOwner", column = "queryowner"),
		@FieldResult(name = "publicQueryAccess", column = "publicqueryaccess"),
		@FieldResult(name = "modifiedBy", column = "modifiedby")

}))

@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "Select_Query", procedureName = "Select_Query", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "SelectAll_Query", procedureName = "SelectAll_Query", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "selectAllQueryByUser", procedureName = "selectallquerybyuser", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "deletequery", procedureName = "deletequery", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "updatequery", procedureName = "updatequery", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "insertquery", procedureName = "insertquery", resultSetMappings = "queryItemResultMap"),
		@NamedStoredProcedureQuery(name = "qidtableindexing_cs", procedureName = "qidtableindexing_cs", resultSetMappings = "queryItemResultMap")

})

public class QueryItem implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int queryId;
	@Column(name = "name")
	private String queryName;
	@Column(name = "createdate")
	private LocalDateTime createDate;
	@Column(name = "updatedate")
	private LocalDateTime updateDate;
	@Column(name = "criteria")
	private String criteria;
	@Column(name = "type")
	private String type;
	@Column(name = "timerange")
	private String timeRange;
	@Column(name = "description")
	private String description;
	@Column(name = "status")
	private String status;
	@Column(name = "parentid")
	private String subqueryid;
	@Column(name = "queryowner")
	private String queryOwner;
	@Column(name = "publicqueryaccess")
	private String publicQueryAccess;
	@Column(name = "modifiedby")
	private String modifiedBy;

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int queryId) {
		this.queryId = queryId;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubqueryid() {
		return subqueryid;
	}

	public void setSubqueryid(String subqueryid) {
		this.subqueryid = subqueryid;
	}

	public String getQueryOwner() {
		return queryOwner;
	}

	public void setQueryOwner(String queryOwner) {
		this.queryOwner = queryOwner;
	}

	public String getPublicQueryAccess() {
		return publicQueryAccess;
	}

	public void setPublicQueryAccess(String publicQueryAccess) {
		this.publicQueryAccess = publicQueryAccess;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "QueryItem [queryId=" + queryId + ", queryName=" + queryName + ", createDate=" + createDate
				+ ", updateDate=" + updateDate + ", criteria=" + criteria + ", type=" + type + ", timeRange="
				+ timeRange + ", description=" + description + ", status=" + status + ", subqueryid=" + subqueryid
				+ ", queryOwner=" + queryOwner + ", publicQueryAccess=" + publicQueryAccess + ", modifiedBy="
				+ modifiedBy + "]";
	}
	
	

}
