package com.ss8.slp.query.model;

public class QueryExecutionLogModel {

private int id;
private String executeTime;
private int queryId;
private String resultJson;

public String getExecuteTime() {
	return executeTime;
}
public void setExecuteTime(String executeTime) {
	this.executeTime = executeTime;
}
public int getQueryId() {
	return queryId;
}
public void setQueryId(int queryId) {
	this.queryId = queryId;
}
public String getResultJson() {
	return resultJson;
}
public void setResultJson(String resultJson) {
	this.resultJson = resultJson;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
}
