package com.ss8.slp.query.appmonitorAgent;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "config-properties")
@Component
public class ConfigProps {

	private String timeZoneId;
	private String thisServiceName;
	
	public String getTimeZoneId() {
		return timeZoneId;
	}
	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}
	public String getThisServiceName() {
		return thisServiceName;
	}
	public void setThisServiceName(String thisServiceName) {
		this.thisServiceName = thisServiceName;
	}
	@Override
	public String toString() {
		return "ConfigProps [timeZoneId=" + timeZoneId + ", thisServicesName=" + thisServiceName + "]";
	}
	


}
