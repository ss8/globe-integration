package com.ss8.slp.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration(excludeName = "DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class")
public class MyApplication extends org.springframework.boot.web.servlet.support.SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication.run(MyApplication.class, args);
	}

}