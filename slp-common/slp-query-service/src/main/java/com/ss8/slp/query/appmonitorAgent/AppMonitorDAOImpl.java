package com.ss8.slp.query.appmonitorAgent;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;


@Service
public class AppMonitorDAOImpl implements AppMonitorDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<AppMonitor> findAll() {
		try {
			String hql = "FROM AppMonitor";
			return entityManager.createQuery(hql).getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	@Transactional
	public AppMonitor update(AppMonitor appMonitor) {
		try {
			return entityManager.merge(appMonitor);
		} catch (Exception e) {
			return null;
		}
	}

	

}
