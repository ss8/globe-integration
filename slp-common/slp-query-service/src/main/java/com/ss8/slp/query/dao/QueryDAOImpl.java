package com.ss8.slp.query.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ss8.slp.query.entity.ConfigItem;
import com.ss8.slp.query.entity.QueryExecutionLogItem;
import com.ss8.slp.query.entity.QueryItem;
import com.ss8.slp.query.model.QueryItemModel;

/**
 * @author : Priyanka (Pb00519606)
 */

@Transactional
@Repository
public class QueryDAOImpl implements QueryDAO {

	@PersistenceContext
	private EntityManager emQuery;

	@Override
	public List<QueryItem> getqueryById(int queryId) {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("Select_Query");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryId);

		List<QueryItem> res = storedProcedure.getResultList();

		return res;

	}

	@Override
	public List<QueryItem> getQueryByUser(String user) {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("selectAllQueryByUser");
		storedProcedure.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
		storedProcedure.setParameter(0, user);
		@SuppressWarnings("unchecked")
		List<QueryItem> res = storedProcedure.getResultList();
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QueryItem> getAllQueries() {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("SelectAll_Query");
		List<QueryItem> res = storedProcedure.getResultList();
		return storedProcedure.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public String deleteArea(int queryId, String userId) {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("deletequery");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryId);
		// create second place in stored procedure for user id
		storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		// setting user id parameter at second place
		storedProcedure.setParameter(1, userId);
		storedProcedure.executeUpdate();
		return "success";
	}

	@Override
	public String updateQuery(QueryItem qryItem, int queryId) {

		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("updatequery");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryId);
		storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		storedProcedure.setParameter(1, qryItem.getQueryName());
		storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		storedProcedure.setParameter(2, qryItem.getDescription());
		storedProcedure.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
		storedProcedure.setParameter(3, qryItem.getType());
		storedProcedure.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
		storedProcedure.setParameter(4, qryItem.getTimeRange());
		storedProcedure.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
		storedProcedure.setParameter(5, qryItem.getCriteria());
		if (StringUtils.isEmpty(qryItem.getStatus())) {
			qryItem.setStatus("");
		}
		storedProcedure.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
		storedProcedure.setParameter(6, qryItem.getStatus());
		storedProcedure.registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
		storedProcedure.setParameter(7, qryItem.getModifiedBy());
		storedProcedure.registerStoredProcedureParameter(8, String.class, ParameterMode.IN);
		storedProcedure.setParameter(8, qryItem.getPublicQueryAccess());

		int executeUpdate = storedProcedure.executeUpdate();
		boolean successFlag = false;

		return "updated";
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isQueryExist(QueryItemModel qryItemObj) {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		String queryName = qryItemObj.getQueryName();
		String criteria = qryItemObj.getCriteria();
		String type = qryItemObj.getType();
		String timerange = qryItemObj.getTimeRange();
		String hql = "FROM QueryItem as loc where loc.queryName = :queryName and loc.criteria=:criteria and loc.type=:type";
		List<QueryItem> queryItemList = (List<QueryItem>) emQuery.createQuery(hql).setParameter("queryName", queryName)
				.setParameter("criteria", criteria).setParameter("type", type).setMaxResults(5).getResultList();

		return (queryItemList.size() > 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public QueryItem addQeryItem(QueryItem queryItem) {

		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("insertquery");

		storedProcedure.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryItem.getQueryName());
		storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		storedProcedure.setParameter(1, queryItem.getDescription());

		storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		storedProcedure.setParameter(2, queryItem.getType());
		storedProcedure.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
		storedProcedure.setParameter(3, queryItem.getTimeRange());
		storedProcedure.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
		if (StringUtils.isEmpty(queryItem.getCriteria()))
			queryItem.setCriteria("");
		storedProcedure.setParameter(4, queryItem.getCriteria());

		storedProcedure.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
		// hard coding for empty status
		if (StringUtils.isEmpty(queryItem.getStatus())) {
			queryItem.setStatus("");
		}
		storedProcedure.setParameter(5, queryItem.getStatus());

		storedProcedure.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
		storedProcedure.setParameter(6, queryItem.getQueryOwner());

		storedProcedure.registerStoredProcedureParameter(7, String.class, ParameterMode.IN);
		storedProcedure.setParameter(7, queryItem.getPublicQueryAccess());

		/*
		 * storedProcedure.registerStoredProcedureParameter(8, String.class,
		 * ParameterMode.IN); storedProcedure.setParameter(8,
		 * queryItem.getModifiedBy());
		 */

		int executeUpdate = storedProcedure.executeUpdate();

		return queryItem;
	}

	@Override
	public void deleteMultipleQureies(List<Integer> ids) {
		for (int id : ids) {
			emQuery.remove(emQuery.find(QueryItem.class, id));
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public String callQidtableindexing_cs(int queryId) {
		// EntityManager emQuery = queryEntityManager.getObject().createEntityManager();
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("qidtableindexing_cs");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryId);
		storedProcedure.executeUpdate();
		return "success";
	}

	@Override
	public QueryItem getQueryItem(QueryItem qryItemObj) {

		String queryName = qryItemObj.getQueryName();
		String criteria = qryItemObj.getCriteria();
		String type = qryItemObj.getType();
		String timerange = qryItemObj.getTimeRange();
		String hql = "FROM QueryItem as loc where loc.queryName = :queryName and loc.criteria=:criteria and loc.type=:type";
		List<QueryItem> queryItemList = (List<QueryItem>) emQuery.createQuery(hql).setParameter("queryName", queryName)
				.setParameter("criteria", criteria).setParameter("type", type).setMaxResults(5).getResultList();

		return queryItemList != null ? queryItemList.get(0) : null;
	}

	@Override
	public int updateConfig(String jsonData) {
		int resultInt = 0;

		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("updateconfig");
		storedProcedure.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
		storedProcedure.setParameter(0, jsonData);
		resultInt = storedProcedure.executeUpdate();
		return resultInt;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigItem> getAllConfig() {
		String hql = "FROM ConfigItem";
		List<ConfigItem> configItemList = (List<ConfigItem>) emQuery.createQuery(hql).getResultList();
		return configItemList;
	}

	public int insertQueryLog(QueryExecutionLogItem quexlog) {
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("insertquerylog");
		storedProcedure.registerStoredProcedureParameter(0, LocalDateTime.class, ParameterMode.IN);
		storedProcedure.setParameter(0, quexlog.getExecuteTime());
		storedProcedure.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(1, quexlog.getQueryId());
		storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		storedProcedure.setParameter(2, quexlog.getResultJson());
		int resultInt = storedProcedure.executeUpdate();
		return resultInt;
	}

	public QueryExecutionLogItem getQueryExecutionLog(LocalDateTime executionTime) {
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("selectquerylog");
		storedProcedure.registerStoredProcedureParameter(0, LocalDateTime.class, ParameterMode.IN);
		storedProcedure.setParameter(0, executionTime);
		QueryExecutionLogItem quexlog = (QueryExecutionLogItem) storedProcedure.getSingleResult();
		return quexlog;

	}

	public String deleteQueryExecutionLog(LocalDateTime executionTime) {
		StoredProcedureQuery storedProcedure = emQuery.createNamedStoredProcedureQuery("deletequerylog");
		storedProcedure.registerStoredProcedureParameter(0, LocalDateTime.class, ParameterMode.IN);
		storedProcedure.setParameter(0, executionTime);
		int result = storedProcedure.executeUpdate();
		String resultStr = "";
		if (result == 0) {
			resultStr = "success";
		}
		return resultStr;
	}

}
