package com.ss8.slp.query.appmonitorAgent;


import java.util.List;

public interface AppMonitorDAO {
	
	public List<AppMonitor> findAll();
	public AppMonitor update(AppMonitor appMonitor);


}
