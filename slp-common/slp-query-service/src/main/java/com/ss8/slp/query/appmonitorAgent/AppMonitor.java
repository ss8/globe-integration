package com.ss8.slp.query.appmonitorAgent;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "app_monitor")
public class AppMonitor implements Serializable {

	private static final long serialVersionUID = -1899287491581228694L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "app_id")
	private int id;

	@Column(name = "app_name")
	private String appname;

	@Column(name = "last_contacted")
	private LocalDateTime lastcontacted;
	
	@Column(name = "last_restarted")
	private LocalDateTime lastRestarted;

	public AppMonitor() {
		super();
	}

	public AppMonitor(int id, String appname, LocalDateTime lastcontacted, LocalDateTime lastRestarted) {
		super();
		this.id = id;
		this.appname = appname;
		this.lastcontacted = lastcontacted;
		this.lastRestarted = lastRestarted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public LocalDateTime getLastcontacted() {
		return lastcontacted;
	}

	public void setLastcontacted(LocalDateTime lastcontacted) {
		this.lastcontacted = lastcontacted;
	}
	
	public LocalDateTime getLastRestarted() {
		return lastRestarted;
	}

	public void setLastRestarted(LocalDateTime lastRestarted) {
		this.lastRestarted = lastRestarted;
	}
	
	@Override
	public String toString() {
		return "AppMonitor [id=" + id + ", appname=" + appname + ", lastcontacted=" + lastcontacted + ", lastRestarted="
				+ lastRestarted + "]";
	}

}
