package com.ss8.slp.query.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ss8.slp.query.dao.QueryDAO;
import com.ss8.slp.query.entity.ConfigItem;
import com.ss8.slp.query.entity.QueryExecutionLogItem;
import com.ss8.slp.query.entity.QueryItem;
import com.ss8.slp.query.model.ConfigItemModel;
import com.ss8.slp.query.model.QueryExecutionLogModel;
import com.ss8.slp.query.model.QueryItemModel;
import com.ss8.slp.query.utils.FormatUtils;

/**
 * @author : Priyanka (Pb00519606)
 */

@Service
public class QueryServiceImpl implements QueryService {
	private static final Logger logger = Logger.getLogger(QueryServiceImpl.class.getName());
	@Autowired
	private QueryDAO queryImpl;
	@Autowired
	FormatUtils formatUtilsService;
	private String[] dateFormat = new String[] { "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'" };


	@Override
	public List<QueryItemModel> getQueryById(Integer queryId) {
		// TODO Auto-generated method stub
		List<QueryItem> objListqueryItem = queryImpl.getqueryById(queryId);
		for (int i = 0; i < objListqueryItem.size(); i++) {
			logger.info(objListqueryItem.get(i));
			QueryItem areItemOBj = (QueryItem) objListqueryItem.get(i);
			logger.info(objListqueryItem.get(i));
		}

		List<QueryItemModel> queryItemModelList = new ArrayList();
		queryItemModelList = objListqueryItem.stream().map(externalToMyLocation)
				.collect(Collectors.<QueryItemModel>toList());

		return queryItemModelList;
	}

	@Override
	public List<QueryItemModel> getQueryByUser(String user) {
		List<QueryItem> objListQueryItem = queryImpl.getQueryByUser(user);
		for (int i = 0; i < objListQueryItem.size(); i++) {
			logger.info(objListQueryItem.get(i));
			QueryItem queryItemObj = objListQueryItem.get(i);
			logger.info(queryItemObj);
		}
		List<QueryItemModel> queryItemModelList = new ArrayList<QueryItemModel>();
		queryItemModelList = objListQueryItem.stream().map(externalToMyLocation)
				.collect(Collectors.<QueryItemModel>toList());
		return queryItemModelList;
	}

	@Override
	public List<QueryItemModel> getAllAreas() {
		List<QueryItemModel> queryItemModelList = new ArrayList();
		queryItemModelList = queryImpl.getAllQueries().stream().map(externalToMyLocation)
				.collect(Collectors.<QueryItemModel>toList());

		return queryItemModelList;
	}

	Function<QueryItem, QueryItemModel> externalToMyLocation = new Function<QueryItem, QueryItemModel>() {

		public QueryItemModel apply(QueryItem t) {
			QueryItemModel myLocation = formatUtilsService.convertToModel(t);
			return myLocation;
		}
	};

	Function<QueryItemModel, QueryItem> modelToEntity = new Function<QueryItemModel, QueryItem>() {

		public QueryItem apply(QueryItemModel t) {
			QueryItem qtt = new QueryItem();

			qtt.setQueryId(t.getQueryId());
			qtt.setCriteria(t.getCriteria());
			// qtt.setCriteria(rul1);
			qtt.setQueryName(t.getQueryName());
			qtt.setDescription(t.getQueryDescription());
			qtt.setTimeRange(t.getTimeRange());
			qtt.setType(t.getType());
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			LocalDateTime ldateTime = LocalDateTime.parse(Instant.now().toString(), formatter1);
			//System.out.println("ldateTime :: " + ldateTime);
			qtt.setCreateDate(ldateTime);
			qtt.setUpdateDate(ldateTime);
			qtt.setStatus(t.getStatus());
			return qtt;
		}
	};

	public QueryItem convertQueryitemModeltoQuery(QueryItemModel qtm) {
		QueryItem qtt = new QueryItem();

		qtt.setCriteria(qtm.getCriteria());
		qtt.setQueryName(qtm.getQueryName());
		qtt.setTimeRange(qtm.getTimeRange());
		qtt.setType(qtm.getType());
		qtt.setDescription(qtm.getQueryDescription());


		LocalDateTime dtcreate = null;
		LocalDateTime updatecreate = null;

		if (qtm.getCreateDate() == null || qtm.getCreateDate().equals("")) {
			for (int i = 0; i < dateFormat.length; i++) {
				try {
					DateTimeFormatter formatter01 = DateTimeFormatter.ofPattern(dateFormat[i]);
					dtcreate = LocalDateTime.parse(Instant.now().toString(), formatter01);
				} catch (Exception e) {
				}
			}
		} else {
			for (int i = 0; i < dateFormat.length; i++) {
				try {
					DateTimeFormatter formatter01 = DateTimeFormatter.ofPattern(dateFormat[i]);
					dtcreate = LocalDateTime.parse(qtm.getCreateDate(), formatter01);
				} catch (Exception e) {
				}
			}
		}

		if (qtm.getUpdateDate() == null || qtm.getCreateDate().equals("")) {
			for (int i = 0; i < dateFormat.length; i++) {
				try {
					DateTimeFormatter formatter01 = DateTimeFormatter.ofPattern(dateFormat[i]);
					updatecreate = LocalDateTime.parse(qtm.getCreateDate(), formatter01);
				} catch (Exception e) {
				}
			}
		} else {
			for (int i = 0; i < dateFormat.length; i++) {
				try {
					DateTimeFormatter formatter01 = DateTimeFormatter.ofPattern(dateFormat[i]);
					updatecreate = LocalDateTime.parse(qtm.getUpdateDate(), formatter01);
				} catch (Exception e) {
				}
			}
			
		}


		qtt.setCreateDate(dtcreate);
		qtt.setUpdateDate(updatecreate);
		qtt.setQueryId(qtm.getQueryId());
		qtt.setStatus(qtm.getStatus());
		qtt.setSubqueryid(qtm.getSubscriberid());
		qtt.setQueryOwner(qtm.getQueryOwner());
		qtt.setPublicQueryAccess(qtm.getPublicQueryAccess());
		qtt.setModifiedBy(qtm.getModifiedBy());
		return qtt;
	}

	@Override
	public String deleteQueryItem(int queryId, String userId) {
		String status = queryImpl.deleteArea(queryId, userId);
		return status;
	}

	@Override
	public List<QueryItemModel> updateQueryItem(QueryItemModel queryItemModel, int queryId) {
		QueryItem queryEntity = convertQueryitemModeltoQuery(queryItemModel);
		List<QueryItem> objListQueryItem = null;
		String status = queryImpl.updateQuery(queryEntity, queryId);
		if (status == "updated") {
			objListQueryItem = queryImpl.getqueryById(queryId);
			for (int i = 0; i < objListQueryItem.size(); i++) {
				logger.info(objListQueryItem.get(i));
				QueryItem queryItemItemOBj = (QueryItem) objListQueryItem.get(i);
				logger.info(objListQueryItem.get(i));
			}
		}
		List<QueryItemModel> queryItemModelList = new ArrayList();
		queryItemModelList = objListQueryItem.stream().map(externalToMyLocation)
				.collect(Collectors.<QueryItemModel>toList());

		return queryItemModelList;

	}

	@Override
	public boolean isQueryExist(QueryItemModel qryItemModel) {
		return queryImpl.isQueryExist(qryItemModel);
	}

	@Override
	public QueryItemModel addQeryItem(QueryItemModel qryItemModel) {
		// TODO Auto-generated method stub
		QueryItem qryEntity = convertQueryitemModeltoQuery(qryItemModel);
		QueryItem qryPersisted = queryImpl.addQeryItem(qryEntity);
		QueryItem savedQueryItem = queryImpl.getQueryItem(qryPersisted);
		if (savedQueryItem != null) {
			logger.info("the addQeryItem queryid after creation date:" + savedQueryItem.getQueryId());
		} else {
			logger.info("the addQeryItem failed not saved after creation date:" + savedQueryItem);
		}

		QueryItemModel savedQueryModel = formatUtilsService.convertToModel(savedQueryItem);

		return savedQueryModel;
	}

	public void deleteMultipleQueriess(List<Integer> ids) {
		queryImpl.deleteMultipleQureies(ids);
	}

	public int updateConfig(String jsonData) {
		int resultInt = queryImpl.updateConfig(jsonData);
		return resultInt;
	}

	public List<ConfigItemModel> getAllConfig() {
		List<ConfigItem> configList = queryImpl.getAllConfig();
		List<ConfigItemModel> configModelList = new ArrayList<>();
		for (ConfigItem configObj : configList) {
			ConfigItemModel configModel = new ConfigItemModel(configObj.getId(), configObj.getType(),
					configObj.getParam(), configObj.getValue());
			configModelList.add(configModel);

		}
		return configModelList;
	}

	public int insertQueryLog(QueryExecutionLogModel quexlog) {
		logger.info("inside after insert before converting string to date****" + quexlog.getExecuteTime());
		logger.info("inside after insert after converting string to date"
				+ formatUtilsService.getExecuteTimeDateFormatted(quexlog.getExecuteTime()));
		QueryExecutionLogItem queryExecutionObj = new QueryExecutionLogItem();
		queryExecutionObj.setExecuteTime(formatUtilsService.getExecuteTimeDateFormatted(quexlog.getExecuteTime()));
		queryExecutionObj.setQueryId(quexlog.getQueryId());
		queryExecutionObj.setResultJson(quexlog.getResultJson());
		int result = queryImpl.insertQueryLog(queryExecutionObj);
		return result;

	}

	public QueryExecutionLogModel getQueryExecutionLog(LocalDateTime executionTime) {
		logger.info("inside getqueryexecutionlog" + executionTime.toString());
		QueryExecutionLogItem queryExecutionObj = queryImpl.getQueryExecutionLog(executionTime);
		QueryExecutionLogModel queryExecutionLogModel = new QueryExecutionLogModel();
		logger.info("inside after getting the results"
				+ formatUtilsService.getExecuteTimeDateStringFormatted(queryExecutionObj.getExecuteTime()));
		queryExecutionLogModel.setExecuteTime(
				formatUtilsService.getExecuteTimeDateStringFormatted(queryExecutionObj.getExecuteTime()));
		queryExecutionLogModel.setQueryId(queryExecutionObj.getQueryId());
		queryExecutionLogModel.setResultJson(queryExecutionObj.getResultJson());
		queryExecutionLogModel.setId(queryExecutionObj.getId());
		return queryExecutionLogModel;
	}

	public String deleteQueryExecutionLog(LocalDateTime executionTime) {
		return queryImpl.deleteQueryExecutionLog(executionTime);
	}

}
