package com.ss8.slp.query.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ss8.slp.query.model.ConfigItemModel;
import com.ss8.slp.query.model.QueryExecutionLogModel;
import com.ss8.slp.query.model.QueryItemModel;
import com.ss8.slp.query.service.QueryService;
import com.ss8.slp.query.utils.FormatUtils;

/**
 * @author : Priyanka (Pb00519606) Purpose : It contains the implementations of
 *         the "Query service" methods.
 * 
 */
@RestController
@RequestMapping
public class QueryController {
	private static final Logger logger = Logger.getLogger(QueryController.class.getName());
	@Autowired
	private QueryService queryService;
	@Autowired
	private FormatUtils formatUtilsService;

	/* Request mapping string for delete query based on queryId and userId */
	public static final String DELETE_QUERY_ID_USERID = "queries/id/{id}/user/{userId}";
	
	/* get query by id */
	@GetMapping("queries/{id}")
	public ResponseEntity<List<QueryItemModel>> getQueryById(@PathVariable("id") Integer queryId) {
		List<QueryItemModel> queryItem = queryService.getQueryById(queryId);
		return new ResponseEntity<List<QueryItemModel>>(queryItem, HttpStatus.OK);
	}
	
	/* get query by user */
	@GetMapping("queriesu/{user}")
	public ResponseEntity<List<QueryItemModel>> getQueryByUser(@PathVariable("user") String user) {
		List<QueryItemModel> queryItem = queryService.getQueryByUser(user);
		return new ResponseEntity<List<QueryItemModel>>(queryItem, HttpStatus.OK);
	}


	/* searches query */
	@GetMapping("queries")
	public ResponseEntity<List<QueryItemModel>> getAllQuery() {
		List<QueryItemModel> queryItemlist = queryService.getAllAreas();
		return new ResponseEntity<List<QueryItemModel>>(queryItemlist, HttpStatus.OK);
	}

	/* Delete query by using query id and user id from path variable*/
	@DeleteMapping(DELETE_QUERY_ID_USERID)
	public ResponseEntity<String> deleteQueryItem(@PathVariable("id") Integer id, 
			@PathVariable("userId") String userId) {
		String status = queryService.deleteQueryItem(id, userId);
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}

	/* update query */
	@PutMapping("queries/{id}")
	public ResponseEntity<List<QueryItemModel>> updateQueryItem(@PathVariable("id") Integer id,
			@RequestBody QueryItemModel queryItemModel) {
		List<QueryItemModel> queryitemList = queryService.updateQueryItem(queryItemModel, id);
		return new ResponseEntity<List<QueryItemModel>>(queryitemList, HttpStatus.OK);
	}

	@RequestMapping(value = "queries", method = RequestMethod.DELETE)
	public ResponseEntity<List<QueryItemModel>> deleteMultipleArticle(@RequestParam List<Integer> ids) {

		// List<SubscriberMeetingDetectionModel>
		// meetingDetectionOneToManyModelList=meetingDetectionOneToManyService.getMeetingDetectionOneToManyModelList(meetingId);

		// logger.info("area id delete areas result"+ meesageResult+" areaid"+ids);

		queryService.deleteMultipleQueriess(ids);
		logger.info("deleted query area id " + ids);

		// areaItemlist = iareaService.getAllAreas();
		// return new ResponseEntity<List<AreaItemModel>>(areaItemlist, headers,
		// HttpStatus.OK);
		return getAllQuery();

	}

	/* Creating query */

	@RequestMapping(value = "/queries", method = RequestMethod.POST)
	public ResponseEntity<QueryItemModel> createQuery(@RequestBody QueryItemModel qryItemObj,
			UriComponentsBuilder ucBuilder) {
		logger.info("Creating query " + qryItemObj.getQueryName());

		if (queryService.isQueryExist(qryItemObj)) {
			logger.info("A query  with name " + qryItemObj.getQueryName() + " already exist");
			return new ResponseEntity<QueryItemModel>(qryItemObj, HttpStatus.CONFLICT);
		}

		// int id = queryService.addQeryItem(qryItemObj);
		QueryItemModel queryItemModelObj = queryService.addQeryItem(qryItemObj);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/queries/{id}").buildAndExpand(queryItemModelObj.getQueryId()).toUri());
		return new ResponseEntity<QueryItemModel>(queryItemModelObj, headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/updateconfig", headers = "content-type=multipart/*", method = RequestMethod.POST)
	public ResponseEntity<String> updateConfig(@RequestParam String configjson) {
		int resultExecuteUpdate = queryService.updateConfig(configjson);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>("success " + resultExecuteUpdate, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/getallconfig", method = RequestMethod.GET)
	public ResponseEntity<List<ConfigItemModel>> getAllConfig() {
		return new ResponseEntity<List<ConfigItemModel>>(queryService.getAllConfig(), HttpStatus.OK);
	}

	@RequestMapping(value = "/insertexecutionlog", method = RequestMethod.POST)
	public ResponseEntity<QueryExecutionLogModel> createQueryExecutionLog(
			@RequestBody QueryExecutionLogModel qryItemObj, UriComponentsBuilder ucBuilder) {
		int result = queryService.insertQueryLog(qryItemObj);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/insertexecutionlog/{id}").buildAndExpand("" + result).toUri());
		return new ResponseEntity<QueryExecutionLogModel>(qryItemObj, headers, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/getexecutionlog", method = RequestMethod.GET)
	public ResponseEntity<QueryExecutionLogModel> getQueryExecutionLog(@RequestParam String executiondate) {
		// String executeDateStr = formatUtilsService.decode(executeTimeStr);
		logger.info("decoded date string inside getexecution log" + executiondate);
		LocalDateTime executeTime = formatUtilsService.getExecuteTimeDateFormatted(executiondate);
		QueryExecutionLogModel result = queryService.getQueryExecutionLog(executeTime);
		HttpHeaders headers = new HttpHeaders();

		return new ResponseEntity<QueryExecutionLogModel>(result, headers, HttpStatus.OK);

	}

	@RequestMapping(value = "/delexecutionlog", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteQueryExecutionLog(@RequestParam String executiondate) {
		// String executeDateStr = formatUtilsService.decode(executeTimeStr);
		logger.info("decoded date string inside getexecution log" + executiondate);
		LocalDateTime executeTime = formatUtilsService.getExecuteTimeDateFormatted(executiondate);
		String result = queryService.deleteQueryExecutionLog(executeTime);
		HttpHeaders headers = new HttpHeaders();

		return new ResponseEntity<String>(result, headers, HttpStatus.OK);

	}

}