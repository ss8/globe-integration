package com.ss8.slp.query.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "config")
@SqlResultSetMapping(name="configItemResultMap",
entities=@EntityResult(entityClass=com.ss8.slp.query.entity.ConfigItem.class,
    fields = {
            @FieldResult(name="id", column = "id"),
            @FieldResult(name="type", column = "type"),
            @FieldResult(name="param", column = "param"),
            @FieldResult(name="value", column = "value")
         
            }))

@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name = "updateconfig", procedureName = "updateconfig", resultSetMappings="configItemResultMap"),
	
})
public class ConfigItem implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "type")
	private String type;
	@Column(name = "param")
	private String param;
	@Column(name = "value")
	private String value;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
