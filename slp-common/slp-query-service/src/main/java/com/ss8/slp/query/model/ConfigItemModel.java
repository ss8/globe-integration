package com.ss8.slp.query.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

public class ConfigItemModel {

	private int id;
	

	private String type;
	
	private String param;
	
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ConfigItemModel(int id, String type, String param, String value) {
		super();
		this.id = id;
		this.type = type;
		this.param = param;
		this.value = value;
	}
}
