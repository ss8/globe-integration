package com.ss8.slp.query.service;

import java.time.LocalDateTime;
import java.util.List;

import com.ss8.slp.query.model.ConfigItemModel;
import com.ss8.slp.query.model.QueryExecutionLogModel;
import com.ss8.slp.query.model.QueryItemModel;

/**
 * @author : Priyanka (Pb00519606)
 */

public interface QueryService {

	List<QueryItemModel> getAllAreas();

	List<QueryItemModel> getQueryById(Integer queryId);

	List<QueryItemModel> getQueryByUser(String user);

	String deleteQueryItem(int queryId, String userId);

	List<QueryItemModel> updateQueryItem(QueryItemModel qryItemObj, int queryId);

	boolean isQueryExist(QueryItemModel qryItemObj);

	QueryItemModel addQeryItem(QueryItemModel qryItemObj);

	public void deleteMultipleQueriess(List<Integer> ids);

	public int updateConfig(String jsonData);

	public List<ConfigItemModel> getAllConfig();

	public int insertQueryLog(QueryExecutionLogModel quexlog);

	public QueryExecutionLogModel getQueryExecutionLog(LocalDateTime executionTime);

	public String deleteQueryExecutionLog(LocalDateTime executionTime);

}
