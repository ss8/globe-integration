package com.ss8.slp.query.model;

import java.io.Serializable;

import com.ss8.slp.query.entity.QueryItem;
import com.ss8.slp.query.utils.FormatUtils;

public class QueryItemModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269839718493435440L;

	private int queryId;

	private String queryName;

	private String createDate;
	private String updateDate;
	private String criteria;

	private String type;
	private String timeRange;
	private String queryDescription;
	private String status;
	private Long requestId;
	private String subscriberid;
	private String queryOwner;
	private String publicQueryAccess;
	private String modifiedBy;

	public QueryItemModel() {
		super();
	}

	public QueryItemModel(int queryId, String queryName, String createDate, String updateDate, String criteria,
			String type, String timeRange, String description, String status, String subscriberid, String queryOwner,
			String publicQueryAccess, String modifiedBy) {
		super();
		this.queryId = queryId;
		this.queryName = queryName;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.criteria = criteria;
		this.type = type;
		this.timeRange = timeRange;
		this.queryDescription = description;
		this.status = status;
		this.subscriberid = subscriberid;
		this.queryOwner = queryOwner;
		this.publicQueryAccess = publicQueryAccess;
		this.modifiedBy = modifiedBy;
	}

	public QueryItemModel(QueryItem myObject) {
		FormatUtils fmtUtl = new FormatUtils();
		// map MyObject fields to MyObjectDTO fields
		setQueryId(myObject.getQueryId());
		setQueryName(myObject.getQueryName());
		setCreateDate(fmtUtl.getDateFormatted(myObject.getCreateDate()));
		setUpdateDate(fmtUtl.getDateFormatted(myObject.getUpdateDate()));
		setCriteria(myObject.getCriteria());
		setType(myObject.getType());
		setTimeRange(myObject.getTimeRange());
		setQueryDescription(myObject.getDescription());
		setStatus(myObject.getStatus());
		setSubscriberid(myObject.getSubqueryid());
		setQueryOwner(myObject.getQueryOwner());
		setPublicQueryAccess(myObject.getPublicQueryAccess());
		setModifiedBy(myObject.getModifiedBy());
	}

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int queryId) {
		this.queryId = queryId;
	}

	public String getQueryName() {
		return queryName;
	}

	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}

	public String getQueryDescription() {
		return queryDescription;
	}

	public void setQueryDescription(String queryDescription) {
		this.queryDescription = queryDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the requestId
	 */
	public Long getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public String getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(String subscriberid) {
		this.subscriberid = subscriberid;
	}

	public String getQueryOwner() {
		return queryOwner;
	}

	public void setQueryOwner(String queryOwner) {
		this.queryOwner = queryOwner;
	}

	public String getPublicQueryAccess() {
		return publicQueryAccess;
	}

	public void setPublicQueryAccess(String publicQueryAccess) {
		this.publicQueryAccess = publicQueryAccess;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Override
	public String toString() {
		return "QueryItemModel [queryId=" + queryId + ", queryName=" + queryName + ", createDate=" + createDate
				+ ", updateDate=" + updateDate + ", criteria=" + criteria + ", type=" + type + ", timeRange="
				+ timeRange + ", queryDescription=" + queryDescription + ", status=" + status + ", requestId="
				+ requestId + ", subscriberid=" + subscriberid + ", queryOwner=" + queryOwner + ", publicQueryAccess="
				+ publicQueryAccess + ", modifiedBy=" + modifiedBy + "]";
	}
	
	

}
