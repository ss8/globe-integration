package com.ss8.slp.areas.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.ss8.slp.areas.entity.QueryItem;


public class QueryItemModel implements  Serializable{
	
	private Long imsi;
	
	private BigDecimal latitude;
	private BigDecimal longitude;
	
	
	 public QueryItemModel() {
			super();
	 }
	 
	 

	public QueryItemModel(Long imsi, long id, BigDecimal latitude, BigDecimal longitude, String coordinates) {
		super();
		this.imsi = imsi;
		
		this.latitude = latitude;
		this.longitude = longitude;
		
	}
	public QueryItemModel(QueryItem myObject) {
		setImsi(myObject.getImsi());
		
		setLatitude(myObject.getLatitude());
		setLongitude(myObject.getLongitude());
		
	}



	public Long getImsi() {
		return imsi;
	}

	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}

	

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}


	
	 
}
