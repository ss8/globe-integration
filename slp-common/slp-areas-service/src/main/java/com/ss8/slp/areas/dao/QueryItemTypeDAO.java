package com.ss8.slp.areas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ss8.slp.areas.entity.QueryItemType;

@Transactional
@Repository

public class QueryItemTypeDAO implements IQueryItemTypeDAO{
	private static final Logger logger = Logger.getLogger(QueryItemTypeDAO.class.getName());
	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public List<QueryItemType> getQuerybyIdAndType(int queryId,String queryType) {
	
			StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("subscriber_queryid_based_v1");
			storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
			storedProcedure.setParameter(0, queryId);
			List<QueryItemType> res = storedProcedure.getResultList();
			
			
			if(res!=null) {
				logger.info("getQuerybyIdAndType"+queryId+"result size "+res.size());
			}
		
		
		
		return res;
	}
}
