package com.ss8.slp.areas.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.ss8.slp.areas.dao.IAreaDAO;
import com.ss8.slp.areas.entity.AreaItem;
import com.ss8.slp.areas.model.AreaItemModel;
import com.ss8.slp.areas.utils.FormatUtils;

@Service
@PropertySource("classpath:application.properties")
public class AreaService implements IAreaService {
	
	private static final Logger logger = Logger.getLogger(AreaService.class.getName());

	
	@Autowired
	private IAreaDAO iareaDao;
	
	@Autowired
	FormatUtils formatUtilsService;

	@Override
	public List<AreaItemModel> getAreaById(int areaId) {
		List<AreaItem> objListAreItem = iareaDao.getAreaById(areaId);
		for (int i = 0; i < objListAreItem.size(); i++) {
			logger.info(objListAreItem.get(i));
		}
		List<AreaItemModel> areaItemModelList = null;
		areaItemModelList = objListAreItem.stream().map(externalToMyLocation)
				.collect(Collectors.<AreaItemModel>toList());
		return areaItemModelList;
	}

	@Override
	public List<AreaItemModel> getAllAreas() {
		List<AreaItemModel> areaItemModelList =null;
		areaItemModelList = iareaDao.getAllAreas().stream().map(externalToMyLocation)
				.collect(Collectors.<AreaItemModel>toList());
		

		return areaItemModelList;
	}

	Function<AreaItem, AreaItemModel> externalToMyLocation = new Function<AreaItem, AreaItemModel>() {

		public AreaItemModel apply(AreaItem t) {
			
			return formatUtilsService.converToModel(t);
		}
	};

	Function<AreaItemModel, AreaItem> modelToEntity = new Function<AreaItemModel, AreaItem>() {
		public AreaItem apply(AreaItemModel t) {
			AreaItem att = new AreaItem();
			att.setArea_name(t.getArea_name());
			att.setGeometry(t.getGeometry());
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			LocalDateTime ldateTime = LocalDateTime.parse(Instant.now().toString(), formatter1);
			att.setCreateDate(ldateTime);
			att.setUpdateDate(ldateTime);
			att.setDescription(t.getDescription());
			//att.setQuerycount(t.getQuerycount());
			return att;
		}
	};

	public AreaItem convertAreaModeltoArea(AreaItemModel atm) {
		AreaItem att = new AreaItem();
		att.setArea_name(atm.getArea_name());
		att.setGeometry(atm.getGeometry());
		att.setDescription(atm.getDescription());
		//att.setQuerycount(atm.getQuerycount());
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
		logger.info("Create Date From AreaItemModel :: " + atm.getCreateDate());
		
		LocalDateTime dtcreate = null;
		LocalDateTime updatecreate = null;
		if (atm.getCreateDate() == null || atm.getCreateDate().equals("")) {
			dtcreate = LocalDateTime.parse(Instant.now().toString(), formatter1);
		} else {
			dtcreate = LocalDateTime.parse(atm.getCreateDate(), formatter1); 
		}
		
		logger.info("Create Date From convertAreaModeltoArea :: " + dtcreate);
		
		if (atm.getUpdateDate() == null || atm.getUpdateDate().equals("")) {
			updatecreate = LocalDateTime.parse(Instant.now().toString(), formatter1);
		} else {
			updatecreate = LocalDateTime.parse(atm.getUpdateDate(), formatter1); 
		}
		
		logger.info("Create/Update Date From convertAreaModeltoArea :: " + updatecreate);

		att.setCreateDate(dtcreate);
		att.setUpdateDate(updatecreate);
		return att;
	}

	@Override
	public boolean isAreaExist(AreaItemModel areaitemModel) {
		return iareaDao.isAreaExists(areaitemModel);
	}

	@Override

	public int addArea(AreaItemModel areaitemModel) {

		AreaItem areaEntity = convertAreaModeltoArea(areaitemModel);
		AreaItem areaPersisted = iareaDao.addArea(areaEntity);

		return areaPersisted != null ? areaPersisted.getAreaId() : 0;
	}

	@Override
	public String deleteArea(int areaId) {
		 
		return iareaDao.deleteArea(areaId);
	}

	@Override
	public List<AreaItemModel> updateArea(AreaItemModel areaItemmodel, int areaId) {
		AreaItem areaEntity = convertAreaModeltoArea(areaItemmodel);
		List<AreaItem> objListAreItem = null;
		String status = iareaDao.updateArea(areaEntity, areaId);
		logger.info("status" + status);
		if (status == "updated") {
			objListAreItem = iareaDao.getAreaById(areaId);
			if(objListAreItem!=null) {
			for (int i = 0; i < objListAreItem.size(); i++) {
				logger.info(objListAreItem.get(i));
			 }
			}
		}
		List<AreaItemModel> areaItemModelList =null;
		if(objListAreItem!=null) {
		areaItemModelList = objListAreItem.stream().map(externalToMyLocation)
				.collect(Collectors.<AreaItemModel>toList());
		}
		return areaItemModelList;
	}

	@Override
	public List<AreaItemModel> getAreaByName(String areaName) {
		List<AreaItem> objListAreItem = iareaDao.getAreaByName(areaName);
		for (int i = 0; i < objListAreItem.size(); i++) {
			logger.info(objListAreItem.get(i));
		}
		List<AreaItemModel> areaItemModelList = null;
		areaItemModelList = objListAreItem.stream().map(externalToMyLocation)
				.collect(Collectors.<AreaItemModel>toList());
		return areaItemModelList;
	}

	@Override
	public List<AreaItemModel> getAreaByIds(List<Integer> areaIds) {
		// TODO Auto-generated method stub

		List<AreaItem> objListAreItem = iareaDao.getAreaByIds(areaIds);
		for (int i = 0; i < objListAreItem.size(); i++) {
			logger.info(objListAreItem.get(i));
		}
		List<AreaItemModel> areaItemModelList=null;
		areaItemModelList = objListAreItem.stream().map(externalToMyLocation)
				.collect(Collectors.<AreaItemModel>toList());
		return areaItemModelList;

	}

	@Override
	public void deleteMultipleAreas(List<Integer> ids) {
		iareaDao.deleteMultipleAreas(ids);

	}

}
