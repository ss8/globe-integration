package com.ss8.slp.areas.model;

import java.io.Serializable;

import com.ss8.slp.areas.entity.QueryItemType;
import com.ss8.slp.areas.utils.FormatUtils;
public class QueryItemTypeModel implements  Serializable{
    private int id;
	private Long imsi;
	private String areaName;
	private String Latitude;
	private String Longitude;
	private String segmentStartTime;
	
	

	public QueryItemTypeModel() {
		super();
	}

	public QueryItemTypeModel(Long imsi, String areaName,String Latitude,String Longitude, String segmentStartTime,int id) {
		super();
		this.id=id;
		this.imsi = imsi;
		this.areaName = areaName;
		this.Latitude=Latitude;
		this.Longitude=Longitude;
		this.segmentStartTime = segmentStartTime;

	}

	public QueryItemTypeModel(QueryItemType myObject) {
		FormatUtils formatUtils=new FormatUtils();
		setId(myObject.getId());
		setImsi(myObject.getImsi());
		setAreaName(myObject.getAreaName());
		setLatitude(myObject.getLatitude());
		setLongitude(myObject.getLongitude());
		setSegmentStartTime(formatUtils.getDateFormatted(myObject.getSegmentStartTime()));
	}

	public String getAreaName() {
		return areaName;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Long getImsi() {
		return imsi;
	}

	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}

	public String getSegmentStartTime() {
		return segmentStartTime;
	}

	public void setSegmentStartTime(String segmentStartTime) {
		this.segmentStartTime = segmentStartTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
