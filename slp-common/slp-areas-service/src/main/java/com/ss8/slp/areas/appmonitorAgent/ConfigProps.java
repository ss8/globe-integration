package com.ss8.slp.areas.appmonitorAgent;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "config-properties")
@Component
public class ConfigProps {

	private String thisServiceName;

	public String getThisServiceName() {
		return thisServiceName;
	}

	public void setThisServiceName(String thisServiceName) {
		this.thisServiceName = thisServiceName;
	}

	@Override
	public String toString() {
		return "ConfigProps [thisServiceName=" + thisServiceName + "]";
	}

}
