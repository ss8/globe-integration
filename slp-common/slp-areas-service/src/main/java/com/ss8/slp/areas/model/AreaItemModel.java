package com.ss8.slp.areas.model;

import java.io.Serializable;

import com.ss8.slp.areas.entity.AreaItem;
import com.ss8.slp.areas.utils.FormatUtils;

public class AreaItemModel implements Serializable{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -1554193052103486992L;


	private int areaId;  
	
   
	private String area_name;
	
	private String createDate;

	private String updateDate;

	private String geometry;
	
	private String description;
	
	private Long requestId;
	
	 public AreaItemModel() {
			super();
		}
	 
	 public AreaItemModel(AreaItem myObject) {
		 FormatUtils fmtUtl = new FormatUtils();
	        // map MyObject fields to MyObjectDTO fields
		 setAreaId(myObject.getAreaId());
		 setArea_name(myObject.getArea_name());
		 setCreateDate(fmtUtl.getDateFormatted(myObject.getCreateDate()));
		 setUpdateDate(fmtUtl.getDateFormatted(myObject.getUpdateDate()));
		 setGeometry(myObject.getGeometry());
		 setDescription(myObject.getDescription());
	    }
	
	 public AreaItemModel(int areaId, String area_name, String createDate, String updateDate, String geometry,String description) {
			super();
			this.areaId = areaId;
			this.area_name = area_name;
			this.createDate = createDate;
			this.updateDate = updateDate;
			this.geometry = geometry;
			this.description =description;
		}

	
	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getArea_name() {
		return area_name;
	}

	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getGeometry() {
		return geometry;
	}

	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Long getRequestId() {
		return requestId;
	}
	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}
}
