package com.ss8.slp.areas.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "queryItemTypeResultMap", entities = @EntityResult(entityClass = com.ss8.slp.areas.entity.QueryItemType.class, fields = {
		@FieldResult(name = "id", column = "id"), @FieldResult(name = "imsi", column = "imsi"),
		@FieldResult(name = "Areaname", column = "location"), @FieldResult(name = "latitude", column = "latitude"),
		@FieldResult(name = "longitude", column = "longitude"),
		@FieldResult(name = "segmentStartTime", column = "segmentstarttime")

}))
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "subscriber_queryid_based_v1", procedureName = "subscriber_queryid_based_v1", resultSetMappings = "queryItemTypeResultMap") })

public class QueryItemType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "imsi")
	private Long imsi;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "segmentstarttime")
	private LocalDateTime segmentStartTime;

	@Column(name = "location")
	private String Areaname;

	public Long getImsi() {
		return imsi;
	}

	public void setImsi(Long imsi) {
		this.imsi = imsi;
	}

	public String getAreaName() {
		return Areaname;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setAreaName(String areaName) {
		this.Areaname = areaName;
	}

	public LocalDateTime getSegmentStartTime() {
		return segmentStartTime;
	}

	public void setSegmentStartTime(LocalDateTime segmentStartTime) {
		this.segmentStartTime = segmentStartTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
