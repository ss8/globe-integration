package com.ss8.slp.areas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ss8.slp.areas.entity.QueryItem;
import com.ss8.slp.areas.entity.TrackingItem;
import com.ss8.slp.areas.model.TrackingModel;


@Transactional
@Repository
public class ExecuteQueryDAO implements IExecuteQueryDAO {
 
	private static final Logger logger = Logger.getLogger(ExecuteQueryDAO.class.getName());
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<QueryItem> getQuerybyId(int queryId) {
		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("subscriber_queryid_based_v11");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, queryId);
		List<QueryItem> res = storedProcedure.getResultList();
		if(res!=null) {
			logger.info("getQueryId"+queryId+"result size "+res.size());
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TrackingItem> getTrackingDetails(TrackingModel trackingModel) {
		logger.info(trackingModel.getImsiIds());
		logger.info(trackingModel.getStartTime());
		logger.info(trackingModel.getEndTime());
		
		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("subscriber_tracking_imsi_v1");
		storedProcedure.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
		storedProcedure.setParameter(0, trackingModel.getImsiIds());
		storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		storedProcedure.setParameter(1, trackingModel.getStartTime());
		storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		storedProcedure.setParameter(2, trackingModel.getEndTime());
		List<TrackingItem> res = storedProcedure.getResultList();
		
		return res;
	}

	

}
