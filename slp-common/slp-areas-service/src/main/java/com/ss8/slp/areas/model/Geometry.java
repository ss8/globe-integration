package com.ss8.slp.areas.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Geometry implements Serializable{



private ArrayList <ArrayList <String>> coordinates = new  ArrayList<ArrayList <String>>();

private String type;


public Geometry() {
super();
// TODO Auto-generated constructor stub
}




public ArrayList<ArrayList <String>> getCoordinates() {
	return coordinates;
}


public String getType() {
	return type;
}




public void setType(String type) {
	this.type = type;
}




public void setCoordinates(ArrayList<ArrayList <String>> coordinatesStr) {
	this.coordinates = coordinatesStr;
}




}