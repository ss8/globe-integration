package com.ss8.slp.areas.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
@SqlResultSetMapping(name = "queryItemResultMap", entities = @EntityResult(entityClass = com.ss8.slp.areas.entity.QueryItem.class, fields = {
		@FieldResult(name = "Imsi", column = "imsi"),
		@FieldResult(name = "latitude", column = "latitude"),
		@FieldResult(name = "longitude", column = "longitude"),}))
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "subscriber_queryid_based_v11", procedureName = "subscriber_queryid_based_v11", resultSetMappings = "queryItemResultMap")
		})


public class QueryItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "imsi")
	private Long Imsi;

	@Column(name = "latitude")
	private BigDecimal latitude;

	@Column(name = "longitude")
	private BigDecimal longitude;

	/**
	 * @return the imsi
	 */
	public Long getImsi() {
		return Imsi;
	}

	/**
	 * @return the latitude
	 */
	public BigDecimal getLatitude() {
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public BigDecimal getLongitude() {
		return longitude;
	}

	
	/**
	 * @param imsi the imsi to set
	 */
	public void setImsi(Long imsi) {
		this.Imsi = imsi;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}



}
