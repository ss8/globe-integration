package com.ss8.slp.areas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
//@EnableJpaRepositories(basePackages="com.ss8.dao,com.ss8.entity")
//@ComponentScan(basePackages="com.ss8.controller, com.ss8.meeting.service, com.ss8.meeting.utils, com.ss8.meeting.model,com.ss8.dao,com.ss8.entity")
public class SLPAreasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SLPAreasApplication.class, args);
	}

}
