package com.ss8.slp.areas.dao;

import java.util.List;

import com.ss8.slp.areas.entity.AreaItem;
import com.ss8.slp.areas.model.AreaItemModel;

public interface IAreaDAO {

	List<AreaItem> getAreaById(int areaId);
	List<AreaItem> getAllAreas();
    AreaItem addArea(AreaItem area);
    String deleteArea(int areaId);
    boolean isAreaExists(AreaItemModel areaItemObj);
    String updateArea(AreaItem areaEntity, int areaId);
	List<AreaItem> getAreaByName(String area_name);
	List<AreaItem> getAreaByIds(List<Integer> areaIds);
	void deleteMultipleAreas(List<Integer> ids);
    
}
