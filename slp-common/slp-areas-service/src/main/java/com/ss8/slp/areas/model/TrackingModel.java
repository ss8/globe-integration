package com.ss8.slp.areas.model;

import java.io.Serializable;

public class TrackingModel implements  Serializable{
	
	private String imsiIds;
	
	private String startTime;
	
	private String endTime;

	public TrackingModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getImsiIds() {
		return imsiIds;
	}

	public void setImsiIds(String imsi) {
		this.imsiIds = imsi;
	}

	
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	

}
