package com.ss8.slp.areas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;

@Entity
//@Table(name = "Query")
@SqlResultSetMapping(name="trackingItemResultMap",
entities=@EntityResult(entityClass=com.ss8.slp.areas.entity.TrackingItem.class,
    fields = {
            @FieldResult(name="trackingId", column = "id"),
            @FieldResult(name="imsId", column = "imsi"),
            @FieldResult(name="latitude", column = "latitude"),
            @FieldResult(name="longitude", column = "longitude"),
            }))

@NamedStoredProcedureQueries({
	@NamedStoredProcedureQuery(name = "subscriber_tracking_imsi_v1", procedureName = "subscriber_tracking_imsi_v1", resultSetMappings="trackingItemResultMap")
})
public class TrackingItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int trackingId;
	
	@Column(name="imsi")
	private String imsId;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;
	
	
	public String getImsId() {
		return imsId;
	}
	public void setImsId(String imsId) {
		this.imsId = imsId;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public int getTrackingId() {
		return trackingId;
	}
	public void setTrackingId(int trackingId) {
		this.trackingId = trackingId;
	}
	
	

}
