package com.ss8.slp.areas.dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.ss8.slp.areas.entity.AreaItem;
import com.ss8.slp.areas.model.AreaItemModel;

@Transactional
@Repository
public class AreaDAO implements IAreaDAO {
	private static final Logger logger = Logger.getLogger(AreaDAO.class.getName());
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaItem> getAreaById(int areaId) {

		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("Select_Area");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, areaId);

		List<AreaItem> res = storedProcedure.getResultList();
		if(res!=null) {
			logger.info("getAreaId"+areaId+"result size "+res.size());
		}

		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaItem> getAllAreas() {
		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("SelectAll_Area");
		List<AreaItem> res = storedProcedure.getResultList();
		if(res!=null) {
			logger.info("getAllAreaId result size "+res.size());
		}
		return storedProcedure.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isAreaExists(AreaItemModel areaItemModelObj) {
		String areaName = areaItemModelObj.getArea_name();
		String geomentry = areaItemModelObj.getGeometry();
		String hql = "FROM AreaItem as loc where loc.area_name = :areaName and loc.geometry=:geomentry";
		List<AreaItem> areaItemList = (List<AreaItem>) entityManager.createQuery(hql).setParameter("areaName", areaName)
				.setParameter("geomentry", geomentry).setMaxResults(5).getResultList();
		if(areaItemList!=null) {
			logger.info("item exist result size "+areaItemList.size());
		}

		return (areaItemList.size() > 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public AreaItem addArea(AreaItem area) {
		boolean successFlag = false;
		try {
			entityManager.persist(area);
			successFlag = true;
		} catch (EntityExistsException eee) {
			eee.printStackTrace();
		} catch (PersistenceException pee) {
			pee.printStackTrace();
		}
		logger.info("Added Area"+successFlag);

		return area;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String updateArea(AreaItem area, int areaId) {
     String result = "updated";
		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("Update_Area");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, areaId);
		storedProcedure.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		storedProcedure.setParameter(1, area.getArea_name());
		storedProcedure.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		storedProcedure.setParameter(2, area.getGeometry());
		storedProcedure.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
		storedProcedure.setParameter(3, area.getDescription());
		storedProcedure.executeUpdate();
//		AreaItem updatedArea =null;
//		try {
//			 updatedArea =entityManager(area);
//			
//		} catch (EntityExistsException eee) {
//			eee.printStackTrace();
//		} catch (PersistenceException pee) {
//			pee.printStackTrace();
//		}
//		if(updatedArea!=null) {
//		logger.info("updated Area"+updatedArea.getAreaId());
//		result = "updated";
//		}else {
//			logger.info("Not updated Area"+updatedArea);
//			result = "failed";
//		}

		return result;

	}

	@SuppressWarnings("unchecked")
	@Override
	public String deleteArea(int areaId) {
		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("Delete_Area");
		storedProcedure.registerStoredProcedureParameter(0, Integer.class, ParameterMode.IN);
		storedProcedure.setParameter(0, areaId);
		storedProcedure.executeUpdate();
		return "success";

	}

	public List<AreaItem> getAreaByName1(String area_name) { // TODO

		StoredProcedureQuery storedProcedure = entityManager.createNamedStoredProcedureQuery("Area_From_Name");
		storedProcedure.registerStoredProcedureParameter(0, String.class, ParameterMode.IN);
		storedProcedure.setParameter(0, area_name);

		List<AreaItem> res = storedProcedure.getResultList();
		

		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AreaItem> getAreaByName(String area_name) {
		String hql = "FROM AreaItem as loc where loc.area_name = :area_name";
		List<AreaItem> areaItemList = (List<AreaItem>) entityManager.createQuery(hql)
				.setParameter("area_name", area_name).setMaxResults(5).getResultList();

		return areaItemList;
	}

	@SuppressWarnings("unchecked")
	public List<AreaItem> getAreaByIds(List<Integer> areaIds) {

		String hql = "FROM AreaItem as loc where loc.id IN (:areaIds)";

		List<AreaItem> areaItemList = (List<AreaItem>) entityManager.createQuery(hql).setParameter("areaIds", areaIds)
				.getResultList();

		return areaItemList;

	}

	@Override
	public void deleteMultipleAreas(List<Integer> ids) {
		for(int id:ids) {
			entityManager.remove(entityManager.find(AreaItem.class, id));
		}
		
	}

}
