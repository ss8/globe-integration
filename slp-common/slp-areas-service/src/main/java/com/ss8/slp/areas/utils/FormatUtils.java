package com.ss8.slp.areas.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.ss8.slp.areas.entity.AreaItem;
import com.ss8.slp.areas.model.AreaItemModel;

@Component
@PropertySource("classpath:application.properties")
public class FormatUtils {
	/**
	 * @param createDate
	 * @param sdfDate
	 */

	public String getDateFormatted(LocalDateTime createDate) {
		if (createDate != null) {
			DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			return createDate.format(formatter1).toString();
		} else
			return null;
	}

	public String colorRandom() {
		// create random object - reuse this as often as possible
		Random random = new Random();
		// create a big random number - maximum is ffffff (hex) = 16777215 (dez)
		int nextInt = random.nextInt(0xffffff + 1);
		// format it as hexadecimal string (with hashtag and leading zeros)
		String colorCode = String.format("#%06x", nextInt);
		// print it
		System.out.println(colorCode);
		return colorCode;
	}

	public static float distance(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 6371000; // meters
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		float dist = (float) (earthRadius * c);
		return dist;
	}

	
	public AreaItemModel converToModel(AreaItem myObject) {
		AreaItemModel areaItemModelObject = new AreaItemModel();
		// map MyObject fields to MyObjectDTO fields
		areaItemModelObject.setAreaId(myObject.getAreaId());
		areaItemModelObject.setArea_name(myObject.getArea_name());
		areaItemModelObject.setCreateDate(getDateFormatted(myObject.getCreateDate()));
		areaItemModelObject.setUpdateDate(getDateFormatted(myObject.getUpdateDate()));
		areaItemModelObject.setGeometry(myObject.getGeometry());
		areaItemModelObject.setDescription(myObject.getDescription());
		// areaItemModelObject.setQuerycount(myObject.getQuerycount());
		return areaItemModelObject;
	}

}
