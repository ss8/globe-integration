package com.ss8.slp.areas.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.ss8.slp.areas.model.AreaItemModel;
import com.ss8.slp.areas.service.IAreaService;

/**
 * @author Manjunath
 * This class is restcontroller for Area API to add,
 * update,search and delete area
 *
 */

@RestController
public class AreaController {
	
	private static final Logger logger = Logger.getLogger(AreaController.class.getName());
	@Autowired
	IAreaService iareaService;


	/**
	 * This method is to get the area 
	 * @param areaId
	 * @return
	 */
	@RequestMapping(value ="areas/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<AreaItemModel>> getAreaById(@PathVariable("id") Integer areaId) {
		
		List<AreaItemModel> areaItem = iareaService.getAreaById(areaId);
		HttpHeaders headers = new HttpHeaders();
		logger.info("area id"+areaId);
		return new ResponseEntity<List<AreaItemModel>>(areaItem, headers, HttpStatus.OK);
	}

	@GetMapping("areas")
	public ResponseEntity<List<AreaItemModel>> getAllAreas() {
		List<AreaItemModel> areaItemlist = iareaService.getAllAreas();
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<AreaItemModel>>(areaItemlist, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/slpaddareas", method = RequestMethod.POST)
	public ResponseEntity<Void> createArea(@RequestBody AreaItemModel areItemObj, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Area " + areItemObj.getArea_name());
		HttpHeaders headers = new HttpHeaders();
	
		
		if (iareaService.isAreaExist(areItemObj)) {
			logger.info("A Area  with name " + areItemObj.getArea_name() + " already exist");
			return new ResponseEntity<Void>(headers, HttpStatus.CONFLICT);
		}
		int id = iareaService.addArea(areItemObj);
		headers.setLocation(ucBuilder.path("/area/{id}").buildAndExpand(id).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}


	@RequestMapping(value = "areas/{id}", method = RequestMethod.PUT)
	public ResponseEntity<List<AreaItemModel>> updateArea(@PathVariable("id") Integer id,
			@RequestBody AreaItemModel areaItemModel) {

		HttpHeaders headers = new HttpHeaders();
		logger.info("updating Area " + areaItemModel.getArea_name()+ " area id "+ id);
		List<AreaItemModel> update_values = iareaService.updateArea(areaItemModel, id);
		return new ResponseEntity<List<AreaItemModel>>(update_values, headers, HttpStatus.OK);
	}

	@RequestMapping(value = "areas/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<List<AreaItemModel>> deleteArea(@PathVariable("id") Integer id) {
		logger.info("deleting Area id " + id);
		HttpHeaders headers = new HttpHeaders();
	    iareaService.deleteArea(id);
		List<AreaItemModel> areaItemlist = null;
		areaItemlist = iareaService.getAllAreas();
		return new ResponseEntity<List<AreaItemModel>>(areaItemlist, headers, HttpStatus.OK);

	}

	
	@DeleteMapping("/areas/bulk_delete")
	public ResponseEntity<List<AreaItemModel>> deleteMultipleArticle(@RequestParam List<Integer> ids) {
		HttpHeaders headers = new HttpHeaders();
		logger.info("deleting Areas id " + ids);
		iareaService.deleteMultipleAreas(ids);
		List<AreaItemModel> areaItemlist = null;
		areaItemlist = iareaService.getAllAreas();
		return new ResponseEntity<List<AreaItemModel>>(areaItemlist, headers, HttpStatus.OK);
		
		
	}	
	@GetMapping("area/{name}")
	public ResponseEntity<List<AreaItemModel>> getAreaByName(@PathVariable("name") String area_name) {
		logger.info("getting Areas id " + area_name);
		List<AreaItemModel> areaItem = iareaService.getAreaByName(area_name);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<AreaItemModel>>(areaItem, headers, HttpStatus.OK);
	}
	
	@GetMapping("/areas_id")
	@ResponseBody
	public ResponseEntity<List<AreaItemModel>> getAreaByIds(@RequestParam List<Integer> id) {
		logger.info("getting Areas list of id " + id);
		List<AreaItemModel> areaItem=iareaService.getAreaByIds(id);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<AreaItemModel>>(areaItem, headers, HttpStatus.OK);
	}
	
	
	
}

