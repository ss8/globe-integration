package com.ss8.slp.areas.service;

import java.util.List;

import com.ss8.slp.areas.model.QueryItemModel;
import com.ss8.slp.areas.model.QueryItemTypeModel;
import com.ss8.slp.areas.model.TrackingModel;
import com.ss8.slp.areas.model.TrackingModelResponse;

public interface IExecuteQueryService {
	

	List<QueryItemTypeModel> getQueryByIdAndType(int queryId,String queryType);
	public List<TrackingModelResponse> getTrackingDetails(TrackingModel trackingModel);
}
