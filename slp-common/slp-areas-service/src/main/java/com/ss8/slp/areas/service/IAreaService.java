package com.ss8.slp.areas.service;

import java.util.List;

import com.ss8.slp.areas.model.AreaItemModel;

public interface IAreaService {
	List<AreaItemModel> getAreaById(int areaId);
	List<AreaItemModel> getAllAreas();
	int addArea(AreaItemModel areaItem);
    List<AreaItemModel> updateArea(AreaItemModel areaItemmodel,int areaId);
    String deleteArea(int articleId);
    boolean isAreaExist(AreaItemModel areaItem);
	List<AreaItemModel> getAreaByName(String area_name);
	List<AreaItemModel> getAreaByIds(List<Integer> areaIds);
	void deleteMultipleAreas(List<Integer> ids);
   
}
