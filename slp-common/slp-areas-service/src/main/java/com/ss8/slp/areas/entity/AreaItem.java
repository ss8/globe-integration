package com.ss8.slp.areas.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "area")
@SqlResultSetMapping(name = "areaItemResultMap", entities = @EntityResult(entityClass = com.ss8.slp.areas.entity.AreaItem.class, fields = {
		@FieldResult(name = "areaId", column = "id"), @FieldResult(name = "area_name", column = "name"),
		@FieldResult(name = "createDate", column = "createDate"),
		@FieldResult(name = "updateDate", column = "updateDate"), @FieldResult(name = "geometry", column = "geometry"),
		@FieldResult(name = "description", column = "description")
		}))
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "Select_Area", procedureName = "Select_Area", resultSetMappings = "areaItemResultMap"),
		@NamedStoredProcedureQuery(name = "SelectAll_Area", procedureName = "SelectAll_Area", resultSetMappings = "areaItemResultMap"),
		@NamedStoredProcedureQuery(name = "Delete_Area", procedureName = "Delete_Area", resultSetMappings = "areaItemResultMap"),
		@NamedStoredProcedureQuery(name = "Update_Area", procedureName = "Update_Area", resultSetMappings = "areaItemResultMap"),
		@NamedStoredProcedureQuery(name = "Area_From_Name", procedureName = "Area_From_Name", resultSetMappings = "areaItemResultMap") })

public class AreaItem implements Serializable {
	
	private static final long serialVersionUID = 2427349448680424002L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int areaId;
	@Column(name = "name")
	private String area_name;
	@Column(name = "createDate")
	private LocalDateTime createDate;
	@Column(name = "updateDate")
	private LocalDateTime updateDate;
	@Column(name = "geometry")
	private String geometry;
	@Column(name = "description")
	private String description;
	

	/**
	 * @return the areaId
	 */
	public int getAreaId() {
		return areaId;
	}

	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	/**
	 * @return the area_name
	 */
	public String getArea_name() {
		return area_name;
	}

	/**
	 * @param area_name the area_name to set
	 */
	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the geometry
	 */
	public String getGeometry() {
		return geometry;
	}

	/**
	 * @param geometry the geometry to set
	 */
	public void setGeometry(String geometry) {
		this.geometry = geometry;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
