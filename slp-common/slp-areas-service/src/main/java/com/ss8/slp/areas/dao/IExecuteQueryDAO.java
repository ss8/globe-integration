package com.ss8.slp.areas.dao;

import java.util.List;

import com.ss8.slp.areas.entity.QueryItem;
import com.ss8.slp.areas.entity.TrackingItem;
import com.ss8.slp.areas.model.TrackingModel;

public interface IExecuteQueryDAO {
	
	List<QueryItem> getQuerybyId(int queryId);
	List<TrackingItem> getTrackingDetails(TrackingModel trackingModel);

}
