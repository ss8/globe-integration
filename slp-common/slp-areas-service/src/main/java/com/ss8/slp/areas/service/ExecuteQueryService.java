package com.ss8.slp.areas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ss8.slp.areas.dao.IExecuteQueryDAO;
import com.ss8.slp.areas.dao.IQueryItemTypeDAO;
import com.ss8.slp.areas.entity.QueryItem;
import com.ss8.slp.areas.entity.QueryItemType;
import com.ss8.slp.areas.entity.TrackingItem;
import com.ss8.slp.areas.model.Geometry;
import com.ss8.slp.areas.model.QueryItemModel;
import com.ss8.slp.areas.model.QueryItemTypeModel;
import com.ss8.slp.areas.model.TrackingModel;
import com.ss8.slp.areas.model.TrackingModelResponse;
import com.ss8.slp.areas.utils.FormatUtils;

@Service
public class ExecuteQueryService implements IExecuteQueryService {
	
	private static final Logger logger = Logger.getLogger(ExecuteQueryService.class.getName());
	@Autowired
	private IExecuteQueryDAO iqueryDao;
	@Autowired
	private IQueryItemTypeDAO iqueryItemTypeDao;

	
	Function<QueryItem, QueryItemModel> externalToMyLocation = new Function<QueryItem, QueryItemModel>() {

		public QueryItemModel apply(QueryItem t) {
			return  new QueryItemModel(t);
			
		}
	};
	
	@Override
	public List<QueryItemTypeModel> getQueryByIdAndType(int queryId, String queryType) {
		// TODO Auto-generated method stub
		
		List<QueryItemType> objListAreItem = iqueryItemTypeDao.getQuerybyIdAndType(queryId,queryType);
		logger.info(objListAreItem.size());
		List<QueryItemTypeModel> queryItemModelModelList = new ArrayList();
		
		for (int i = 0; i < objListAreItem.size(); i++) {
			QueryItemType areItemOBj = (QueryItemType) objListAreItem.get(i);
			logger.info("queryitem ***********"+areItemOBj.getAreaName());
			QueryItemTypeModel qitmObj = new QueryItemTypeModel(areItemOBj);
			queryItemModelModelList.add(qitmObj);
			logger.info("queryItemModel ***********"+qitmObj.getAreaName());
		}
		
		
		return queryItemModelModelList;
	}
	Function<QueryItemType, QueryItemTypeModel> quetyItemtoModel = new Function<QueryItemType, QueryItemTypeModel>() {

		public QueryItemTypeModel apply(QueryItemType t) {
			return new QueryItemTypeModel(t);
			
		}
	};
	
	@Override
	public List<TrackingModelResponse> getTrackingDetails(TrackingModel trackingModel) {
		List<TrackingItem> objListAreItem = iqueryDao.getTrackingDetails(trackingModel);
		
		List<TrackingModelResponse> objListTrackingModel = new ArrayList<TrackingModelResponse>();
		boolean firstimsci = true;
		String imsiIdOld = "";
		TrackingModelResponse trs =null;
		FormatUtils fmt = new FormatUtils();
		
		ArrayList<ArrayList<String>> coordiStrList = new ArrayList<ArrayList<String>>();
		int last=objListAreItem.size();
		int i=0;
		for (TrackingItem tckmng : objListAreItem) {

			String imsiId = tckmng.getImsId();
			i++;
			if(firstimsci) {
				imsiIdOld = imsiId;

			}

			if (!imsiIdOld.equalsIgnoreCase(imsiId) || firstimsci == true) {
				
				
				
				if(!firstimsci) {
				trs = new TrackingModelResponse();

                trs.setImscId(imsiIdOld);
              
				
				Geometry geometryobj = new Geometry();
				
				geometryobj.setCoordinates(coordiStrList);
				geometryobj.setType("LineString");
				trs.setGeometry(geometryobj);
				trs.setColor(fmt.colorRandom());
				
				
				
				objListTrackingModel.add(trs);
				
				coordiStrList = new ArrayList<ArrayList<String>>();
				
				imsiIdOld = imsiId;
				}
				firstimsci = false;

			}
			
			ArrayList<String> coordinateStringList = new ArrayList<String>();
			coordinateStringList.add(tckmng.getLongitude());
			coordinateStringList.add(tckmng.getLatitude());
			
			coordiStrList.add(coordinateStringList);
			
			if(i==last) {
				
				trs = new TrackingModelResponse();
				trs.setImscId(imsiIdOld);
				
                Geometry geometryobj = new Geometry();
                
				geometryobj.setCoordinates(coordiStrList);
				geometryobj.setType("LineString");
				trs.setGeometry(geometryobj);
				trs.setColor(fmt.colorRandom());
				
				
				objListTrackingModel.add(trs);
			}

		}

		return objListTrackingModel;
	}

}
