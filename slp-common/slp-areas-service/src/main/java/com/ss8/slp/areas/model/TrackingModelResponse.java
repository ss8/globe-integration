package com.ss8.slp.areas.model;

import java.io.Serializable;

public class TrackingModelResponse implements  Serializable{
private String imscId;
private Geometry geometry;

//private String type="LineString";
private String color;

public TrackingModelResponse() {
super();
// TODO Auto-generated constructor stub
}

public TrackingModelResponse(Geometry geometry, String type, String imscid,String color) {
super();
this.geometry = geometry;

this.imscId=imscid;

}

public Geometry getGeometry() {
return geometry;
}

public String getColor() {
	return color;
}

public void setColor(String color) {
	this.color = color;
}

public void setGeometry(Geometry geometry) {
this.geometry = geometry;
}

public String getImscId() {
return imscId;
}

public void setImscId(String imscId) {
this.imscId = imscId;
}




}
