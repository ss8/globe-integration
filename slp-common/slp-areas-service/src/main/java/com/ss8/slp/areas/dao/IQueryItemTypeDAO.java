package com.ss8.slp.areas.dao;

import java.util.List;

import com.ss8.slp.areas.entity.QueryItemType;

public interface IQueryItemTypeDAO {
	List<QueryItemType> getQuerybyIdAndType(int queryId,String queryType);
}
