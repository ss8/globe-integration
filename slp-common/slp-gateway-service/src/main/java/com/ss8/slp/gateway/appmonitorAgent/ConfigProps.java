package com.ss8.slp.gateway.appmonitorAgent;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "config-properties")
@Component
public class ConfigProps {

	private String thisServiceName;
	private String getallservicesUrl;
	private String updateserviceUrl;

	public String getThisServiceName() {
		return thisServiceName;
	}

	public void setThisServiceName(String thisServiceName) {
		this.thisServiceName = thisServiceName;
	}

	public String getGetallservicesUrl() {
		return getallservicesUrl;
	}

	public void setGetallservicesUrl(String getallservicesUrl) {
		this.getallservicesUrl = getallservicesUrl;
	}

	public String getUpdateserviceUrl() {
		return updateserviceUrl;
	}

	public void setUpdateserviceUrl(String updateserviceUrl) {
		this.updateserviceUrl = updateserviceUrl;
	}

	@Override
	public String toString() {
		return "ConfigProps [thisServiceName=" + thisServiceName + ", getallservicesUrl=" + getallservicesUrl
				+ ", updateserviceUrl=" + updateserviceUrl + "]";
	}

}
