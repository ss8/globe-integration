package com.ss8.slp.gateway.appmonitorAgent;


//@JsonIgnoreProperties(ignoreUnknown = true)
public class AppMonitor {

	private int id;

	private String appname;

	private String lastcontacted;
	
	private String lastRestarted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getLastcontacted() {
		return lastcontacted;
	}

	public void setLastcontacted(String lastcontacted) {
		this.lastcontacted = lastcontacted;
	}

	public String getLastRestarted() {
		return lastRestarted;
	}

	public void setLastRestarted(String lastRestarted) {
		this.lastRestarted = lastRestarted;
	}

	@Override
	public String toString() {
		return "AppMonitor [id=" + id + ", appname=" + appname + ", lastcontacted=" + lastcontacted + ", lastRestarted="
				+ lastRestarted + "]";
	}

}
