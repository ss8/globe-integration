package com.ss8.slp.gateway.filters;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class PreFilter extends ZuulFilter {

	private static final Logger LOG = LoggerFactory.getLogger(PreFilter.class);

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		try {
			RequestContext ctx = RequestContext.getCurrentContext();
			HttpServletRequest request = ctx.getRequest();
			LOG.info(
					"Request Method : " + request.getMethod() + " Request URL : " + request.getRequestURL().toString());
			// System.out.println("Request Method : " + request.getMethod() + " Request URL
			// : " + request.getRequestURL().toString());
			// System.out.println("COOKIES :: " +request.getCookies().toString());
			// RequestContext context = RequestContext.getCurrentContext();
			// @SuppressWarnings("unchecked") Set<String> ignoredHeaders = (Set<String>)
			// context.get("ignoredHeaders");
			// ignoredHeaders.remove("authorization");
			return null;
		} catch (Exception e) {
			LOG.error("Exception :: PreFilter :: ", e);
			ReflectionUtils.rethrowRuntimeException(e);
			return null;
		}
	}

}