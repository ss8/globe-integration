package com.ss8.slp.gateway.appmonitorAgent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AppMonitorService {
	private static final Logger LOG = LoggerFactory.getLogger(AppMonitorService.class);

	@Autowired
	ConfigProps cfg;

	@Lazy
	@Autowired
	private RestTemplate restTemplate;

	private HttpEntity<Object> requestEntity;

	@Bean
	private RestTemplate restTemplate() {
		return new RestTemplate();
	}

	private String thisServiceName;
	private String getallservicesUrl;
	private String updateserviceUrl;

	@PostConstruct
	public void initProperties() {
		thisServiceName = cfg.getThisServiceName().trim();
		getallservicesUrl = cfg.getGetallservicesUrl().trim();
		updateserviceUrl = cfg.getUpdateserviceUrl().trim();
	}

	public void createRestTemplate(String putJsonString) {
		try {
			List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(acceptableMediaTypes);
			headers.setContentType(MediaType.APPLICATION_JSON);
			requestEntity = new HttpEntity<Object>(putJsonString, headers);
			restTemplate.put(updateserviceUrl, requestEntity);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Scheduled(fixedDelayString = "${config-properties.healthupdate-interval-in-seconds:120}000")
	private void scheduleMonitor() {

		ResponseEntity<List<AppMonitor>> appMonitorResponse = restTemplate.exchange(getallservicesUrl, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<AppMonitor>>() {
				});
		List<AppMonitor> findAll = appMonitorResponse.getBody();
		ObjectMapper objectMapper = new ObjectMapper();

		if (findAll == null || findAll.isEmpty()) {

			LOG.error(
					"Issue with DB Connection!.. Or Table appmonitor is Empty!.. :: Unable to monitor/manage the applications");
		} else {
			for (AppMonitor am : findAll) {
				if (am.getAppname().trim().equalsIgnoreCase(thisServiceName)) {
					DateTimeFormatter formatter1 = DateTimeFormatter.ISO_DATE_TIME;			
					LocalDateTime timeNow = LocalDateTime.parse(Instant.now().toString(), formatter1);
					am.setLastcontacted(timeNow.toString());
					try {
						createRestTemplate(objectMapper.writeValueAsString(am));
					} catch (JsonProcessingException e1) {
						LOG.error("Exception ::" + e1.getMessage());
					} catch (Exception e) {
						LOG.error("Exception ::" + e.getMessage());
					}

				}
			}
		}

	}

}
