package com.ss8.slp.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableResourceServer
@PropertySource("classpath:application.properties")
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	private static final String RESOURCE_ID = "resource_id";
	@Value("${auth.server.check.token}")
	private String checkTokenURL;

	@Value("${auth.client.id}")
	private String clientId;

	@Value("${auth.client.secret}")
	private String clientSecret;

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {
		http.cors().and().anonymous().and().headers().xssProtection().block(true).and().frameOptions().deny().and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers("/slpareas/**").access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpqueries/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpsubscriberingeofence/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpsimpleintersection/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpsimplehistory/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpraomer/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slptracking/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpmeetingdetection/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slprealtimecriteria/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slprealtimereport/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpgmlcprofile/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpsubscribersimswap/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slptraveldetection/**")
				.access("hasAnyRole('ADMIN','ANALYST')").antMatchers("/slpusermgmt/**").access("hasRole ('ADMIN')")
				.antMatchers("/slpauthserver/**").permitAll().antMatchers("/oauth/**").permitAll();

	}

	@Primary
	@Bean
	public RemoteTokenServices tokenServices() {
		final RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setCheckTokenEndpointUrl(checkTokenURL);
		tokenService.setClientId(clientId);
		tokenService.setClientSecret(clientSecret);
		return tokenService;
	}

}
