package com.ss8.slp.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableScheduling
@EnableResourceServer
@EnableJpaRepositories
public class SlpOauth2AuthenticationApplication {
	public static void main(String[] args) {
		SpringApplication.run(SlpOauth2AuthenticationApplication.class, args);
	}

}
