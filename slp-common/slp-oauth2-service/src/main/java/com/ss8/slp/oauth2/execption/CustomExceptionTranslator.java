package com.ss8.slp.oauth2.execption;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
@Configuration
public class CustomExceptionTranslator implements WebResponseExceptionTranslator {


	 @Override
	    public ResponseEntity<OAuth2Exception> translate(Exception exception) throws Exception {
	        if (exception instanceof OAuth2Exception) {
	            OAuth2Exception oAuth2Exception = (OAuth2Exception) exception;
	            return ResponseEntity
	                    .status(oAuth2Exception.getHttpErrorCode())
	                    .body(new ApiException(oAuth2Exception.getMessage(),"OAuth2 Error"));
	        }else if(exception instanceof AuthenticationException){
	            AuthenticationException authenticationException = (AuthenticationException) exception;
	            return ResponseEntity
	                    .status(HttpStatus.UNAUTHORIZED)
	                    .body(new ApiException(authenticationException.getMessage()));
	        }
	        return ResponseEntity
	                .status(HttpStatus.OK)
	                .body(new ApiException(exception.getMessage()));
	    }

}
