package com.ss8.slp.oauth2.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;


/**
 * Spring Security logout handler
 */
@Component
public class CustomLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler
		implements LogoutSuccessHandler {

	private static final String BEARER_AUTHENTICATION = "Bearer ";
	private static final String HEADER_AUTHORIZATION = "authorization";
	@Autowired
	private TokenStore tokenStore;
	

	public CustomLogoutSuccessHandler(TokenStore tokenStore) {
		this.tokenStore = tokenStore;
		
	}
	/**
	 * This method is used to logout the current user with given token
	 */
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		logger.info("inside onLogoutSuccess");

		String token = request.getHeader(HEADER_AUTHORIZATION);
		JSONObject json = new JSONObject(); 
		if (token != null && token.startsWith(BEARER_AUTHENTICATION)) {

			OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(token.split(" ")[1]);
			
			if (oAuth2AccessToken != null) {
				tokenStore.removeAccessToken(oAuth2AccessToken);
				String loggedInUser = (String)oAuth2AccessToken.getAdditionalInformation().get("username");
				json.put("code", "200");
				json.put("msg", "User successfuly logout");
				response.setStatus(HttpServletResponse.SC_OK);
			}else {
				json.put("error", "invalid_token");
				json.put("error_description", "Invalid access token: "+token.split(" ")[1]);
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print(json.toString());
		out.flush();
	}

}