package com.ss8.slp.oauth2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.ss8.slp.oauth2.execption.CustomAccessDeniedHandler;
import com.ss8.slp.oauth2.execption.CustomAuthenticationEntryPoint;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource_id";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {

		http.cors().and().anonymous().disable().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout", "DELETE"))
                .logoutSuccessHandler(new CustomLogoutSuccessHandler(tokenStore())).deleteCookies("JSESSIONID").
                invalidateHttpSession(true).permitAll()
				.and().anonymous().and().headers().xssProtection().block(true).and().frameOptions().deny().and()
				.authorizeRequests().antMatchers("/oauth/**").permitAll().antMatchers("/logout/**").permitAll().antMatchers("/**").authenticated()
				.and()
				.exceptionHandling().accessDeniedHandler(accessDeniedHandler()).authenticationEntryPoint(authenticationEntryPoint());
	}
	
	@Bean
	CustomAccessDeniedHandler accessDeniedHandler() {
		return new CustomAccessDeniedHandler();
	}

	@Bean
	CustomAuthenticationEntryPoint authenticationEntryPoint() {
		return new CustomAuthenticationEntryPoint();
	}

	@Bean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}

}