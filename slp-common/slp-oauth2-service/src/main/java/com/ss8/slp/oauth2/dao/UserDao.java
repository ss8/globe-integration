package com.ss8.slp.oauth2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ss8.slp.oauth2.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

	User findByfirstName(String firstName);
}
