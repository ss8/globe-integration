package com.ss8.slp.oauth2.execption;

import java.util.Map;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

public class ApiException extends OAuth2Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String error;
	private int status;

	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public ApiException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

	public ApiException(String message, String error) {
		super(message);
		this.error = error;
	}

	public ApiException(String message, String error,int status) {
		super(message);
		this.error = error;
		this.status = status;
	}

	@Override
	public String getOAuth2ErrorCode() {
		return this.error;
	}

	@Override
	public int getHttpErrorCode() {
		// Return Bad Request or some other client-specific error
		return this.status;
	}
}
