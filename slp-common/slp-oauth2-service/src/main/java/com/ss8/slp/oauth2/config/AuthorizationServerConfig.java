package com.ss8.slp.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;

import com.ss8.slp.oauth2.execption.CustomExceptionTranslator;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private UserApprovalHandler userApprovalHandler;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	
	@Autowired
	Environment env;
	
	private static final String SCOPE_READ = "read";
	private static final  String SCOPE_WRITE = "write";
	private static final  String TRUST = "trust";

	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {

		configurer.inMemory()
				  .withClient(env.getProperty("application.oauth2.client.id"))
				  .secret(passwordEncoder.encode(env.getProperty("application.oauth2.client.secret")))
				  .authorizedGrantTypes(env.getProperty("application.oauth2.grant.type1"))
						  //env.getProperty("application.oauth2.authorization.code"), 
//						  env.getProperty("application.oauth2.grant.type2"), 
						  //env.getProperty("application.oauth2.implicit"))
				  .autoApprove(true).scopes(SCOPE_READ, SCOPE_WRITE, TRUST)
				  .accessTokenValiditySeconds(Integer.parseInt(env.getProperty("application.oauth2.token.validity")))
				  .refreshTokenValiditySeconds(Integer.parseInt(env.getProperty("application.oauth2.refreshtoken.validity")));
		;
	}

	/*
	 * This method is used to configure the token store
	 */
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints.tokenStore(tokenStore).tokenEnhancer(tokenEnhancer()).reuseRefreshTokens(false)
				.userApprovalHandler(userApprovalHandler).authenticationManager(authenticationManager)
				.exceptionTranslator(new CustomExceptionTranslator());
	}
	 @Override 
	   public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception { 
	       oauthServer.checkTokenAccess("isAuthenticated()"); 
	   }
	
	@Bean
	public TokenEnhancer tokenEnhancer() {
		return new CustomTokenEnhancer();
	}

	@Bean
	public DefaultAccessTokenConverter accessTokenConverter() {
		return new DefaultAccessTokenConverter();
	}

}