#! /bin/sh


#
# Contains script for creating the SLP DB Controller schema and its contents
# @author Sudeep Chandran M C
# @version: 1.0
# Copyright 2019: Tech Mahindra Limited, India
#

read -p "Enter Controller DB HOST IP : " HOST
sed -i "s|^host.*|host = $HOST|g"  slpdb_controller_config.txt

read -p "Enter Controller DB PORT NO : " PORT
sed -i "s|^port.*|port = $PORT|g"  slpdb_controller_config.txt

read -p "Enter Controller DB UserName : " USER
sed -i "s|^user.*|user = $USER|g"  slpdb_controller_config.txt

read -p "Enter Controller DB Password : " PASSWORD
sed -i "s|^password.*|password = $PASSWORD|g"  slpdb_controller_config.txt

echo 'Creating SLP Database Controller'

mysql --defaults-extra-file=./slpdb_controller_config.txt -t<<EOF

DROP DATABASE IF EXISTS slpdb_controller;
create database slpdb_controller;
use slpdb_controller;
source ./slpdb_controller.sql
EOF

echo 'slpdb_controller created'
