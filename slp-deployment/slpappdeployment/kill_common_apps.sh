#!/bin/bash
#Script to kill all slp-processes running in respective ports

source app_common.conf

#Kill gateway.url=http://localhost:9444
kill $(sudo lsof -t -i:$GATEWAY_PORT)

#Kill areas.url=http://localhost:9651
kill $(sudo lsof -t -i:$AREAS_PORT)

#Kill queries.url=http://localhost:9653
kill $(sudo lsof -t -i:$QUERIES_PORT)


#Kill slpusermgmt.url=http://localhost:8082
kill $(sudo lsof -t -i:$USER_MANAGEMENT_PORT)

#Kill slpauthserver.url=http://localhost:8081
kill $(sudo lsof -t -i:$OAUTH_SERVER_PORT)

