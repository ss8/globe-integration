#!/bin/bash

#sourcing the config files.
source app_common.conf

#changing the directory path to project(slp-release-1.7)
cd $APP_DIRECTORY

#Unzip all zip folders
#if [ -f "*.zip" ]
#then
#unzip \*.zip
#else
#echo "File does not exit"
#exit 0
#fi

sleep 1

#remove all the .zip  directories.
rm -rf *.zip
sleep 1

for i in $APP_COMMON;
do

        echo $i   
         if [ $i == "slp-gatewaycontroller" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$GATEWAY_PORT|g"  application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed  
           sleep 1
           cd $APP_DIRECTORY
        }
        fi
        if [ $i == "slp-area" ]
        then
       	 {
           cd "$i"
	   current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$AREAS_PORT|g"  application.properties
           sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
	   sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
	   sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY
        }
        fi
   
        if [ $i == "slp-query" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$QUERIES_PORT|g"  application.properties
           sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
           sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY
        }
        fi

         if [ $i == "slp-oauth2-service" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$OAUTH_SERVER_PORT|g"  application.properties
           sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
           sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/slp-oauthserver-service.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY
         }
        fi

        
         if [ $i == "slp-usermgmt" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$USER_MANAGEMENT_PORT|g"  application.properties
           sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
           sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/slp-usermgmtservice.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY
        }
        fi
        
        if [ $i == "slp-ui" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           cd $APP_DIRECTORY/$i/tomcat1/bin/
           sleep 1
           ./shutdown.sh
           sleep 1
	   cd $APP_DIRECTORY/$i/tomcat1/conf/
           cp $DEPLOYMENT_DIRECTORY/UI_server.xml server.xml
           sleep 1
           echo updated server.xml
           sleep 1
	   cd $APP_DIRECTORY/$i/tomcat1/webapps/ROOT
           cp $DEPLOYMENT_DIRECTORY/UI_config.js env_config.js
           sleep 1
           echo updated env_config.js
           sleep 1
           cd $APP_DIRECTORY/$i/tomcat1/bin/
           ./startup.sh
           sleep 1
        }
        fi
done

