#!/bin/bash

#Script to kill all slp-processes running in respective ports

source app_northbound.conf

#Kill tracking.url=http://localhost:9652
kill $(sudo lsof -t -i:$RETROSPECTIVE_PORT)

#Kill tracking.url=http://localhost:9601
kill $(sudo lsof -t -i:$GEO_FENCE_ALERT_PORT)
