#!/bin/bash

#sourcing the config files.
source app_northbound.conf

cd $APP_DIRECTORY

for i in $APP_NB;
do
        echo $i   

         if [ $i == "slp-retrospective" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder	   
           java -jar *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY

        }
        elif [ $i == "slp-geofencing-alert" ]
        then
          {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$GEO_FENCE_ALERT_PORT|g"  application.properties
           sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
           sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
           sed '/^slpdb_data.*/d'  < application.properties  > application.new
           mv -f application.new application.properties
           echo "$DB_NAME_READAGENT_PORT" >> application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed
           sleep 1
           cd $APP_DIRECTORY
        }
        fi
done
