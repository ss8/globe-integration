#!/bin/bash


echo "Please input the below parameters"

read -p "Enter the Release version (eg: R_x.0.y) : " RELEASE
if [ "_" = _$RELEASE ] ; then
RELEASE="R_1.2"
fi
sed -i "s|\/opt\/slp\-release\/R\_[0-9]\.[0-9]\.[0-9]*|\/opt\/slp\-release\/$RELEASE|g" app_maintenance.conf
sed -i "s|\/opt\/slp\-release\/R\_[0-9]\.[0-9]\.[0-9]*|\/opt\/slp\-release\/$RELEASE|g" app_common.conf
sed -i "s|\/opt\/slp\-release\/R\_[0-9]\.[0-9]\.[0-9]*|\/opt\/slp\-release\/$RELEASE|g" app_northbound.conf

read -p "Enter Controller DB username [ root ]: " USER
if [ "_" = _$USER ] ; then
USER="root"
fi
sed -i "s|^DATASOURCE_USERNAME.*|DATASOURCE_USERNAME='$USER'|g"  app_maintenance.conf
sed -i "s|^DATASOURCE_USERNAME.*|DATASOURCE_USERNAME='$USER'|g"  app_common.conf
sed -i "s|^DATASOURCE_USERNAME.*|DATASOURCE_USERNAME='$USER'|g"  app_northbound.conf

read -p "Enter Controller DB password : " PASSWORD
sed -i "s|^DATASOURCE_PASSWORD.*|DATASOURCE_PASSWORD='$PASSWORD'|g" app_maintenance.conf
sed -i "s|^DATASOURCE_PASSWORD.*|DATASOURCE_PASSWORD='$PASSWORD'|g" app_common.conf
sed -i "s|^DATASOURCE_PASSWORD.*|DATASOURCE_PASSWORD='$PASSWORD'|g" app_northbound.conf

read -p "Enter Controller DB host IP : " HOST

read -p "Enter Controller DB PORT NO [ 3306 ]: " PORT
if [ "_" = _$PORT ] ; then
PORT="3306"
fi

VAR1='jdbc:mysql://'
VAR2=':'
VAR3='/slpdb_controller'
VAR4='?useSSL=false\\\&useLegacyDatetimeCode=false\\\&serverTimezone=UTC\\\&allowPublicKeyRetrieval=true'
URL=$VAR1$HOST$VAR2$PORT$VAR3$VAR4;
echo "$URL"

sed -i "s|^DATASOURCE_URL.*|DATASOURCE_URL='$URL'|g" app_maintenance.conf
sed -i "s|^DATASOURCE_URL.*|DATASOURCE_URL='$URL'|g" app_common.conf
sed -i "s|^DATASOURCE_URL.*|DATASOURCE_URL='$URL'|g" app_northbound.conf

read -p "Enter Application IP : " APP_IP
TO_BE_REPLACED=`grep "Host name=" UI_server.xml | awk -F"Host name=" '{print $2}' | awk -F " " '{print $1}'| awk -F "\"" '{print $2}' | head -1`
sed -i "s|Host name=\"$TO_BE_REPLACED|Host name=\"$APP_IP|g" UI_server.xml

VAR6='http://'
VAR7=':9444'
APP_URL_WITH_PORT=$VAR6$APP_IP$VAR7
echo $APP_URL_WITH_PORT
sed -i "s|^var APP_URL.*|var APP_URL = '$APP_URL_WITH_PORT'|g"  UI_config.js
awk '/^var APP_URL/{$0=$0";";}1'  UI_config.js > newconfig.js
mv newconfig.js UI_config.js

echo "****The deployment categories****" 
echo "1 for Common Apps Deployment"
echo "2 for Northbound Apps Deployment"
echo "3 for Maintenance Apps(AppMonitor) Deployment"
echo "4 for Complete Application Deployment"
read -p "Enter the respective category : " input

#while [[ ! $xyzzy =~ ^[0-9]+$ ]] ; then


if [ $input == 1 ]
        then
               sleep 1
               echo "Trying to kill common applications"
               sleep 1
               ./kill_common_apps.sh
               echo "executing script to start all common applications"
               ./deploy_app_common.sh


elif [ $input == 2 ]
        then
               sleep 1
               echo "Trying to kill Northbound applications"
               sleep 1
               ./kill_northbound_apps.sh
               echo "executing script to start all northbound applications"
               ./deploy_app_northbound.sh 


elif [ $input == 3 ]
        then
               sleep 1
               echo "Trying to kill maintenance applications"
               sleep 1
               ./kill_maintenance_apps.sh
               echo "executing script to start all maintenance applications"
               ./deploy_app_maintenance.sh


elif [ $input == 4 ]
        then
               sleep 1
               echo "Trying to kill all applications"
               sleep 1
               ./kill_common_apps.sh
               ./kill_northbound_apps.sh
               ./kill_maintenance_apps.sh
               echo "executing script to start all applications"
               ./deploy_app_common.sh
               ./deploy_app_northbound.sh
               ./deploy_app_maintenance.sh

else
        echo "Please enter valid input!"
fi
