#!/bin/bash
#Script to kill all slp-processes running in respective ports

#source app_maintenance.conf

#Kill gateway.url=http://localhost:9444
#kill $(sudo lsof -t -i:$MAINTENANCE_PORT)


process_id1=`ps -ef | grep slp-app-monitor | grep -v grep | awk '{print $2}'`

if [ $process_id1 > 0 ]
then
   echo "Stopping the process : slp-app-monitor, with process id : $process_id"
   process_killed=`ps -ef | grep slp-app-monitor | grep -v grep | awk '{print $2}' |xargs kill`
else
   echo "No such process to Kill: The Process may not be running"
   echo "Proceeding with re-starting the service"
fi

