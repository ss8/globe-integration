#!/bin/bash

#sourcing the config files.
source app_maintenance.conf

#changing the directory path to project(slp-release-1.7)
cd $APP_DIRECTORY

sleep 1

#remove all the .zip  directories.
rm -rf *.zip
sleep 1

for i in $APP_MAINTENANCE;
do

        echo $i   
        if [ $i == "slp-app-monitor" ]
        then
         {
           echo $i
           cd "$i"
           current_folder=`pwd`
           echo $current_folder
           sed -i -e "s|^server\.port.*|server\.port=$APP_MONITOR_PORT|g"  application.properties
	   sed -i -e "s|^spring\.datasource\.url.*|spring\.datasource\.url=$DATASOURCE_URL|g" application.properties
           sed -i -e "s|^spring\.datasource\.username.*|spring\.datasource\.username=$DATASOURCE_USERNAME|g"  application.properties
           sed -i -e "s|^spring\.datasource\.password.*|spring\.datasource\.password=$DATASOURCE_PASSWORD|g"  application.properties
           sleep 1
           echo updated $i:application.properties
           java -jar $JVM_OPTIONS  *.jar > $LOG_DIRECTORY/$i-service.out 2>&1 &
           sleep 1
           echo $i:deployed  
           sleep 1
           cd $APP_DIRECTORY
        }
        fi

done

