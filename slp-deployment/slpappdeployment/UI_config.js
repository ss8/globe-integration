/**
 * Application Base API URL
 */
//var APP_URL = "http://13.52.69.179:9444";
//var APP_URL = "http://54.153.16.113:9444";
var APP_URL = 'http://10.0.156.143:9444';

/**
 * * Map Configuration -> { mapPosition , mapSource }
 * * #> mapPosition :
 * ``````````````````
 * @zoom - Default Zoom Configuration (0 to 20) e.g. zoom: [15]
 * @center - Default Map Location [Longitude, Latitude] e.g. center: [77.59969418365637, 12.977407969339495]
 *
 * * #> mapSource :
 * ````````````````
 * @name - Name of third party map api |###| e.g. "googlemaps"
 * @url - map api url  |###| e.g. "http://mt3.google.com/vt/lyrs=m&x={x}&y={y}&z={z}"
 * @tileSize - Number of tiles |###| e.g. 256
 * @zoomOffset - Zoom Offset value |###| e.g. 0
 *
 * * Sample Config for Mapbox, OpenStreetMap, GoogleMaps and Navmii is given below:
 * * ::: Mapbox :::
 * * ~~~~~~~~~~~~~~
 *  mapSource: {
        name: "mapbox",
        url: "https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
        tileSize: 512,
        zoomOffset: -1
    }
 *
* * ::: Open Street Map :::
* * ~~~~~~~~~~~~~~~~~~~~~
 *  mapSource: {
        name: "openstreetmap",
        url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        tileSize: 256,
        zoomOffset: 0
    }
 *
* * ::: Google Maps:::
* * ~~~~~~~~~~~~~~~~~~
 *  mapSource: {
        name: "googlemap",
        url: "http://mt3.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
        tileSize: 256,
        zoomOffset: 0
    }
 *
* * ::: Navmii :::
* * ~~~~~~~~~~~~~~~~~~
 *  mapSource: {
        name: "navmii",
        url: "http://spb.navmii.com:8089/tiles/{z}/{x}/{y}.png",
        tileSize: 256,
        zoomOffset: 0
    }
 *
 */

var MAP_CONFIG = {
  mapPosition: {
    // 0 - 20
    zoom: [15],
    //[Logitude, Latitude]
    center: [77.59969418365637, 12.977407969339495]
  },
  mapBaseLayer: [
    {
      layerName: "Mapbox-Streets",
      url:
        "https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
      tileSize: 512,
      zoomOffset: -1
    },
    {
      layerName: "Mapbox-Light",
      url:
        "https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA",
      tileSize: 512,
      zoomOffset: -1
    },
    {
      layerName: "Google Maps",
      url: "http://mt3.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
      tileSize: 256,
      zoomOffset: 0
    },
    {
      layerName: "OpenStreetMap.BlackAndWhite",
      url: "https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png",
      tileSize: 256,
      zoomOffset: 0
    },
    {
      layerName: "OpenStreetMap.Mapnik",
      url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      tileSize: 256,
      zoomOffset: 0
    }
  ],
  overlayPOI: [
    {
      key: "Cities - SL",
      color: "purple",
      radius: 500,
      poi: [
        {
          name: "Colombo",
          coords: [6.932, 79.8578],
          popmessage: "Sri Lanka - Population: 217000"
        },
        {
          name: "Ratnapura",
          coords: [6.693, 80.386],
          popmessage: "Sri Lanka - Population: 47832"
        },
        {
          name: "Matara",
          coords: [5.949, 80.5428],
          popmessage: "Sri Lanka - Population: 68244"
        },
        {
          name: "Sri Jawewardenepura Kotte",
          coords: [6.9, 79.95],
          popmessage: "Sri Lanka - Population: 115826"
        },
        {
          name: "Moratuwa",
          coords: [6.7804, 79.88],
          popmessage: "Sri Lanka - Population: 200000"
        },
        {
          name: "Anuradhapura",
          coords: [8.35, 80.3833],
          popmessage: "Sri Lanka - Population: 118302"
        },
        {
          name: "Galle",
          coords: [6.03, 80.24],
          popmessage: "Sri Lanka - Population: 99478"
        },
        {
          name: "Puttalan",
          coords: [8.033, 79.826],
          popmessage: "Sri Lanka - Population: 45661"
        },
        {
          name: "Kandy",
          coords: [7.28, 80.67],
          popmessage: "Sri Lanka - Population: 111701"
        },
        {
          name: "Badulla",
          coords: [6.9837, 81.0499],
          popmessage: "Sri Lanka - Population: 47587"
        },
        {
          name: "Trincomalee",
          coords: [8.569, 81.233],
          popmessage: "Sri Lanka - Population: 108420"
        },
        {
          name: "Kilinochchi",
          coords: [9.4004, 80.3999],
          popmessage: "Sri Lanka - Population: 103717"
        },
        {
          name: "Batticaloa",
          coords: [7.717, 81.7],
          popmessage: "Sri Lanka - Population: 129222"
        },
        {
          name: "Jaffna",
          coords: [9.675, 80.005],
          popmessage: "Sri Lanka - Population: 250000"
        }
      ]
    }
  ]
};
