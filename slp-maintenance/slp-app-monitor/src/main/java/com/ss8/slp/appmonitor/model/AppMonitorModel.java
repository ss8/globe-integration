package com.ss8.slp.appmonitor.model;

import com.ss8.slp.appmonitor.entity.AppMonitor;

public class AppMonitorModel {

	private int id;
	private String appname;
	private String lastcontacted;
	private String lastrestarted;

	public AppMonitorModel() {
		super();
	}

	public AppMonitorModel(int id, String appname, String lastcontacted, String lastrestarted) {
		super();
		this.id = id;
		this.appname = appname;
		this.lastcontacted = lastcontacted;
		this.lastrestarted = lastrestarted;
	}

	public AppMonitorModel(AppMonitor appMonitor) {
		setId(appMonitor.getId());
		if (appMonitor.getAppname() != null || !appMonitor.getAppname().toString().isEmpty())
			setAppname(appMonitor.getAppname());
		if (appMonitor.getLastcontacted() != null || !appMonitor.getLastcontacted().toString().isEmpty())
			setLastcontacted(appMonitor.getLastcontacted().toString());
		if (appMonitor.getLastRestarted() != null || !appMonitor.getLastRestarted().toString().isEmpty())
			setLastrestarted(appMonitor.getLastRestarted().toString());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getLastcontacted() {
		return lastcontacted;
	}

	public void setLastcontacted(String lastcontacted) {
		this.lastcontacted = lastcontacted;
	}

	public String getLastrestarted() {
		return lastrestarted;
	}

	public void setLastrestarted(String lastrestarted) {
		this.lastrestarted = lastrestarted;
	}

	@Override
	public String toString() {
		return "AppMonitorModel [id=" + id + ", appname=" + appname + ", lastcontacted=" + lastcontacted
				+ ", lastrestarted=" + lastrestarted + "]";
	}

}
