package com.ss8.slp.appmonitor.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ss8.slp.appmonitor.entity.AppMonitor;
import com.ss8.slp.appmonitor.model.AppMonitorModel;
import com.ss8.slp.appmonitor.service.AppMonitorService;

@RestController
@RequestMapping(value = "/appmonitor")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AppMonitorController {

	private static final Logger LOG = LoggerFactory.getLogger(AppMonitorController.class);

	@Autowired
	AppMonitorService amService;

	@GetMapping("/getallservices")
	public ResponseEntity<List<AppMonitorModel>> getAllRows() {
		List<AppMonitorModel> allRowsOfAppMonitorTable = amService.getAllRowsOfAppMonitorTable();
		HttpHeaders headers = new HttpHeaders();
		if (allRowsOfAppMonitorTable == null || allRowsOfAppMonitorTable.isEmpty()) {
			return null;
		} else
			return new ResponseEntity<List<AppMonitorModel>>(allRowsOfAppMonitorTable, headers, HttpStatus.OK);
	}

	@PutMapping("/updateservice")
	public ResponseEntity<AppMonitorModel> updateAppMonitor(@RequestBody AppMonitorModel amModel) {
		AppMonitorModel updateAppMonitor = null;
		try {
			updateAppMonitor = amService.updateAppMonitor(new AppMonitor(amModel));
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		if (updateAppMonitor != null)
			return new ResponseEntity<AppMonitorModel>(updateAppMonitor, HttpStatus.OK);
		else
			return null;

	}

}
