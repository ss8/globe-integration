package com.ss8.slp.appmonitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableAutoConfiguration
public class SLPAppMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SLPAppMonitorApplication.class, args);
	}

}
