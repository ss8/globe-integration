package com.ss8.slp.appmonitor.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.ss8.slp.appmonitor.dao.AppMonitorDAO;
import com.ss8.slp.appmonitor.entity.AppMonitor;
import com.ss8.slp.appmonitor.model.AppMonitorModel;
import com.ss8.slp.appmonitor.utilities.ConfigProps;

@Service
public class AppMonitorService {

	private static final Logger LOG = LoggerFactory.getLogger(AppMonitorService.class);

	@Autowired
	AppMonitorDAO amDao;

	@Autowired
	ConfigProps cfg;

	private long healthCheckIntervalInMillis;
	private String servicesRestartMode;

	@PostConstruct
	public void initProperties() {
		healthCheckIntervalInMillis = Long.parseLong(cfg.getHealthcheckIntervalInSeconds().trim()) * 1000;
		servicesRestartMode = cfg.getServicesRestartMode().trim();
	}

	@Scheduled(fixedDelayString = "${config-properties.healthcheck-interval-in-seconds:900}000", initialDelay = 1000)
	private void scheduleMonitor() {

		List<AppMonitor> findAll = amDao.findAll();

		if (findAll == null || findAll.isEmpty()) {
			LOG.error(
					"Issue with DB Connection!.. Or Table appmonitor is Empty!.. :: Unable to monitor/manage the applications");
		} else {

			for (AppMonitor am : findAll) {
				for (String monitoringServices : cfg.getMonitoringServices().split(",")) {
					if (am.getAppname().equals(monitoringServices)) {
						LocalDateTime lastContactedTime = am.getLastcontacted();
						DateTimeFormatter formatter1 = DateTimeFormatter.ISO_DATE_TIME;

						LocalDateTime timeNow = LocalDateTime.parse(Instant.now().toString(), formatter1);
						long timeElapsedInMillis = Duration.between(lastContactedTime, timeNow).toMillis();

						if (timeElapsedInMillis > healthCheckIntervalInMillis) {
							if (servicesRestartMode.equalsIgnoreCase(ConfigProps.SERVICE_RESTART_MODE_AUTO)) {
								LOG.info(am.getAppname() + " :: It's been more than "
										+ (healthCheckIntervalInMillis / 1000) + " Seconds since last contact");
								LOG.info("Service Restart Mode is Set to AUTO :: Restarting the service :: "
										+ am.getAppname());
								manageProcess(am.getAppname());
								timeNow = LocalDateTime.parse(Instant.now().toString(), formatter1);
								am.setLastRestarted(timeNow);
								updateAppMonitor(am);
							} else {
								LOG.info(am.getAppname() + " :: It's been more than "
										+ (healthCheckIntervalInMillis / 1000) + " Seconds since last contact");
								LOG.info("Service Restart Mode is Not Set to AUTO :: Not doing anything !...");
							}

							LOG.info(
									"******************************************************************************************************************************");
						}
					}
				}
			}

		}

	}

	private void manageProcess(String appName) {

		String stdMessage = "";

		String shellCommand = "sh " + ConfigProps.SHELL_SCRIPT_NAME + " " + appName + " ";

		if (appName.equalsIgnoreCase(ConfigProps.SLP_AREA))
			shellCommand = shellCommand + cfg.getSlpArea();
		else if (appName.equalsIgnoreCase(ConfigProps.SLP_GATEWAY))
			shellCommand = shellCommand + cfg.getSlpGateway();
		else if (appName.equalsIgnoreCase(ConfigProps.SLP_QUERY))
			shellCommand = shellCommand + cfg.getSlpQuery();
		else if (appName.equalsIgnoreCase(ConfigProps.SLP_OAUTH))
			shellCommand = shellCommand + cfg.getSlpOauthserver();
		else if (appName.equalsIgnoreCase(ConfigProps.SLP_USERMGMT))
			shellCommand = shellCommand + cfg.getSlpUsermgmtservice();
		else {
			LOG.error(appName + " Not Found!.. Unable to monitor/Manage it...");
		}

		try {
			// Kill the running jar using the Runtime exec method:
			Process stopProcess = Runtime.getRuntime().exec(shellCommand);

			if (servicesRestartMode.equalsIgnoreCase(ConfigProps.SERVICE_RESTART_MODE_AUTO)) {
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(stopProcess.getInputStream()));
				BufferedReader stdError = new BufferedReader(new InputStreamReader(stopProcess.getErrorStream()));
				while ((stdMessage = stdInput.readLine()) != null) {
					LOG.info("Output Message :: " + stdMessage);
				}

				// Error from the command
				while ((stdMessage = stdError.readLine()) != null) {
					LOG.error("Error Message :: " + stdMessage);

				}
				stopProcess.destroy();
			}
		} catch (IOException e) {
			LOG.error("Exception :: " + e.getMessage());
		}
	}

	public AppMonitorModel updateAppMonitor(AppMonitor appMonitor) {

		try {
			List<AppMonitor> findAll = amDao.findAll();
			if (findAll == null || findAll.isEmpty()) {
				LOG.error(
						"Issue with DB Connection!.. Or Table appmonitor is Empty!.. :: Unable to monitor/manage the applications");
				return null;
			} else {
				for (AppMonitor am : findAll) {
					if (appMonitor.getId() == am.getId()) {
						appMonitor.setId(am.getId());
						if (appMonitor.getAppname() == null || appMonitor.getAppname().toString().isEmpty())
							appMonitor.setAppname(am.getAppname().trim());
						if (appMonitor.getLastcontacted() == null || appMonitor.getLastcontacted().toString().isEmpty())
							appMonitor.setLastcontacted(am.getLastcontacted());
						if (appMonitor.getLastRestarted() == null || appMonitor.getLastRestarted().toString().isEmpty())
							appMonitor.setLastRestarted(am.getLastRestarted());
						AppMonitor update = amDao.update(appMonitor);
						return new AppMonitorModel(update);
					} else if (appMonitor.getAppname().trim().equalsIgnoreCase(am.getAppname().trim())) {
						appMonitor.setId(am.getId());
						if (appMonitor.getLastcontacted() == null || appMonitor.getLastcontacted().toString().isEmpty())
							appMonitor.setLastcontacted(am.getLastcontacted());
						if (appMonitor.getLastRestarted() == null || appMonitor.getLastRestarted().toString().isEmpty())
							appMonitor.setLastRestarted(am.getLastRestarted());
						AppMonitor update = amDao.update(appMonitor);
						return new AppMonitorModel(update);
					}

				}

				LOG.info("Invalid id and app-name values!.. Not Inserting this :: " + appMonitor.toString());
				return null;
			}

		} catch (Exception e) {
			return null;
		}

	}

	public List<AppMonitorModel> getAllRowsOfAppMonitorTable() {
		try {
			List<AppMonitorModel> findAll = amDao.findAll().stream().map(entityToModel)
					.collect(Collectors.<AppMonitorModel>toList());
			return findAll;
		} catch (Exception e) {
			return null;
		}
	}

	Function<AppMonitor, AppMonitorModel> entityToModel = new Function<AppMonitor, AppMonitorModel>() {
		public AppMonitorModel apply(AppMonitor amEntity) {
			return new AppMonitorModel(amEntity);
		}
	};

}
