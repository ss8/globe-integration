package com.ss8.slp.appmonitor.dao;

import java.util.List;

import com.ss8.slp.appmonitor.entity.AppMonitor;

public interface AppMonitorDAO {

	public List<AppMonitor> findAll();

	public AppMonitor update(AppMonitor appMonitor);

}
