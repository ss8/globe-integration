package com.ss8.slp.appmonitor.utilities;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "config-properties")
@Component
public class ConfigProps {
	public static final String SERVICE_RESTART_MODE_AUTO = "AUTO";
	public static final String SERVICE_RESTART_MODE_MANUAL = "MANUAL";
	public static final String SHELL_SCRIPT_NAME = "restart_process.sh";

	// slp_app_common
	public static final String SLP_AREA = "slp-areas-service";
	public static final String SLP_GATEWAY = "slp-gateway-service";
	public static final String SLP_QUERY = "slp-query-service";
	public static final String SLP_OAUTH = "slp-oauth2-service";
	public static final String SLP_USERMGMT = "slp-usermgmt-service";

	private String releaseVersion;
	private String healthcheckIntervalInSeconds;
	private String servicesRestartMode;
	private String monitoringServices;

	// slp_app_common
	private String slpArea;
	private String slpGateway;
	private String slpQuery;
	private String slpUi;
	private String slpOauthserver;
	private String slpUsermgmtservice;

	public String getMonitoringServices() {
		return monitoringServices;
	}

	public void setMonitoringServices(String monitoringServices) {
		this.monitoringServices = monitoringServices;
	}

	public String getReleaseVersion() {
		return releaseVersion;
	}

	public void setReleaseVersion(String releaseVersion) {
		this.releaseVersion = releaseVersion;
	}

	public String getHealthcheckIntervalInSeconds() {
		return healthcheckIntervalInSeconds;
	}

	public void setHealthcheckIntervalInSeconds(String healthcheckIntervalInSeconds) {
		this.healthcheckIntervalInSeconds = healthcheckIntervalInSeconds;
	}

	public String getServicesRestartMode() {
		return servicesRestartMode;
	}

	public void setServicesRestartMode(String servicesRestartMode) {
		this.servicesRestartMode = servicesRestartMode;
	}

	public String getSlpArea() {
		return slpArea;
	}

	public void setSlpArea(String slpArea) {
		this.slpArea = slpArea;
	}

	public String getSlpGateway() {
		return slpGateway;
	}

	public void setSlpGateway(String slpGateway) {
		this.slpGateway = slpGateway;
	}

	public String getSlpQuery() {
		return slpQuery;
	}

	public void setSlpQuery(String slpQuery) {
		this.slpQuery = slpQuery;
	}

	public String getSlpUi() {
		return slpUi;
	}

	public void setSlpUi(String slpUi) {
		this.slpUi = slpUi;
	}

	public String getSlpOauthserver() {
		return slpOauthserver;
	}

	public void setSlpOauthserver(String slpOauthserver) {
		this.slpOauthserver = slpOauthserver;
	}

	public String getSlpUsermgmtservice() {
		return slpUsermgmtservice;
	}

	public void setSlpUsermgmtservice(String slpUsermgmtservice) {
		this.slpUsermgmtservice = slpUsermgmtservice;
	}

	@Override
	public String toString() {
		return "ConfigProps [releaseVersion=" + releaseVersion + ", healthcheckIntervalInSeconds="
				+ healthcheckIntervalInSeconds + ", servicesRestartMode=" + servicesRestartMode + ", slpArea=" + slpArea
				+ ", slpGateway=" + slpGateway + ", slpQuery=" + slpQuery + ", slpUi=" + slpUi + ", slpOauthserver="
				+ slpOauthserver + ", slpUsermgmtservice=" + slpUsermgmtservice + "]";
	}

}
