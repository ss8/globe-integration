package com.ss8.slp.appmonitor.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.ss8.slp.appmonitor.entity.AppMonitor;

@Service
public class AppMonitorDAOImpl implements AppMonitorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public List<AppMonitor> findAll() {
		String hql = "FROM AppMonitor";
		@SuppressWarnings("unchecked")
		List<AppMonitor> allRows = entityManager.createQuery(hql).getResultList();
		return allRows;
	}

	@Override
	@Transactional
	public AppMonitor update(AppMonitor appMonitor) {
		AppMonitor merge;
		try {
			merge = entityManager.merge(appMonitor);
			return merge;
		} catch (Exception e) {
			return null;
		}
	}

}
