import * as actiontype from '../actions/types';
import {
  agUserListColDefs,
  agAppStatusListColDefs
} from '../config/agUserGridHeaders';
import produce from 'immer';
/**
 * USER Initial State
 */
const initialState = {
  userList: [],
  isCreatePaneActive: false,
  userRoles: [],
  user: {
    opMode: null,
    formFields: {
      firstName: '',
      lastName: '',
      password: '',
      userRole: []
    }
  },
  userGridColState: [],
  agGrid: {
    columnDefs: agUserListColDefs,
    checkedRows: [],
    contextItems: [
      { item: 'Edit', anticon: 'edit', funcname: 'onUserEdit' },
      { item: 'Delete', anticon: 'delete', funcname: 'onUserDelete' }
    ],
    contextId: 'agUser'
  },
  appStatus: [],
  appStatusColDefs: agAppStatusListColDefs
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.LOAD_USERLIST: {
      return {
        ...state,
        isCreatePaneActive: false,
        userList: action.payload.data
      };
    }
    case actiontype.SAVE_USERGRID_STATE: {
      return {
        ...state,
        userGridColState: action.payload
      };
    }
    case actiontype.LOAD_ROLES: {
      return {
        ...state,
        userRoles: action.payload
      };
    }
    case actiontype.DELETE_USER: {
      return {
        ...state,
        userList: action.payload.data
      };
    }
    case actiontype.USER_GRID_CHECKED: {
      const base = produce(state.agGrid, draft => {
        draft.checkedRows = action.payload.checkedRows;
      });
      return {
        ...state,
        agGrid: base
      };
    }
    case actiontype.CREATE_USER: {
      const base = produce(state.user, draft => {
        draft.opMode = action.payload.opMode;
        draft.formFields = initialState.user.formFields;
      });
      return {
        ...state,
        isCreatePaneActive: false,
        user: base
      };
    }
    case actiontype.EDIT_USER: {
      const base = produce(state.user, draft => {
        draft.opMode = action.payload.opMode;
        draft.formFields = action.payload.formFields;
      });
      return {
        ...state,
        isCreatePaneActive: true,
        user: base
      };
    }
    case actiontype.CLEAR_CREATE_USER_PANE: {
      return {
        ...state,
        isCreatePaneActive: false
      };
    }
    case actiontype.CREATE_USER_MODE: {
      const base = produce(state.user, draft => {
        draft.opMode = action.payload.opMode;
        draft.formFields = initialState.user.formFields;
      });
      return {
        ...state,
        isCreatePaneActive: true,
        user: base
      };
    }
    case actiontype.RESET_ADMIN: {
      return initialState;
    }
    case actiontype.LOAD_APPSTATUS: {
      return {
        ...state,
        appStatus: action.payload
      };
    }
    default:
      return state;
  }
};

export default userReducer;
