import * as actiontype from '../actions/types';
import produce from 'immer';
export const { APP_URL } = window;

/**
 * Map Initial State
 */
const initialState = {
  hasSearch: false,
  hasDrawControl: false,
  mapBaseLayer: [],
  overlayPOI: [],
  selectedPOI: [],
  selectedMapName: null,
  mapPosition: {},
  geofenceMapResponse: []
};
/**
 *
 * @param {*} state
 * @param {*} action
 * Reducers for action types 'ADDON_DRAWCONTROL, ADDON_SEARCH'
 */
const mapReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.ADDON_DRAWCONTROL: {
      return {
        ...state,
        hasDrawControl: action.payload.hasDrawControl
      };
    }
    case actiontype.INIT_BASEMAP: {
      return {
        ...state,
        selectedMapName: action.payload
      };
    }
    case actiontype.SELECTED_POI: {
      return {
        ...state,
        selectedPOI: action.payload
      };
    }
    case actiontype.ADDON_SEARCH: {
      return {
        ...state,
        hasSearch: action.payload.hasSearch
      };
    }
    case actiontype.INIT_MAP_PARAMS: {
      const base = produce(state, draft => {
        let params = Object.keys(action.payload);
        params.forEach(key => {
          draft[key] = action.payload[key];
        });
      });
      return {
        ...state,
        ...base
      };
    }
    case actiontype.CLEAR_ON_EXITMAP: {
      return initialState;
    }
    case actiontype.RESET_MAP: {
      return {
        ...initialState,
        mapPosition: { zoom: [], center: state.mapPosition.center || [] }
      };
    }

  // geofenceAlert
  case actiontype.LOAD_GEOFENCE_MAP_RESPONSE: {
    return {
      ...state,
      geofenceMapResponse: action.payload ? action.payload : []
    };
  }

    default:
      return state;
  }
};

export default mapReducer;
