import * as actiontype from '../actions/types';
import { agQueriesListColDefs } from '../config/agGridHeaders';
import { Ruleset } from '../config/queryRulesets';
import produce from 'immer';
/**
 * Query Initial State
 */
const initialState = {
  queriesList: [],
  isCreatePaneActive: false,
  queryExecLast: null,
  queryExecInfo: [],
  queryExecInProgress: [],
  queryExecFails: [],
  queryBuilder: {
    opMode: null,
    formFields: {
      queryId: '',
      queryName: '',
      queryDesc: '',
      queryType: '',
      queryPreview: ''
    },
    queryRules: null
  },
  retroDataSources: [],
  retroInterceptNames: [],
  queryGridColState: [],
  agGrid: {
    columnDefs: agQueriesListColDefs,
    checkedRows: [],
    contextItems: [
      {
        item: 'Execute',
        anticon: 'play-circle',
        funcname: 'onQueryExecute',
        disabled: false
      },
      {
        item: 'Edit',
        anticon: 'edit',
        funcname: 'onEditQuery',
        disabled: false
      },
      {
        item: 'Delete',
        anticon: 'delete',
        funcname: 'onDeleteQuery',
        disabled: false
      }
    ],
    contextId: 'agQueries'
  }
};

const queryReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.LOAD_QUERIESLIST: {
      const base = produce(action.payload.data, draft => {
        if (state.queryExecInfo.length) {
          action.payload.data.forEach((o, inx) => {
            let qinx = state.queryExecInfo.findIndex(
              obj => obj.queryId === o.queryId
            );
            if (qinx > -1) {
              draft[inx].status = state.queryExecInfo[qinx].resultsCount;
            }
          });
        }
        if (state.queryExecFails.length) {
          /**
           * Failed / Error - Query Status
           */
          action.payload.data.forEach((o, inx) => {
            let qinx = state.queryExecFails.findIndex(
              obj => obj.queryId === o.queryId
            );
            if (qinx > -1) {
              draft[inx].status = -2;
            }
          });
        }
        if (state.queryExecInProgress.length) {
          /**
           * Execution In Progress - Query Status
           */
          action.payload.data.forEach((o, inx) => {
            let qinx = state.queryExecInProgress.findIndex(
              id => id === o.queryId
            );
            if (qinx > -1) {
              draft[inx].status = -1;
            }
          });
        }
        return draft;
      });

      return {
        ...state,
        queriesList: base,
        isCreatePaneActive: false
      };
    }
    case actiontype.LOAD_RETRO_DATASOURCES: {
      return {
        ...state,
        retroDataSources: action.payload
      };
    }
    case actiontype.LOAD_INTERCEPT_NAMES: {
      return {
        ...state,
        retroInterceptNames: action.payload
      };
    }
    case actiontype.DELETE_QUERY: {
      return {
        ...state,
        queriesList: action.payload.data,
        isCreatePaneActive: false
      };
    }
    case actiontype.SAVE_QUERYGRID_STATE: {
      return {
        ...state,
        queryGridColState: action.payload
      };
    }
    case actiontype.QUERY_EXECUTION_START: {
      const base = produce(state.queryExecInProgress, draft => {
        if (draft.findIndex(id => id === action.payload) === -1) {
          draft.push(action.payload);
        }
      });
      return {
        ...state,
        queryExecInProgress: base
      };
    }
    case actiontype.QUERY_EXECUTION_END: {
      const base = produce(state.queryExecInProgress, draft => {
        draft.splice(draft.findIndex(id => id === action.payload), 1);
      });
      return {
        ...state,
        queryExecInProgress: base
      };
    }
    case actiontype.QUERY_EXEC_INFO: {
      const base = produce(state.queryExecInfo, draft => {
        let inx = draft.findIndex(
          obj => obj.queryId === action.payload.queryId
        );
        if (inx > -1) {
          draft[inx].execTime = action.payload.execTime;
          draft[inx].execTimeHHMMSS = new Date(
            parseInt(action.payload.execTime)
          )
            .toISOString()
            .slice(11, -1);
          draft[inx].resultsCount = action.payload.resultsCount;
          draft[inx].execMoment = action.payload.execMoment;
          draft[inx].queryString = action.payload.queryString;
          draft[inx].execAt = action.payload.execAt;
          draft[inx].execStatus = `${
            action.payload.resultsCount === 0
              ? 'No'
              : action.payload.resultsCount
          } results in ${new Date(parseInt(action.payload.execTime))
            .toISOString()
            .slice(11, -1)}! Executed @ ${action.payload.execAt}`;
        } else {
          draft.push({
            queryId: action.payload.queryId,
            execTime: action.payload.execTime,
            execTimeHHMMSS: new Date(parseInt(action.payload.execTime))
              .toISOString()
              .slice(11, -1),
            resultsCount: action.payload.resultsCount,
            execMoment: action.payload.execMoment,
            queryString: action.payload.queryString,
            execAt: action.payload.execAt,
            execStatus: `${
              action.payload.resultsCount === 0
                ? 'No'
                : action.payload.resultsCount
            } results in ${new Date(parseInt(action.payload.execTime))
              .toISOString()
              .slice(11, -1)}! Executed @ ${action.payload.execAt}`
          });
        }
      });
      return {
        ...state,
        queryExecInfo: base,
        queryExecLast: action.payload.resultsCount
          ? action.payload.queryId
          : state.queryExecLast
      };
    }
    case actiontype.TOGGLE_CREATE_QUERY_PANE: {
      const base = produce(state.queryBuilder, draft => {
        draft.opMode = action.payload.opMode;
      });
      return {
        ...state,
        queryBuilder: base,
        isCreatePaneActive: action.payload.bool
      };
    }
    case actiontype.LOAD_QUERIES_FORM_FIELDS: {
      const base = produce(state.queryBuilder, draft => {
        draft.formFields[action.payload.field] = action.payload.value;
        if (action.payload.field === 'queryType') {
          draft.queryRules = Ruleset[action.payload.value];
        }
      });
      return {
        ...state,
        queryBuilder: base,
        isCreatePaneActive: true
      };
    }
    case actiontype.LOAD_QUERY_RULES_FORM_FIELDS: {
      const base = produce(state.queryBuilder, draft => {
        draft.queryRules.rulesform[action.payload.index] = {
          [action.payload.field]: action.payload.value
        };
      });
      return {
        ...state,
        queryBuilder: base,
        isCreatePaneActive: true
      };
    }
    case actiontype.QUERY_GRID_CHECKED: {
      const base = produce(state.agGrid, draft => {
        draft.checkedRows = action.payload.checkedRows;
      });
      return {
        ...state,
        agGrid: base
      };
    }
    case actiontype.QUERY_EXEC_STATUS: {
      const base = produce(state.queriesList, draft => {
        draft[
          draft.findIndex(obj => obj.queryId === action.payload.queryId)
        ].status = action.payload.status;
      });
      const baseExecErrs = produce(state.queryExecFails, draft => {
        let inx = draft.findIndex(
          obj => obj.queryId === action.payload.queryId
        );
        /**
         * Execution in progress - clear any previous queryExecFails
         */
        if (action.payload.status === -1 && inx > -1) {
          draft.splice(inx, 1);
        }
        if (action.payload.message !== undefined) {
          if (inx > -1) {
            draft[inx].message = action.payload.message;
            draft[inx].queryId = action.payload.queryId;
          } else {
            draft.push({
              message: action.payload.message,
              queryId: action.payload.queryId
            });
          }
        }
      });

      return {
        ...state,
        queriesList: base,
        queryExecFails: baseExecErrs
      };
    }
    case actiontype.QUERY_HEATMAP_CHILDID: {
      const base = produce(state.queriesList, draft => {
        draft[
          draft.findIndex(obj => obj.queryId === action.payload.queryId)
        ].subscriberid = action.payload.subscriberid;
      });

      return {
        ...state,
        queriesList: base
      };
    }
    case actiontype.RESET_QUERY: {
      return initialState;
    }
    default:
      return state;
  }
};

export default queryReducer;
