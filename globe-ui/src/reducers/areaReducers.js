import * as actiontype from '../actions/types';
import { agAreasListColDefs } from '../config/agGridHeaders';
import produce from 'immer';
import { pullAt } from 'lodash';
/**
 * Area Initial State
 */
const initialState = {
  areasList: [],
  homecountry: '',
  timezone: '',
  mapArea: {
    opMode: null,
    formFields: {
      areaId: '',
      areaName: '',
      areaDesc: '',
      areaLayerCoords: []
    }
  },
  areaGridColState: [],
  agGrid: {
    columnDefs: agAreasListColDefs,
    checkedRows: [],
    contextItems: [
      { item: 'Edit', anticon: 'edit', funcname: 'navigateToEditArea' },
      { item: 'Delete', anticon: 'delete', funcname: 'onAreaDelete' }
    ],
    contextId: 'agAreas'
  },
  areasquaremeters: ''
};

const areaReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.LOAD_AREASLIST: {
      return {
        ...state,
        areasList: action.payload.data
      };
    }
    case actiontype.SET_HOMECOUNTRYCODE: {
      return {
        ...state,
        homecountry: action.payload
      };
    }
    case actiontype.SET_TIMEZONE: {
      return {
        ...state,
        timezone: action.payload
      };
    }
    case actiontype.SAVE_AREAGRID_STATE: {
      return {
        ...state,
        areaGridColState: action.payload
      };
    }
    case actiontype.LOAD_MAP_AREA_DRAWN_COORDS: {
      const base = produce(state.mapArea, draft => {
        let leafletId;
        if (
          typeof action.payload.areaLayerCoords === 'string' &&
          action.payload.areaLayerCoords !== ''
        ) {
          leafletId = JSON.parse(action.payload.areaLayerCoords).properties
            .leafletId;
          let updated_indexes = [];
          draft.formFields.areaLayerCoords.forEach((o, inx) => {
            if (JSON.parse(o).properties.leafletId === leafletId) {
              updated_indexes.push({
                index: inx,
                coords: action.payload.areaLayerCoords
              });
            }
          });
          if (updated_indexes.length === 0) {
            draft.formFields.areaLayerCoords = [
              ...draft.formFields.areaLayerCoords,
              action.payload.areaLayerCoords
            ];
          } else if (updated_indexes.length > 0) {
            updated_indexes.forEach(o => {
              draft.formFields.areaLayerCoords[o.index] = o.coords;
            });
          }
        } else if (
          typeof action.payload.areaLayerCoords === 'object' &&
          action.payload.areaLayerCoords.length
        ) {
          draft.formFields.areaLayerCoords = action.payload.areaLayerCoords;
        }
      });
      return {
        ...state,
        mapArea: base
      };
    }
    case actiontype.DELETE_LAYERS: {
      const base = produce(state.mapArea, draft => {
        let indexes = [];
        draft.formFields.areaLayerCoords.forEach((o, inx) => {
          action.payload.forEach(i => {
            if (JSON.parse(o).properties.leafletId === i) {
              indexes.push(inx);
            }
          });
        });
        pullAt(draft.formFields.areaLayerCoords, indexes);
      });
      return {
        ...state,
        mapArea: base
      };
    }
    case actiontype.LOAD_MAP_AREA_FORM_FIELDS: {
      const base = produce(state.mapArea, draft => {
        draft.formFields[action.payload.field] = action.payload.value;
      });
      return {
        ...state,
        mapArea: base
      };
    }
    case actiontype.CLEAR_ON_EXITMAP: {
      return {
        ...state,
        mapArea: initialState.mapArea
      };
    }
    case actiontype.CREATE_MAP_AREA_MODE: {
      const base = produce(state.mapArea, draft => {
        draft.opMode = action.payload.opMode;
        draft.formFields = initialState.mapArea.formFields;
      });
      return {
        ...state,
        mapArea: base
      };
    }
    case actiontype.EDIT_MAP_AREA: {
      const base = produce(state.mapArea, draft => {
        draft.opMode = action.payload.opMode;
        draft.formFields = action.payload.formFields;
      });
      return {
        ...state,
        mapArea: base
      };
    }
    case actiontype.DELETE_AREA: {
      return {
        ...state,
        areasList: action.payload.data
      };
    }
    case actiontype.AREA_GRID_CHECKED: {
      const base = produce(state.agGrid, draft => {
        draft.checkedRows = action.payload.checkedRows;
      });
      return {
        ...state,
        agGrid: base
      };
    }
    case actiontype.AREA_SQUAREMETERS: {
      return {
        ...state,
        areasquaremeters:
          action.payload === ''
            ? ''
            : action.payload > state.areasquaremeters
            ? action.payload
            : state.areasquaremeters
      };
    }
    case actiontype.RESET_AREA: {
      return initialState;
    }
    default:
      return state;
  }
};

export default areaReducer;
