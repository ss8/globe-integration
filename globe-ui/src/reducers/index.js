import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage/session';
import { combineReducers } from 'redux';
import authReducer from './authReducers';
import navReducer from './navReducers';
import mapReducer from './mapReducers';
import areaReducer from './areaReducers';
import queryReducer from './queryReducers';
import modalReducer from './modalReducers';
import notifyReducer from './notifyReducers';
import analyzeReducer from './analyzeReducers';
import adminReducer from './adminReducers';

/**
 * persist store "auth"
 */
const persistAuthConfig = {
  key: 'auth',
  storage,
  whitelist: ['username', 'status', 'token', 'roles']
};

/**
 * persist store "maps"
 */
const persistMapsConfig = {
  key: 'maps',
  storage,
  whitelist: [
    'hasSearch',
    'hasDrawControl',
    'selectedMapName',
    'selectedPOI',
    'mapBaseLayer',
    'mapPosition',
    'overlayPOI'
  ]
};

/**
 * persist store "areasList"
 */
const persistAreaConfig = {
  key: 'areas',
  storage,
  whitelist: [
    'areasList',
    'mapArea',
    'homecountry',
    'timezone',
    'areaGridColState'
  ]
};

/**
 * persist store "queriesList"
 */
const persistQueryConfig = {
  key: 'queries',
  storage,
  whitelist: [
    'queriesList',
    'queryExecInfo',
    'queryExecLast',
    'queryExecFails',
    'queryGridColState'
  ]
};

/**
 * persist store "analyzeList"
 */
const persistAnalyzeConfig = {
  key: 'analyze',
  storage,
  whitelist: ['analyzeList', 'agGrid', 'analyzeListFilter', 'analyzeArea']
};

/**
 * persist store "userList"
 */
const persistUserConfig = {
  key: 'users',
  storage,
  whitelist: ['userList', 'userGridColState']
};

export default combineReducers({
  auth: persistReducer(persistAuthConfig, authReducer),
  nav: navReducer,
  maps: persistReducer(persistMapsConfig, mapReducer),
  areas: persistReducer(persistAreaConfig, areaReducer),
  queries: persistReducer(persistQueryConfig, queryReducer),
  analyze: persistReducer(persistAnalyzeConfig, analyzeReducer),
  users: persistReducer(persistUserConfig, adminReducer),
  modal: modalReducer,
  notify: notifyReducer
});
