import * as actiontype from '../actions/types';
/**
 * Authentication Initial State
 */
const initialState = {
  token: null,
  status: false,
  username: '',
  roles: []
};
/**
 *
 * @param {*} state
 * @param {*} action
 * Reducers for action types 'AUTH_LOGIN, AUTH_LOGOUT'
 */
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.AUTH_LOGIN: {
      return {
        ...state,
        status: action.payload.status,
        username: action.payload.username,
        token: action.payload.token,
        roles: action.payload.roles
      };
    }

    case actiontype.AUTH_LOGOUT: {
      return initialState;
    }

    default:
      return state;
  }
};

export default authReducer;
