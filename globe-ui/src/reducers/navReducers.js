import * as actiontype from '../actions/types';
/**
 * Navigation Initial State
 */
const initialState = {
  selectedmenu: []
};
/**
 *
 * @param {*} state
 * @param {*} action
 * Reducers for action types 'CURRENT_MENU'
 */
const navReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.CURRENT_MENU: {
      return {
        ...state,
        selectedmenu: action.payload.selectedmenu
      };
    }
    case actiontype.RESET_NAV: {
      return initialState;
    }
    default:
      return state;
  }
};

export default navReducer;
