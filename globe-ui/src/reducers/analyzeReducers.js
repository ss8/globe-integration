import * as actiontype from '../actions/types';
import produce from 'immer';
/**
 * Analyze Initial State
 */
const initialState = {
  analyzeList: [],
  analyseRealTimePool: { id: null, interval: 0, timer: 0, timerid: null },
  analyseRealTimePoolAll: {
    id: null,
    interval: 0,
    timer: 0,
    timerid: null,
    iteration: null
  },
  analyzeListFilter: [],
  analyzeArea: '',
  trackimsi: [],
  heatmapdata: [],
  playpause: 'play',
  isCreatePaneActive: false,
  agGrid: {
    columnDefs: [],
    checkedRows: [],
    contextItems: [],
    contextId: 'agAnalyze'
  },
  trackparams: {}
};

const analyzeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.LOAD_ANALYZELIST: {
      return {
        ...state,
        analyzeList: action.payload,
        analyzeListFilter: action.payload
      };
    }
    case actiontype.LOAD_ANALYZELISTFILTER: {
      return {
        ...state,
        analyzeListFilter: action.payload
      };
    }
    case actiontype.TRACK_PLAY_PAUSE: {
      return {
        ...state,
        playpause: state.playpause === 'play' ? 'pause' : 'play'
      };
    }
    case actiontype.REALTIME_POOL_ID: {
      return {
        ...state,
        analyseRealTimePool: {
          id:
            action.payload.id === null
              ? null
              : action.payload.id || state.analyseRealTimePool.id,
          interval:
            action.payload.interval || state.analyseRealTimePool.interval,
          timer: action.payload.timer || state.analyseRealTimePool.timer,
          timerid:
            action.payload.timerid === null
              ? null
              : action.payload.timerid || state.analyseRealTimePool.timerid
        }
      };
    }
    case actiontype.REALTIME_POOL_ALL_ID: {
      return {
        ...state,
        analyseRealTimePoolAll: {
          id:
            action.payload.id === null
              ? null
              : action.payload.id || state.analyseRealTimePoolAll.id,
          interval:
            action.payload.interval || state.analyseRealTimePoolAll.interval,
          timer: action.payload.timer || state.analyseRealTimePoolAll.timer,
          timerid:
            action.payload.timerid === null
              ? null
              : action.payload.timerid || state.analyseRealTimePoolAll.timerid,
          iteration:
            action.payload.iteration === null
              ? null
              : action.payload.iteration ||
                state.analyseRealTimePoolAll.iteration
        }
      };
    }
    case actiontype.ANALYZE_GRID_CHECKED: {
      const base = produce(state.agGrid, draft => {
        draft.checkedRows = action.payload.checkedRows;
      });
      return {
        ...state,
        agGrid: base
      };
    }
    case actiontype.ANALYZE_GRID_COLNAMES: {
      const base = produce(state.agGrid, draft => {
        draft.columnDefs = action.payload;
      });
      return {
        ...state,
        agGrid: base
      };
    }
    case actiontype.LOAD_ANALYZEAREA: {
      return {
        ...state,
        analyzeArea: action.payload
      };
    }
    case actiontype.LOAD_TRACKLIST: {
      return {
        ...state,
        trackimsi: action.payload
      };
    }
    case actiontype.LOAD_HEATMAPLIST: {
      return {
        ...state,
        heatmapdata: action.payload
      };
    }
    case actiontype.CLEAR_ANALYZEDATA: {
      return {
        ...state,
        trackimsi: [],
        heatmapdata: [],
        analyzeList: [],
        analyzeListFilter: [],
        analyzeArea: ''
      };
    }
    case actiontype.TRACK_PARAMS: {
      return {
        ...state,
        trackparams: action.payload
      };
    }
    case actiontype.RESET_ANALYSE: {
      return initialState;
    }
    default:
      return state;
  }
};

export default analyzeReducer;
