import * as actiontype from '../actions/types';
/**
 * Modal Initial State
 */
const initialState = {
  execLoading: false,
  modalTip: '',
  isOpen: false,
  modalProps: {},
  modalType: null
};
/**
 *
 * @param {*} state
 * @param {*} action
 * Reducers for action types 'SHOW_MODAL, HIDE_MODAL'
 */
const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.SHOW_MODAL: {
      return {
        ...state,
        isOpen: action.payload.isOpen,
        modalProps: action.payload.modalProps,
        modalType: action.payload.modalType
      };
    }
    case actiontype.HIDE_MODAL: {
      return initialState;
    }
    case actiontype.EXEC_SPINNING: {
      return {
        ...state,
        execLoading: action.payload.bool,
        modalLoadingTip: action.payload.tip
      };
    }
    case actiontype.RESET_MODAL: {
      return initialState;
    }
    default:
      return state;
  }
};

export default modalReducer;
