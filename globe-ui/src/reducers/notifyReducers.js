import * as actiontype from '../actions/types';
/**
 * Notification Initial State
 */
const initialState = {
  isNotifyOpen: false,
  notifyProps: {},
  notifyType: null
};
/**
 *
 * @param {*} state
 * @param {*} action
 * Reducers for action types 'SHOW_NOTIFICATION, HIDE_NOTIFICATION'
 */
const notifyReducer = (state = initialState, action) => {
  switch (action.type) {
    case actiontype.SHOW_NOTIFICATION: {
      return {
        ...state,
        isNotifyOpen: true,
        notifyProps: action.payload.notifyProps,
        notifyType: action.payload.notifyType
      };
    }
    case actiontype.HIDE_NOTIFICATION: {
      return initialState;
    }
    case actiontype.RESET_NOTIFICATION: {
      return initialState;
    }
    default:
      return state;
  }
};

export default notifyReducer;
