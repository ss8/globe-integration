const countrylist = [
  {
    countrycode: 'af',
    countryname: 'Afghanistan'
  },
  {
    countrycode: 'al',
    countryname: 'Albania'
  },
  {
    countrycode: 'dz',
    countryname: 'Algeria'
  },
  {
    countrycode: 'as',
    countryname: 'American Samoa'
  },
  {
    countrycode: 'ad',
    countryname: 'Andorra'
  },
  {
    countrycode: 'ao',
    countryname: 'Angola'
  },
  {
    countrycode: 'ai',
    countryname: 'Anguilla'
  },
  {
    countrycode: 'ag',
    countryname: 'Antigua and Barbuda'
  },
  {
    countrycode: 'ar',
    countryname: 'Argentina Republic'
  },
  {
    countrycode: 'am',
    countryname: 'Armenia'
  },
  {
    countrycode: 'aw',
    countryname: 'Aruba'
  },
  {
    countrycode: 'au',
    countryname: 'Australia'
  },
  {
    countrycode: 'at',
    countryname: 'Austria'
  },
  {
    countrycode: 'az',
    countryname: 'Azerbaijan'
  },
  {
    countrycode: 'bs',
    countryname: 'Bahamas'
  },
  {
    countrycode: 'bh',
    countryname: 'Bahrain'
  },
  {
    countrycode: 'bd',
    countryname: 'Bangladesh'
  },
  {
    countrycode: 'bb',
    countryname: 'Barbados'
  },
  {
    countrycode: 'by',
    countryname: 'Belarus'
  },
  {
    countrycode: 'be',
    countryname: 'Belgium'
  },
  {
    countrycode: 'bz',
    countryname: 'Belize'
  },
  {
    countrycode: 'bj',
    countryname: 'Benin'
  },
  {
    countrycode: 'bm',
    countryname: 'Bermuda'
  },
  {
    countrycode: 'bt',
    countryname: 'Bhutan'
  },
  {
    countrycode: 'bo',
    countryname: 'Bolivia'
  },
  {
    countrycode: 'ba',
    countryname: 'Bosnia & Herzegov.'
  },
  {
    countrycode: 'bw',
    countryname: 'Botswana'
  },
  {
    countrycode: 'br',
    countryname: 'Brazil'
  },
  {
    countrycode: 'vg',
    countryname: 'British Virgin Islands'
  },
  {
    countrycode: 'bn',
    countryname: 'Brunei Darussalam'
  },
  {
    countrycode: 'bg',
    countryname: 'Bulgaria'
  },
  {
    countrycode: 'bf',
    countryname: 'Burkina Faso'
  },
  {
    countrycode: 'bi',
    countryname: 'Burundi'
  },
  {
    countrycode: 'kh',
    countryname: 'Cambodia'
  },
  {
    countrycode: 'cm',
    countryname: 'Cameroon'
  },
  {
    countrycode: 'ca',
    countryname: 'Canada'
  },
  {
    countrycode: 'cv',
    countryname: 'Cape Verde'
  },
  {
    countrycode: 'ky',
    countryname: 'Cayman Islands'
  },
  {
    countrycode: 'cf',
    countryname: 'Central African Rep.'
  },
  {
    countrycode: 'td',
    countryname: 'Chad'
  },
  {
    countrycode: 'cl',
    countryname: 'Chile'
  },
  {
    countrycode: 'cn',
    countryname: 'China'
  },
  {
    countrycode: 'co',
    countryname: 'Colombia'
  },
  {
    countrycode: 'km',
    countryname: 'Comoros'
  },
  {
    countrycode: 'cd',
    countryname: 'Congo'
  },
  {
    countrycode: 'cg',
    countryname: 'Congo'
  },
  {
    countrycode: 'ck',
    countryname: 'Cook Islands'
  },
  {
    countrycode: 'cr',
    countryname: 'Costa Rica'
  },
  {
    countrycode: 'hr',
    countryname: 'Croatia'
  },
  {
    countrycode: 'cu',
    countryname: 'Cuba'
  },
  {
    countrycode: 'cw',
    countryname: 'Curacao'
  },
  {
    countrycode: 'cy',
    countryname: 'Cyprus'
  },
  {
    countrycode: 'cz',
    countryname: 'Czech Rep.'
  },
  {
    countrycode: 'dk',
    countryname: 'Denmark'
  },
  {
    countrycode: 'dj',
    countryname: 'Djibouti'
  },
  {
    countrycode: 'dm',
    countryname: 'Dominica'
  },
  {
    countrycode: 'do',
    countryname: 'Dominican Republic'
  },
  {
    countrycode: 'ec',
    countryname: 'Ecuador'
  },
  {
    countrycode: 'eg',
    countryname: 'Egypt'
  },
  {
    countrycode: 'sv',
    countryname: 'El Salvador'
  },
  {
    countrycode: 'gq',
    countryname: 'Equatorial Guinea'
  },
  {
    countrycode: 'er',
    countryname: 'Eritrea'
  },
  {
    countrycode: 'ee',
    countryname: 'Estonia'
  },
  {
    countrycode: 'et',
    countryname: 'Ethiopia'
  },
  {
    countrycode: 'fk',
    countryname: 'Falkland Islands (Malvinas)'
  },
  {
    countrycode: 'fo',
    countryname: 'Faroe Islands'
  },
  {
    countrycode: 'fj',
    countryname: 'Fiji'
  },
  {
    countrycode: 'fi',
    countryname: 'Finland'
  },
  {
    countrycode: 'fr',
    countryname: 'France'
  },
  {
    countrycode: 'fg',
    countryname: 'French Guiana'
  },
  {
    countrycode: 'pf',
    countryname: 'French Polynesia'
  },
  {
    countrycode: 'ga',
    countryname: 'Gabon'
  },
  {
    countrycode: 'gm',
    countryname: 'Gambia'
  },
  {
    countrycode: 'ge',
    countryname: 'Georgia'
  },
  {
    countrycode: 'de',
    countryname: 'Germany'
  },
  {
    countrycode: 'gh',
    countryname: 'Ghana'
  },
  {
    countrycode: 'gi',
    countryname: 'Gibraltar'
  },
  {
    countrycode: 'gr',
    countryname: 'Greece'
  },
  {
    countrycode: 'gl',
    countryname: 'Greenland'
  },
  {
    countrycode: 'gd',
    countryname: 'Grenada'
  },
  {
    countrycode: 'gp',
    countryname: 'Guadeloupe '
  },
  {
    countrycode: 'gu',
    countryname: 'Guam'
  },
  {
    countrycode: 'gt',
    countryname: 'Guatemala'
  },
  {
    countrycode: 'gn',
    countryname: 'Guinea'
  },
  {
    countrycode: 'gw',
    countryname: 'Guinea-Bissau'
  },
  {
    countrycode: 'gy',
    countryname: 'Guyana'
  },
  {
    countrycode: 'ht',
    countryname: 'Haiti'
  },
  {
    countrycode: 'hn',
    countryname: 'Honduras'
  },
  {
    countrycode: 'hk',
    countryname: 'Hongkong'
  },
  {
    countrycode: 'hu',
    countryname: 'Hungary'
  },
  {
    countrycode: 'is',
    countryname: 'Iceland'
  },
  {
    countrycode: 'in',
    countryname: 'India'
  },
  {
    countrycode: 'id',
    countryname: 'Indonesia'
  },

  {
    countrycode: 'ir',
    countryname: 'Iran '
  },
  {
    countrycode: 'iq',
    countryname: 'Iraq'
  },
  {
    countrycode: 'ie',
    countryname: 'Ireland'
  },
  {
    countrycode: 'il',
    countryname: 'Israel'
  },
  {
    countrycode: 'it',
    countryname: 'Italy'
  },
  {
    countrycode: 'ci',
    countryname: 'Ivory Coast'
  },
  {
    countrycode: 'jm',
    countryname: 'Jamaica'
  },
  {
    countrycode: 'jp',
    countryname: 'Japan'
  },
  {
    countrycode: 'jo',
    countryname: 'Jordan'
  },
  {
    countrycode: 'kz',
    countryname: 'Kazakhstan'
  },
  {
    countrycode: 'ke',
    countryname: 'Kenya'
  },
  {
    countrycode: 'ki',
    countryname: 'Kiribati'
  },
  {
    countrycode: 'kp',
    countryname: 'Korea N.'
  },
  {
    countrycode: 'kr',
    countryname: 'Korea S'
  },
  {
    countrycode: 'kw',
    countryname: 'Kuwait'
  },
  {
    countrycode: 'kg',
    countryname: 'Kyrgyzstan'
  },
  {
    countrycode: 'la',
    countryname: 'Laos P.D.R.'
  },
  {
    countrycode: 'lv',
    countryname: 'Latvia'
  },
  {
    countrycode: 'lb',
    countryname: 'Lebanon'
  },
  {
    countrycode: 'ls',
    countryname: 'Lesotho'
  },
  {
    countrycode: 'lr',
    countryname: 'Liberia'
  },
  {
    countrycode: 'ly',
    countryname: 'Libya'
  },
  {
    countrycode: 'li',
    countryname: 'Liechtenstein'
  },
  {
    countrycode: 'lt',
    countryname: 'Lithuania'
  },
  {
    countrycode: 'lu',
    countryname: 'Luxembourg'
  },
  {
    countrycode: 'mo',
    countryname: 'Macao'
  },
  {
    countrycode: 'mk',
    countryname: 'Macedonia'
  },
  {
    countrycode: 'mg',
    countryname: 'Madagascar'
  },
  {
    countrycode: 'mw',
    countryname: 'Malawi'
  },
  {
    countrycode: 'my',
    countryname: 'Malaysia'
  },
  {
    countrycode: 'mv',
    countryname: 'Maldives'
  },
  {
    countrycode: 'ml',
    countryname: 'Mali'
  },
  {
    countrycode: 'mt',
    countryname: 'Malta'
  },
  {
    countrycode: 'mq',
    countryname: 'Martinique (French Department of)'
  },
  {
    countrycode: 'mr',
    countryname: 'Mauritania'
  },
  {
    countrycode: 'mu',
    countryname: 'Mauritius'
  },
  {
    countrycode: 'mx',
    countryname: 'Mexico'
  },
  {
    countrycode: 'fm',
    countryname: 'Micronesia'
  },
  {
    countrycode: 'md',
    countryname: 'Moldova'
  },
  {
    countrycode: 'mc',
    countryname: 'Monaco'
  },
  {
    countrycode: 'mn',
    countryname: 'Mongolia'
  },
  {
    countrycode: 'me',
    countryname: 'Montenegro'
  },
  {
    countrycode: 'ms',
    countryname: 'Montserrat'
  },
  {
    countrycode: 'ma',
    countryname: 'Morocco'
  },
  {
    countrycode: 'mz',
    countryname: 'Mozambique'
  },
  {
    countrycode: 'mm',
    countryname: 'Myanmar (Burma)'
  },
  {
    countrycode: 'na',
    countryname: 'Namibia'
  },
  {
    countrycode: 'np',
    countryname: 'Nepal'
  },
  {
    countrycode: 'nl',
    countryname: 'Netherlands'
  },
  {
    countrycode: 'an',
    countryname: 'Netherlands Antilles'
  },
  {
    countrycode: 'nc',
    countryname: 'New Caledonia'
  },
  {
    countrycode: 'nz',
    countryname: 'New Zealand'
  },
  {
    countrycode: 'ni',
    countryname: 'Nicaragua'
  },
  {
    countrycode: 'ne',
    countryname: 'Niger'
  },
  {
    countrycode: 'ng',
    countryname: 'Nigeria'
  },
  {
    countrycode: 'nu',
    countryname: 'Niue'
  },
  {
    countrycode: 'no',
    countryname: 'Norway'
  },
  {
    countrycode: 'om',
    countryname: 'Oman'
  },
  {
    countrycode: 'pk',
    countryname: 'Pakistan'
  },
  {
    countrycode: 'pw',
    countryname: 'Palau (Republic of)'
  },
  {
    countrycode: 'ps',
    countryname: 'Palestinian Territory'
  },
  {
    countrycode: 'pa',
    countryname: 'Panama'
  },
  {
    countrycode: 'pg',
    countryname: 'Papua New Guinea'
  },
  {
    countrycode: 'py',
    countryname: 'Paraguay'
  },
  {
    countrycode: 'pe',
    countryname: 'Peru'
  },
  {
    countrycode: 'ph',
    countryname: 'Philippines'
  },
  {
    countrycode: 'pl',
    countryname: 'Poland'
  },
  {
    countrycode: 'pt',
    countryname: 'Portugal'
  },
  {
    countrycode: 'pr',
    countryname: 'Puerto Rico'
  },
  {
    countrycode: 'qa',
    countryname: 'Qatar'
  },
  {
    countrycode: 're',
    countryname: 'Reunion'
  },
  {
    countrycode: 'ro',
    countryname: 'Romania'
  },
  {
    countrycode: 'ru',
    countryname: 'Russian Federation'
  },
  {
    countrycode: 'rw',
    countryname: 'Rwanda'
  },
  {
    countrycode: 'kn',
    countryname: 'Saint Kitts and Nevis'
  },
  {
    countrycode: 'lc',
    countryname: 'Saint Lucia'
  },
  {
    countrycode: 'ws',
    countryname: 'Samoa'
  },
  {
    countrycode: 'sm',
    countryname: 'San Marino'
  },
  {
    countrycode: 'st',
    countryname: 'Sao Tome & Principe'
  },
  {
    countrycode: 'sa',
    countryname: 'Saudi Arabia'
  },
  {
    countrycode: 'sn',
    countryname: 'Senegal'
  },
  {
    countrycode: 'rs',
    countryname: 'Serbia '
  },
  {
    countrycode: 'sc',
    countryname: 'Seychelles'
  },
  {
    countrycode: 'sl',
    countryname: 'Sierra Leone'
  },
  {
    countrycode: 'sg',
    countryname: 'Singapore'
  },
  {
    countrycode: 'sk',
    countryname: 'Slovakia'
  },
  {
    countrycode: 'si',
    countryname: 'Slovenia'
  },
  {
    countrycode: 'sb',
    countryname: 'Solomon Islands'
  },
  {
    countrycode: 'so',
    countryname: 'Somalia'
  },
  {
    countrycode: 'za',
    countryname: 'South Africa'
  },
  {
    countrycode: 'ss',
    countryname: 'South Sudan (Republic of)'
  },
  {
    countrycode: 'es',
    countryname: 'Spain'
  },
  {
    countrycode: 'lk',
    countryname: 'Sri Lanka'
  },
  {
    countrycode: 'pm',
    countryname: 'St. Pierre & Miquelon'
  },
  {
    countrycode: 'vc',
    countryname: 'St. Vincent & Gren.'
  },
  {
    countrycode: 'sd',
    countryname: 'Sudan'
  },
  {
    countrycode: 'sr',
    countryname: 'Suriname'
  },
  {
    countrycode: 'sz',
    countryname: 'Swaziland'
  },
  {
    countrycode: 'se',
    countryname: 'Sweden'
  },
  {
    countrycode: 'ch',
    countryname: 'Switzerland'
  },
  {
    countrycode: 'sy',
    countryname: 'Syrian Arab Republic'
  },
  {
    countrycode: 'tw',
    countryname: 'Taiwan'
  },
  {
    countrycode: 'tk',
    countryname: 'Tajikistan'
  },
  {
    countrycode: 'tz',
    countryname: 'Tanzania'
  },
  {
    countrycode: 'th',
    countryname: 'Thailand'
  },
  {
    countrycode: 'tp',
    countryname: 'Timor-Leste'
  },
  {
    countrycode: 'tg',
    countryname: 'Togo'
  },
  {
    countrycode: 'to',
    countryname: 'Tonga'
  },
  {
    countrycode: 'tt',
    countryname: 'Trinidad and Tobago'
  },
  {
    countrycode: 'tn',
    countryname: 'Tunisia'
  },
  {
    countrycode: 'tr',
    countryname: 'Turkey'
  },
  {
    countrycode: 'tm',
    countryname: 'Turkmenistan'
  },
  {
    countrycode: 'tc',
    countryname: 'Turks and Caicos Islands'
  },
  {
    countrycode: 'tv',
    countryname: 'Tuvalu'
  },
  {
    countrycode: 'ug',
    countryname: 'Uganda'
  },
  {
    countrycode: 'ua',
    countryname: 'Ukraine'
  },
  {
    countrycode: 'ae',
    countryname: 'United Arab Emirates'
  },
  {
    countrycode: 'gb',
    countryname: 'United Kingdom'
  },
  {
    countrycode: 'us',
    countryname: 'United States'
  },
  {
    countrycode: 'uy',
    countryname: 'Uruguay'
  },
  {
    countrycode: 'uz',
    countryname: 'Uzbekistan'
  },
  {
    countrycode: 'vu',
    countryname: 'Vanuatu'
  },
  {
    countrycode: 've',
    countryname: 'Venezuela'
  },
  {
    countrycode: 'vn',
    countryname: 'Viet Nam'
  },
  {
    countrycode: 'vi',
    countryname: 'Virgin Islands'
  },
  {
    countrycode: 'ye',
    countryname: 'Yemen'
  },
  {
    countrycode: 'zm',
    countryname: 'Zambia'
  },
  {
    countrycode: 'zw',
    countryname: 'Zimbabwe'
  }
];
export default countrylist;
