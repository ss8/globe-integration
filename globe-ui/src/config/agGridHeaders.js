import store from '../store/store';
import moment from 'moment-timezone';
import { getQueryNameBytype, isJsonString } from '../utils/uitility';

/**
 * ag-grid Column Definitions
 */
// Areas List
export const agAreasListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: params => {
      let querycount = 0;
      if (isJsonString(params.data.geometry)) {
        querycount = JSON.parse(params.data.geometry)[0].properties.querycount;
      }
      return querycount === undefined
        ? true
        : querycount && querycount > 0
        ? false
        : true;
    },
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    width: 30,
    cellRenderer: params => {
      let querycount = 0;
      if (isJsonString(params.data.geometry)) {
        querycount = JSON.parse(params.data.geometry)[0].properties.querycount;
      }
      if (querycount !== undefined && querycount > 0) {
        return `<span class="area-del-disabled" title="Area(s) associated with active Queries. Locked from Deleting..."><svg viewBox="0 0 240 349" xmlns="http://www.w3.org/2000/svg">
        <path d="m57.309 28.614c12.105 0 19.901 12.699 20.064 28.446h-40.128c0.16259-15.747 7.9589-28.446 20.064-28.446zm0-15.723c-18.861 0-34.145 19.958-34.145 44.575l0.00813 0.61785c-6.4143 2.3495-10.983 8.4874-10.983 15.69v39.209c0 17.454 20.202 31.608 45.12 31.608s45.12-14.154 45.12-31.608v-39.209c0-7.2029-4.5689-13.341-10.983-15.69l0.00813-0.61785c0-24.617-15.284-44.575-34.145-44.575zm-12.804 81.248c0-7.0484 5.7314-12.764 12.804-12.764s12.804 5.7152 12.804 12.764c0 4.512-2.3495 8.4792-5.894 10.747v11.747h-13.82v-11.747c-3.5445-2.2682-5.894-6.2354-5.894-10.747z" fill="#242424" fill-rule="evenodd"/>
       </svg></span>`;
      }
    }
  },
  {
    headerName: 'Action',
    field: 'action',
    width: 70,
    resizable: false
  },
  {
    headerName: 'Id',
    field: 'areaId',
    width: 100,
    sortable: true,
    resizable: true
  },
  {
    headerName: 'Area Name',
    field: 'area_name',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipField: 'area_name'
  },
  {
    headerName: 'Description',
    field: 'description',
    width: 865,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipField: 'description'
  }
];

// Queries List
export const agQueriesListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: params => {
      return params.data.status === -1 ? false : true;
    },
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    width: 36,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    },
    cellRenderer: params => {
      if (params.data.status === -1) {
        return `<span class="exec-abort" title="Abort Execution"><svg viewBox="0 0 96 96" class="abortico">
        <defs>
         <radialGradient id="b" cx="15.891" cy="15.163" r="27.545" gradientUnits="userSpaceOnUse">
          <stop stop-color="#fff" offset="0"/>
          <stop stop-color="#00f038" stop-opacity="0" offset="1"/>
         </radialGradient>
         <radialGradient id="a" cx="16.028" cy="16.847" r="27.545" gradientTransform="matrix(.91888 .94532 -.71707 .69701 13.381 -10.047)" gradientUnits="userSpaceOnUse">
          <stop stop-color="#ff9b9b" offset="0"/>
          <stop stop-color="#f00000" offset="1"/>
         </radialGradient>
        </defs>
        <g transform="translate(-59.5 -.5)">
         <path transform="matrix(.99849 0 0 .99849 63.996 4.8147)" d="m55.091 27.727a27.545 27.545 0 1 1 -55.091 0 27.545 27.545 0 1 1 55.091 0z" fill="url(#b)"/>
         <path transform="matrix(.99849 0 0 .99849 63.996 4.8147)" d="m55.091 27.727a27.545 27.545 0 1 1 -55.091 0 27.545 27.545 0 1 1 55.091 0z" fill="url(#a)" stroke="#be0000" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5038"/>
         <path d="m95.594 19.312c-1.798 1.5509-3.1446 4.3692-4.5312 5.8438-2.6315-2.177-3.8188-6.2358-6.6562-7.375-3.4406 0.91828-8.9042 4.1354-4.875 7.625 2.0576 3.0423 6.1945 5.7464 7.1562 8.9062-1.4673 3.7894-4.291 6.615-7.0312 9.375 1.3399 4.6754 7.7548 0.52478 10.344-1.75 2.0648-4.6338 3.5683 0.89824 5.4994 2.891 1.5405 6.2227 9.8042 4.4034 10.522-0.73944-2.8396-4.5489-6.7818-8.9244-9.521-13.402 1.0851-4.6542 5.6334-7.7704 6.6875-12.281-1.7893-3.1223-5.2813 0.35232-7.5938 0.90625z" fill="#fff"/>
        </g>
       </svg></span>`;
      }
    }
  },
  {
    headerName: 'Action',
    field: 'action',
    width: 66,
    resizable: false,
    cellRenderer: params => {
      let qid = params.data.queryId;
      let currstate = store.getState();
      if (
        currstate.queries.queryExecInProgress.length &&
        currstate.queries.queryExecInProgress.findIndex(id => id === qid) > -1
      ) {
        return `<div class="loadingwrap"><svg viewBox="64 64 896 896" focusable="false" class="anticon-spin" data-icon="sync" width="1.5em" height="1.5em" fill="#f96921" aria-hidden="true"><path d="M168 504.2c1-43.7 10-86.1 26.9-126 17.3-41 42.1-77.7 73.7-109.4S337 212.3 378 195c42.4-17.9 87.4-27 133.9-27s91.5 9.1 133.8 27A341.5 341.5 0 0 1 755 268.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 0 0 3 14.1l175.7 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c0-6.7-7.7-10.5-12.9-6.3l-56.4 44.1C765.8 155.1 646.2 92 511.8 92 282.7 92 96.3 275.6 92 503.8a8 8 0 0 0 8 8.2h60c4.4 0 7.9-3.5 8-7.8zm756 7.8h-60c-4.4 0-7.9 3.5-8 7.8-1 43.7-10 86.1-26.9 126-17.3 41-42.1 77.8-73.7 109.4A342.45 342.45 0 0 1 512.1 856a342.24 342.24 0 0 1-243.2-100.8c-9.9-9.9-19.2-20.4-27.8-31.4l60.2-47a8 8 0 0 0-3-14.1l-175.7-43c-5-1.2-9.9 2.6-9.9 7.7l-.7 181c0 6.7 7.7 10.5 12.9 6.3l56.4-44.1C258.2 868.9 377.8 932 512.2 932c229.2 0 415.5-183.7 419.8-411.8a8 8 0 0 0-8-8.2z"></path></svg></div>`;
      }
      return `<span class="exec-grid"></span>`;
    },
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Id',
    field: 'queryId',
    width: 60,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Type',
    field: 'type',
    width: 110,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    filterValueGetter: params => {
      return getQueryNameBytype(params.data.type);
    },
    cellRenderer: params => {
      return getQueryNameBytype(params.value);
    },
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Name',
    field: 'queryName',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Description',
    field: 'queryDescription',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Query',
    field: 'criteria',
    width: 260,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipField: 'criteria',
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Status',
    field: 'status',
    width: 180,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      return params.valueFormatted || params.value;
    },
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    },
    filterValueGetter: params => {
      let qid = params.data.queryId;
      if (params.data.status === '') {
        return null;
      } else if (!params.data.status) {
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `${txt}`;
      } else if (params.data.status === -1) {
        return `Execution In Progress...`;
      } else if (params.data.status === -2) {
        let currstate = store.getState();
        let msg = '';
        if (currstate.queries.queryExecFails.length) {
          msg = currstate.queries.queryExecFails.find(el => el.queryId === qid)
            .message;
        }
        if (msg === 'Aborted Execution!') {
          return `${msg}`;
        }
        return `Error! ${msg}`;
      } else if (params.data.status > 0) {
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `${txt}`;
      }
    },
    valueFormatter: params => {
      let qid = params.data.queryId;
      if (params.value === '') {
        return null;
      } else if (!params.value) {
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `${txt}`;
      } else if (params.value === -1) {
        return `Execution In Progress...`;
      } else if (params.value === -2) {
        let currstate = store.getState();
        let msg = '';
        if (currstate.queries.queryExecFails.length) {
          msg = currstate.queries.queryExecFails.find(el => el.queryId === qid)
            .message;
        }
        if (msg === 'Aborted Execution!') {
          return `${msg}`;
        }
        return `Error! ${msg}`;
      } else if (params.value > 0) {
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `${txt}`;
      }
    },
    cellRenderer: params => {
      let qid = params.data.queryId;

      if (params.value === '') {
        // Not yet executed!
        return null;
      } else if (!params.value) {
        // 0 No Results
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `${txt}`;
      } else if (params.value === -1) {
        return `<span class="exec-inprogess"></span> Execution In Progress...`;
      } else if (params.value === -2) {
        let currstate = store.getState();
        let msg = '';
        if (currstate.queries.queryExecFails.length) {
          msg = currstate.queries.queryExecFails.find(el => el.queryId === qid)
            .message;
        }
        if (msg === 'Aborted Execution!') {
          return `<span class="exec-err"></span> ${msg}`;
        }
        return `<span class="exec-err"></span> Error! ${msg}`;
      } else if (params.value > 0) {
        // +ve results
        let currstate = store.getState();
        let txt = '';
        if (currstate.queries.queryExecInfo.length) {
          txt = currstate.queries.queryExecInfo.find(el => el.queryId === qid)
            .execStatus;
        }
        return `<span class="exec-tick"></span> ${txt}`;
      }
    }
  },
  {
    headerName: 'Query Owner',
    field: 'queryOwner',
    width: 90,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    },
    filterValueGetter: params => {
      return `${params.data.queryOwner}`;
    },
    valueFormatter: params => {
      return `${params.value}`;
    },
    cellRenderer: params => {
      let qacc = params.data.publicQueryAccess;
      if (!qacc || qacc === 'false') {
        return `${params.value}`;
      } else if (qacc || qacc === 'true') {
        return `<span class="public-access" title="Public access"></span> ${params.value}`;
      }
    }
  },
  {
    headerName: 'Modified By',
    field: 'modifiedBy',
    width: 80,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Created Date',
    field: 'createDate',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.valueFormatted} (${currstate.areas.timezone})`;
    },
    filterValueGetter: params => {
      let currstate = store.getState();
      return moment
        .tz(params.data.createDate, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    valueFormatter: params => {
      let currstate = store.getState();
      return moment
        .tz(params.value, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    cellRenderer: params => {
      let currstate = store.getState();
      return moment
        .tz(params.value, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  },
  {
    headerName: 'Updated Date',
    field: 'updateDate',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.valueFormatted} (${currstate.areas.timezone})`;
    },
    filterValueGetter: params => {
      let currstate = store.getState();
      return moment
        .tz(params.data.updateDate, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    valueFormatter: params => {
      let currstate = store.getState();
      return moment
        .tz(params.value, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    cellRenderer: params => {
      let currstate = store.getState();
      return moment
        .tz(params.value, currstate.areas.timezone)
        .format('YYYY-MM-DD HH:mm:ss z');
    },
    cellStyle: params => {
      if (params.data.status > 0) {
        let currstate = store.getState();
        if (currstate.queries.queryExecLast === params.data.queryId) {
          return { 'background-color': '#bdeeac' };
        }
      }
    }
  }
];
