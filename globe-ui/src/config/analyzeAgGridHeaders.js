import store from '../store/store';

// Intersection and Simple
export const agListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 30
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Area Name',
    field: 'areaName',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  }
];
// Simple Geo Fence
export const agSgfListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 50
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  }
];

// RoamerDetection
export const agRdListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 30
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Area Label',
    field: 'arealabel',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Country',
    field: 'country',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];

// SimSwap
export const agSSListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 30
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 250,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];

// Simple Realtime
export const agSRListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 30
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 300,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];

// Meeting
export const agMeetingListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 30
  },
  {
    headerName: 'Meeting ID',
    field: 'meetingId',
    width: 120,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 160,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 170,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Segment Duration',
    field: 'segmentDuration',
    width: 160,
    sortable: true,
    resizable: true
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 110,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 110,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];
// Travel Detection
export const agTDListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 50
  },
  {
    headerName: 'ID',
    field: 'id',
    width: 50,
    sortable: true,
    resizable: true
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 230,
    sortable: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Segment Duration',
    field: 'segmentDuration',
    width: 190,
    sortable: true,
    resizable: true
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 150,
    sortable: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 150,
    sortable: true,
    resizable: true
  }
];
// Simple History
export const agSHListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    width: 50
  },
  {
    headerName: 'IMSI',
    field: 'imsi',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'IMEI',
    field: 'imei',
    width: 200,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'MSISDN',
    field: 'msisdn',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'LIID',
    field: 'liid',
    width: 100,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Segment Start Time',
    field: 'segmentStartTime',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    tooltipValueGetter: function(params) {
      let currstate = store.getState();
      return `${params.value} (${currstate.areas.timezone})`;
    }
  },
  {
    headerName: 'Latitude',
    field: 'latitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Longitude',
    field: 'longitude',
    width: 150,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];
