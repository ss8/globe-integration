/**
 * Query Ruleset
 */

export const Ruleset = {
  /**
   * SimpleGeoFence Rules
   */
  simplegeofence: {
    rulesform: [[], []], // Max 2 Rules
    maxrules: 2,
    rulescount: 0,
    repeatrules: false,
    rules: [
      {
        type: 'Area',
        conjuction: '',
        subrules: [
          {
            subcontrol: 'list',
            placeholder: 'Select Area',
            value: [], //Areas list
            selectedValue: null
          },
          {
            subcontrol: 'startdatetime',
            placeholder: 'Select Start DateTime',
            value: '',
            pretext: 'between'
          },
          {
            subcontrol: 'enddatetime',
            placeholder: 'Select End DateTime',
            value: '',
            pretext: 'and'
          }
        ]
      },
      {
        type: 'MSIDType',
        conjuction: '',
        subrules: [
          {
            subcontrol: 'list',
            placeholder: 'Select MSIDType',
            value: ['IMSI', 'IMEI', 'MSISDN'],
            selectedValue: null
          },
          {
            subcontrol: 'list',
            placeholder: 'Select Operator',
            value: ['Contains', 'Starts With'],
            selectedValue: null
          },
          {
            subcontrol: 'textarea',
            placeholder: '', //Dynamic based on MSIDType
            value: ''
          }
        ]
      }
    ]
  }
};
