/**
 * ag-grid Column Definitions
 */
// USER List
export const agUserListColDefs = [
  {
    headerName: '',
    field: 'selected',
    checkboxSelection: true,
    headerCheckboxSelection: true,
    headerCheckboxSelectionFilteredOnly: true,
    width: 30
  },
  {
    headerName: 'First Name (Username)',
    field: 'firstName',
    width: 220,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Last Name',
    field: 'lastName',
    width: 220,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Password',
    field: 'passWord',
    width: 220,
    resizable: true,
    valueFormatter: function() {
      return '********';
    }
  },
  {
    headerName: 'User Role',
    field: 'roleDtos',
    width: 230,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true,
    valueFormatter: function(o) {
      if (o.value.length) {
        return o.value.map(items => items.description);
      } else {
        return '';
      }
    }
  }
];

export const agAppStatusListColDefs = [
  {
    headerName: 'ID',
    field: 'id',
    width: 80,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'App Name',
    field: 'appname',
    width: 220,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Last Contacted',
    field: 'lastcontacted',
    width: 220,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  },
  {
    headerName: 'Last Restarted',
    field: 'lastrestarted',
    width: 220,
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: { newRowsAction: 'keep' },
    suppressMenu: true,
    resizable: true
  }
];
