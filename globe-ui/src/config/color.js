import randomColor from 'randomcolor';
/**
 * !!! Don't modify this colors - impact will be on travel detection marker points ...
 */

export const color = [
  '#e49614',
  '#5086b7',
  '#2c8c14',
  '#607c83',
  '#0a4308',
  '#466277',
  '#60a363',
  '#aca730',
  '#4f563b',
  '#985d4e',
  '#d23be7',
  '#331e15',
  '#5086b7'
];

const hue = ['green', 'blue', 'red', 'yellow', 'purple', 'orange', 'pink'];

export const colorGen = count => {
  if (!count) {
    return null;
  }
  let color = [];
  let opcolor = [];
  let tempcolor;
  let i = 0;
  for (i; i < count; i++) {
    tempcolor = randomColor({
      count: 1,
      luminosity: 'dark',
      hue: hue[i % 7],
      format: 'rgba',
      alpha: 0.6
    });
    opcolor.push(tempcolor[0]);
    color.push(tempcolor[0].replace(/0.6/, '1'));
  }
  return {
    color: color,
    opcolor: opcolor
  };
};
