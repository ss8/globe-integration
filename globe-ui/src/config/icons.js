import L from 'leaflet';
export const startIcon = new L.Icon({
  iconUrl: require('../images/start.png'),
  iconRetinaUrl: require('../images/start.png'),
  iconAnchor: [12, 41],
  popupAnchor: [0, -41],
  iconSize: [25, 41],
  shadowUrl: require('../images/start.png')
});
export const stopIcon = new L.Icon({
  iconUrl: require('../images/stop.png'),
  iconRetinaUrl: require('../images/stop.png'),
  iconAnchor: [12, 41],
  popupAnchor: [0, -41],
  iconSize: [25, 41],
  shadowUrl: require('../images/stop.png')
});
export const meetingIcon = new L.Icon({
  iconUrl: require('../images/yellow.png'),
  iconRetinaUrl: require('../images/yellow.png'),
  iconAnchor: [12, 41],
  popupAnchor: [0, -41],
  iconSize: [25, 41],
  shadowUrl: require('../images/yellow.png')
});
export const towerIcon = new L.Icon({
  iconUrl: require('../images/tower.png'),
  iconRetinaUrl: require('../images/tower.png'),
  iconAnchor: [12, 41],
  popupAnchor: [0, -41],
  iconSize: [25, 41],
  shadowUrl: require('../images/tower.png')
});
export const towerIconnew = L.divIcon({ className: 'tower-icon' });

export const testIcon = L.divIcon({
  className: 'custom-div-icon',
  html:
    "<div style='background-color:#c30b82;' class='marker-pin'></div><i class='material-icons'>weekend</i>",
  iconSize: [30, 42],
  iconAnchor: [15, 42]
});
