const globalconfig = {
  distance: 30,
  duration: '00:10:00',
  travelduration: '01:00:00',
  defaultcountrycode: 'in',
  simchanges: 1,
  nofsubscribers: '2',
  proximity: 10,
  enableTowerInfo: false,
  minarea: 100 // kilo meter square
};

export default globalconfig;
