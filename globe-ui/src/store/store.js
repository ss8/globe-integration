import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { persistStore } from 'redux-persist';
import rootReducer from '../reducers';

/**
 * Store's Initial State
 */
const initialState = {};
/**
 * Add Middleware
 */
const middlewares = [thunk, logger];
/**
 * Create Store
 */
const store = createStore(
  rootReducer,
  initialState,
  compose(applyMiddleware(...middlewares))
);

export const persistor = persistStore(store);

export default store;
