import L from 'leaflet';
export const getShapeType = layer => {
  if (layer instanceof L.Circle) {
    return 'circle';
  }

  if (layer instanceof L.Marker) {
    return 'marker';
  }

  if (layer instanceof L.Polyline && !(layer instanceof L.Polygon)) {
    return 'polyline';
  }

  if (layer instanceof L.Polygon && !(layer instanceof L.Rectangle)) {
    return 'polygon';
  }

  if (layer instanceof L.Rectangle) {
    return 'rectangle';
  }
};

export const createGeoJSONCircle = (
  center,
  radiusInKm,
  points,
  index,
  basesubscriber
) => {
  if (!points) points = 64;

  var coords = {
    latitude: center[1],
    longitude: center[0]
  };

  var km = radiusInKm;

  var ret = [];
  var distanceX = km / (111.32 * Math.cos((coords.latitude * Math.PI) / 180));
  var distanceY = km / 110.574;

  var theta, x, y;
  for (var i = 0; i < points; i++) {
    theta = (i / points) * (2 * Math.PI);
    x = distanceX * Math.cos(theta);
    y = distanceY * Math.sin(theta);

    ret.push([coords.longitude + x, coords.latitude + y]);
  }
  ret.push(ret[0]);
  //let date = new Date();
  return {
    id: 'polycircle' + index,
    type: 'Feature',
    properties: {
      subinfo: basesubscriber
    },
    geometry: {
      coordinates: [ret],
      type: 'Polygon'
    },
    center: center
  };
};

export const toggleMarkerButton = state => {
  // toggle button dimming and clickability
  var button_circle = document.getElementsByClassName(
    'leaflet-draw-draw-circle'
  )[0];
  var button_polygon = document.getElementsByClassName(
    'leaflet-draw-draw-polygon'
  )[0];
  var button_rectangle = document.getElementsByClassName(
    'leaflet-draw-draw-rectangle'
  )[0];
  if (state) {
    // enable button
    button_circle.onClick = null;
    button_circle.className =
      'leaflet-draw-draw-circle leaflet-draw-toolbar-button-enabled';
    button_polygon.onClick = null;
    button_polygon.className =
      'leaflet-draw-draw-polygon leaflet-draw-toolbar-button-enabled';
    button_rectangle.onClick = null;
    button_rectangle.className =
      'leaflet-draw-draw-rectangle leaflet-draw-toolbar-button-enabled';
  } else {
    // disable button
    button_circle.onClick = 'preventEventDefault(); return false';
    button_circle.className = 'leaflet-draw-draw-circle draw-control-disabled';
    button_polygon.onClick = 'preventEventDefault(); return false';
    button_polygon.className =
      'leaflet-draw-draw-polygon draw-control-disabled';
    button_rectangle.onClick = 'preventEventDefault(); return false';
    button_rectangle.className =
      'leaflet-draw-draw-rectangle draw-control-disabled';
  }
};

export const getLatDirection = lat => {
  return Number(lat) >= 0 ? 'N' : 'S';
};

export const getLngDirection = lng => {
  return Number(lng) >= 0 ? 'E' : 'W';
};
