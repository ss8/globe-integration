import moment from 'moment-timezone';
import { getLatDirection, getLngDirection } from './mapUtils';

// get center of polygon
export const getcenterCoordinates = data => {
  let latarray = [];
  let langarray = [];
  let finallats = [];
  let finallang = [];
  let center = [];
  data.forEach((mapgeometry, index) => {
    let geometry = JSON.parse(mapgeometry.geometry);
    geometry.geometry.coordinates[0].forEach((coordinateval, cindex) => {
      latarray.push(coordinateval[0]);
      langarray.push(coordinateval[1]);
    });
    let x1 = Math.min(...latarray);
    let y1 = Math.min(...langarray);
    let x2 = Math.max(...latarray);
    let y2 = Math.max(...langarray);
    finallats.push(x1 + (x2 - x1) / 2);
    finallang.push(y1 + (y2 - y1) / 2);
  });
  let nx1 = Math.min(...finallats);
  let ny1 = Math.min(...finallang);
  let nx2 = Math.max(...finallats);
  let ny2 = Math.max(...finallang);
  center.push(nx1 + (nx2 - nx1) / 2);
  center.push(ny1 + (ny2 - ny1) / 2);
  return center;
};

// get center of points
export const getcenterCoordinatesPoints = data => {
  let latarray = [];
  let langarray = [];
  let finallatlang = [];
  data.forEach((mapgeometry, index) => {
    latarray.push(mapgeometry.latitude);
    langarray.push(mapgeometry.longitude);
  });
  let x1 = Math.min(...latarray);
  let y1 = Math.min(...langarray);
  let x2 = Math.max(...latarray);
  let y2 = Math.max(...langarray);
  finallatlang.push(x1 + (x2 - x1) / 2);
  finallatlang.push(y1 + (y2 - y1) / 2);
  return finallatlang;
};
export const createGeoJSONCircle = (
  center,
  radiusInKm,
  points,
  index,
  basesubscriber
) => {
  if (!points) points = 64;

  var coords = {
    latitude: center[1],
    longitude: center[0]
  };

  var km = radiusInKm;

  var ret = [];
  var distanceX = km / (111.32 * Math.cos((coords.latitude * Math.PI) / 180));
  var distanceY = km / 110.574;

  var theta, x, y;
  for (var i = 0; i < points; i++) {
    theta = (i / points) * (2 * Math.PI);
    x = distanceX * Math.cos(theta);
    y = distanceY * Math.sin(theta);

    ret.push([coords.longitude + x, coords.latitude + y]);
  }
  ret.push(ret[0]);
  //let date = new Date();
  return {
    id: 'polycircle' + index,
    type: 'Feature',
    properties: {
      subinfo: basesubscriber
    },
    geometry: {
      coordinates: [ret],
      type: 'Polygon'
    },
    center: center
  };
};
export const getQueryNameBytype = querytype => {
  switch (querytype) {
    case 'simplegeofence':
      return 'Simple Geo Fence';

    case 'geofencealert':
        return 'GeoFence Retrospective';

    case 'geofencemeeting':
      return 'GeoFence Meeting';

    case 'intermeeting':
      return 'Inter Meeting';

    case 'intrameeting':
      return 'Intra Meeting';

    case 'simplerealtime':
      return 'Simple Real Time';

    case 'roamingdetection':
      return 'Roamer detection';

    case 'simswap':
      return 'Sim Swap';

    case 'simpleHistory':
      return 'Simple History';

    case 'intersect':
      return 'Intersection';

    case 'traveldetection':
      return 'Travel Detection';
    case 'heatmap':
      return 'Heatmap';
    case 'simple':
      return 'Simple';
    default:
      return querytype;
  }
};

export const isJsonString = str => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

/**
 *
 * @param {*} dateString
 * @param {*} format
 */

export const isCorrectDateFormat = (dateString, format) => {
  return moment(dateString, format, true).isValid();
};

/**
 * HSV to RGB color conversion
 *
 * H runs from 0 to 360 degrees
 * S and V run from 0 to 100
 *
 * Ported from the excellent java algorithm by Eugene Vishnevsky at:
 * http://www.cs.rit.edu/~ncs/color/t_convert.html
 */
function hsvToRgb(h, s, v) {
  var r, g, b;
  var i;
  var f, p, q, t;

  // Make sure our arguments stay in-range
  h = Math.max(0, Math.min(360, h));
  s = Math.max(0, Math.min(100, s));
  v = Math.max(0, Math.min(100, v));

  // We accept saturation and value arguments from 0 to 100 because that's
  // how Photoshop represents those values. Internally, however, the
  // saturation and value are calculated from a range of 0 to 1. We make
  // That conversion here.
  s /= 100;
  v /= 100;

  if (s === 0) {
    // Achromatic (grey)
    r = g = b = v;
    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
  }

  h /= 60; // sector 0 to 5
  i = Math.floor(h);
  f = h - i; // factorial part of h
  p = v * (1 - s);
  q = v * (1 - s * f);
  t = v * (1 - s * (1 - f));

  switch (i) {
    case 0:
      r = v;
      g = t;
      b = p;
      break;

    case 1:
      r = q;
      g = v;
      b = p;
      break;

    case 2:
      r = p;
      g = v;
      b = t;
      break;

    case 3:
      r = p;
      g = q;
      b = v;
      break;

    case 4:
      r = t;
      g = p;
      b = v;
      break;

    default:
      // case 5:
      r = v;
      g = p;
      b = q;
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

export const randomColors = total => {
  var i = 360 / (total - 1); // distribute the colors evenly on the hue range
  var r = []; // hold the generated colors
  for (var x = 0; x < total; x++) {
    r.push(hsvToRgb(i * x, 100, 100)); // you can also alternate the saturation and value for even more contrast between the colors
  }
  return r;
};

export const popupDetails = (popupdata) => {
  let popupContent = "<span>";
  popupContent = popupContent.concat("<strong>Location </strong> : " + popupdata.eventLocations[0].latitude + "," + popupdata.eventLocations[0].longitude + "</br>");
  popupContent = popupContent.concat("<strong>Datasource Id </strong> : " + popupdata.datasourceId + "</br>");
  popupContent = popupContent.concat("<strong>Import Id </strong> : " + popupdata.importId + "</br>");
  Object.keys(popupdata).forEach(function (key) {
    if (!Array.isArray(popupdata[key]) && typeof (popupdata[key]) !== 'boolean' && typeof (popupdata[key]) === 'string') {
      const keyName = key.split('_')[0];
      popupContent = popupContent.concat("<strong>" + keyName + "</strong> : " + popupdata[key] + "</br>");
    }
  });
  popupContent = popupContent.concat("</span>");
  return popupContent;
};

export const popMarker = (popupdata) => {
  return (popupdata['title'] && popupdata['title'] !== '' ?
      `<p>${popupdata['title']}` : '') +
    (popupdata['description'] && popupdata['description'] !== '' ?
      `<p>${popupdata['description']}` : '') +
    (popupdata['imsi'] && popupdata['imsi'] !== '' ?
      `<p>IMSI: ${popupdata['imsi']}` : '') +
    (popupdata['imei'] && popupdata['imei'] !== '' ?
      `<p>IMEI: ${popupdata['imei']}` : '') +
    (popupdata['msisdn'] && popupdata['msisdn'] !== '' ?
      `<p>MSISDN: ${popupdata['msisdn']}` : '') +
    (popupdata['liid'] && popupdata['liid'] !== '' ?
      `<p>LIID: ${popupdata['liid']}` : '') +
    (popupdata['country'] ? `<p>Country:${popupdata['country']}</p>` : '') +
    (popupdata['segmentStartTime'] ?
      `<div>
          <p>
          Date:
          ${popupdata['segmentStartTime'].split(' ').length > 0
          ? popupdata['segmentStartTime'].split(' ')[0]
          : ''}
          </p>
          <p>
          Time:
          ${popupdata['segmentStartTime'].split(' ').length > 0
          ? popupdata['segmentStartTime'].split(' ')[1]
          : ''}
          </p>
          </div>` : '') +
    (popupdata['latitude'] !== '' ?
      `<p>
          ${popupdata['latitude']}
          <sup>o</sup> ${getLatDirection(popupdata['latitude'])}, ${' '}
          ${popupdata['longitude']}
          <sup>o</sup> ${getLngDirection(popupdata['longitude'])}
        </p>` : '')
}