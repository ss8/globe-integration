import React, { useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import ContextMenu from '../common/ContextMenu';
import { contextMenu as contexify } from 'react-contexify';
import axios from 'axios';
import store from '../../store/store';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import QuerySubHeader from '../header/QuerySubHeader';
import { execSpinner } from '../../actions/modalActions';
import CreateQuery from './CreateQuery';
import { setSelectedMenu } from '../../actions/navActions';

import {
  getQueries,
  deleteQueries,
  setQueriesGridChecked,
  toggleCreatePane,
  execQueries,
  createQuery,
  updateQuery,
  saveQueryGridCols,
  getRetroDataSources,
  getRetroInterceptNames
} from '../../actions/queryActions';
import { showNotification } from '../../actions/notifyActions';
import { showModal } from '../../actions/modalActions';
import { HIDE_MODAL } from '../../actions/types';
import { Collapse } from 'antd';
import Spinner from '../common/Spinner';
const { Panel } = Collapse;

const CancelToken = axios.CancelToken;
let source = CancelToken.source();
let execSource = {};

const Query = () => {
  const child = useRef(null);
  const agGrid = useRef(null);
  const dispatch = useDispatch();
  const querystore = useSelector(state => state.queries);
  const loggedInUser = useSelector(state => state.auth);
  /**
   * Set menu 'Queries' Selected
   * Get Queries List
   */
  useEffect(() => {
    window.localStorage.setItem('selectable', '');
    dispatch(setSelectedMenu('mn-queries'));
    dispatch(getRetroDataSources());
    dispatch(getRetroInterceptNames());
    dispatch(getQueries());
  }, [dispatch]);

  const onRowSelected = e => {
    const selectedNodes = e.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    dispatch(setQueriesGridChecked(selectedData));
  };

  const getRowClass = params => {
    if (params.node.firstChild === true && window.localStorage.getItem('selectable')) {
      params.node.setSelected(true);
      return 'ag-row-selected';
    }
  }
  // open Context menu on left click
  const onCellClicked = e => {
    e.event.preventDefault();
    if (
      e.colDef.headerName === 'Action' &&
      e.event.target.className === 'exec-grid'
    ) {
      contexify.show({
        id: 'agQueries',
        event: e.event
      });
    }
    if (
      e.colDef.headerName === '' &&
      (e.event.target.className === 'exec-abort' ||
        (e.event.target.ownerSVGElement &&
          e.event.target.ownerSVGElement.className.baseVal === 'abortico'))
    ) {
      execSource[e.data.queryId].cancel('Operation canceled by the user!');
    }
  };

  const confirmQueryDelete = () => {
    dispatch({ type: HIDE_MODAL, isOpen: false });
    dispatch(execSpinner(true, 'Query Deletion In Progress...'));
    const ids = querystore.agGrid.checkedRows
      .map(node => node.queryId)
      .join(',');
    const userName = loggedInUser.username;
    dispatch(deleteQueries(ids, userName));
  };

  const fetchQueryList = () => {
    dispatch(getQueries());
  };

  const togglePane = () => {
    child.current.child.current.resetRule();
    child.current.clearQueryInps();
    dispatch(toggleCreatePane(false, null));
  };

  const callQueryCreate = (method, json, id) => {
    if (method === 'post') {
      dispatch(execSpinner(true, 'Query Creation In Progress...'));
      source = CancelToken.source();
      dispatch(createQuery(json, source));
    } else if (method === 'put') {
      dispatch(execSpinner(true, 'Query Update in Progress...'));
      source = CancelToken.source();
      dispatch(updateQuery(id, json, source));
    }
    setTimeout(() => {
      togglePane();
    }, 10);
  };

  const callNotify = (type, title, message) => {
    dispatch(
      showNotification({
        notifyType: type,
        notifyProps: {
          title: title,
          message: message
        }
      })
    );
  };

  const externs = {
    /**
     * Pass agGrid store to render context menu items
     */
    gridstore: querystore.agGrid,

    /**
     * Open Pane - Create Query
     * onClick submenu - "Create Query"
     */
    onCreateQuery: () => {
      if (child.current !== null) {
        child.current.child.current.resetRule();
        child.current.clearQueryInps();
      }
      dispatch(toggleCreatePane(true, 'create'));
    },

    /**
     * Open pane - Edit Query {id}
     * onClick submenu - "Edit Query"
     */
    onEditQuery: () => {
      let scrollableElement = document.getElementById('scroll-to-top');
      if (scrollableElement !== null) {
        scrollableElement.scrollIntoView(true);
      }
      const rows = querystore.agGrid.checkedRows;
      let nonSelectedRows = querystore.queriesList.filter(grid => {
        return grid.queryId !== querystore.agGrid.checkedRows[0].queryId;
      });
      querystore.queriesList = rows.concat(nonSelectedRows);
      window.localStorage.setItem('selectable', 'rowSelected');
      if (rows.length === 1) {
        dispatch(toggleCreatePane(true, 'edit'));
        setTimeout(() => {
          child.current.child.current.editRule(rows[0]);
        }, 100);
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please select one query to edit !`
            }
          })
        );
      }
    },
    abortExecution: () => {
      const query = querystore.agGrid.checkedRows[0];
      execSource[query.queryId].cancel('Operation canceled by the user!');
    },
    abortCreateUpdate: () => {
      source.cancel('Operation canceled by the user!');
    },
    /**
     * Open pane - Execute Query {id}
     * onClick submenu - "Execute Query"
     */
    onQueryExecute: e => {
      //dispatch(execSpinner(true, "Query Execution In Progress..."));
      const query = querystore.agGrid.checkedRows[0];
      source = CancelToken.source();
      execSource[query.queryId] = CancelToken.source();
      dispatch(execQueries(query, querystore.queriesList, execSource));
    },
    /**
     * Delete Query in Grid
     * onClick submenu "Delete Query"
     */
    onDeleteQuery: () => {
      if (querystore.agGrid.checkedRows.length >= 1) {
        dispatch(
          showModal({
            modalProps: {
              title: 'Confirm Delete?',
              description:
                'Are you sure you want to delete the selected Query(s)?',
              confirmOk: confirmQueryDelete
            },
            modalType: 'confirm'
          })
        );
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please Select Query(s) to Delete!`
            }
          })
        );
      }
    }
  };

  return (
    <React.Fragment>
      <QuerySubHeader {...externs} />
      <ContextMenu {...externs} />
      <Collapse
        className="collapsePane panel-scroll"
        accordion={true}
        activeKey={querystore.isCreatePaneActive ? '1' : '0'}
      >
        <Panel showArrow={false} key="1">
          <CreateQuery
            ref={child}
            callNotify={callNotify}
            callfn={togglePane}
            callQueryCreate={callQueryCreate}
            callQlist={fetchQueryList}
            querieslist={querystore.queriesList}
            currentQuery={querystore.agGrid.checkedRows}
          />
        </Panel>
      </Collapse>
      <div
        style={{
          height: 'calc(100vh - 115px)',
          width: '100%',
          padding: '6px 0 15px',
          backgroundColor: '#f0f2f5'
        }}
        className="ag-theme-balham querylisttbl"
      >
        <Spinner {...externs} />
        <div className="ag-grid-notes">
          <span>
            <span className="exec-limit-note">*</span> Maximum 5 Concurrent
            Executions!
          </span>
          <span>
            <span className="public-access"></span> Public Access
          </span>
          <span>
            <span className="exec-success-note notes"></span> Last Successful
            Execution (+ve results)
          </span>
          <span>
            <span className="selected-note notes"></span> Selected Row(s)
          </span>
        </div>
        <AgGridReact
          ref={agGrid}
          columnDefs={querystore.agGrid.columnDefs}
          rowData={querystore.queriesList}
          floatingFilter={true}
          rowSelection="multiple"
          suppressScrollOnNewData={true}
          onRowSelected={onRowSelected}
          getRowClass={getRowClass}
          onGridReady={e => {
            onRowSelected(e);
          }}
          onColumnResized={e => {
            if (e.source === 'uiColumnDragged') {
              let colstate = e.columnApi.getColumnState();
              dispatch(saveQueryGridCols(colstate));
            }
          }}
          onColumnMoved={e => {
            if (e.source === 'uiColumnDragged') {
              let colstate = e.columnApi.getColumnState();
              dispatch(saveQueryGridCols(colstate));
            }
          }}
          onFirstDataRendered={e => {
            let currstate = store.getState();
            if (currstate.queries.queryGridColState.length) {
              e.columnApi.setColumnState(currstate.queries.queryGridColState);
            } else {
              e.api.sizeColumnsToFit();
            }
          }}
          isRowSelectable={rowNode => {
            return rowNode.data.status === -1 ? false : true;
          }}
          onCellClicked={onCellClicked}
        />
      </div>
    </React.Fragment>
  );
};

export default Query;
