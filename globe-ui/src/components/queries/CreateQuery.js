import React, { Component } from 'react';
import _ from 'lodash-es';
import moment from 'moment-timezone';
import store from '../../store/store';
import { isCorrectDateFormat } from '../../utils/uitility';
import { Button, Row } from 'antd';
import QueryBuilder from './QueryBuilder';

class CreateQuery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      error: null,
      inpText: '',
      inpDesc: '',
      disabled: false
    };

    this.child = React.createRef();

    this.qbobj = [];
    this.postQuery = this.postQuery.bind(this);
    this.getQueryBuild = this.getQueryBuild.bind(this);
    this.clearQueryInps = this.clearQueryInps.bind(this);
    this.queryChange = this.queryChange.bind(this);
  }
  getQueryBuild(o) {
    this.qbobj = { ...o };
  }

  clearQueryInps() {
    document.querySelector('#inpText').value = '';
    document.querySelector('#inpText').setAttribute('data-id', '');
    document.querySelector('#inpText').setAttribute('data-qowner', '');
    document.querySelector('#inpDesc').value = '';
    document.querySelector('#inpType').value = '';
  }

  simpleHistoryQueryBuilder(o) {
    let txtarea = o.name.replace(/(?:\r|\n|\r\n| )/g, '');
    let parts = txtarea.split(',');
    if (parts.length === 1) {
      return o.type.toUpperCase() + '=' + txtarea;
    } else if (parts.length > 1) {
      return o.type.toUpperCase() + ' in (' + parts.join() + ')';
    }
  }

  xhrCall(qId, qname, criteria, qtype, qdesc, qowner, qaccess) {
    // TimeZone UTC Conversion
    let fmt = 'YYYY-MM-DD HH:mm:ss';
    let currstate = store.getState();
    let current_user = currstate.auth.username;
    let zone = currstate.areas.timezone;
    if (this.isJsonString(criteria)) {
      let critjson = JSON.parse(criteria);
      let fromdate = critjson.starttime;
      let todate = critjson.endtime;
      critjson.starttime = moment
        .tz(fromdate, fmt, zone)
        .utc()
        .format(fmt);
      critjson.endtime = moment
        .tz(todate, fmt, zone)
        .utc()
        .format(fmt);
      if (critjson.expression !== undefined && qtype === 'simpleHistory') {
        let expr = critjson.expression;
        let exparts = expr.split("'");
        exparts[1] = `'${critjson.starttime}'`;
        exparts[3] = `'${critjson.endtime}'`;
        critjson.expression = exparts.join('');
      }
      criteria = JSON.stringify(critjson);
    } else {
      let inter_criteria = criteria
        .replace(/\|AND\|/g, ',|AND|,')
        .replace(/\|OR\|/g, ',|OR|,')
        .split(',');

      if (inter_criteria.length) {
        inter_criteria.forEach((str, i) => {
          if (isCorrectDateFormat(str, 'YYYY-MM-DD HH:mm:ss')) {
            inter_criteria[i] = moment
              .tz(str, fmt, zone)
              .utc()
              .format(fmt);
          }
        });
        criteria = inter_criteria
          .join(',')
          .replace(/,\|AND\|,/g, '|AND|')
          .replace(/,\|OR\|,/g, '|OR|');
      }
    }
    let postJSON = {
      queryId: qId,
      queryName: qname,
      queryDescription: qdesc,
      criteria: criteria,
      createDate: '',
      updateDate: '',
      type: qtype === '' ? 'simple' : qtype,
      timeRange: '',
      publicQueryAccess: qaccess
    };

    if (qId !== null) {
      let json = {
        ...postJSON,
        modifiedBy: current_user,
        queryOwner: qowner
      };
      this.props.callQueryCreate('put', json, qId);
    } else {
      let json = {
        ...postJSON,
        modifiedBy: null,
        queryOwner: current_user
      };
      this.props.callQueryCreate('post', json);
    }
  }
  isJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  postQuery() {
    this.setState({ disabled: true });
    let qname = document.querySelector('#inpText').value;
    let qId = document.querySelector('#inpText').getAttribute('data-id');
    qId = qId.length ? Number(qId) : null;
    let qdesc = document.querySelector('#inpDesc').value;
    let qtype = document.querySelector('#inpType').value;
    let qstring = document.querySelector('#queryStringWrapper').textContent;
    let checkduplicate = this.props.querieslist.filter(
      a => a.queryName === qname
    );

    let publicQueryAccess = false;
    let queryOwner = document
      .querySelector('#inpText')
      .getAttribute('data-qowner');

    if (qId === null) {
      if (checkduplicate.length > 0) {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'Query name already exists!');
        return;
      }
    }
    if (!qname.length || !qname.trim().length) {
      this.setState({ disabled: false });
      this.props.callNotify('error', 'Error', 'Please add Query name!');
      return;
    }

    if (this.isJsonString(qstring)) {
      var fcriteria = JSON.parse(qstring);
      // Set public query access state
      publicQueryAccess = fcriteria.publicquery;
      if (fcriteria.realtimeRT !== undefined && fcriteria.realtimeRT) {
        if (!fcriteria.intervalRT) {
          this.props.callNotify(
            'error',
            'Error',
            'Please Select Interval. Interval is Mandatory!'
          );
          return;
        }
        if (!fcriteria.iterationRT) {
          this.props.callNotify(
            'error',
            'Error',
            'Please Select Iteration. Iteration is Mandatory!'
          );
          return;
        }
        if (!fcriteria.durationRT) {
          this.props.callNotify(
            'error',
            'Error',
            'Please Select Duration. Duration is Mandatory!'
          );
          return;
        }
      }
    }
    if (qtype === 'simswap' || qtype === 'simplegeofence') {
      let cteria = JSON.parse(qstring);
      if (!cteria.areaid) {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'Area is Mandatory');
        return;
      }
      if (cteria.starttime === null) {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'Start Time is Mandatory');
        return;
      }
      if (cteria.endtime === null) {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'End Time is Mandatory');
        return;
      }
      if (cteria.msidtype === '') {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'IMSI / IMEI/ MSISDN is Mandatory'
        );
        return;
      }
    }
    if (
      qtype === 'geofencemeeting' ||
      qtype === 'intermeeting' ||
      qtype === 'intrameeting'
    ) {
      let cteria = JSON.parse(qstring);
      if (!cteria.areaid && qtype === 'geofencemeeting') {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'Area is Mandatory');
        return;
      }
      if (!cteria.hasOwnProperty('endtime')) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Start and End "Date/Time" is Mandatory'
        );
        return;
      }
    }
    if (qtype === 'traveldetection') {
      let cteria = JSON.parse(qstring);
      if (!cteria.areaid) {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'Area is Mandatory');
        return;
      }
      if (!cteria.hasOwnProperty('endtime')) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Start and end Time is Mandatory'
        );
        return;
      }
    }
    if (qtype === 'simpleHistory') {
      if (!(qstring.indexOf('segmentstarttime') > -1)) {
        this.props.callNotify('error', 'Error', 'Date is Mandatory');
        return;
      }
      if (qstring.indexOf('segmentstarttime') > -1) {
        let tdate = document.querySelector('.todate input').value;
        let fdate = document.querySelector('.fromdate input').value;
        if (fdate === '') {
          this.setState({ disabled: false });
          this.props.callNotify(
            'error',
            'Error',
            'From date is mandatory field'
          );
          return;
        }
        if (tdate === '') {
          this.setState({ disabled: false });
          this.props.callNotify('error', 'Error', 'To date is mandatory field');
          return;
        }
      }
    }

    let datefield = document.getElementsByClassName('fromdate');
    if (datefield.length > 0) {
      let tdate = document.querySelector('.todate input').value;
      let fdate = document.querySelector('.fromdate input').value;
      let fromdate = Date.parse(fdate);
      let todate = Date.parse(tdate);
      if (fdate === '') {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'From date is mandatory field');
        return;
      }
      if (tdate === '') {
        this.setState({ disabled: false });
        this.props.callNotify('error', 'Error', 'To date is mandatory field');
        return;
      }
      if (fromdate > todate) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Start Date should be less than Enddate'
        );
        return;
      }
    }

    if (
      qtype === 'simpleHistory' ||
      qtype === 'geofencemeeting' ||
      qtype === 'geofencealert' ||
      qtype === 'intermeeting' ||
      qtype === 'intrameeting' ||
      qtype === 'simswap' ||
      qtype === 'simplegeofence' ||
      qtype === 'roamingdetection' ||
      qtype === 'traveldetection'
    ) {
      if (qstring && !qstring.length) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Please Create Valid Query ruleset!'
        );
        return;
      }
      if (Object.values(JSON.parse(qstring)).filter(v => v === '').length) {
        if (qtype === 'simpleHistory') {
          if (
            Object.values(JSON.parse(qstring)).filter(v => v === '').length ===
            4
          ) {
            this.props.callNotify(
              'error',
              'Error',
              'Atleast one MSIDType value is required!'
            );
            return;
          }
        } else {
          this.props.callNotify(
            'error',
            'Error',
            'Please ensure Input Fields of Rules are not blank!'
          );
          return;
        }
      }
      this.xhrCall(
        qId,
        qname,
        qstring,
        qtype,
        qdesc,
        queryOwner,
        publicQueryAccess
      );
    } else if (qtype === 'simplerealtime') {
      if (qstring && !qstring.length) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Please Create Valid Query ruleset!'
        );
        return;
      }
      let validchk = JSON.parse(qstring);
      if (
        validchk.GMLCProfileId === null ||
        validchk.serviceType === '' ||
        validchk.templateName === '' ||
        validchk.msidType === '' ||
        _.indexOf(_.values(JSON.parse(qstring)), '') === 4
      ) {
        this.props.callNotify(
          'error',
          'Error',
          "GMLC Profile, Service Type, Template, MSID Type and it's Value are mandatory for Query!"
        );
        return;
      }
      this.xhrCall(
        qId,
        qname,
        qstring,
        qtype,
        qdesc,
        queryOwner,
        publicQueryAccess
      );
    } else if (qtype === 'intersect' || qtype === 'simple' || qtype === '') {
      let criteria = '';
      this.qbobj.rules.forEach(obj => {
        if (obj.name !== '' && obj.fromdate !== '' && obj.todate !== '') {
          criteria +=
            (obj.conj === '' ? '' : '|' + obj.conj + '|') +
            obj.name +
            ',' +
            obj.id +
            ',' +
            obj.fromdate +
            ',' +
            obj.todate;
        }
      });
      if (!criteria.length) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Please Create Valid Query ruleset!'
        );
        return;
      }
      if (this.qbobj.rules.length < 2 || !criteria.includes('|')) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          'Atleast two Areas are required for Intersection Query!'
        );
        return;
      }
      if (criteria.includes('null')) {
        this.setState({ disabled: false });
        this.props.callNotify(
          'error',
          'Error',
          "'Area', 'From Date', 'To Date' are mandatory!"
        );
        return;
      }
      this.xhrCall(
        qId,
        qname,
        criteria,
        qtype,
        qdesc,
        queryOwner,
        publicQueryAccess
      );
    }
    window.localStorage.setItem('selectable', '');
  }

  queryChange(event) {
    let type =
      event.target.value === 'intersect' || event.target.value === 'simple'
        ? 'generic'
        : event.target.value;
    this.child.current.queryTypeChanged(type);
  }

  render() {
    let currentQueryId = '';
    if(this.props.currentQuery[0]){
      currentQueryId = this.props.currentQuery[0] && this.props.currentQuery[0].queryId;
    }
    return (
      <div className="querybuilder pt-3" id="scroll-to-top">
        <form className="formfilter">
          <div className="form-row p-2">
            <div className="form-group col-md-4">
            {currentQueryId && <span className="editPage-querynames">Name: </span>}
              <input
                id="inpText"
                data-id=""
                type="text"
                className="form-control mb-3"
                placeholder="Name * "
              />
              {currentQueryId && <span className="editPage-querynames">Description: </span>}
              <textarea
                id="inpDesc"
                className="form-control mb-2"
                rows="2"
                placeholder="Description"
              />
              {currentQueryId && <span className="editPage-querynames">Query Type: </span>}
              <select
                onChange={this.queryChange}
                id="inpType"
                className="form-control form-control-sm p-0"
              >
                <option value="">Select Query Type</option>
                <option value="geofencealert">GeoFence Retrospective</option>
                {/* <option value="geofencemeeting">GeoFence Meeting</option>
                <option value="intermeeting">Inter Meeting</option>
                <option value="intersect">Intersection</option>
                <option value="intrameeting">Intra Meeting</option>
                <option value="roamingdetection">Roamer detection</option>
                <option value="simswap">Sim Swap</option>
                <option value="simplegeofence">Simple Geo Fence</option>
                <option value="simpleHistory">Simple History</option>
                <option value="simplerealtime">Simple Real Time</option>
                <option value="traveldetection">Travel Detection</option> */}
              </select>
              <label className="previewheader">Preview Query String:</label>
              <span id="queryStringWrapper" />
            </div>
            <div className="form-group col-md-8 ">
              <QueryBuilder ref={this.child} fn={this.getQueryBuild} />
              <div className="formbtns">
              <Row style={{ textAlign: 'right', paddingTop: '20px' }}>
                <Button
                  icon="close"
                  type="primary"
                  htmlType="button"
                  className="matui"
                  onClick={this.props.callfn}
                >
                  CLOSE
                </Button>
                <Button
                  icon="save"
                  type="primary"
                  htmlType="button"
                  className="matui"
                  onClick={this.postQuery}
                >
                  SUBMIT
                </Button>
              </Row>
            </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default CreateQuery;
