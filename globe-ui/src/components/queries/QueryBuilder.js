import React, { Component } from 'react';
import produce from 'immer';
import moment from 'moment-timezone';
import { orderBy } from 'lodash';
import store from '../../store/store';
import 'antd/dist/antd.css';
import { Button as AntBtn } from 'antd';
import { isJsonString } from '../../utils/uitility';
import GeoFenceAlertRuleset from './GeoFenceAlert';
import QueryAccessCheck from '../common/QueryAccessCheck';

function Ruleslist(props) {
  if (props.querytype === 'geofencealert') {
    if (!props.geofencealertrules.length) {
      return <div />;
    }
    const geofencealertrules =
      props.geofencealertrules.length > 1 ? (
        props.geofencealertrules.map((obj, inx) => {
          return (
            <GeoFenceAlertRuleset
              key={obj.index + '_sh'}
              index={inx}
              geofencealertrules={props.geofencealertrules}
              areas={props.areas}
              fn={props.callfn}
            />
          );
        })
      ) : (
        <GeoFenceAlertRuleset
          key={props.geofencealertrules[0].index + '_sh'}
          index={0}
          geofencealertrules={props.geofencealertrules}
          areas={props.areas}
          fn={props.callfn}
        />
      );

    return <div>{geofencealertrules}</div>;
  }
}

class QueryBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      areaslist: [],
      querytype: 'generic',
      geofencealertrules: [],
      geofencealertESexpr: {
        "startRow":0,
        "endRow":80,
        "timeRange":{
           "name":"eventDate",
           "startTime": 0 ,
           "endTime": 0
        },
        "sortModel":{
           "colId":"eventDate",
           "sort":"asc"
        },
        "iql":{
           "version":"2",
           "source":"fusion",
           "queries":[
              {
                 "eventTypes":[
                    "all"
                 ],
                 "condition":{
                    "operator":"and",
                    "terms":[]
                 }
              }
           ]
        }
      },
      qtype: '',
      publicQuery: false
    };
    this.addRule = this.addRule.bind(this);
    this.resetRule = this.resetRule.bind(this);
    this.resetRuleset = this.resetRuleset.bind(this);
    this.editRule = this.editRule.bind(this);
    this.setPublicQuery = this.setPublicQuery.bind(this);
  }

  componentWillMount() {
    const currstate = store.getState();
    const areastore = currstate.areas;
    moment.tz.setDefault(areastore.timezone);
    let sortedareas = orderBy(
      areastore.areasList,
      [list => list.area_name.toLowerCase()],
      ['asc']
    );
    this.setState({
      areaslist: sortedareas
    });
  }

  setPublicQuery(bool) {
    this.setState({ publicQuery: bool });
  }

  addRule() {
    if (this.state.querytype === 'geofencealert') {
      if (this.state.geofencealertrules.length > 4) {
        return;
      }

      let conj = !this.state.geofalertcounter ? '' : 'AND';

      this.setState({
        geofencealertrules: [
          ...this.state.geofencealertrules,
          {
            index: this.state.geofalertcounter,
            type: '',
            id: null,
            name: '',
            fromdate: null,
            todate: null,
            conj: conj
          }
        ],
        geofalertcounter: this.state.simplegeofcounter + 1
      });
    } 
  }

  resetRuleset() {
    this.queryTypeChanged(this.state.querytype);
  }

  resetRule() {
    this.setState({
      qtype: '',
      geofencealertrules: [
        {
          index: 0,
          type: '',
          id: null,
          name: '',
          fromdate: null,
          todate: null,
          conj: ''
        }
      ],
      geofalertcounter: 1,
      publicQuery: false
    });
  }

  updateGeoFAlertRule(o) {
    this.setState({
      geofencealertrules: this.state.geofencealertrules.map((obj, index) => {
        return o.index === index
          ? {
              ...obj,
              id: o.id ? o.id : obj.id,
              name: o.value !== undefined ? o.value : obj.name,
              event_id: o.event_id !== undefined ? o.event_id : obj.event_id,
              fromdate: o.datefrom !== undefined ? o.datefrom : obj.fromdate,
              todate: o.dateto !== undefined ? o.dateto : obj.todate,
              conj: o.conj ? o.conj : obj.conj
            }
          : obj;
      })
    });
  }

  deleteGeoFAlertRule(inx) {
    let arr = this.state.geofencealertrules.filter((_, i) => i !== inx);
    this.setState({
      geofencealertrules: arr.map((el, i) => {
        return !i ? { ...el, conj: '' } : el;
      })
    });
  }

  editRule(o) {
    if (o.criteria && o.criteria.length && isJsonString(o.criteria)) {
      let parsedCriteria = JSON.parse(o.criteria);
      this.setState({
        publicQuery: parsedCriteria.publicquery || false
      });
    }

    if (o.type === 'geofencealert') {
      let rulesset = JSON.parse(o.criteria);
      let rules = [];
      if (rulesset.areaid !== '') {
        rules.push({
          index: 0,
          type: 'area',
          id: rulesset.areaid ? rulesset.areaid : null,
          name: rulesset.areaname ? rulesset.areaname : '',
          fromdate: rulesset.starttime ? rulesset.starttime : null,
          todate: rulesset.endtime ? rulesset.endtime : null,
          conj: ''
        });
      }
      if (rulesset.intercept_name && rulesset.intercept_name !== '') {
        rules.push({
          index: 2,
          type: 'InterceptName',
          id: null,
          name: rulesset.intercept_name,
          event_id: rulesset.event_id,
          conj: 'AND'
        });
      }
      if (rulesset.datasource_name && rulesset.datasource_name !== '') {
        rules.push({
          index: 2,
          type: 'dataSourceName',
          id: null,
          name: rulesset.datasource_name,
          event_id: rulesset.event_id,
          conj: 'AND'
        });
      }      
      if (rulesset.imsi && rulesset.imsi !== '') {
        rules.push({
          index: 3,
          type: 'imsi',
          id: null,
          name: rulesset.imsi,
          conj: 'AND'
        });
      }
      if (rulesset.imei && rulesset.imei !== '') {
        rules.push({
          index: 3,
          type: 'imei',
          id: null,
          name: rulesset.imei,
          conj: 'AND'
        });
      }
      if (rulesset.msisdn && rulesset.msisdn !== '') {
        rules.push({
          index: 3,
          type: 'msisdn',
          id: null,
          name: rulesset.msisdn,
          conj: 'AND'
        });
      }
      if (rulesset.liid && rulesset.liid !== '') {
        rules.push({
          index: 3,
          type: 'liid',
          id: null,
          name: rulesset.liid,
          conj: 'AND'
        });
      }
      
      document.querySelector('#inpText').value = o.queryName;
      document.querySelector('#inpText').setAttribute('data-id', o.queryId);
      document
        .querySelector('#inpText')
        .setAttribute('data-qowner', o.queryOwner);
      document.querySelector('#inpType').value = o.type;
      document.querySelector('#inpDesc').value = o.queryDescription || '';
      this.setState({
        geofencealertrules: rules,
        geofalertcounter: rules.length,
        querytype: 'geofencealert',
        qtype: 'geofencealert'
      });
    }
  }

  setGeoFAlertRuleType(val, inx) {
    this.setState({
      geofencealertrules: this.state.geofencealertrules.map((obj, index) => {
        if(inx===1){
          return inx === index ? { ...obj, type: val, id: null, name: '' } : obj;
        }else{
          return inx === index ? { ...obj, type: val } : obj;
        }
      })
    });
  }

  queryTypeChanged(type) {
    this.setState({
      publicQuery: false
    });
    if (type === 'geofencealert') {
      this.setState({
        querytype: type,
        qtype: type,
        geofencealertrules: [
          {
            index: 0,
            type: 'area',
            id: null,
            name: '',
            fromdate: null,
            todate: null,
            conj: ''
          },
          {
            index: 1,
            type: 'InterceptName',
            id: null,
            name: '',
            event_id: null,
            conj: 'AND'
          },
          {
            index: 2,
            type: 'imsi',
            id: null,
            name: '',
            conj: 'AND'
          }
        ],
        geofalertcounter: 3
      });
    } else {
      this.setState({
        qtype: ''
      });
    }
  }

  buildQueryString(qtype) {
    if (!qtype) return;
    qtype = qtype.value;
    let criteria = '';

    if (qtype === 'geofencealert') {
      let area = {};
      let msiddata = {};
      let intercept_name = {};
      let datasource_name = {};
      let timeRange = { name: "eventDate" };
      let terms = [];

      this.state.geofencealertrules.forEach(obj => {
        if (obj.type === 'area' && obj.name !== '') {
          const currstate = store.getState();
          let selarea = currstate.areas.areasList.filter((o)=> o.areaId === obj.id);
          let coords = JSON.parse(selarea[0].geometry)[0].geometry.coordinates;
          
          area = {
            areaname: obj.name,
            areaid: obj.id,
            starttime: obj.fromdate,
            endtime: obj.todate
          };
          timeRange["startTime"] = obj.fromdate ? moment(obj.fromdate).format('x') : 0;
          timeRange["endTime"] = obj.todate ? moment(obj.todate).format('x') : 0;
          terms.push({
            "name": "eventLocations",
            "operator":"geoPoly",
            "values": coords[0].map((o)=>o.join())
          });          
        } else if (
          obj.type === 'imsi' ||
          obj.type === 'imei' ||
          obj.type === 'msisdn'
        ) {
          if (obj.name !== '') {
            msiddata = { [obj.type]: obj.name };
          }
        } else if (obj.type === 'InterceptName') {
          intercept_name = {
            intercept_name:
              obj.name != null ? obj.name : null,
            event_id: obj.event_id != null ? obj.event_id : null,
          };
          terms.push({
            "name": "intercept_name",
            "operator":"equals",
            "value": obj.name
          });          
        }else if(obj.type === 'dataSourceName'){
          datasource_name = {
            datasource_name:
              obj.name != null ? obj.name : null,
            event_id: obj.event_id != null ? obj.event_id : null,
          };
          terms.push({
            "name": "datasourceName",
            "operator":"equals",
            "value": obj.name
          });
        }

        const base = produce(this.state.geofencealertESexpr, draft => {
          draft["timeRange"] = timeRange;
          draft["iql"]["queries"][0]["condition"]["terms"] = terms;
        });

        criteria = JSON.stringify({
          expression: base,
          ...area,
          ...msiddata,
          ...intercept_name,
          ...datasource_name
        });
      });

    }

    if (isJsonString(criteria)) {
      const currstate = store.getState();
      let jsonCriteria = JSON.parse(criteria);
      criteria = {
        ...jsonCriteria,
        publicquery: this.state.publicQuery,
        user: currstate.auth.username
      };
      criteria = JSON.stringify(criteria);
    }
    document.querySelector('#queryStringWrapper').innerText = criteria;
  }

    render() {
    this.props.fn(this.state);
    this.buildQueryString(document.querySelector('#inpType'));
    let currstate = store.getState();
    return (
      <div className="qbwrap">
        {this.state.qtype !== '' ? (
          <div>
            <div className="qbheader">
              <label>
                Create Query Ruleset
              </label>
              <div className="addrulewrap">
                <AntBtn
                  type="default"
                  icon="undo"
                  size="small"
                  disabled={
                    currstate.queries.queryBuilder.opMode === 'edit'
                      ? true
                      : false
                  }
                  onClick={this.resetRuleset}
                >
                  Reset
                </AntBtn>
                &nbsp;&nbsp;
                <AntBtn
                  type="default"
                  icon="plus-circle"
                  size="small"
                  disabled={
                    this.state.querytype === 'geofencealert' && this.state.geofencealertrules.length === 3
                      ? true
                      : false
                  }
                  onClick={this.addRule}
                >
                  Add Rule
                </AntBtn>
              </div>
            </div>
            <div id="qbrulewrap" className={this.state.querytype}>
              <Ruleslist
                geofencealertrules={this.state.geofencealertrules}
                querytype={this.state.querytype}
                areas={this.state.areaslist}
                callfn={this}
              />
            </div>
            {this.state.querytype !== 'generic' ? (
              <QueryAccessCheck
                fn={this.setPublicQuery}
                qps={this.state.publicQuery}
              />
            ) : null}
          </div>
        ) : (
          <div>Please Select Query Type to Add Criteria</div>
        )}
      </div>
    );
  }
}

export default QueryBuilder;
