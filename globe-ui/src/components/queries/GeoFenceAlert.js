import React from 'react';
import store from '../../store/store';
import 'antd/dist/antd.css';
import {
  Form,
  DatePicker,
  Select,
  Tooltip,
  Radio,
  Button as AntBtn
} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import moment from 'moment';

const Option = Select.Option;

function GeoFenceAlertRuleset(props) {
  console.info(props);
  const currstate = store.getState();

  let preselects = props.geofencealertrules.map(o => {
    return o.type;
  });
  const listItems = props.areas.map(obj => {
    return (
      <Option key={obj.areaId} value={obj.areaId}>
        {obj.area_name}
      </Option>
    );
  });

  const retrosItems = currstate.queries?.retroDataSources?.data?.map(src => {
    return (
      <Option key={src} value={src}>
        {src}
      </Option>
    );
  })

  const interceptItems = currstate.queries?.retroInterceptNames?.data?.map(item => {
    return (
      <Option key={item} value={item}>
        {item}
      </Option>
    );
  })

  return (
    <div className="qbrule">
      {props.index ? (
        <div>
          <Radio.Group
            value={props.geofencealertrules[props.index].conj || undefined}
            defaultValue="AND"
            buttonStyle="solid"
            size="small"
            onChange={e =>
              props.fn.updateGeoFAlertRule({
                index: props.index,
                conj: e.target.value
              })
            }
          >
            <Radio.Button value={'AND'}>AND</Radio.Button>
            <Radio.Button disabled={true} value={'OR'}>
              OR
            </Radio.Button>
          </Radio.Group>
        </div>
      ) : (
        ''
      )}
      <Select
        size="small"
        value={props.geofencealertrules[props.index].type || undefined}
        style={{ width: 135 }}
        placeholder="Select Rule"
        onChange={(val, b) => {
          props.fn.setGeoFAlertRuleType(val, props.index, 'geofencealertrules');
        }}
      >
        <Option
          disabled={preselects.indexOf('area') > -1 ? true : null}
          value="area"
        >
          Area
        </Option>
        <Option
          disabled={
            preselects.indexOf('InterceptName') > -1 || !props.index || props.index===2
              ? true
              : null
          }
          value="InterceptName"
        >
          Intercept-Name
        </Option>
        <Option
          disabled={
            preselects.indexOf('dataSourceName') > -1 || !props.index || props.index === 2
              ? true
              : null
          }
          value="dataSourceName"
        >
          DataSource-Name
        </Option>        
        <Option
          disabled={
            
            preselects.indexOf('imsi') > -1 || props.index!==2 || !props.index ? true : null
          }
          value="imsi"
        >
          IMSI
        </Option>
        <Option
          disabled={
            preselects.indexOf('imei') > -1 ||
           props.index!==2 || !props.index ? true : null
          }
          value="imei"
        >
          IMEI
        </Option>
        <Option
          disabled={
            preselects.indexOf('msisdn') > -1 || props.index!==2 || !props.index ? true : null
          }
          value="msisdn"
        >
          MSISDN
        </Option>
      </Select>
      <div className="droplist">
        {(() => {
          switch (props.geofencealertrules[props.index].type) {
            case 'area':
              return (
                <div>
                  <Select
                    showSearch
                    optionFilterProp="children"
                    value={
                      props.geofencealertrules[props.index].name || undefined
                    }
                    onChange={(id, o) =>
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        value: o.props.children,
                        id: id
                      })
                    }
                    size="small"
                    placeholder={
                      'Select ' + props.geofencealertrules[props.index].type
                    }
                    style={{ width: 150 }}
                  >
                    {listItems}
                  </Select>
                  <label>&nbsp;&nbsp;between</label>
                  <div className="fromdate">
                    <Tooltip
                      placement="topLeft"
                      overlayStyle={{
                        fontSize: '0.85em',
                        minWidth: '100px',
                        zIndex: 9999
                      }}
                      title={<span>Timezone: {currstate.areas.timezone}</span>}
                    >
                      <DatePicker
                        showTime={{
                          defaultValue: moment('00:00:00', 'HH:mm:ss')
                        }}
                        value={
                          props.geofencealertrules[props.index].fromdate !==
                          null
                            ? moment(
                                props.geofencealertrules[props.index].fromdate
                              )
                            : null
                        }
                        style={{ minWidth: 150, maxWidth: 170 }}
                        onChange={(dobj, dstr) =>
                          props.fn.updateGeoFAlertRule({
                            index: props.index,
                            datefrom: dobj == null ? null : dstr
                          })
                        }
                      />
                    </Tooltip>
                  </div>
                  <label>and</label>
                  <div className="todate">
                    <Tooltip
                      placement="topLeft"
                      overlayStyle={{
                        fontSize: '0.85em',
                        minWidth: '100px',
                        zIndex: 9999
                      }}
                      title={<span>Timezone: {currstate.areas.timezone}</span>}
                    >
                      <DatePicker
                        showTime={{
                          defaultValue: moment('23:59:59', 'HH:mm:ss')
                        }}
                        value={
                          props.geofencealertrules[props.index].todate !== null
                            ? moment(
                                props.geofencealertrules[props.index].todate
                              )
                            : null
                        }
                        style={{ minWidth: 150, maxWidth: 170 }}
                        onChange={(dobj, dstr) =>
                          props.fn.updateGeoFAlertRule({
                            index: props.index,
                            dateto: dobj == null ? null : dstr
                          })
                        }
                      />
                    </Tooltip>
                  </div>
                </div>
              );
            case 'InterceptName':
              return (
                <Form.Item>
                  <Select 
                    showSearch
                    optionFilterProp="children"
                    value={
                      props.geofencealertrules[props.index].name || undefined
                    }
                    onChange={(id, o) =>
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        value: o.props.children,
                        id: id
                      })
                    }
                    size="small"
                    placeholder={
                      'Select Intercept Name'
                    }
                    style={{ width: 150 }}>
                    {/* <Option value="intercept1">intercept1</Option>
                    <Option value="intercept2">intercept2</Option>
                    <Option value="intercept3">intercept3</Option> */}
                    {interceptItems}
                  </Select>
                  {/* <label style={{ marginLeft: 20 }}>Event Id:</label>
                  <Input
                    size="small"
                    value={props.geofencealertrules[props.index].event_id}
                    onChange={e => {
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        event_id: e.target.value
                      });
                    }}
                    style={{ width: 130, marginLeft: 5, marginRight: 5 }}
                  /> */}
                </Form.Item>
              );
              case 'dataSourceName':
                return (
                  <Form.Item>
                    <Select 
                      showSearch
                      optionFilterProp="children"
                      value={
                        props.geofencealertrules[props.index].name || undefined
                      }
                      onChange={(id, o) =>
                        props.fn.updateGeoFAlertRule({
                          index: props.index,
                          value: o.props.children,
                          id: id
                        })
                      }
                      size="small"
                      placeholder={
                        'Select DataSource Name'
                      }
                      style={{ width: 150 }}>
                      {retrosItems}
                    </Select>
                    {/* <label style={{ marginLeft: 20 }}>Event Id:</label>
                    <Input
                      size="small"
                      value={props.geofencealertrules[props.index].event_id}
                      onChange={e => {
                        props.fn.updateGeoFAlertRule({
                          index: props.index,
                          event_id: e.target.value
                        });
                      }}
                      style={{ width: 130, marginLeft: 5, marginRight: 5 }}
                    /> */}
                  </Form.Item>
                );              
            case 'imsi':
              return (
                <Form.Item>
                  <TextArea
                    value={
                      props.geofencealertrules[props.index].name !== null
                        ? props.geofencealertrules[props.index].name
                        : null
                    }
                    onChange={e => {
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        value: e.target.value
                      });
                    }}
                    placeholder="Add IMSI"
                    style={{
                      height: 32,
                      resize: 'none',
                      lineHeight: 1.1,
                      width: 170
                    }}
                  />
                </Form.Item>
              );
            case 'imei':
              return (
                <Form.Item>
                  <TextArea
                    value={
                      props.geofencealertrules[props.index].name !== null
                        ? props.geofencealertrules[props.index].name
                        : null
                    }
                    onChange={e => {
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        value: e.target.value
                      });
                    }}
                    placeholder="Add IMEI"
                    style={{
                      height: 32,
                      resize: 'none',
                      lineHeight: 1.1,
                      width: 170
                    }}
                  />
                </Form.Item>
              );
            case 'msisdn':
              return (
                <Form.Item>
                  <TextArea
                    value={
                      props.geofencealertrules[props.index].name !== null
                        ? props.geofencealertrules[props.index].name
                        : null
                    }
                    onChange={e => {
                      props.fn.updateGeoFAlertRule({
                        index: props.index,
                        value: e.target.value
                      });
                    }}
                    placeholder="Add MSISDN"
                    style={{
                      height: 32,
                      resize: 'none',
                      lineHeight: 1.1,
                      width: 170
                    }}
                  />
                </Form.Item>
              );
            default:
              return <div />;
          }
        })()}
      </div>
      {props.geofencealertrules.length > 1 ? (
        <div className="delicon">
          <AntBtn
            key={props.index + '__delbtn'}
            type="danger"
            icon="delete"
            onClick={() => props.fn.deleteGeoFAlertRule(props.index)}
          />
        </div>
      ) : (
        ''
      )}
    </div>
  );
}

export default GeoFenceAlertRuleset;
