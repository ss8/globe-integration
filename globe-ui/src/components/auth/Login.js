import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import history from '../../history';
import axios from 'axios';
import store from '../../store/store';
import { isEqual } from 'lodash';
import qs from 'qs';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css';
import oauth from '../../config/authconf';
import { showNotification } from '../../actions/notifyActions';
import { userSignedIn, userSignedOut } from '../../actions/authActions';
import { getHomeCode } from '../../actions/areaActions';
import { setBaseMap, setMapPosition } from '../../actions/mapActions';
export const { APP_URL } = window;

const LoginForm = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    axios
      .get('xt_globe_settings.json')
      .then(function(response) {
        if (typeof response.data === 'string') {
          dispatch(
            showNotification({
              notifyType: 'error',
              notifyProps: {
                title: 'Error',
                message: `Invalid JSON! Cannot parse 'xt_globe_settings.json'`
              }
            })
          );
        } else {
          let currstate = store.getState();
          if (
            !isEqual(
              currstate.maps.mapPosition.center,
              response.data.mapPosition.center
            )
          ) {
            dispatch(getHomeCode(response.data));
          }
          dispatch(setMapPosition(response.data));
          dispatch(setBaseMap(null, response.data));
        }
      })
      .catch(function() {
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Invalid JSON! Error parsing 'xt_globe_settings.json'`
            }
          })
        );
      });
  }, [dispatch]);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .post(
            `${APP_URL}/slpauthserver`,
            qs.stringify({
              username: values.username,
              password: values.password,
              grant_type: 'password',
              client_id: oauth.user,
              client_secret: oauth.pass
            }),
            {
              headers: {
                'Content-type':
                  'application/x-www-form-urlencoded; charset=utf-8',
                Authorization: 'Basic ' + btoa(`${oauth.user}:${oauth.pass}`)
              }
            }
          )
          .then(function(res) {
            let roles = res.data.roles
              ? res.data.roles.map(o => o.authority)
              : [];
            if (res.data.access_token && res.data.username && roles.length) {
              dispatch(
                userSignedIn(res.data.username, res.data.access_token, roles)
              );
              history.push('/areas');
            } else {
              dispatch(userSignedOut());
              history.push('/');
              dispatch(
                showNotification({
                  notifyType: 'error',
                  notifyProps: {
                    title: 'Error Signing In!',
                    message:
                      'Username or Password you entered is incorrect! Please try again.'
                  }
                })
              );
            }
          })
          .catch(function(error) {
            history.push('/');
            if (error.response) {
              dispatch(
                showNotification({
                  notifyType: 'error',
                  notifyProps: {
                    title: 'Error',
                    message: `${error.response.data.error_description}`
                  }
                })
              );
            } else {
              dispatch(
                showNotification({
                  notifyType: 'error',
                  notifyProps: {
                    title: 'Error',
                    message: `Error Signing In! [${error.message}]`
                  }
                })
              );
            }
          });
      }
    });
  };

  const { getFieldDecorator } = props.form;

  return (
    <div className="row login-wrap">
      <div className="form-wrap">
        <Form onSubmit={handleSubmit} className="login-form">
          <div className="form-header">
            <div className="logo_login">&nbsp;</div>
          </div>
          <Form.Item style={{ width: '100%' }}>
            {getFieldDecorator('username', {
              rules: [
                { required: true, message: 'Please input your username!' }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="First Name (Username)"
              />
            )}
          </Form.Item>
          <Form.Item style={{ width: '100%' }}>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Please input your Password!' }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                type="password"
                placeholder="Password"
              />
            )}
          </Form.Item>
          <Form.Item style={{ width: '100%' }}>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true
            })(<Checkbox>Remember me</Checkbox>)}

            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              SIGN IN
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

const Login = Form.create({ name: 'ss8_login' })(LoginForm);

export default Login;
