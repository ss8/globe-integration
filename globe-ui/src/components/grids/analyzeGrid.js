import React, { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import store from '../../store/store';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import moment from 'moment';
import { Icon } from 'antd';
import GeoFenceAlertMap from '../maps/GeoFenceAlertMap';
import ReactTooltip from 'react-tooltip';
import { setSelectedMenu } from '../../actions/navActions';
import { showNotification } from '../../actions/notifyActions';
export const { RETRO_URL } = window;


const AnalyzeGrid = props => {
  const queryGrid = useRef(null);
  const [colDefs, setcolDefs] = useState([]);
  const [gridApi, updateGridApi] = useState({});
  const [errorClassName, updateErrorClassName] = useState('');
  const dispatch = useDispatch();
  const querystore = useSelector(state => state.queries);
  const analyzestore = useSelector(state => state.analyze);
  const areastore = useSelector(state => state.areas);
  let queryId = Number(props?.match?.params?.id) || null;
  const sideBar = {
    toolPanels: [{
      id: 'columns',
      labelDefault: 'Columns',
      labelKey: 'columns',
      iconKey: 'columns',
      toolPanel: 'agColumnsToolPanel',
      toolPanelParams: {
        suppressPivots: true,
        suppressPivotMode: true,
        suppressValues: true
      }
    }]
  }

  let analyzedQuery = querystore.queriesList.filter(el => {
    let id = Number(el.queryId);
    return id === Number(props.match.params.id);
  });
  
  const externs = {
    analyzedQuery: analyzedQuery,
    onMapLoaded: (map, L, drawControl) => {
      dispatch(setSelectedMenu());
      if (drawControl === null) {
        return;
      }
      if (analyzestore.analyzeArea !== '' && drawControl.current !== null) {
        let areasarray = [];
        if (analyzestore.analyzeArea.toString().indexOf(',') > -1) {
          areasarray = analyzestore.analyzeArea.split(',');
        } else {
          areasarray.push(analyzestore.analyzeArea);
        }
        let ftgroup = new L.featureGroup();
        let ftgroupBounds = [];
        areasarray.forEach(areaid => {
          let filteredarea = areastore.areasList.filter(el => {
            return el.areaId === parseInt(areaid);
          });

          let geoCoordsArr = JSON.parse(filteredarea[0]['geometry']);
          geoCoordsArr.forEach(geoCoords => {
            geoCoords.geometry.coordinates[0].map(arr => arr.reverse());
            let leafletGeoJSON = L.geoJSON(geoCoords, {
              style: function () {
                return {
                  fillColor: '#fff',
                  fillOpacity: '0'
                };
              }
            });
            let leafletFG = drawControl.current.leafletElement;
            if (geoCoords.properties.leafletCircle !== undefined) {
              /**
               * Handle Leaflet Circle
               */
              leafletGeoJSON = L.geoJSON(geoCoords.properties.leafletCircle, {
                pointToLayer: function (feature, latlng) {
                  if (geoCoords.properties.radius) {
                    return new L.Circle(latlng, geoCoords.properties.radius);
                  }
                  return;
                },
                style: function () {
                  return {
                    fillColor: '#fff',
                    fillOpacity: '0'
                  };
                }
              });
              leafletGeoJSON.eachLayer(layer => {
                layer.bindTooltip(filteredarea[0]['area_name'], {
                  opacity: 0.6,
                  className: 'area_label_tip',
                  permanent: true
                });
                return leafletFG.addLayer(layer);
              });
            } else if (geoCoords.properties.leafletRectangle !== undefined) {
              /**
               * Handle Leaflet Rectangle
               */
              leafletGeoJSON = L.GeoJSON.geometryToLayer(geoCoords);
              var rectLayer = L.rectangle(leafletGeoJSON.getBounds(), {
                fillColor: '#fff',
                fillOpacity: '0'
              });
              rectLayer.bindTooltip(filteredarea[0]['area_name'], {
                opacity: 0.6,
                className: 'area_label_tip',
                permanent: true
              });
              leafletFG.addLayer(rectLayer);
            } else {
              /**
               * Handle Leaflet Polygon
               */
              leafletGeoJSON.eachLayer(layer => {
                layer.bindTooltip(filteredarea[0]['area_name'], {
                  opacity: 0.6,
                  className: 'area_label_tip',
                  permanent: true
                });
                return leafletFG.addLayer(layer);
              });
            }
            ftgroup.addLayer(leafletGeoJSON);
            ftgroupBounds.push(ftgroup.getBounds());
          });
        });
        setTimeout(() => {
          map.fitBounds(ftgroupBounds);
        }, 10);
      }
    }
  };

  useEffect(() => {
    var myDatasource = createDatasource();
    queryGrid.current.api.setServerSideDatasource(myDatasource);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  

  const createDatasource = () => {
    return {
      // called by the grid when more rows are required
      getRows: function (params) {
        let currstate = store.getState();
        let query = currstate.queries.queryExecInfo.filter((o) => o.queryId === queryId);
        let queryCriteria = query[0] && JSON.parse(query[0].queryString);
        const {
          request
        } = params;
        const iql = cloneDeep(queryCriteria && queryCriteria.expression && queryCriteria.expression.iql);
        const sortModel = request.sortModel && request.sortModel.length > 0 ? request.sortModel[0] : null;
        if (queryCriteria) {
          queryCriteria.expression.startRow = request.startRow;
          queryCriteria.expression.endRow = request.endRow;
          queryCriteria.expression.sortModel = sortModel;
        }
        applyFilteringToIql(request, params, iql, queryCriteria);
      }
    };
  }

  const applyFilteringToIql = (request, params, iql, queryCriteria) => {
    if (request.filterModel && Object.keys(request.filterModel).length) {
      const filterKeys = Object.keys(request.filterModel);
      let filters = [];
      for (let index = 0; index < filterKeys.length; index += 1) {
        const {
          filter,
          type,
          filterTo,
          filterType,
          dateFrom,
          dateTo,
          values
        } = request.filterModel[filterKeys[index]];
        const term = {
          name: filterKeys[index],
          operator: filterType === 'set' ? 'in' : type
        };
        if (type === 'inRange') {
          if (filterType === 'date') {
            term.valueL = moment.utc(dateFrom).format();
            term.valueR = moment.utc(dateTo).format();
          } else {
            term.valueL = filter;
            term.valueR = filterTo;
          }
        } else if (filterType === 'date') {
          term.value = moment.utc(dateFrom).format();
        } else {
          term.value = filter;
        }
        if (filterType === 'set') {
          if (values && values.length > 0)
            filters.push(term);
        } else {
          filters.push(term);
        }
      }
      const updatedTermValue = iql.queries[0].condition.terms.concat(filters);
      iql.queries[0].condition.terms = updatedTermValue;
      queryCriteria.expression.iql = iql;
      getGridResponse(queryCriteria, params);
    } else {
      getGridResponse(queryCriteria, params);
    }
  }

  const getGridResponse = (queryCriteria, params) => {
    axios({
        method: 'post',
        url: `${RETRO_URL}/retrospective-query/search`,
        data: queryCriteria && queryCriteria.expression
      })
      .then(res => {
       let cols;
       let colName;
       let gridColDefs;
       if (res.data && res.data.data && res.data.data.length > 0) {
         cols = Object.keys(res.data && res.data.data[0]);
         gridColDefs = cols.map((head) => {
           colName = head.replace(/_txt/g, '').replace(/_db/g, '').replace(/_loc/g, '');
           colName = colName.charAt(0).toUpperCase() + colName.slice(1);
           return {
             headerName: colName,
             field: head,
             sortable: true,
             resizable: true,
             filter: updateFilterType(head),
             floatingFilter: true
           }
         })
         setcolDefs(gridColDefs);
       };

        // update date format with time stamp
        for (let i = 0; i < res.data.data.length; i++) {
          const allKeys = Object.keys(res.data.data[i]);
          for (let j = 0; j < allKeys.length; j++) {
            if (allKeys[j] === 'eventDate' || allKeys[j] === 'ingestDate' || allKeys[j].split('_')[1] === 'dt') {
              res.data.data[i][allKeys[j]] = dateFormat(res.data.data[i][allKeys[j]]);
            }
            if (res.data.data[i][allKeys[j]][0] && Object.keys(res.data.data[i][allKeys[j]][0])[0] === 'latitude') {
              res.data.data[i][allKeys[j]] = `{${res.data.data[i][allKeys[j]][0]['latitude']}, ${res.data.data[i][allKeys[j]][0]['longitude']}}`;
            }
          }
        }

        const {
          data,
          lastRow
        } = res.data;
        // eslint-disable-next-line
        data.map((o) => {
          cols.forEach(field => {
            if (o[field] && typeof (o[field]) === 'object') {
              o[field] = JSON.stringify(o[field]);
            }
          });
        });
        params.successCallback(data, lastRow);
      })
      .catch(err => {
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error while executing Query!`
            }
          })
        );
        updateErrorClassName('hideEmptyColumns');
      });
  }

  const dateFormat = (date) => {
    return moment(date).format('MMM DD YYYY HH:mm:ss')
  }

  const updateFilterType = (data) => {
    let receivedData = data.split('_')[1];
    let filteredType;
    if (receivedData === 'db') {
      filteredType = 'number';
    } else if (receivedData === 'dt' || data === 'eventDate' || data === 'ingestDate') {
      filteredType = 'date';
    } else {
      filteredType = 'text';
    }
    return filteredType;
  }

  const resetAppliedFilters = () => {
    if (gridApi) {
      gridApi.setFilterModel(null);
    }
  }

  const onGridReady = (params) => {
    updateGridApi(params.api);
  }

  const ex = {
    defaultColDef: {
      width: 180,
      filter: true,
      enableValue: true,
      enableRowGroup: false,
      enablePivot: true,
      sortable: true,
      filterParams: {
        applyButton: true,
        newRowsAction: 'keep',
        caseSensitive: true,
        clearButton: true,
        suppressAndOrCondition: true
      },
    },
    sideBar: true
  }

  return (
    <React.Fragment>
      <div className="mapwrapper">
        <GeoFenceAlertMap currentQueryId={queryId} {...externs}/>
      </div>
      <div>
        <ReactTooltip />
        <Icon
            style={{
              fontSize: '20px',
              marginTop: '20px',
              display: 'inline-block',
              float: 'right'
            }}
            type="undo"
            onClick={resetAppliedFilters}
            className="resetIconStyle"
            data-tip="Reset applied column Filters"
        />
      </div>
      <div
        style={{
          height: 'calc(100vh - 72px)',
          width: '100%',
          padding: '6px 0 15px',
          marginTop: '20px',
          backgroundColor: '#f0f2f5',
          display: 'inline-block'
        }}
        className={`ag-theme-balham ${errorClassName}`}
      > 
        <AgGridReact
         ref={queryGrid}
         columnDefs={colDefs}
         defaultColDef={ex.defaultColDef}
         sideBar={sideBar}
         enableSorting={true}
         enableFilter={true}
         enableColResize={true}
         rowSelection="single"
         rowModelType = 'serverSide'
         maxConcurrentDatasourceRequests={8}
         serverSideSortingAlwaysResets={true}
         purgeClosedRowNodes={true}
         enableCellTextSelection={true}
         maxBlocksInCache={10}
         cacheBlockSize={40} 
         rowGroupPanelShow="never"
         pivotPanelShow="always"
         animateRows={true}
         onGridReady={onGridReady}
        />
      </div>
    </React.Fragment>
  );
};

export default AnalyzeGrid;
