import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Map from '../maps/Map';
import store from '../../store/store';
import { toggleMarkerButton } from '../../utils/mapUtils';
import MapAreaForm from '../forms/MapAreaForm';
import {
  loadMapAreaCoords,
  layersDeleted,
  loadMapAreaFormFields
} from '../../actions/areaActions';

const AddArea = props => {
  const dispatch = useDispatch();
  const areastore = useSelector(state => state.areas);

  const handleFormChange = changedFields => {
    let key = Object.keys(changedFields).pop();
    dispatch(loadMapAreaFormFields(key, changedFields[key]));
  };

  /**
   * externs holds methods to be passed to child, i.e external calls
   */
  const externs = {
    onDrawn: geoJSON => {
      dispatch(loadMapAreaCoords(geoJSON));
    },
    clearDeletedLayers: layers => {
      dispatch(layersDeleted(layers));
    },
    onMapLoaded: (map, L, drawControl) => {
      if (drawControl === null) {
        return;
      }
      if (
        areastore.mapArea.opMode !== null &&
        drawControl.current !== null &&
        areastore.mapArea.formFields.areaLayerCoords.length
      ) {
        let currstate = store.getState();
        let areaCoords = currstate.areas.mapArea.formFields.areaLayerCoords;
        if (areaCoords.length > 2) {
          toggleMarkerButton(false);
        } else {
          toggleMarkerButton(true);
        }
        let mapAreas = areastore.mapArea.formFields.areaLayerCoords;
        let formAreas = [];
        let boundAreas = [];
        mapAreas.forEach(geo => {
          let geoCoords = JSON.parse(geo);
          let leafletGeoJSON = L.geoJSON(geoCoords, {
            style: function(feature) {
              return { fillColor: '#fff', fillOpacity: '0' };
            }
          });
          let leafletFG = drawControl.current.leafletElement;
          if (geoCoords.properties.leafletCircle !== undefined) {
            /**
             * Handle Leaflet Circle
             */
            leafletGeoJSON = L.geoJSON(geoCoords.properties.leafletCircle, {
              pointToLayer: function(feature, latlng) {
                if (geoCoords.properties.radius) {
                  return new L.Circle(latlng, geoCoords.properties.radius);
                }
                return;
              }
            });
            leafletGeoJSON.eachLayer(layer => {
              geoCoords.properties['leafletId'] = layer._leaflet_id;
              formAreas.push(JSON.stringify(geoCoords));
              //dispatch(loadMapAreaCoords(JSON.stringify(geoCoords)));
              return leafletFG.addLayer(layer);
            });
            boundAreas.push(leafletGeoJSON.getBounds());
            //map.fitBounds(leafletGeoJSON.getBounds());
          } else if (geoCoords.properties.leafletRectangle !== undefined) {
            /**
             * * Handle Leaflet Rectangle
             */
            leafletGeoJSON = L.GeoJSON.geometryToLayer(geoCoords);
            var rectLayer = L.rectangle(leafletGeoJSON.getBounds());
            leafletFG.addLayer(rectLayer);
            // Reset to newly created leaflet Id
            geoCoords.properties['leafletId'] = rectLayer._leaflet_id;
            formAreas.push(JSON.stringify(geoCoords));
            //dispatch(loadMapAreaCoords(JSON.stringify(geoCoords)));
            // Fit to Bounds
            boundAreas.push(leafletGeoJSON.getBounds());
            //map.fitBounds(leafletGeoJSON.getBounds());
          } else {
            /**
             * Handle Leaflet Polygon
             */
            leafletGeoJSON.eachLayer(layer => {
              geoCoords.properties['leafletId'] = layer._leaflet_id;
              formAreas.push(JSON.stringify(geoCoords));
              //dispatch(loadMapAreaCoords(JSON.stringify(geoCoords)));
              return leafletFG.addLayer(layer);
            });
            boundAreas.push(leafletGeoJSON.getBounds());
            //map.fitBounds(leafletGeoJSON.getBounds());
          }
        });
        dispatch(loadMapAreaCoords(formAreas));
        map.fitBounds(boundAreas);
      }
    }
  };

  return (
    <div>
      <div className="area">
        <div className="map">
          <Map {...externs} />
        </div>
        <div className="addarea">
          <MapAreaForm {...areastore} onChange={handleFormChange} />
        </div>
      </div>
    </div>
  );
};

export default AddArea;
