import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import ContextMenu from '../common/ContextMenu';
import axios from 'axios';
import store from '../../store/store';
import { isEqual } from 'lodash';
import { contextMenu as contexify } from 'react-contexify';
import history from '../../history';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import AreaSubHeader from '../header/AreaSubHeader';
import { setSelectedMenu } from '../../actions/navActions';
import { mapAddonSearch, mapAddonDrawControl } from '../../actions/mapActions';
import {
  getAreas,
  deleteAreas,
  editArea,
  getHomeCode,
  setCreateMapAreaMode,
  setAreasGridChecked,
  saveAreaGridCols
} from '../../actions/areaActions';
import { setMapPosition } from '../../actions/mapActions';
import { showNotification } from '../../actions/notifyActions';
import { showModal } from '../../actions/modalActions';
import { isJsonString } from '../../utils/uitility';

const Area = () => {
  const dispatch = useDispatch();
  const areastore = useSelector(state => state.areas);
  const [currSelRow, setCurrSelRow] = useState(null);

  /**
   * Set menu 'Areas' Selected
   * Get Areas List
   */
  useEffect(() => {
    dispatch(setSelectedMenu('mn-areas'));
    dispatch(getAreas());

    axios
      .get('xt_globe_settings.json')
      .then(function(response) {
        if (typeof response.data === 'string') {
          dispatch(
            showNotification({
              notifyType: 'error',
              notifyProps: {
                title: 'Error',
                message: `Invalid JSON! Cannot parse 'xt_globe_settings.json'`
              }
            })
          );
        } else {
          let currstate = store.getState();
          if (
            !isEqual(
              currstate.maps.mapPosition.center,
              response.data.mapPosition.center
            ) ||
            currstate.areas.homecountry === ''
          ) {
            dispatch(getHomeCode(response.data));
          }
          dispatch(setMapPosition(response.data));
          //dispatch(setBaseMap(null, response.data));
        }
      })
      .catch(function() {
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Invalid JSON! Error parsing 'xt_globe_settings.json'`
            }
          })
        );
      });
  }, [dispatch]);

  const onRowSelected = e => {
    const selectedNodes = e.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    dispatch(setAreasGridChecked(selectedData));
  };

  // open Context menu on left click
  const onCellClicked = e => {
    e.event.preventDefault();
    if (e.colDef.headerName === 'Action') {
      setCurrSelRow(e.data);
      contexify.show({
        id: 'agAreas',
        event: e.event,
        props: { data: e.data }
      });
    }
  };

  const confirmAreaDelete = () => {
    const ids = areastore.agGrid.checkedRows.map(node => node.areaId).join(',');
    dispatch(deleteAreas(ids));
  };

  const externs = {
    /**
     * Pass agGrid store to render context menu items
     */
    gridstore: areastore.agGrid,

    /**
     * Edit disabled rows data
     */
    currSelRow: currSelRow,

    /**
     * Navigate to - Create Area
     * onClick submenu - "Create Area"
     */
    navigateToCreateArea: () => {
      dispatch(setCreateMapAreaMode('create'));
      dispatch(mapAddonSearch(true));
      dispatch(mapAddonDrawControl(true));
      dispatch(setSelectedMenu());
      history.push('/addarea');
    },

    /**
     * Navigate to - Edit Area {id}
     * onClick submenu - "Edit Area"
     */
    navigateToEditArea: e => {
      if (areastore.agGrid.checkedRows.length === 1) {
        const id = areastore.agGrid.checkedRows.map(node => node.areaId);
        dispatch(editArea(id));
      } else if (e.props.data && e.props.data.areaId !== '') {
        dispatch(editArea(e.props.data.areaId));
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please Select any one Area to Edit!`
            }
          })
        );
      }
    },

    /**
     * Delete Area in Grid
     * onClick submenu "Delete Area"
     */
    onAreaDelete: () => {
      if (areastore.agGrid.checkedRows.length) {
        dispatch(
          showModal({
            modalProps: {
              title: 'Confirm Delete?',
              description:
                'Are you sure you want to delete the selected Area(s)?',
              confirmOk: confirmAreaDelete
            },
            modalType: 'confirm'
          })
        );
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please Select Area(s) to Delete!`
            }
          })
        );
      }
    }
  };

  return (
    <React.Fragment>
      <AreaSubHeader {...externs} />
      <ContextMenu {...externs} />
      <div
        style={{
          height: 'calc(100vh - 92px)',
          width: '100%',
          padding: '6px 0 15px',
          backgroundColor: '#f0f2f5'
        }}
        className="ag-theme-balham"
      >
        <AgGridReact
          columnDefs={areastore.agGrid.columnDefs}
          rowData={areastore.areasList}
          floatingFilter={true}
          rowSelection="multiple"
          onRowSelected={onRowSelected}
          onGridReady={e => {
            onRowSelected(e);
          }}
          onColumnResized={e => {
            if (e.source === 'uiColumnDragged') {
              let colstate = e.columnApi.getColumnState();
              dispatch(saveAreaGridCols(colstate));
            }
          }}
          onColumnMoved={e => {
            if (e.source === 'uiColumnDragged') {
              let colstate = e.columnApi.getColumnState();
              dispatch(saveAreaGridCols(colstate));
            }
          }}
          onFirstDataRendered={e => {
            let currstate = store.getState();
            if (currstate.areas.areaGridColState.length) {
              e.columnApi.setColumnState(currstate.areas.areaGridColState);
            } else {
              e.api.sizeColumnsToFit();
            }
          }}
          isRowSelectable={rowNode => {
            let querycount = 0;
            if (
              isJsonString(rowNode.data.geometry) &&
              JSON.parse(rowNode.data.geometry)[0].properties.querycount !==
                undefined
            ) {
              querycount = JSON.parse(rowNode.data.geometry)[0].properties
                .querycount;
            }
            return querycount === undefined || querycount === 0 ? true : false;
          }}
          onCellClicked={onCellClicked}
        />
      </div>
    </React.Fragment>
  );
};

export default Area;
