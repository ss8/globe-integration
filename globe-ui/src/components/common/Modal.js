import React from 'react';
import { Modal as AntModal } from 'antd';
import { useDispatch } from 'react-redux';
import { hideModal } from '../../actions/modalActions';

const Modal = props => {
  const dispatch = useDispatch();
  const { isOpen, modalProps, modalType } = props;

  const onCancel = () => {
    dispatch(hideModal());
  };

  if (modalType === 'confirm') {
    return (
      <AntModal
        title={modalProps.title}
        visible={isOpen}
        closable={false}
        centered={true}
        bodyStyle={{ backgroundColor: '#eee', borderRadius: '8px' }}
        onOk={modalProps.confirmOk}
        onCancel={onCancel}
      >
        <span>{modalProps.description}</span>
      </AntModal>
    );
  } else if (modalType === 'infopop') {
    return (
      <AntModal
        className="aboutpop"
        title={modalProps.title}
        visible={isOpen}
        closable={true}
        centered={true}
        bodyStyle={{ backgroundColor: '#242733' }}
        footer={null}
        onCancel={onCancel}
      >
        <div className="logo_about_ico"></div>
        <span className="aboutrel">{modalProps.description}</span>
      </AntModal>
    );
  } else {
    return null;
  }
};

export default Modal;
