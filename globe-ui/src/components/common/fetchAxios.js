import axios from 'axios';
import store from '../../store/store';
export const { APP_URL } = window;

const fetchAxios = () => {
  const defaultOptions = {
    baseURL: APP_URL,
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  // Set the AUTH token for any request
  instance.interceptors.request.use(function(config) {
    const currstate = store.getState();
    config.headers.Authorization = currstate.auth.token
      ? `Bearer ${currstate.auth.token}`
      : '';
    return config;
  });

  return instance;
};

export default fetchAxios();
