import React from 'react';
import { useSelector } from 'react-redux';
import { Spin, Modal, Button } from 'antd';
import Timer from '../common/Timer';
import { getQueryNameBytype } from '../../utils/uitility';

const Spinner = props => {
  const modalstore = useSelector(state => state.modal);
  const querystore = useSelector(state => state.queries);

  return (
    <Modal
      visible={modalstore.execLoading}
      closable={false}
      footer={null}
      centered={true}
      width={350}
      bodyStyle={{
        backgroundColor: '#eee',
        borderRadius: '8px',
        textAlign: 'center'
      }}
    >
      <Spin tip={modalstore.modalLoadingTip} spinning={true} delay={100} />
      {modalstore.modalLoadingTip === 'Query Execution In Progress...' ||
      modalstore.modalLoadingTip === 'Query Creation In Progress...' ||
      modalstore.modalLoadingTip === 'Query Update in Progress...' ||
      modalstore.modalLoadingTip === 'Tracking is in Progress....' ||
      modalstore.modalLoadingTip === 'Tracking is in Progress...' ||
      modalstore.modalLoadingTip === "Fetching 'HeatMap' details!" ||
      modalstore.modalLoadingTip === "Fetching 'HeatMap' details!." ? (
        <div>
          {querystore.agGrid.checkedRows.length === 1 &&
          (modalstore.modalLoadingTip !== 'Tracking is in Progress....' &&
            modalstore.modalLoadingTip !== 'Tracking is in Progress...' &&
            modalstore.modalLoadingTip !== "Fetching 'HeatMap' details!" &&
            modalstore.modalLoadingTip !== "Fetching 'HeatMap' details!.") ? (
            <div className="query-spinner-txt">
              <span>
                Query Id:&nbsp;{querystore.agGrid.checkedRows[0].queryId}
              </span>
              <br />
              <span>
                Type:&nbsp;
                {getQueryNameBytype(querystore.agGrid.checkedRows[0].type)}
              </span>
            </div>
          ) : null}
          <Timer options={{ delay: 100 }} />
          <Button
            icon="stop"
            type="primary"
            htmlType="button"
            className="execabort"
            onClick={
              modalstore.modalLoadingTip === 'Query Execution In Progress...'
                ? props.abortExecution
                : modalstore.modalLoadingTip ===
                    'Query Creation In Progress...' ||
                  modalstore.modalLoadingTip === 'Query Update in Progress...'
                ? props.abortCreateUpdate
                : modalstore.modalLoadingTip === 'Tracking is in Progress....'
                ? props.abortTrackUpdate
                : modalstore.modalLoadingTip === 'Tracking is in Progress...'
                ? props.abortTrackCreate
                : modalstore.modalLoadingTip === "Fetching 'HeatMap' details!"
                ? props.abortHeatmapCreate
                : props.abortHeatmapUpdate
            }
          >
            Abort{' '}
            {modalstore.modalLoadingTip === 'Query Execution In Progress...'
              ? 'Execution'
              : modalstore.modalLoadingTip === 'Query Creation In Progress...'
              ? 'Create'
              : modalstore.modalLoadingTip === 'Query Update in Progress...'
              ? 'Update'
              : modalstore.modalLoadingTip === 'Tracking is in Progress....' ||
                modalstore.modalLoadingTip === 'Tracking is in Progress...'
              ? 'Tracking'
              : modalstore.modalLoadingTip === "Fetching 'HeatMap' details!" ||
                modalstore.modalLoadingTip === "Fetching 'HeatMap' details!."
              ? 'Fetching Heatmap'
              : ''}
          </Button>
        </div>
      ) : null}
    </Modal>
  );
};
export default Spinner;
