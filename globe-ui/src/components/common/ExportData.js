import React from 'react';
import { Icon } from 'antd';
import store from '../../store/store';

const ExportData = props => {
  const { querydetails, onBtExport } = props;
  let currstate = store.getState();
  let txttime = '';
  if (
    currstate.queries.queryExecInfo.length &&
    currstate.queries.queryExecInfo.find(
      el => el.queryId === querydetails[0].queryId
    ).execTimeHHMMSS
  ) {
    txttime = currstate.queries.queryExecInfo.find(
      el => el.queryId === querydetails[0].queryId
    ).execTimeHHMMSS;
  }
  return (
    <ul className="submenu text-left qrdetails">
      <li>
        Query Id:&nbsp;<span>{querydetails[0].queryId}</span>
      </li>
      <li>
        Query Name:&nbsp;<span>{querydetails[0].queryName}</span>
      </li>
      <li>
        Execution Time:&nbsp;<span>{txttime}</span>
      </li>
      <li>
        Results:&nbsp;
        <span>
          {currstate.analyze.analyzeList[querydetails[0].queryId].length}
        </span>
      </li>
      <li className="float-right export" onClick={onBtExport}>
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="file-add"
          theme="filled"
        />
        <span>EXPORT TO CSV</span>
      </li>
    </ul>
  );
};

export default ExportData;
