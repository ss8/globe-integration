import React, { useEffect, useState } from 'react';
import { Checkbox } from 'antd';
import store from '../../store/store';
import FormItem from 'antd/lib/form/FormItem';

const QueryAccessCheck = props => {
  const [chkstatus, setChkstatus] = useState(true);
  const { fn, qps } = props;
  const currstate = store.getState();
  let currentuser = currstate.auth.username;
  let qowner = document.querySelector('#inpText').getAttribute('data-qowner');

  useEffect(() => {
    if (currentuser === qowner || qowner === '') {
      setChkstatus(false);
    } else {
      setChkstatus(true);
    }
  }, [currentuser, qowner]);

  const checkboxchange = e => {
    fn(e.target.checked);
  };

  return (
    <ul className="realtime">
      <li>
        <FormItem>
          <Checkbox
            checked={qps}
            disabled={chkstatus}
            id="queryAccessChk"
            onChange={e => {
              checkboxchange(e);
            }}
          >
            Public
          </Checkbox>
        </FormItem>
      </li>
    </ul>
  );
};
export default QueryAccessCheck;
