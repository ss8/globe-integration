import React from 'react';
import { Menu, Item } from 'react-contexify';
import { Icon } from 'antd';
import store from '../../store/store';
import uuid from 'uuid';
import 'react-contexify/dist/ReactContexify.min.css';
import { isJsonString } from '../../utils/uitility';

const ContextMenu = props => {
  const { gridstore, currSelRow } = props;
  const fn = {};
  gridstore.contextItems.forEach(menu => {
    fn[menu.funcname] = props[menu.funcname];
  });

  return (
    <Menu id={gridstore.contextId}>
      {gridstore.contextItems.map(menu => (
        <Item
          key={uuid.v4()}
          onClick={fn[menu.funcname]}
          disabled={() => {
            /**
             * Generic Context Menu
             */
            let isAutoupdate = false;
            let isAreaInQuery = false;
            let currstate = store.getState();
            let isSimpleRealTime =
              gridstore.checkedRows.length &&
              gridstore.checkedRows[0].type === 'simplerealtime'
                ? true
                : false;
            let isGeoFenceAlert =
            gridstore.checkedRows.length &&
            gridstore.checkedRows[0].type === 'geofencealert'
              ? true
              : false;                
            if (
              gridstore.checkedRows.length &&
              isJsonString(gridstore.checkedRows[0].criteria)
            ) {
              isAutoupdate =
                JSON.parse(gridstore.checkedRows[0].criteria).realtimeRT !==
                undefined
                  ? true
                  : false;
            }
            if (currSelRow && isJsonString(currSelRow.geometry)) {
              let querycount = JSON.parse(currSelRow.geometry)[0].properties
                .querycount;
              isAreaInQuery =
                querycount === undefined || querycount === 0 ? false : true;
            }
            if (
              (gridstore.checkedRows.length &&
                gridstore.checkedRows[0].status < 1 &&
                menu.item === 'Show Results') ||
              (isAutoupdate && menu.item === 'Show Results') ||
              (isGeoFenceAlert && menu.item === 'Show Results') ||
              (isSimpleRealTime && menu.item === 'Show Results') ||
              (currstate.queries.queryExecInProgress.length > 4 &&
                menu.item === 'Execute') ||
              (isAreaInQuery && menu.item === 'Delete') ||
              (menu.item === 'Delete' &&
                (gridstore.checkedRows.length &&
                  gridstore.checkedRows[0].queryOwner !== undefined &&
                  gridstore.checkedRows[0].queryOwner !==
                    currstate.auth.username))
            ) {
              return true;
            } else {
              return false;
            }
          }}
        >
          <Icon
            theme="filled"
            type={menu.anticon}
            style={{
              fontSize: '16px',
              display: 'inline-block',
              marginRight: '8px',
              marginTop: '3px',
              color: 'rgba(0, 0, 0, 0.65)'
            }}
          />
          {menu.item}
        </Item>
      ))}
    </Menu>
  );
};

export default ContextMenu;
