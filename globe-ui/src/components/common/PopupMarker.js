import React from 'react';
import { Popup } from 'react-leaflet';
import { getLatDirection, getLngDirection } from '../../utils/mapUtils';

const PopupMarker = props => {
  const { popupdata } = props;

  return (
    <Popup>
      {popupdata['title'] && popupdata['title'] !== '' ? (
        <p>{popupdata['title']}</p>
      ) : (
        ''
      )}
      {popupdata['description'] && popupdata['description'] !== '' ? (
        <p>{popupdata['description']}</p>
      ) : (
        ''
      )}
      {popupdata['imsi'] && popupdata['imsi'] !== '' ? (
        <p>IMSI:{popupdata['imsi']}</p>
      ) : (
        ''
      )}
      {popupdata['imei'] && popupdata['imei'] !== '' ? (
        <p>IMEI:{popupdata['imei']}</p>
      ) : (
        ''
      )}
      {popupdata['msisdn'] && popupdata['msisdn'] !== '' ? (
        <p>MSISDN:{popupdata['msisdn']}</p>
      ) : (
        ''
      )}
      {popupdata['liid'] && popupdata['liid'] !== '' ? (
        <p>LIID:{popupdata['liid']}</p>
      ) : (
        ''
      )}
      {popupdata['country'] ? <p>Country:{popupdata['country']}</p> : ''}
      {popupdata['segmentStartTime'] ? (
        <div>
          <p>
            Date:
            {popupdata['segmentStartTime'].split(' ').length > 0
              ? popupdata['segmentStartTime'].split(' ')[0]
              : ''}
          </p>
          <p>
            Time:
            {popupdata['segmentStartTime'].split(' ').length > 0
              ? popupdata['segmentStartTime'].split(' ')[1]
              : ''}
          </p>
        </div>
      ) : (
        ''
      )}
      {popupdata['latitude'] !== '' ? (
        <p>
          {popupdata['latitude']}
          <sup>o</sup> {getLatDirection(popupdata['latitude'])},{' '}
          {popupdata['longitude']}
          <sup>o</sup> {getLngDirection(popupdata['longitude'])}
        </p>
      ) : (
        ''
      )}
    </Popup>
  );
};

export default PopupMarker;
