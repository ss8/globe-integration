import React from 'react';
import { Route } from 'react-router-dom';
import history from '../history';
import { useDispatch } from 'react-redux';
import { Layout } from 'antd';
import AppHeader from './header/AppHeader';
import { userSignedOut } from '../actions/authActions';
const { Content } = Layout;

const ProtectedRoute = ({ component: Component, auth, ...rest }) => {
  const dispatch = useDispatch();
  const currentUrl = window.location.href;
  let activePageClassName = '';
  let splitedUrl = currentUrl.split("/").pop();
  if (splitedUrl === 'queries') {
    activePageClassName = 'remove-panel-scroll';
  }

  return (
    <Route
      {...rest}
      render={props => {
        if (auth.status) {
          return (
            <div className="row content-wrap">
              <Layout style={{ minHeight: '100vh' }} className={activePageClassName}>
                <AppHeader />
                <Content style={{ padding: '0 20px', marginTop: 50 }}>
                  <Component auth={auth} {...props} />
                </Content>
              </Layout>
            </div>
          );
        } else {
          dispatch(userSignedOut());
          history.push('/');
        }
      }}
    />
  );
};

export default ProtectedRoute;
