import React, { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import L from 'leaflet';
import { circle } from '@turf/turf';
import store from '../../store/store';
import { Map as LeafletMap, FeatureGroup, ScaleControl } from 'react-leaflet';
import * as ELG from 'esri-leaflet-geocoder';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css';
import { EditControl } from 'react-leaflet-draw';
import { getShapeType, toggleMarkerButton } from '../../utils/mapUtils';
import { AREA_SQUAREMETERS } from '../../actions/types';
import { setBaseMap, setMapPOI } from '../../actions/mapActions';

/**
 * FIX for Icons missing in react-leaflet-draw
 */
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const Map = props => {
  const dispatch = useDispatch();
  const leafletMap = useRef(null);
  const drawControl = useRef(null);
  const { mapBaseLayer, overlayPOI } = useSelector(state => state.maps);
  const { onMapLoaded, onDrawn, clearDeletedLayers } = props;
  const mapstore = useSelector(state => state.maps);
  /**
   * Set basemaps, overlays from Config!
   */
  let basemaps = {};
  let overlays = {};
  if (mapBaseLayer && mapBaseLayer.length) {
    mapBaseLayer.forEach(function(layer) {
      basemaps[layer.layerName] = L.tileLayer(layer.url, {
        tileSize: layer.tileSize,
        zoomOffset: layer.zoomOffset,
        maxZoom: 20
      });
    });
  }
  if (overlayPOI && overlayPOI.length) {
    let poilist;
    overlayPOI.forEach(function(overlay) {
      poilist = overlay.poi.map(o =>
        L.circle(o.coords, overlay.radius, {
          color: overlay.color
        }).bindPopup(`<p class="poihead">${o.name}</p>
          <p>${o.popmessage}</p>`)
      );
      overlays[overlay.key] = L.layerGroup(poilist);
    });
  }

  useEffect(() => {
    if (Object.keys(basemaps).length === 0 && basemaps.constructor === Object) {
      return;
    }
    const map = leafletMap && leafletMap.current && leafletMap.current.leafletElement;
    if (map !== null) {
      map.on('unload', function () {});
      map.off('baselayerchange').on('baselayerchange', function (e) {
        let currstate = store.getState();
        dispatch(setBaseMap(e.name, currstate.maps));
      });
      map.off('overlayadd').on('overlayadd', function (e) {
        let currstate = store.getState();
        let selpoi = [...currstate.maps.selectedPOI];
        selpoi.push(e.name);
        dispatch(setMapPOI(selpoi));
      });
      map.off('overlayremove').on('overlayremove', function (e) {
        let currstate = store.getState();
        let selpoi = [...currstate.maps.selectedPOI];
        selpoi = selpoi.filter(el => el !== e.name);
        dispatch(setMapPOI(selpoi));
      });
    }
    L.drawLocal.edit.handlers.edit.tooltip.subtext =
      'Click save to update changes, Click cancel to undo changes.';
    L.drawLocal.edit.handlers.remove.tooltip.text =
      'Click on a feature to remove (AND) Click save to update changes.';
  }, [leafletMap, dispatch, basemaps]);

  const drawComplete = geoJSON => {
    if (Object.keys(geoJSON).length === 0 && geoJSON.constructor === Object) {
      onDrawn('');
    } else {
      onDrawn(JSON.stringify(geoJSON));
    }
  };

  const appendLayersOverlay = (map, L) => {
    L.control.layers(basemaps, overlays, { position: 'topleft' }).addTo(map);
  };

  if (Object.keys(basemaps).length === 0 && basemaps.constructor === Object) {
    return (
      <div>
        Map api source missing in Config file (OR) Incorrect Configuration!
      </div>
    );
  }

  return (
    <LeafletMap
      style={{ height: 'calc(100vh - 50px)', width: '100%' }}
      center={mapstore.mapPosition.center}
      zoom={mapstore.mapPosition.zoom}
      maxZoom={20}
      attributionControl={false}
      zoomControl={true}
      doubleClickZoom={true}
      scrollWheelZoom={true}
      dragging={true}
      animate={true}
      easeLinearity={0.35}
      ref={leafletMap}
      layers={[basemaps[mapstore.selectedMapName]]}
      whenReady={e => {
        const map = e.target;
        appendLayersOverlay(map, L);
        ELG.geosearch({ useMapBounds: false, position: 'topleft' }).addTo(map);
        L.layerGroup().addTo(map);
        onMapLoaded(map, L, drawControl);
        map.zoomControl.setPosition('bottomright');
        let currstate = store.getState();
        if (currstate.areas.mapArea.opMode === 'edit') {
          document.querySelector('.leaflet-draw-edit-edit').click();
        }
      }}
    >
      <ScaleControl />
      {true ? (
        <FeatureGroup ref={drawControl}>
          <EditControl
            position="topleft"
            onEdited={e => {
              dispatch({ type: AREA_SQUAREMETERS, payload: '' });
              let currstate = store.getState();
              let areaCoords =
                currstate.areas.mapArea.formFields.areaLayerCoords;
              if (areaCoords.length > 2) {
                toggleMarkerButton(false);
              } else {
                toggleMarkerButton(true);
              }
              e.layers.eachLayer(function(layer) {
                let layerType = getShapeType(layer);
                if (layerType === 'circle') {
                  let center = [layer.getLatLng().lng, layer.getLatLng().lat];
                  let radius = layer.getRadius();
                  let options = {
                    steps: 300,
                    units: 'meters'
                  };
                  let poly = circle(center, radius, options);
                  drawComplete({
                    ...poly,
                    properties: {
                      leafletCircle: layer.toGeoJSON(),
                      radius: radius,
                      leafletId: layer._leaflet_id
                    }
                  });
                  let theRadius = layer.getRadius();
                  let area = (3.14 * theRadius * theRadius) / 1000000; // In Kilometer Square
                  dispatch({ type: AREA_SQUAREMETERS, payload: area });
                } else if (layerType === 'rectangle') {
                  drawComplete({
                    ...layer.toGeoJSON(),
                    properties: {
                      leafletRectangle: true,
                      leafletId: layer._leaflet_id
                    }
                  });
                  let area = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
                  area = area / 1000000; // In Kilometer Square
                  dispatch({ type: AREA_SQUAREMETERS, payload: area });
                } else if (layerType === 'polygon') {
                  drawComplete({
                    ...layer.toGeoJSON(),
                    properties: {
                      leafletPolygon: true,
                      leafletId: layer._leaflet_id
                    }
                  });
                  let area = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
                  area = area / 1000000; // In Kilometer Square
                  dispatch({ type: AREA_SQUAREMETERS, payload: area });
                }
              });
            }}
            onCreated={e => {
              dispatch({ type: AREA_SQUAREMETERS, payload: '' });
              if (e.layerType === 'circle') {
                let center = [e.layer.getLatLng().lng, e.layer.getLatLng().lat];
                let radius = e.layer.getRadius();
                let options = {
                  steps: 300,
                  units: 'meters'
                };
                let poly = circle(center, radius, options);
                drawComplete({
                  ...poly,
                  properties: {
                    leafletCircle: e.layer.toGeoJSON(),
                    radius: radius,
                    leafletId: e.layer._leaflet_id
                  }
                });
                let theRadius = e.layer.getRadius();
                let area = (3.14 * theRadius * theRadius) / 1000000; // In Kilometer Square
                dispatch({ type: AREA_SQUAREMETERS, payload: area });
              } else if (e.layerType === 'rectangle') {
                drawComplete({
                  ...e.layer.toGeoJSON(),
                  properties: {
                    leafletRectangle: true,
                    leafletId: e.layer._leaflet_id
                  }
                });
                let layer = e.layer;
                let area = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
                area = area / 1000000; // In Kilometer Square
                dispatch({ type: AREA_SQUAREMETERS, payload: area });
              } else if (e.layerType === 'polygon') {
                drawComplete({
                  ...e.layer.toGeoJSON(),
                  properties: {
                    leafletPolygon: true,
                    leafletId: e.layer._leaflet_id
                  }
                });
                let layer = e.layer;
                let area = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
                area = area / 1000000; // In Kilometer Square
                dispatch({ type: AREA_SQUAREMETERS, payload: area });
              }
              let currstate = store.getState();
              let areaCoords =
                currstate.areas.mapArea.formFields.areaLayerCoords;
              if (areaCoords.length > 2) {
                toggleMarkerButton(false);
              } else {
                toggleMarkerButton(true);
              }
            }}
            onDeleted={e => {
              let currstate = store.getState();
              let areaCoords =
                currstate.areas.mapArea.formFields.areaLayerCoords;
              if (areaCoords.length) {
                let layers_deleted = [];
                e.layers.eachLayer(function(layer) {
                  areaCoords.forEach(o => {
                    if (
                      layer._leaflet_id === JSON.parse(o).properties.leafletId
                    ) {
                      layers_deleted.push(layer._leaflet_id);
                    }
                  });
                });
                clearDeletedLayers(layers_deleted);
              }
              currstate = store.getState();
              areaCoords = currstate.areas.mapArea.formFields.areaLayerCoords;
              if (areaCoords.length > 2) {
                toggleMarkerButton(false);
              } else {
                toggleMarkerButton(true);
              }
            }}
            draw={{
              marker: false,
              circlemarker: false,
              polyline: false
            }}
          />
        </FeatureGroup>
      ) : (
        ''
      )}
    </LeafletMap>
  );
};

export default Map;
