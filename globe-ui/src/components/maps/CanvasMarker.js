import React, { useEffect } from 'react';
import CanvasMarkersLayer from '../../utils/CanvasMarkersLayer';
import { Marker } from 'react-leaflet';
import PopupMarker from '../common/PopupMarker';
import L from 'leaflet';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';
import 'leaflet.markercluster/dist/leaflet.markercluster';
import { popMarker } from '../../utils/uitility';

const CanvasMarker = props => {
  const { analyzedQuery, markerClick, currentMap } = props;

  useEffect(() => {
    const map = currentMap.current;
    let markerClusters = [];
    markerClusters = L.markerClusterGroup();
    let marker;
    let markerList = [];
    if (analyzedQuery && analyzedQuery.length) {
      for (let i = 0; i < analyzedQuery.length; i++) {
        marker = L.marker(L.latLng(Number(analyzedQuery[i].latitude), Number(analyzedQuery[i].longitude)));
        marker.bindPopup(popMarker(analyzedQuery[i]));
        markerList.push(marker);
      }
    }
    if (map != null) {
      markerClusters.addLayers(markerList);
      map.leafletElement.addLayer(markerClusters);
    }
  });
  if (
    analyzedQuery &&
    analyzedQuery.length === 1 &&
    (analyzedQuery[0].latitude === null || analyzedQuery[0].longitude === null)
  ) {
    return null;
  } else if (analyzedQuery &&
    analyzedQuery.length >= 1
  ) {
    return '';
  }

  return (
    <CanvasMarkersLayer>
      {analyzedQuery.map((data, key) => {
        if (data['latitude'] === null || data['longitude'] === null) {
          return null;
        } else {
          return (
            <Marker
              onClick={markerClick}
              position={[data['latitude'], data['longitude']]}
              key={'list-' + key}
              draggable={true}
            >
              <PopupMarker popupdata={data} />
            </Marker>
          );
        }
      })}
    </CanvasMarkersLayer>
  );
};

export default CanvasMarker;
