import React, { createRef } from 'react';
import L from 'leaflet';
import { Map as LeafletMap, ScaleControl, FeatureGroup } from 'react-leaflet';
import store from '../../store/store';
import 'leaflet/dist/leaflet.css';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';
import 'leaflet.markercluster/dist/leaflet.markercluster';
import { connect } from 'react-redux';
import { popupDetails } from '../../utils/uitility';
import { getGeofenceMapResponse, setBaseMap } from '../../actions/mapActions';

class GeoFenceAlertMap extends React.Component {
    markerClusters = [];

    constructor(){
        super();
        this.state= {
            mapResponse: '',
            mapRef: createRef(),
            drawControl: createRef()
        }
    }

    componentDidMount() {
        this.props.getMapResponse(this.props.query, this.props.currentQueryId);
    }

    render(){
        /* Added Map with marker Clusters*/
        const { onMapLoaded } = this.props;
        const map = this.state.mapRef.current;

        if (this.markerClusters && map != null) {
            map.leafletElement.removeLayer(this.markerClusters);
        }
        this.markerClusters = L.markerClusterGroup({
            chunkedLoading: true
        });
        let markerList = [];
        let allbounds = [];
        if (this.props.mapResponse.length) {
            for (let i = 0; i < this.props.mapResponse.length; i++) {
                let marker = L.marker(L.latLng(Number(this.props.mapResponse[i].eventLocations[0].latitude), Number(this.props.mapResponse[i].eventLocations[0].longitude)));
                marker.bindPopup(popupDetails(this.props.mapResponse[i]));
                markerList.push(marker);
                allbounds.push([this.props.mapResponse[i].eventLocations[0].latitude, this.props.mapResponse[i].eventLocations[0].longitude]);
            }
        }
        if (map != null && markerList.length) {
            this.markerClusters.addLayers(markerList);
            map.leafletElement.addLayer(this.markerClusters);

            if (allbounds.length) {
                setTimeout(() => {
                    map.leafletElement && map.leafletElement.fitBounds(allbounds);
                }, 10);
            }
        }

        let basemaps = {};
        const { mapBaseLayer } = this.props.mapStore;
        if (mapBaseLayer && mapBaseLayer.length) {
            mapBaseLayer.forEach(function (layer) {
                basemaps[layer.layerName] = L.tileLayer(layer.url, {
                    tileSize: layer.tileSize,
                    zoomOffset: layer.zoomOffset,
                    maxZoom: 20
                });
            });
        }

        return(
            <React.Fragment>
                {
                   (mapBaseLayer && mapBaseLayer.length) ?  <LeafletMap
                   style={{ height: 'calc(100vh - 50px)', width: '100%' }}
                   center={this.props.mapStore.mapPosition.center}
                   zoom={this.props.mapStore.mapPosition.zoom}
                   maxZoom={20}
                   attributionControl={false}
                   zoomControl={true}
                   doubleClickZoom={true}
                   scrollWheelZoom={true}
                   dragging={true}
                   animate={true}
                   easeLinearity={0.35}
                   ref={this.state.mapRef}
                   layers={[basemaps[this.props.mapStore.selectedMapName]]}
                   whenReady={e => {
                       const map = e.target;
                       map.zoomControl.setPosition('bottomright');
                       if (!document.getElementsByClassName('leaflet-control-layers leaflet-control').length) {
                           L.control.layers(basemaps, {}, { position: 'topleft' }).addTo(map);
                       }
                       map.off('baselayerchange').on('baselayerchange', (e) => {
                           let currstate = store.getState();
                           this.props.setBaseMap(e.name, currstate.maps);
                       });
                       onMapLoaded(map, L, this.state.drawControl);
                   }}
               >
                   <FeatureGroup ref={this.state.drawControl}></FeatureGroup>
                   <ScaleControl />
                </LeafletMap> : ""
                }
                 
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        query: state.queries.queryExecInfo,
        mapResponse: state.maps.geofenceMapResponse,
        mapStore: state.maps
    }
}

const mapDispatchToProps = {
    getMapResponse: getGeofenceMapResponse,
    setBaseMap: setBaseMap
}

export default connect(mapStateToProps, mapDispatchToProps)(GeoFenceAlertMap);