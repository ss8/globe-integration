import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input, Button, Row, Col, Radio } from 'antd';
import { showNotification } from '../../actions/notifyActions';
import {
  updateUser,
  createUser,
  clearCreatePane
} from '../../actions/adminActions';

const UserForm = Form.create({
  name: 'create_user',
  onValuesChange(props, values) {
    props.onChange(values);
  },
  mapPropsToFields(props) {
    return {
      firstName: Form.createFormField({
        ...props.user.formFields,
        value: props.user.formFields.firstName
      }),
      lastName: Form.createFormField({
        ...props.user.formFields,
        value: props.user.formFields.lastName
      }),
      passWord: Form.createFormField({
        ...props.user.formFields,
        value: props.user.formFields.passWord
      }),
      userRole: Form.createFormField({
        ...props.user.formFields,
        value: props.user.formFields.userRole[0]
      })
    };
  }
})(props => {
  const dispatch = useDispatch();
  const userstore = useSelector(state => state.users);

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        let userId =
          userstore.user.opMode === 'edit'
            ? userstore.agGrid.checkedRows[0].id
            : '';
        const selUser = {
          firstName: values.firstName,
          lastName: values.lastName,
          password: values.passWord,
          roleDtos: (() => {
            return [{ id: values.userRole }];
          })()
        };
        // Require Update - > o.firstName === values.firstName
        if (
          userstore.userList.find(o => o.firstName === values.firstName) ===
            undefined &&
          userstore.user.opMode === 'create'
        ) {
          dispatch(createUser(selUser));
        } else if (userstore.user.opMode === 'edit') {
          dispatch(updateUser({ ...selUser, id: userId }));
        } else {
          dispatch(
            showNotification({
              notifyType: 'warn',
              notifyProps: {
                title: 'Warning',
                message: `User '${values.firstName.toUpperCase()}' already exists!`
              }
            })
          );
        }
      } else {
        return;
      }
    });
  };

  const { getFieldDecorator } = props.form;

  return (
    <Form onSubmit={handleSubmit} className="user-form" layout="inline">
      <Row>
        <Col span={7}>
          <Form.Item label="First Name (Username)">
            {getFieldDecorator('firstName', {
              rules: [
                {
                  required: true,
                  message: 'Please input first name!',
                  whitespace: true
                }
              ]
            })(<Input allowClear placeholder="First Name" />)}
          </Form.Item>
          <Form.Item label="Last Name:">
            {getFieldDecorator('lastName', {
              rules: [
                {
                  required: true,
                  message: 'Please input last name!',
                  whitespace: true
                }
              ]
            })(<Input allowClear placeholder="Last Name" />)}
          </Form.Item>
        </Col>
        <Col span={1}></Col>
        <Col span={7}>
          <Form.Item label="Password:">
            {getFieldDecorator('passWord', {
              rules: [
                {
                  required: true,
                  message: 'Please input password!',
                  whitespace: true
                }
              ]
            })(<Input allowClear type="password" placeholder="Password" />)}
          </Form.Item>
          <Form.Item label="User Role:">
            {getFieldDecorator('userRole', {
              rules: [
                {
                  required: true,
                  message: 'Please select user role!'
                }
              ]
            })(<Radio.Group options={userstore.userRoles}></Radio.Group>)}
          </Form.Item>
        </Col>
        <Col span={1}></Col>
        <Col span={7}></Col>
      </Row>

      <Row style={{ textAlign: 'right' }}>
        <Form.Item style={{ display: 'inline-block', marginBottom: '6px' }}>
          <Button
            icon="close"
            type="primary"
            htmlType="button"
            className="matui"
            onClick={() => {
              dispatch(clearCreatePane());
            }}
          >
            CANCEL
          </Button>
        </Form.Item>
        <Form.Item style={{ display: 'inline-block', marginBottom: '6px' }}>
          <Button
            icon="save"
            type="primary"
            htmlType="submit"
            className="matui"
          >
            SUBMIT
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
});

export default UserForm;
