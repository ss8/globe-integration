import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input, Button, Row } from 'antd';
import history from '../../history';
import { showNotification } from '../../actions/notifyActions';
import { createArea, updateArea } from '../../actions/areaActions';

const { TextArea } = Input;

const MapAreaForm = Form.create({
  name: 'create_area',
  onValuesChange(props, values) {
    props.onChange(values);
  },
  mapPropsToFields(props) {
    return {
      areaName: Form.createFormField({
        ...props.mapArea.formFields,
        value: props.mapArea.formFields.areaName
      }),
      areaDesc: Form.createFormField({
        ...props.mapArea.formFields,
        value: props.mapArea.formFields.areaDesc
      }),
      areacoordinates: Form.createFormField({
        ...props.mapArea.formFields,
        value: props.mapArea.formFields.areaLayerCoords.length
          ? `[${props.mapArea.formFields.areaLayerCoords}]`
          : props.mapArea.formFields.areaLayerCoords
      })
    };
  }
})(props => {
  const dispatch = useDispatch();
  const areastore = useSelector(state => state.areas);

  const handleSubmit = e => {
    e.preventDefault();

    props.form.validateFields((err, values) => {
      if (!err) {
        let areaId =
          areastore.mapArea.opMode === 'edit'
            ? areastore.mapArea.formFields.areaId
            : '';
        let areaNames = areastore.areasList.length
          ? areastore.areasList
              .filter(obj => {
                return obj.areaId !== Number(areaId);
              })
              .map(obj => obj.area_name)
          : [];

        if (areaNames.includes(values.areaName)) {
          dispatch(
            showNotification({
              notifyType: 'warn',
              notifyProps: {
                title: 'Warning',
                message: `Duplicate Area name! Please use another name!`
              }
            })
          );
        } else {
          const coords = JSON.parse(values.areacoordinates);
          coords.map(o => {
            o.geometry.coordinates[0].map(arr => arr.reverse());
            return o;
          });
          const selArea = {
            areaId: areaId,
            area_name: values.areaName,
            description: values.areaDesc,
            geometry: JSON.stringify(coords)
          };
          if (areastore.mapArea.opMode === 'edit') {
            dispatch(updateArea(areaId, selArea));
          } else {
            dispatch(createArea(selArea));
          }
        }
      } else {
        return;
      }
    });
  };

  const { getFieldDecorator } = props.form;

  return (
    <Form
      style={{ width: '280px' }}
      onSubmit={handleSubmit}
      className="area-form"
    >
      <Form.Item label="Area Name">
        {getFieldDecorator('areaName', {
          rules: [
            {
              required: true,
              message: 'Please input area name!',
              whitespace: true
            }
          ]
        })(<Input allowClear placeholder="Area Name" />)}
      </Form.Item>
      <Form.Item label="Area Description">
        {getFieldDecorator('areaDesc', {
          rules: [
            {
              required: true,
              message: 'Please input area description!',
              whitespace: true
            }
          ]
        })(
          <TextArea
            autoSize={{ minRows: 2, maxRows: 6 }}
            placeholder="Description"
          />
        )}
      </Form.Item>
      <Form.Item label="Area GeoJSON">
        {getFieldDecorator('areacoordinates', {
          rules: [{ required: true, message: 'Please draw area!' }]
        })(
          <TextArea
            readOnly={true}
            style={{ backgroundColor: '#e0e0e0' }}
            autoSize={{ minRows: 2, maxRows: 6 }}
            placeholder="Coordinates"
          />
        )}
      </Form.Item>
      <span className="footnoteAreas">
        <i>
          * Maximun 3 polygons.
          <br />* Each not to exceed 100 Square KM.
        </i>
      </span>
      <Row style={{ textAlign: 'right' }}>
        <Form.Item style={{ display: 'inline-block', marginBottom: '6px' }}>
          <Button
            icon="close"
            type="primary"
            htmlType="button"
            className="matui"
            onClick={() => {
              history.push('/areas');
            }}
          >
            CLOSE
          </Button>
        </Form.Item>
        <Form.Item style={{ display: 'inline-block', marginBottom: '6px' }}>
          <Button
            disabled={
              areastore.mapArea.formFields.areaName !== '' &&
              areastore.mapArea.formFields.areaDesc !== '' &&
              areastore.mapArea.formFields.areaLayerCoords.length
                ? false
                : true
            }
            icon="save"
            type="primary"
            htmlType="submit"
            className="matui"
          >
            SUBMIT
          </Button>
        </Form.Item>
      </Row>
    </Form>
  );
});

export default MapAreaForm;
