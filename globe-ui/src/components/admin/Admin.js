import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import ContextMenu from '../common/ContextMenu';
import { contextMenu as contexify } from 'react-contexify';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import AdminSubHeader from '../header/AdminSubHeader';
import store from '../../store/store';
import { setSelectedMenu } from '../../actions/navActions';
import { HIDE_MODAL } from '../../actions/types';
import {
  getUser,
  deleteUser,
  editUser,
  setCreateUserMode,
  loadUserFormFields,
  setUserGridChecked,
  saveUserGridCols,
  getAppStatus
} from '../../actions/adminActions';
import { showNotification } from '../../actions/notifyActions';
import { showModal, execSpinner } from '../../actions/modalActions';
import UserForm from '../forms/UserForm';
import { Tabs, Row, Collapse, Icon } from 'antd';
import Spinner from '../common/Spinner';
const { Panel } = Collapse;
const { TabPane } = Tabs;

const Admin = () => {
  const dispatch = useDispatch();
  const adminstore = useSelector(state => state.users);
  const authstore = useSelector(state => state.auth);

  const handleFormChange = changedFields => {
    let key = Object.keys(changedFields).pop();
    dispatch(loadUserFormFields(key, changedFields[key]));
  };

  /**
   *
   * @param {*} tabkey
   * Call App Monitor Status on tab change
   */
  const handleTabChange = tabkey => {
    if (tabkey === '2') {
      dispatch(getAppStatus());
    }
  };

  /**
   * Set menu 'ADMIN' Selected
   * Get USER List
   */
  useEffect(() => {
    dispatch(setSelectedMenu('mn-admin'));
    dispatch(getUser());
  }, [dispatch]);

  const onRowSelected = e => {
    const selectedNodes = e.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    dispatch(setUserGridChecked(selectedData));
  };

  // open Context menu on left click
  const onCellClicked = e => {
    e.event.preventDefault();
    if (e.colDef.headerName === 'Action') {
      contexify.show({
        id: 'agUser',
        event: e.event
      });
    }
  };

  const confirmUserDelete = () => {
    dispatch({ type: HIDE_MODAL, isOpen: false });
    dispatch(execSpinner(true, 'User Deletion In Progress...'));
    const ids = adminstore.agGrid.checkedRows.map(node => {
      let roleids = node.roleDtos.map(role => {
        return { userRoleId: role.userRoleId };
      });
      return { id: node.id, roleDtos: roleids };
    });
    dispatch(deleteUser(ids));
  };

  const externs = {
    /**
     * Pass agGrid store to render context menu items
     */
    gridstore: adminstore.agGrid,

    /**
     * Navigate to - Create USER
     * onClick submenu - "Create USER"
     */
    onUserCreate: () => {
      dispatch(setCreateUserMode('create'));
    },

    /**
     * Navigate to - Edit USER {id}
     * onClick submenu - "Edit USER"
     */
    onUserEdit: () => {
      if (adminstore.agGrid.checkedRows.length === 1) {
        dispatch(editUser(adminstore.agGrid.checkedRows[0]));
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please Select any one USER to Edit!`
            }
          })
        );
      }
    },

    /**
     * Delete USER in Grid
     * onClick submenu "Delete USER"
     */
    onUserDelete: () => {
      if (adminstore.agGrid.checkedRows.length >= 1) {
        let curruser = false;
        adminstore.agGrid.checkedRows.forEach(node => {
          if (authstore.username === node.firstName) {
            curruser = true;
          }
        });
        if (curruser) {
          dispatch(
            showNotification({
              notifyType: 'warn',
              notifyProps: {
                title: 'Warning',
                message: `Cannot delete currently 'Signed In' user ${authstore.username.toUpperCase()}!`
              }
            })
          );
        } else {
          dispatch(
            showModal({
              modalProps: {
                title: 'Confirm Delete?',
                description:
                  'Are you sure you want to delete the selected User(s)?',
                confirmOk: confirmUserDelete
              },
              modalType: 'confirm'
            })
          );
        }
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `Please Select User(s) to Delete!`
            }
          })
        );
      }
    }
  };

  return (
    <React.Fragment>
      <Tabs
        defaultActiveKey="1"
        className="settingswrap"
        onChange={handleTabChange}
      >
        <TabPane
          tab={
            <span className="tabspan">
              <Icon
                style={{
                  fontSize: '18px',
                  display: 'inline-block',
                  verticalAlign: 'baseline'
                }}
                type="interaction"
              />
              USERS
            </span>
          }
          key="1"
        >
          <AdminSubHeader {...externs} />
          <Collapse
            className="collapsePane createadmin"
            accordion={true}
            activeKey={adminstore.isCreatePaneActive ? '1' : '0'}
          >
            <Panel showArrow={false} key="1">
              <Row>
                <UserForm {...adminstore} onChange={handleFormChange} />
              </Row>
            </Panel>
          </Collapse>
          <ContextMenu {...externs} />
          <div
            style={{
              height: 'calc(100vh - 88px)',
              width: '100%',
              padding: '6px 0 15px',
              backgroundColor: '#f0f2f5'
            }}
            className="ag-theme-balham"
          >
            <Spinner {...externs} />
            <AgGridReact
              columnDefs={adminstore.agGrid.columnDefs}
              rowData={adminstore.userList}
              floatingFilter={true}
              rowSelection="multiple"
              onRowSelected={onRowSelected}
              onGridReady={e => {
                onRowSelected(e);
              }}
              onColumnResized={e => {
                if (e.source === 'uiColumnDragged') {
                  let colstate = e.columnApi.getColumnState();
                  dispatch(saveUserGridCols(colstate));
                }
              }}
              onColumnMoved={e => {
                if (e.source === 'uiColumnDragged') {
                  let colstate = e.columnApi.getColumnState();
                  dispatch(saveUserGridCols(colstate));
                }
              }}
              onFirstDataRendered={e => {
                let currstate = store.getState();
                if (currstate.users.userGridColState.length) {
                  e.columnApi.setColumnState(currstate.users.userGridColState);
                } else {
                  e.api.sizeColumnsToFit();
                }
              }}
              onCellClicked={onCellClicked}
            />
          </div>
        </TabPane>
        <TabPane
          tab={
            <span className="tabspan">
              <Icon
                style={{
                  fontSize: '18px',
                  display: 'inline-block',
                  verticalAlign: 'baseline'
                }}
                type="interaction"
              />
              APP MONITOR
            </span>
          }
          key="2"
        >
          <div
            style={{
              height: 'calc(100vh - 88px)',
              width: '100%',
              padding: '6px 0 15px',
              backgroundColor: '#f0f2f5'
            }}
            className="ag-theme-balham"
          >
            <AgGridReact
              columnDefs={adminstore.appStatusColDefs}
              rowData={adminstore.appStatus}
              floatingFilter={true}
              rowSelection="multiple"
              onFirstDataRendered={e => {
                // let currstate = store.getState();
                // if (currstate.users.userGridColState.length) {
                //   e.columnApi.setColumnState(currstate.users.userGridColState);
                // } else {
                //   e.api.sizeColumnsToFit();
                // }
                e.api.sizeColumnsToFit();
              }}
            />
          </div>
        </TabPane>
      </Tabs>
    </React.Fragment>
  );
};

export default Admin;
