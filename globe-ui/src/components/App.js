import React, { useEffect } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import { useSelector, useDispatch } from 'react-redux';
import { notification } from 'antd';
import Modal from './common/Modal';
import history from '../history';
import Login from './auth/Login';
import Area from './areas/Area';
import AddArea from './areas/AddArea';
import Query from './queries/Query';
import AnalyzeGrid from './grids/analyzeGrid';
import { hideNotification } from '../actions/notifyActions';

import Admin from './admin/Admin';

import { LicenseManager } from 'ag-grid-enterprise';

LicenseManager.setLicenseKey(
  'SS8_Networks_MultiApp_1Devs17_July_2020__MTU5NDk0MDQwMDAwMA==dbf856c42e67d7b8eaad22a5e449b61a'
);

const App = () => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const modalstore = useSelector(state => state.modal);
  const notifystore = useSelector(state => state.notify);

  useEffect(() => {
    /**
     * Notification Messages - Handler
     */
    if (notifystore.isNotifyOpen) {
      notification.destroy();
      notification.open({
        message: `${notifystore.notifyProps.title}`,
        description: `${notifystore.notifyProps.message}`,
        top: 56,
        onClose: () => {
          dispatch(hideNotification());
        },
        style: {
          backgroundColor: '#fcf4e5',
          marginRight: '-4px'
        },
        className: `notify_${notifystore.notifyType}`
      });
    }
  });

  return (
    <Router history={history}>
      <div className="container-fluid">
        <Switch>
          <Route exact path="/" component={Login} />
          <ProtectedRoute exact path="/areas" component={Area} auth={auth} />
          <ProtectedRoute exact path="/admin" component={Admin} auth={auth} />
          <ProtectedRoute
            path={['/addarea', '/editarea']}
            component={AddArea}
            auth={auth}
          />
          <ProtectedRoute exact path="/queries" component={Query} auth={auth} />
          <ProtectedRoute
            exact
            path="/analyzegrid/:id"
            component={AnalyzeGrid}
            auth={auth}
          />
          <Route path="*" component={() => '404 NOT FOUND'} />
        </Switch>
        <Modal {...modalstore} />
      </div>
    </Router>
  );
};

export default App;
