import React from 'react';
import { Menu, Icon } from 'antd';

const AdminSubHeader = props => {
  const { onUserDelete, onUserCreate, onUserEdit, gridstore } = props;

  return (
    <Menu
      className="subMenuHead adminPage"
      theme="light"
      mode="horizontal"
      defaultSelectedKeys={['create-user']}
      style={{
        lineHeight: '30px',
        fontSize: '0.9rem',
        marginTop: '6px',
        marginBottom: '4px'
      }}
    >
      <Menu.Item key="create-user" onClick={onUserCreate}>
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="file-add"
          theme="filled"
        />
        CREATE USER
      </Menu.Item>
      <Menu.Item
        key="delete-user"
        onClick={onUserDelete}
        disabled={gridstore.checkedRows.length > 0 ? false : true}
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="delete"
          theme="filled"
        />
        DELETE USER
      </Menu.Item>
      <Menu.Item
        key="edit-user"
        onClick={onUserEdit}
        disabled={gridstore.checkedRows.length === 1 ? false : true}
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="edit"
          theme="filled"
        />
        EDIT USER
      </Menu.Item>
    </Menu>
  );
};

export default AdminSubHeader;
