import React, { useEffect, useState } from 'react';
import { Menu, Icon } from 'antd';
import store from '../../store/store';

const QuerySubHeader = props => {
  const { onCreateQuery, onDeleteQuery, onEditQuery, gridstore } = props;
  const [deldisable, setDeldisable] = useState(null);
  const currstate = store.getState();
  let currentuser = currstate.auth.username;

  useEffect(() => {
    if (gridstore.checkedRows.length > 0) {
      let flagcntr = 0;
      gridstore.checkedRows.forEach(row => {
        if (row.queryOwner === currentuser) {
          flagcntr++;
        }
      });
      if (gridstore.checkedRows.length === flagcntr) {
        setDeldisable(null);
      } else {
        setDeldisable(true);
      }
    }
  }, [currentuser, gridstore]);

  return (
    <Menu
      className="subMenuHead queriesPage"
      theme="light"
      mode="horizontal"
      style={{
        lineHeight: '30px',
        fontSize: '0.9rem',
        marginTop: '6px',
        marginBottom: '4px'
      }}
    >
      <Menu.Item key="create-query" onClick={onCreateQuery}>
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="file-add"
          theme="filled"
        />
        CREATE QUERY
      </Menu.Item>
      <Menu.Item
        key="delete-query"
        onClick={onDeleteQuery}
        disabled={
          gridstore.checkedRows.length > 0
            ? deldisable !== null && deldisable
              ? true
              : false
            : true
        }
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="delete"
          theme="filled"
        />
        DELETE QUERY
      </Menu.Item>
      <Menu.Item
        key="edit-query"
        onClick={onEditQuery}
        disabled={gridstore.checkedRows.length === 1 ? false : true}
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="edit"
          theme="filled"
        />
        EDIT QUERY
      </Menu.Item>
    </Menu>
  );
};

export default QuerySubHeader;
