import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import history from '../../history';
import { userSignedOut } from '../../actions/authActions';
import { showModal } from '../../actions/modalActions';
export const { APP_RELEASE_VER } = window;
const { Header } = Layout;
const { SubMenu } = Menu;

const AppHeader = props => {
  const username = useSelector(state => state.auth.username);
  const userroles = useSelector(state => state.auth.roles);
  const selectedmenu = useSelector(state => state.nav.selectedmenu);
  const dispatch = useDispatch();
  const isAdmin = userroles.indexOf('ROLE_ADMIN') > -1 ? true : false;
  const signout = () => {
    dispatch(userSignedOut());
    history.push('/');
  };

  const about = () => {
    dispatch(
      showModal({
        modalProps: {
          title: 'About Intellego XT Globe',
          description: `Release ${APP_RELEASE_VER}`
        },
        modalType: 'infopop'
      })
    );
  };

  const navigateToAreas = () => {
    history.push('/areas');
  };

  const navigateToQueries = () => {
    history.push('/queries');
  };

  const navigateToAdmin = () => {
    history.push('/admin');
  };

  return (
    <Header
      style={{
        position: 'fixed',
        zIndex: 1099,
        width: '100%',
        height: '50px',
        backgroundColor: '#242733'
      }}
    >
      <div className="logo">&nbsp;</div>
      <Menu
        theme="dark"
        mode="horizontal"
        selectedKeys={selectedmenu}
        style={{ lineHeight: '38px', fontSize: '0.98rem', marginTop: '6px' }}
      >
        <Menu.Item
          key="mn-areas"
          onClick={navigateToAreas}
          className="themed-menu menu-1"
        >
          <Icon
            style={{
              fontSize: '22px',
              display: 'inline-block',
              verticalAlign: 'baseline'
            }}
            type="form"
          />
          AREAS
        </Menu.Item>
        <Menu.Item
          key="mn-queries"
          onClick={navigateToQueries}
          className="themed-menu"
        >
          <Icon
            style={{
              fontSize: '22px',
              display: 'inline-block',
              verticalAlign: 'baseline'
            }}
            type="file-search"
          />
          QUERIES
        </Menu.Item>
        {isAdmin ? (
          <Menu.Item
            key="mn-admin"
            className="themed-menu"
            onClick={navigateToAdmin}
          >
            <Icon
              style={{
                fontSize: '22px',
                display: 'inline-block',
                verticalAlign: 'baseline'
              }}
              type="usergroup-add"
            />
            ADMIN
          </Menu.Item>
        ) : null}
        <SubMenu
          className="themed-menu right-menu"
          title={
            <span className="submenu-title-wrapper">
              <Icon
                type="menu"
                style={{
                  fontSize: '22px',
                  display: 'inline-block',
                  verticalAlign: 'baseline'
                }}
              />
            </span>
          }
        >
          <Menu.Item
            onClick={signout}
            key="mn-logout"
            className="themed-menu"
            title="Sign Out"
          >
            <Icon
              style={{
                fontSize: '22px',
                display: 'inline-block',
                verticalAlign: 'baseline'
              }}
              type="logout"
            />
            Sign Out
          </Menu.Item>
          <Menu.Item
            onClick={about}
            key="mn-about"
            className="themed-menu"
            title="About Intellego XT Globe"
          >
            <div className="logo_ico"></div>
            <span className="aboutlnk">About Intellego XT Globe</span>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key="mn-user" className="themed-menu right-menu user">
          <span>{username.toUpperCase()}</span>
          <Icon
            style={{
              fontSize: '18px',
              display: 'inline-block',
              verticalAlign: 'baseline',
              marginLeft: '6px'
            }}
            type="user"
          />
        </Menu.Item>
      </Menu>
    </Header>
  );
};

export default AppHeader;
