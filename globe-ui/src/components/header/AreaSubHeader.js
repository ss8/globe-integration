import React from 'react';
import { Menu, Icon } from 'antd';

const AreaSubHeader = props => {
  const {
    onAreaDelete,
    navigateToCreateArea,
    navigateToEditArea,
    gridstore
  } = props;

  return (
    <Menu
      className="subMenuHead areasPage"
      theme="light"
      mode="horizontal"
      defaultSelectedKeys={['create-area']}
      style={{
        lineHeight: '30px',
        fontSize: '0.9rem',
        marginTop: '6px',
        marginBottom: '4px'
      }}
    >
      <Menu.Item key="create-area" onClick={navigateToCreateArea}>
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="file-add"
          theme="filled"
        />
        CREATE AREA
      </Menu.Item>
      <Menu.Item
        key="delete-area"
        onClick={onAreaDelete}
        disabled={gridstore.checkedRows.length > 0 ? false : true}
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="delete"
          theme="filled"
        />
        DELETE AREA
      </Menu.Item>
      <Menu.Item
        key="edit-area"
        onClick={navigateToEditArea}
        disabled={gridstore.checkedRows.length === 1 ? false : true}
      >
        <Icon
          style={{
            fontSize: '18px',
            display: 'inline-block',
            verticalAlign: 'baseline'
          }}
          type="edit"
          theme="filled"
        />
        EDIT AREA
      </Menu.Item>
    </Menu>
  );
};

export default AreaSubHeader;
