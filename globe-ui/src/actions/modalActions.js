import { SHOW_MODAL, HIDE_MODAL, EXEC_SPINNING } from './types';
/**
 * Action -> Show Modal
 */
const showModal = ({ modalProps, modalType }) => dispatch => {
  dispatch({
    type: SHOW_MODAL,
    payload: { isOpen: true, modalProps, modalType }
  });
};
/**
 * Action -> Hide Modal
 */
const hideModal = () => dispatch => {
  dispatch({ type: HIDE_MODAL, isOpen: false });
};

/**
 * Action -> Execution Spinner
 */
const execSpinner = (bool, tip) => dispatch => {
  dispatch({ type: EXEC_SPINNING, payload: { bool: bool, tip: tip } });
};

export { showModal, hideModal, execSpinner };
