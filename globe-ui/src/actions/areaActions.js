import fetchAxios from '../components/common/fetchAxios';
import {
  LOAD_AREASLIST,
  DELETE_AREA,
  LOAD_MAP_AREA_DRAWN_COORDS,
  LOAD_MAP_AREA_FORM_FIELDS,
  CREATE_MAP_AREA_MODE,
  EDIT_MAP_AREA,
  AREA_GRID_CHECKED,
  HIDE_MODAL,
  DELETE_LAYERS,
  SET_HOMECOUNTRYCODE,
  SET_TIMEZONE,
  SAVE_AREAGRID_STATE
} from './types';
import { userSignedOut } from './authActions';
import history from '../history';
import { showNotification } from '../actions/notifyActions';
import { setSelectedMenu } from '../actions/navActions';
import { mapAddonSearch, mapAddonDrawControl } from '../actions/mapActions';
import store from '../store/store';
import globalconfig from '../config/globalconfig';
import tz_lookup from 'tz-lookup';

/**
 * Action -> Get Home Country Code
 */
const getHomeCode = mapconfig => dispatch => {
  let homeCoords = mapconfig.mapPosition.center;
  let tz = tz_lookup(homeCoords[0], homeCoords[1]);
  if (tz !== undefined || tz !== null) {
    dispatch({
      type: SET_TIMEZONE,
      payload: tz
    });
  }
  let url = `https://nominatim.openstreetmap.org/reverse?format=json&lat=${
    homeCoords[0]
  }&lon=${homeCoords[1]}`;
  fetchAxios
    .get(url)
    .then(result => {
      dispatch({
        type: SET_HOMECOUNTRYCODE,
        payload: result.data.address.country_code
      });
    })
    .catch(error => {});
};

/**
 * Action -> Load Areas List
 */

const getAreas = () => dispatch => {
  fetchAxios({
    url: `slpareas/areas`,
    method: 'get'
  })
    .then(res => {
      dispatch({ type: LOAD_AREASLIST, payload: res });
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error fetching Areas! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Delete Areas
 * @param {*} ids
 */
const deleteAreas = ids => dispatch => {
  fetchAxios({ url: `slpareas/areas/bulk_delete?ids=${ids}`, method: 'delete' })
    .then(res => {
      dispatch({ type: DELETE_AREA, payload: res });
      dispatch({ type: HIDE_MODAL, isOpen: false });
      dispatch(
        showNotification({
          notifyType: 'success',
          notifyProps: {
            title: 'Success',
            message: `Area(s) deleted Successfully!`
          }
        })
      );
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error deleting Areas! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Create Map Area
 * @param {*} areaJson
 */
const createArea = areaJson => dispatch => {
  let currstate = store.getState();
  if (currstate.areas.areasquaremeters < globalconfig.minarea) {
    fetchAxios({
      url: `slpareas/slpaddareas`,
      method: 'post',
      data: areaJson
    })
      .then(() => {
        history.push('/areas');
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `Area created Successfully!`
            }
          })
        );
      })
      .catch(err => {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error creating Area! [${err.message}]`
            }
          })
        );
      });
  } else {
    dispatch(
      showNotification({
        notifyType: 'warn',
        notifyProps: {
          title: 'Warning',
          message: `Created Polygon(s) is ${currstate.areas.areasquaremeters} Square KM. It should not exceed 100 Square KM.`
        }
      })
    );
  }
};

/**
 * Action -> Update Map Area
 * @param {*} id
 * @param {*} areaJson
 */
const updateArea = (id, areaJson) => dispatch => {
  let currstate = store.getState();
  if (currstate.areas.areasquaremeters < globalconfig.minarea) {
    fetchAxios({ url: `slpareas/areas/${id}`, method: 'put', data: areaJson })
      .then(() => {
        history.push('/areas');
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `Area Updated Successfully!`
            }
          })
        );
      })
      .catch(err => {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error updating Area! [${err.message}]`
            }
          })
        );
      });
  } else {
    dispatch(
      showNotification({
        notifyType: 'warn',
        notifyProps: {
          title: 'Warning',
          message: `Created Polygon(s) is ${currstate.areas.areasquaremeters} Square KM. It should not exceed 100 Square KM.`
        }
      })
    );
  }
};

/**
 * Action -> Edit Map Area
 * @param {*} id
 */
const editArea = id => dispatch => {
  fetchAxios({ url: `slpareas/areas_id?id=${id}`, method: 'get' })
    .then(res => {
      const selArea = res.data[0];
      const coords = JSON.parse(selArea.geometry);
      let areaLayerCoords = coords.map(o => {
        o.geometry.coordinates[0].map(arr => arr.reverse());
        return JSON.stringify(o);
      });
      dispatch({
        type: EDIT_MAP_AREA,
        payload: {
          opMode: 'edit',
          formFields: {
            areaId: selArea.areaId,
            areaName: selArea.area_name,
            areaDesc: selArea.description,
            areaLayerCoords: areaLayerCoords
          }
        }
      });
      dispatch(mapAddonSearch(true));
      dispatch(mapAddonDrawControl(true));
      dispatch(setSelectedMenu());
      history.push(`/editarea/${id}`);
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error editing Area! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Save area grid columns state
 * @param {*} savedcols
 */
const saveAreaGridCols = savedcols => dispatch => {
  dispatch({
    type: SAVE_AREAGRID_STATE,
    payload: savedcols
  });
};

/**
 * Clear Deleted Layers
 * @param {*} layers - leaflet_id
 */
const layersDeleted = layers => dispatch => {
  dispatch({
    type: DELETE_LAYERS,
    payload: layers
  });
};

/**
 * Load Map area coordinates
 * @param {*} coords
 */
const loadMapAreaCoords = coords => dispatch => {
  dispatch({
    type: LOAD_MAP_AREA_DRAWN_COORDS,
    payload: { areaLayerCoords: coords }
  });
};

/**
 * Load Map Area form fields
 * @param {*} areaName
 * @param {*} areaDesc
 */
const loadMapAreaFormFields = (field, value) => dispatch => {
  dispatch({
    type: LOAD_MAP_AREA_FORM_FIELDS,
    payload: { field: field, value: value }
  });
};

/**
 * Create Map Area - mode
 * @param {*} mode
 */
const setCreateMapAreaMode = mode => dispatch => {
  dispatch({
    type: CREATE_MAP_AREA_MODE,
    payload: { opMode: mode }
  });
};

/**
 * agGrid set checked rows in store
 * @param {*} nodes
 */
const setAreasGridChecked = nodes => dispatch => {
  dispatch({ type: AREA_GRID_CHECKED, payload: { checkedRows: nodes } });
};

export {
  getAreas,
  loadMapAreaCoords,
  layersDeleted,
  loadMapAreaFormFields,
  createArea,
  updateArea,
  editArea,
  deleteAreas,
  setAreasGridChecked,
  setCreateMapAreaMode,
  getHomeCode,
  saveAreaGridCols
};
