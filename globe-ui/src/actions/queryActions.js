import axios from 'axios';
import fetchAxios from '../components/common/fetchAxios';
import store from '../store/store';
import { isCorrectDateFormat } from '../utils/uitility';
import { execSpinner } from '../actions/modalActions';
import { userSignedOut } from './authActions';
import {
  LOAD_QUERIESLIST,
  LOAD_QUERIES_FORM_FIELDS,
  LOAD_QUERY_RULES_FORM_FIELDS,
  QUERY_GRID_CHECKED,
  TOGGLE_CREATE_QUERY_PANE,
  QUERY_EXEC_STATUS,
  ANALYZE_GRID_COLNAMES,
  CLEAR_ANALYZEDATA,
  LOAD_ANALYZELIST,
  LOAD_ANALYZEAREA,
  EXEC_SPINNING,
  REALTIME_POOL_ID,
  QUERY_HEATMAP_CHILDID,
  QUERY_EXEC_INFO,
  REALTIME_POOL_ALL_ID,
  TRACK_PARAMS,
  QUERY_EXECUTION_START,
  QUERY_EXECUTION_END,
  SAVE_QUERYGRID_STATE,
  LOAD_RETRO_DATASOURCES,
  LOAD_INTERCEPT_NAMES
} from './types';
import { showNotification } from '../actions/notifyActions';
import history from '../history';
import {
  agSgfListColDefs,
  agRdListColDefs,
  agSSListColDefs,
  agSRListColDefs,
  agMeetingListColDefs,
  agListColDefs,
  agTDListColDefs,
  agSHListColDefs
} from '../config/analyzeAgGridHeaders';
import moment from 'moment-timezone';
import { trackSubscriber, getHeatMapData } from '../actions/analyzeActions';
import { isJsonString } from '../utils/uitility';
export const { RETRO_URL } = window;

/**
 * Action -> Load Queries List
 */
const getQueries = (qmode, ids) => dispatch => {
  let currstate = store.getState();
  fetchAxios({
    url: `slpqueries/queriesu/${currstate.auth.username}`,
    method: 'get'
  })
    .then(res => {
      let newdata = {};
      newdata.data = res.data.filter(dataitem => {
        return dataitem.type !== 'heatmap' && dataitem.type !== 'tracking';
      });
      let fmt = 'YYYY-MM-DD HH:mm:ss';
      let currstate = store.getState();
      let zone = currstate.areas.timezone;
      newdata.data.map(qrow => {
        if (isJsonString(qrow.criteria)) {
          let critjson = JSON.parse(qrow.criteria);
          let fromdate = critjson.starttime;
          let todate = critjson.endtime;
          critjson.starttime = moment
            .utc(fromdate)
            .tz(zone)
            .format(fmt);
          critjson.endtime = moment
            .utc(todate)
            .tz(zone)
            .format(fmt);
          if (
            critjson.expression !== undefined &&
            qrow.type === 'simpleHistory'
          ) {
            let expr = critjson.expression;
            let exparts = expr.split("'");
            exparts[1] = `'${critjson.starttime}'`;
            exparts[3] = `'${critjson.endtime}'`;
            critjson.expression = exparts.join('');
          }
          qrow.criteria = JSON.stringify(critjson);
        } else {
          let inter_criteria = qrow.criteria
            .replace(/\|AND\|/g, ',|AND|,')
            .replace(/\|OR\|/g, ',|OR|,')
            .split(',');
          if (inter_criteria.length) {
            inter_criteria.forEach((str, i) => {
              if (isCorrectDateFormat(str, 'YYYY-MM-DD HH:mm:ss')) {
                inter_criteria[i] = moment
                  .utc(str)
                  .tz(zone)
                  .format(fmt);
              }
            });
            qrow.criteria = inter_criteria
              .join(',')
              .replace(/,\|AND\|,/g, '|AND|')
              .replace(/,\|OR\|,/g, '|OR|');
          }
        }
        return qrow;
      });
      dispatch({ type: LOAD_QUERIESLIST, payload: newdata });
      if (qmode && qmode === 'create') {
        dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `Query created Successfully! (Query ID : ${ids})`
            }
          })
        );
      }
      if (qmode && qmode === 'update') {
        dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
        dispatch({
          type: QUERY_EXEC_STATUS,
          payload: { status: '', queryId: ids }
        });
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `Query Updated Successfully! (Query ID : ${ids})`
            }
          })
        );
      }
      if (qmode && qmode === 'delete') {
        dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `Query(s) deleted Successfully! (Query ID(s) : ${ids})`
            }
          })
        );
      }
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error fetching Queries! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Create Query
 * @param {*} queryJson
 */
const createQuery = (queryJson, source) => dispatch => {
  fetchAxios({
    url: `slpqueries/queries`,
    method: 'post',
    data: queryJson,
    cancelToken: source.token
  })
    .then(res => {
      dispatch(getQueries('create', res.data.queryId));
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error creating Query! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Update Query
 * @param {*} id
 * @param {*} queryJson
 */
const updateQuery = (id, queryJson, source) => dispatch => {
  fetchAxios({
    url: `slpqueries/queries/${id}`,
    method: 'put',
    data: queryJson,
    cancelToken: source.token
  })
    .then(() => {
      dispatch(getQueries('update', id));
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error updating Query! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Delete Queries
 * @param {*} ids
 */
const deleteQueries = (ids, userName) => dispatch => {
  fetchAxios({ url: `slpqueries/queries/id/${ids}/user/${userName}`, method: 'delete' })
    .then(res => {
      //dispatch({ type: DELETE_QUERY, payload: res });
      dispatch({ type: QUERY_GRID_CHECKED, payload: { checkedRows: [] } });
      dispatch(getQueries('delete', ids)); // Fetching once again as 'Delete' response is empty
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error deleting Query(s)! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Get Retrospective Data Sources
 */
const getRetroDataSources = () => dispatch => {
  axios({ url: `${RETRO_URL}/retrospective-query/data-sources`, method: 'get' })
    .then(res => {
      dispatch({ type: LOAD_RETRO_DATASOURCES, payload: res.data });
    })
    .catch(err => {});
};

/**
 * Action -> Get Intercept names
 */
const getRetroInterceptNames = () => dispatch => {
  axios({ url: `${RETRO_URL}/retrospective-query/intercept-names`, method: 'get' })
    .then(res => {
      dispatch({ type: LOAD_INTERCEPT_NAMES, payload: res.data });
    })
    .catch(err => {});
};

const loadQueriesFormFields = (field, value) => dispatch => {
  dispatch({
    type: LOAD_QUERIES_FORM_FIELDS,
    payload: { field: field, value: value }
  });
};

const loadQueryRulesFormFields = (field, value, index) => dispatch => {
  dispatch({
    type: LOAD_QUERY_RULES_FORM_FIELDS,
    payload: { field: field, value: value, index: index }
  });
};

const toggleCreatePane = (bool, opMode) => dispatch => {
  dispatch({ type: TOGGLE_CREATE_QUERY_PANE, payload: { bool, opMode } });
};

/**
 * agGrid set checked rows in store
 * @param {*} nodes
 */
const setQueriesGridChecked = nodes => dispatch => {
  dispatch({ type: QUERY_GRID_CHECKED, payload: { checkedRows: nodes } });
};

const realTimePooling = query => dispatch => {
  let criteria = JSON.parse(query.criteria);
  if (
    !criteria.interval ||
    (criteria.interval && isNaN(Number(criteria.interval)))
  ) {
    return;
  }

  let interval = Number(criteria.interval) * 60 * 1000;
  let poolvar = {};
  let watchtimer = {};
  let endTime = criteria.starttime;
  let currstate = store.getState();
  if (currstate.analyze.analyseRealTimePool.id !== null) {
    clearInterval(currstate.analyze.analyseRealTimePool.id);
  }
  if (currstate.analyze.analyseRealTimePool.timerid !== null) {
    clearInterval(currstate.analyze.analyseRealTimePool.timerid);
  }
  /**
   * Default call with same start time, end time
   *
   */
  fetchAxios({
    url: `slprealtimereport/slprealtimereport/${query.queryId}`,
    method: 'get'
  })
    .then(results => {
      let analyzeobj = {};
      let res = [];
      let msidtype = '';
      if (results.data.length) {
        msidtype = Object.keys(results.data[0]).filter(o => {
          return results.data[0][o] !== null && o !== 'geometry';
        });
        if (
          msidtype.length &&
          (msidtype[0] === 'msisdn' ||
            msidtype[0] === 'imei' ||
            msidtype[0] === 'imsi')
        ) {
          let msids = {};
          res = results.data[0].geometry.map(o => {
            msidtype.forEach(type => {
              msids[type] = results.data[0][type];
            });
            return { ...o, ...msids };
          });
        }
      }
      analyzeobj[query.queryId] = res;
      dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
      let maxtime = interval / 1000 - 1;
      watchtimer[query.queryId] = setInterval(function tock() {
        if (maxtime) {
          dispatch({
            type: REALTIME_POOL_ID,
            payload: {
              timer: maxtime,
              timerid: watchtimer[query.queryId]
            }
          });
          maxtime--;
        } else {
          clearInterval(watchtimer[query.queryId]);
        }
      }, 1000);
      dispatch({
        type: REALTIME_POOL_ID,
        payload: {
          timerid: watchtimer[query.queryId]
        }
      });
    })
    .catch(err => {
      //Error in Pooling requests...Nothing to notify on error....
    });
  /**
   * End Default Call
   */

  poolvar[query.queryId] = setInterval(function tick() {
    endTime = moment
      .tz(endTime, 'America/Los_Angeles')
      .add(interval, 'ms')
      .format('YYYY-MM-DD HH:mm:ss');
    if (
      moment
        .tz(criteria.endtime, 'America/Los_Angeles')
        .diff(moment.tz('America/Los_Angeles')) >= 0
    ) {
      fetchAxios({
        url: `slprealtimereport/slprealtimereport/${query.queryId}`,
        method: 'get'
      })
        .then(results => {
          let analyzeobj = {};
          let res = [];
          let msidtype = '';
          if (results.data.length) {
            msidtype = Object.keys(results.data[0]).filter(o => {
              return results.data[0][o] !== null && o !== 'geometry';
            });
            if (
              msidtype.length &&
              (msidtype[0] === 'msisdn' ||
                msidtype[0] === 'imei' ||
                msidtype[0] === 'imsi')
            ) {
              let msids = {};
              res = results.data[0].geometry.map(o => {
                msidtype.forEach(type => {
                  msids[type] = results.data[0][type];
                });
                return { ...o, ...msids };
              });
            }
          }
          analyzeobj[query.queryId] = res;
          dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
          /**
           * <<<<<<< Display Timer
           */
          currstate = store.getState();
          if (currstate.analyze.analyseRealTimePool.timerid !== null) {
            clearInterval(currstate.analyze.analyseRealTimePool.timerid);
          }
          let maxtime = interval / 1000 - 1;
          watchtimer[query.queryId] = setInterval(function tock() {
            if (maxtime) {
              dispatch({
                type: REALTIME_POOL_ID,
                payload: {
                  timer: maxtime,
                  timerid: watchtimer[query.queryId]
                }
              });
              maxtime--;
            } else {
              clearInterval(watchtimer[query.queryId]);
            }
          }, 1000);
          /**
           *  Display Timer >>>>>>>
           */
        })
        .catch(err => {
          //Error in Pooling requests...Nothing to notify on error....
        });
    } else {
      clearInterval(poolvar[query.queryId]);
      dispatch({
        type: REALTIME_POOL_ID,
        payload: { id: null, timerid: null }
      });
    }
  }, interval);

  dispatch({
    type: REALTIME_POOL_ID,
    payload: {
      id: poolvar[query.queryId],
      interval: interval / 1000
    }
  });
};

const realTimePoolingAll = query => dispatch => {
  if (!isJsonString(query.criteria)) {
    return;
  }

  let currstate = store.getState();
  let criteria = JSON.parse(query.criteria);

  if (!criteria.intervalRT) {
    return;
  }

  let intervalRTval =
    currstate.autoupdate.autoupdateList.find(o => o.type === 'interval')[
      criteria.intervalRT.split('|')[0]
    ] || null;
  let iterationRTval =
    currstate.autoupdate.autoupdateList.find(o => o.type === 'iteration')[
      criteria.iterationRT.split('|')[0]
    ] || null;
  let durationRTval =
    currstate.autoupdate.autoupdateList.find(o => o.type === 'duration')[
      criteria.durationRT.split('|')[0]
    ] || null;

  if (
    intervalRTval === null ||
    iterationRTval === null ||
    durationRTval === null
  ) {
    return;
  }

  let interval = Number(intervalRTval) * 60 * 1000;
  let iteration = Number(iterationRTval);
  let watchtimer = {};
  let poolvar = {};

  if (currstate.analyze.analyseRealTimePoolAll.id !== null) {
    clearInterval(currstate.analyze.analyseRealTimePoolAll.id);
  }
  if (currstate.analyze.analyseRealTimePoolAll.timerid !== null) {
    clearInterval(currstate.analyze.analyseRealTimePoolAll.timerid);
  }

  /**
   * <<<<<<< Default Display Timer
   */

  let maxtime = interval / 1000 - 1;
  watchtimer[query.queryId] = setInterval(function tock() {
    if (maxtime) {
      dispatch({
        type: REALTIME_POOL_ALL_ID,
        payload: {
          timer: maxtime,
          timerid: watchtimer[query.queryId]
        }
      });
      maxtime--;
    } else {
      clearInterval(watchtimer[query.queryId]);
    }
  }, 1000);
  /**
   *  Display Timer >>>>>>>
   */

  poolvar[query.queryId] = setInterval(function tick() {
    iteration--;
    let urlparam = getUrlParambyQueryType(query.type);
    let url = `${urlparam}/${query.queryId}/${query.type}`;
    if (query.type === 'traveldetection') {
      let criteria = JSON.parse(query.criteria);
      if (
        criteria.hasOwnProperty('imsi') ||
        criteria.hasOwnProperty('imei') ||
        criteria.hasOwnProperty('msisdn') ||
        criteria.hasOwnProperty('liid')
      ) {
        url = `${urlparam}/${query.queryId}/getgivensubscribertraveldetection`;
      } else {
        url = `${urlparam}/${query.queryId}/getsubscribertraveldetection`;
      }
    }
    let ctria = JSON.parse(query.criteria);
    ctria.endtime = moment().format('YYYY-MM-DD HH:mm:ss');
    ctria.starttime = moment()
      .add(-durationRTval, 'minutes')
      .format('YYYY-MM-DD HH:mm:ss');

    let queryJson = {
      queryId: query.queryId,
      queryName: query.queryName,
      queryDescription: query.queryDescription,
      criteria: JSON.stringify(ctria),
      createDate: '',
      updateDate: '',
      type: query.type,
      timeRange: '',
      queryOwner: query.queryOwner,
      publicQueryAccess: query.publicQueryAccess,
      modifiedBy: currstate.auth.username
    };
    dispatch({
      type: EXEC_SPINNING,
      payload: { bool: true, tip: 'Auto Update in Progress...' }
    });

    fetchAxios({
      url: `slpqueries/queries/${query.queryId}`,
      //url: `http://localhost:3000/jsonStubs/traveldetection.json`,
      method: 'put',
      //method: 'get'
      data: queryJson
    })
      .then(() => {
        //url = 'http://localhost:3000/jsonStubs/traveldetection.json';
        fetchAxios({ url: url, method: 'get' })
          .then(res => {
            let analyzeobj = {};
            analyzeobj[query.queryId] = res.data;
            //if (res.data.length > 0) {
            dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
            //}
            /**
             * <<<<<<< Display Timer
             */
            currstate = store.getState();
            if (currstate.analyze.analyseRealTimePoolAll.timerid !== null) {
              clearInterval(currstate.analyze.analyseRealTimePoolAll.timerid);
            }
            let maxtime = interval / 1000 - 1;
            watchtimer[query.queryId] = setInterval(function tock() {
              if (maxtime) {
                dispatch({
                  type: REALTIME_POOL_ALL_ID,
                  payload: {
                    timer: maxtime,
                    timerid: watchtimer[query.queryId]
                  }
                });
                maxtime--;
              } else {
                clearInterval(watchtimer[query.queryId]);
              }
            }, 1000);
            /**
             *  Display Timer >>>>>>>
             */
            dispatch({
              type: EXEC_SPINNING,
              payload: { bool: false, tip: '' }
            });
          })
          .catch(err => {
            //Error in Pooling requests...Nothing to notify on error....
            dispatch({
              type: EXEC_SPINNING,
              payload: { bool: false, tip: '' }
            });
          });
      })
      .catch(err => {
        dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
      });
    if (iteration === 0) {
      clearInterval(poolvar[query.queryId]);
      dispatch({
        type: REALTIME_POOL_ALL_ID,
        payload: { id: null, timerid: null }
      });
    }
    dispatch({
      type: REALTIME_POOL_ALL_ID,
      payload: {
        iteration: iteration
      }
    });
  }, interval);

  dispatch({
    type: REALTIME_POOL_ALL_ID,
    payload: {
      id: poolvar[query.queryId],
      interval: interval / 1000,
      iteration: iteration
    }
  });
};

/**
 * Action -> Execute Query
 * @param {*} query
 * @param {*} querylist
 */
const execQueries = (query, querylist, source) => dispatch => {
  let urlparam = getUrlParambyQueryType(query.type);
  let analyzeobj = {};
  // Auto Update Check
  let isAutoUp = false;
  if (isJsonString(query.criteria)) {
    let autoupdatechk = JSON.parse(query.criteria);
    if (autoupdatechk.hasOwnProperty('realtimeRT')) {
      isAutoUp = autoupdatechk.realtimeRT ? true : false;
    }
  }

  let url = `${urlparam}/${query.queryId}/${query.type}`;

  if (isAutoUp || query.type === 'simplerealtime' || query.type === 'geofencealert') {
    // Reset Execution Status
    dispatch({
      type: QUERY_EXEC_STATUS,
      payload: { status: '', queryId: query.queryId }
    });
    dispatch(execSpinner(true, 'Query Execution In Progress...'));
  }

  if (query.type === 'simplerealtime') {
    if (isJsonString(query.criteria)) {
      let endTime = JSON.parse(query.criteria).endtime;
      if (
        endTime &&
        moment
          .tz(endTime, 'America/Los_Angeles')
          .diff(moment.tz('America/Los_Angeles')) <= 0
      ) {
        dispatch(execSpinner(false, ''));
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: "Query 'End Date/Time' elapsed. Please refine query!"
            }
          })
        );
        return;
      }
    }
  }
  if (query.type === 'traveldetection') {
    let criteria = JSON.parse(query.criteria);
    if (
      criteria.hasOwnProperty('imsi') ||
      criteria.hasOwnProperty('imei') ||
      criteria.hasOwnProperty('msisdn') ||
      criteria.hasOwnProperty('liid')
    ) {
      url = `${urlparam}/${query.queryId}/getgivensubscribertraveldetection`;
    } else {
      url = `${urlparam}/${query.queryId}/getsubscribertraveldetection`;
    }
  }

  let start = Date.now();
  let execMoment = moment()
    .utc()
    .format('YYYY-MM-DD HH:mm:ss.SSS');

  if(!isAutoUp && query.type === 'geofencealert') {
    let currstate = store.getState();
    //Geo Fence Component Call will be here
    dispatch({
      type: QUERY_EXEC_INFO,
      payload: {
        execTime: Date.now() - start,
        queryId: query.queryId,
        resultsCount: 10,
        execAt: moment(Date.now())
          .tz(currstate.areas.timezone)
          .format('YYYY-MM-DD HH:mm:ss z'),
        execMoment: execMoment,
        queryString: query.criteria
      }
    });
    if (isJsonString(query.criteria)) {
      let areaid = JSON.parse(query.criteria).areaid;
      if (areaid) {
        dispatch({ type: LOAD_ANALYZEAREA, payload: areaid });
      }
    } else {
      dispatch({ type: LOAD_ANALYZEAREA, payload: '' });
    }
    dispatch(execSpinner(false, ''));
    dispatch(analyzeGrid(query));
    return;
    //dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
  };

  if (!isAutoUp && query.type !== 'simplerealtime' && query.type !== 'geofencealert') {
    // Reset Execution Status
    dispatch({
      type: QUERY_EXEC_STATUS,
      payload: { status: -1, queryId: query.queryId }
    });
    dispatch({ type: QUERY_EXECUTION_START, payload: query.queryId });
    dispatch({ type: QUERY_GRID_CHECKED, payload: { checkedRows: [] } });
  }
  fetchAxios({
    url: url,
    method: 'get',
    cancelToken: source[query.queryId].token
  })
    .then(res => {
      let utcstr = '';
      let currstate = store.getState();
      let fmt = 'YYYY-MM-DD HH:mm:ss z';
      res.data.map(o => {
        if (o.segmentStartTime && o.segmentStartTime !== null) {
          utcstr = `${o.segmentStartTime.replace(/ /g, 'T')}Z`;
          o.segmentStartTime = moment
            .tz(utcstr, currstate.areas.timezone)
            .format(fmt);
        }
        return o;
      });
      dispatch({
        type: QUERY_EXEC_INFO,
        payload: {
          execTime: Date.now() - start,
          queryId: query.queryId,
          resultsCount: res.data.length,
          execAt: moment(Date.now())
            .tz(currstate.areas.timezone)
            .format('YYYY-MM-DD HH:mm:ss z'),
          execMoment: execMoment,
          queryString: query.criteria
        }
      });
      dispatch({
        type: EXEC_SPINNING,
        payload: { bool: false, tip: '' }
      });
      analyzeobj[query.queryId] = res.data;

      if (query.type === 'simplerealtime') {
        // Init Timer Calls
        dispatch(realTimePooling(query));
      }

      if (
        (isAutoUp && res.data.length >= 0) ||
        (!isAutoUp && res.data.length > 0)
      ) {
        if (!isAutoUp && query.type !== 'simplerealtime' && query.type !== 'geofencealert') {
          fetchAxios({
            url: 'slpqueries/insertexecutionlog',
            method: 'post',
            data: {
              executeTime: execMoment,
              queryId: query.queryId,
              resultJson: JSON.stringify(res.data)
            }
          })
            .then(() => {
              let length = res.data.length;
              dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
              dispatch({
                type: QUERY_EXEC_STATUS,
                payload: { status: length, queryId: query.queryId }
              });
            })
            .catch(err => {
              dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
            });
        } else if(!isAutoUp && query.type === 'geofencealert') {
          //Geo Fence Component Call will be here
          //dispatch(analyzeGrid(query));
          //dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
        } else {
          dispatch(realTimePoolingAll(query));
          dispatch({ type: LOAD_ANALYZELIST, payload: analyzeobj });
          if (isAutoUp && isJsonString(query.criteria)) {
            let areaid = JSON.parse(query.criteria).areaid;
            if (areaid) {
              dispatch({ type: LOAD_ANALYZEAREA, payload: areaid });
            }
          } else {
            dispatch({ type: LOAD_ANALYZEAREA, payload: '' });
          }

          let length = res.data.length;
          dispatch({
            type: QUERY_EXEC_STATUS,
            payload: { status: length, queryId: query.queryId }
          });
          let currstate = store.getState();
          let txt = '';
          if (
            currstate.queries.queryExecInfo.length &&
            currstate.queries.queryExecInfo.find(
              el => el.queryId === query.queryId
            ).execTimeHHMMSS
          ) {
            txt = currstate.queries.queryExecInfo.find(
              el => el.queryId === query.queryId
            ).execTimeHHMMSS;
          }
          let msg = `${res.data.length} results found in ${txt} (Query ID : ${query.queryId})`;
          if (
            query.type === 'simplerealtime' &&
            analyzeobj[query.queryId][0].latitude === null &&
            analyzeobj[query.queryId][0].longitude === null
          ) {
            msg = 'Request sent to GMLC. Please wait!';
          }
          dispatch(
            showNotification({
              notifyType: 'success',
              notifyProps: {
                title: 'Success',
                message: msg
              }
            })
          );

          dispatch(analyzeexeQuery());
        }
      } else {
        dispatch({
          type: QUERY_EXEC_STATUS,
          payload: { status: res.data.length, queryId: query.queryId }
        });
        dispatch({ type: CLEAR_ANALYZEDATA });
        dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
        if (isAutoUp || query.type === 'simplerealtime') {
          dispatch(
            showNotification({
              notifyType: 'success',
              notifyProps: {
                title: 'Success',
                message: 'No results found'
              }
            })
          );
        }
      }
    })
    .catch(err => {
      if (axios.isCancel(err)) {
        if (!isAutoUp && query.type !== 'simplerealtime' && query.type !== 'geofencealert') {
          dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
          dispatch({
            type: QUERY_EXEC_STATUS,
            payload: {
              status: -2,
              queryId: query.queryId,
              message: `Aborted Execution!`
            }
          });
        } else {
          dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
          dispatch({ type: CLEAR_ANALYZEDATA });
        }
      } else {
        // handle error
        let msg = err.message;
        if (
          err.response &&
          err.response.data &&
          err.response.data.message &&
          err.response.data.message.length
        ) {
          msg = err.response.data.message;
          if (!isAutoUp && query.type !== 'simplerealtime' && query.type !== 'geofencealert') {
            dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
            dispatch({
              type: QUERY_EXEC_STATUS,
              payload: {
                status: -2,
                queryId: query.queryId,
                message: `${msg}`
              }
            });
          } else {
            dispatch({
              type: EXEC_SPINNING,
              payload: { bool: false, tip: '' }
            });
            dispatch(
              showNotification({
                notifyType: 'warn',
                notifyProps: {
                  title: 'Warning',
                  message: `Please Refine Query! (Query Id: ${query.queryId}) - [${msg}]`
                }
              })
            );
          }
        } else {
          if (!isAutoUp && query.type !== 'simplerealtime' && query.type !== 'geofencealert') {
            dispatch({ type: QUERY_EXECUTION_END, payload: query.queryId });
            dispatch({
              type: QUERY_EXEC_STATUS,
              payload: {
                status: -2,
                queryId: query.queryId,
                message: `Error while executing Query! [${msg}]`
              }
            });
          } else {
            dispatch({
              type: EXEC_SPINNING,
              payload: { bool: false, tip: '' }
            });
            dispatch(
              showNotification({
                notifyType: 'error',
                notifyProps: {
                  title: 'Error',
                  message: `Error while executing Query! [${msg}]`
                }
              })
            );
          }
        }
      }
    });
};
const analyzeexeQuery = () => dispatch => {
  const currstate = store.getState();
  let isAutoUp = false;
  if (isJsonString(currstate.queries.agGrid.checkedRows[0].criteria)) {
    let autoupdatechk = JSON.parse(
      currstate.queries.agGrid.checkedRows[0].criteria
    );
    if (autoupdatechk.hasOwnProperty('realtimeRT')) {
      isAutoUp = autoupdatechk.realtimeRT ? true : false;
    }
  }
  let qkeys = Object.keys(currstate.analyze.analyzeList);
  if (qkeys.length && currstate.analyze.analyzeList[qkeys[0]].length) {
    if (
      currstate.analyze.analyzeList[
        currstate.queries.agGrid.checkedRows[0].queryId
      ]
    ) {
      if (currstate.queries.agGrid.checkedRows[0].type === 'intersect') {
        let splittext = currstate.queries.agGrid.checkedRows[0].criteria
          .replace(/\|AND\|/g, '|')
          .replace(/\|OR\|/g, '|');
        let criteriaarray = splittext.split('|');
        let areaIds = [];
        criteriaarray.forEach(areas => {
          areaIds.push(areas.split(',')[1]);
        });
        dispatch(analyzeAreas(areaIds.join(',')));
      } else {
        if (currstate.queries.agGrid.checkedRows[0].type !== 'simpleHistory') {
          let criteria = JSON.parse(
            currstate.queries.agGrid.checkedRows[0].criteria
          );
          if (criteria.areaid) {
            dispatch(analyzeAreas(criteria.areaid));
          }
        }
      }
      dispatch(analyzeQuery(currstate.queries.agGrid.checkedRows[0]));
    } else {
      if (isAutoUp) {
        let criteria = JSON.parse(
          currstate.queries.agGrid.checkedRows[0].criteria
        );
        if (criteria.areaid) {
          dispatch(analyzeAreas(criteria.areaid));
        }
        dispatch(analyzeQuery(currstate.queries.agGrid.checkedRows[0]));
      } else {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `There is no data for current query !`
            }
          })
        );
      }
    }
  } else {
    if (isAutoUp) {
      dispatch(analyzeQuery(currstate.queries.agGrid.checkedRows[0]));
    } else {
      dispatch(
        showNotification({
          notifyType: 'warn',
          notifyProps: {
            title: 'Warning',
            message: `There is no data to Analyze!`
          }
        })
      );
    }
  }
};
/**
 * Action -> Create and Execute Query for Heatmap
 * @param {*} queryJson
 */
const createexecuteheatmapQuery = (
  queryJson,
  id,
  postsource,
  getsource
) => dispatch => {
  dispatch({
    type: EXEC_SPINNING,
    payload: { bool: true, tip: "Fetching 'HeatMap' details!" }
  });
  fetchAxios({
    url: `slpqueries/queries`,
    method: 'post',
    data: queryJson,
    cancelToken: postsource.token
  })
    .then(response => {
      let queryid = response.data.queryId;
      dispatch({
        type: QUERY_HEATMAP_CHILDID,
        payload: { subscriberid: queryid, queryId: id }
      });
      dispatch(getHeatMapData(queryid, getsource));
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Fetching Heatmap!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error creating Heatmap Query! [${err.message}]`
            }
          })
        );
      }
    });
};
/**
 * Action -> update and Execute Query for Heatmap
 * @param {*} queryJson
 */
const updateexecuteheatmapQuery = (
  id,
  childid,
  queryJson,
  postsource,
  getsource
) => dispatch => {
  dispatch({
    type: EXEC_SPINNING,
    payload: { bool: true, tip: "Fetching 'HeatMap' details!." }
  });
  fetchAxios({
    url: `slpqueries/queries/${childid}`,
    method: 'put',
    data: queryJson,
    cancelToken: postsource.token
  })
    .then(response => {
      dispatch(getHeatMapData(childid, getsource));
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Fetching Heatmap!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error creating Heatmap Query! [${err.message}]`
            }
          })
        );
      }
    });
};
/**
 * Action -> Create and Execute Query for Heatmap
 * @param {*} queryJson
 */
const createexecutetrackingQuery = (
  queryJson,
  id,
  calltrackData,
  postsource,
  getsource
) => dispatch => {
  dispatch({
    type: EXEC_SPINNING,
    payload: { bool: true, tip: 'Tracking is in Progress...' }
  });
  fetchAxios({
    url: `slpqueries/queries`,
    method: 'post',
    data: queryJson,
    cancelToken: postsource.token
  })
    .then(response => {
      let queryid = response.data.queryId;
      dispatch({
        type: QUERY_HEATMAP_CHILDID,
        payload: { subscriberid: queryid, queryId: id }
      });
      dispatch(trackSubscriber(queryid, calltrackData, getsource));
      queryJson.queryId = queryid;
      queryJson.subscriberid = id;
      dispatch({ type: TRACK_PARAMS, payload: queryJson });
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Tracking!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error creating Tracking Query! [${err.message}]`
            }
          })
        );
      }
    });
};
/**
 * Action -> update and Execute Query for Heatmap
 * @param {*} queryJson
 */
const updateexecutetrackingQuery = (
  childid,
  queryJson,
  calltrackData,
  postsource,
  getsource
) => dispatch => {
  dispatch({
    type: EXEC_SPINNING,
    payload: { bool: true, tip: 'Tracking is in Progress....' }
  });
  fetchAxios({
    url: `slpqueries/queries/${childid}`,
    method: 'put',
    data: queryJson,
    cancelToken: postsource.token
  })
    .then(response => {
      dispatch(trackSubscriber(childid, calltrackData, getsource));
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Tracking!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error creating Tracking Query! [${err.message}]`
            }
          })
        );
      }
    });
};

/**
 * Save query grid columns state
 * @param {*} savedcols
 */
const saveQueryGridCols = savedcols => dispatch => {
  dispatch({
    type: SAVE_QUERYGRID_STATE,
    payload: savedcols
  });
};

const getUrlParambyQueryType = querytype => {
  switch (querytype) {
    case 'simplegeofence':
      return 'slpsubscriberingeofence/slpgetsubscriberingeofence';

    case 'geofencemeeting':
    case 'intermeeting':
    case 'intrameeting':
      return 'slpmeetingdetection/slpgetmeetingdetection';

    case 'simplerealtime':
      return 'slprealtimecriteria/slprealtimequerycriteria';

    case 'roamingdetection':
      return 'slpraomer/slpgetroamersubscribers';

    case 'simswap':
      return 'slpsubscribersimswap/slpgetsubscribersimswap';

    case 'simpleHistory':
      return 'slpsimplehistory/slpgetsimplehistory';

    case 'intersect':
      return 'slpsimpleintersection/slpgetsimpleintersection';

    case 'traveldetection':
      return 'slptraveldetection/slptraveldetection';

    default:
      return 'slpsimpleintersection/slpgetsimpleintersection';
  }
};
const getagGridColNameByQueryType = querytype => {
  switch (querytype) {
    case 'simplegeofence':
      return agSgfListColDefs;

    case 'roamingdetection':
      return agRdListColDefs;

    case 'simswap':
      return agSSListColDefs;

    case 'simplerealtime':
      return agSRListColDefs;

    case 'simpleHistory':
      return agSHListColDefs;

    case 'traveldetection':
      return agTDListColDefs;

    case 'geofencemeeting':
    case 'intermeeting':
    case 'intrameeting':
      return agMeetingListColDefs;

    default:
      return agListColDefs;
  }
};

const analyzeQuery = queryinfo => dispatch => {
  let gridname = getagGridColNameByQueryType(queryinfo.type);
  dispatch({ type: ANALYZE_GRID_COLNAMES, payload: gridname });
  history.push(`/analyze/${queryinfo.queryId}`);
};

const analyzeGrid = queryinfo => dispatch => {
  history.push(`/analyzegrid/${queryinfo.queryId}`);
};

const analyzeAreas = coords => dispatch => {
  dispatch({ type: LOAD_ANALYZEAREA, payload: coords });
};

export {
  getQueries,
  deleteQueries,
  loadQueriesFormFields,
  loadQueryRulesFormFields,
  setQueriesGridChecked,
  toggleCreatePane,
  execQueries,
  getUrlParambyQueryType,
  analyzeQuery,
  analyzeAreas,
  createQuery,
  updateQuery,
  createexecuteheatmapQuery,
  updateexecuteheatmapQuery,
  createexecutetrackingQuery,
  updateexecutetrackingQuery,
  analyzeexeQuery,
  saveQueryGridCols,
  getRetroDataSources,
  getRetroInterceptNames
};
