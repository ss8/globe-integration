import fetchAxios from '../components/common/fetchAxios';
import history from '../history';
import moment from 'moment-timezone';
import {
  AUTH_LOGIN,
  LOAD_USERLIST,
  LOAD_ROLES,
  EDIT_USER,
  USER_GRID_CHECKED,
  LOAD_USER_FORM_FIELDS,
  CLEAR_CREATE_USER_PANE,
  CREATE_USER_MODE,
  SAVE_USERGRID_STATE,
  LOAD_APPSTATUS
} from './types';
import { isJsonString } from '../utils/uitility';
import { showNotification } from '../actions/notifyActions';
import { execSpinner } from '../actions/modalActions';
import store from '../store/store';
import { userSignedOut } from './authActions';
export const { APP_URL } = window;
/**
 * Action -> Load User List
 */
const getUser = mode => dispatch => {
  fetchAxios({ url: `slpusermgmt/roles`, method: 'get' })
    .then(res => {
      let data = res.data.map(roles => {
        return { value: roles.roleId, label: roles.description };
      });
      dispatch({ type: LOAD_ROLES, payload: data });
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error fetching Roles! [${err.message}]`
          }
        })
      );
    });

  fetchAxios({ url: `slpusermgmt/users`, method: 'get' })
    .then(res => {
      dispatch({ type: LOAD_USERLIST, payload: res });
      if (mode && mode === 'create') {
        dispatch(clearCreatePane());
        dispatch(execSpinner(false, ''));
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `User created Successfully!`
            }
          })
        );
      }
      if (mode && mode === 'update') {
        dispatch(clearCreatePane());
        dispatch(execSpinner(false, ''));
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `User Updated Successfully!`
            }
          })
        );
      }
      if (mode && mode === 'delete') {
        dispatch(clearCreatePane());
        dispatch(execSpinner(false, ''));
        dispatch(
          showNotification({
            notifyType: 'success',
            notifyProps: {
              title: 'Success',
              message: `User(s) deleted Successfully!`
            }
          })
        );
      }
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error fetching User List! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Delete USER
 * @param {*} ids
 */
const deleteUser = UserJson => dispatch => {
  fetchAxios({
    url: `slpusermgmt/users`,
    method: 'delete',
    data: UserJson
  })
    .then(res => {
      dispatch({ type: USER_GRID_CHECKED, payload: { checkedRows: [] } });
      dispatch(getUser('delete'));
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error deleting User! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Create USER
 * @param {*} UserJson
 */
const createUser = UserJson => dispatch => {
  dispatch(execSpinner(true, 'User Creation In Progress...'));
  fetchAxios({
    url: `slpusermgmt/users`,
    method: 'post',
    data: UserJson
  })
    .then(() => {
      dispatch(getUser('create'));
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error creating User! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Update USER
 * @param {*} UserJson
 */
const updateUser = UserJson => dispatch => {
  dispatch(execSpinner(true, 'User Updation In Progress...'));
  fetchAxios({
    url: `slpusermgmt/users`,
    method: 'put',
    data: UserJson
  })
    .then(res => {
      dispatch(execSpinner(false, ''));
      if (res.data.status && res.data.status === 409) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Warning',
              message: `User First Name cannot be updated!`
            }
          })
        );
      } else {
        if (res.config && isJsonString(res.config.data)) {
          let edUser = JSON.parse(res.config.data);
          let currstate = store.getState();
          if (
            edUser.firstName === currstate.auth.username &&
            edUser.roleDtos[0].id === 2
          ) {
            // -- Admin to Analyst Current User, Nav to Areas
            dispatch({
              type: AUTH_LOGIN,
              payload: { ...currstate.auth, roles: ['ROLE_ANALYST'] }
            });
            history.push('/areas');
          }
        }
        dispatch(getUser('update'));
      }
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error updating User! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Edit User
 * @param {*} userRecord
 */
const editUser = userRecord => dispatch => {
  dispatch({
    type: EDIT_USER,
    payload: {
      opMode: 'edit',
      formFields: {
        firstName: userRecord.firstName,
        lastName: userRecord.lastName,
        passWord: userRecord.password,
        userRole: (() => userRecord.roleDtos.map(role => role.id))()
      }
    }
  });
};

/**
 * Save user grid columns state
 * @param {*} savedcols
 */
const saveUserGridCols = savedcols => dispatch => {
  dispatch({
    type: SAVE_USERGRID_STATE,
    payload: savedcols
  });
};

/**
 * Load USER form fields
 * @param {*} firstName
 * @param {*} lastName
 * @param {*} passWord
 * @param {*} userRole
 */
const loadUserFormFields = (field, value) => dispatch => {
  dispatch({
    type: LOAD_USER_FORM_FIELDS,
    payload: { field: field, value: value }
  });
};

/**
 * Create USER View  - mode
 * @param {*} mode
 */
const setCreateUserMode = mode => dispatch => {
  dispatch({
    type: CREATE_USER_MODE,
    payload: { opMode: mode }
  });
};

/**
 * agGrid set checked rows in store
 * @param {*} nodes
 */
const setUserGridChecked = nodes => dispatch => {
  dispatch({ type: USER_GRID_CHECKED, payload: { checkedRows: nodes } });
};
/**
 * Hide Create USER Form
 */
const clearCreatePane = () => dispatch => {
  dispatch({ type: CLEAR_CREATE_USER_PANE });
};

/**
 * Action -> Load APP Monitor Status
 */
const getAppStatus = () => dispatch => {
  fetchAxios({
    url: `http://54.153.16.113:6789/appmonitor/getallservices`,
    //url: `http://localhost:3000/jsonStubs/appstatus.json`,
    method: 'get'
  })
    .then(res => {
      let fmt = 'YYYY-MM-DD HH:mm:ss z';
      let currstate = store.getState();
      let zone = currstate.areas.timezone;
      let data = res.data.map(row => {
        row.lastcontacted = moment
          .utc(row.lastcontacted)
          .tz(zone)
          .format(fmt);
        row.lastrestarted = moment
          .utc(row.lastrestarted)
          .tz(zone)
          .format(fmt);
        return row;
      });
      dispatch({ type: LOAD_APPSTATUS, payload: data });
    })
    .catch(err => {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error fetching App Monitor Status! [${err.message}]`
          }
        })
      );
    });
};

export {
  getUser,
  loadUserFormFields,
  createUser,
  updateUser,
  editUser,
  deleteUser,
  setUserGridChecked,
  setCreateUserMode,
  clearCreatePane,
  saveUserGridCols,
  getAppStatus
};
