import axios from 'axios';
import fetchAxios from '../components/common/fetchAxios';
import {
  ANALYZE_GRID_CHECKED,
  REALTIME_POOL_ID,
  LOAD_TRACKLIST,
  CLEAR_ANALYZEDATA,
  EXEC_SPINNING,
  LOAD_HEATMAPLIST,
  TRACK_PLAY_PAUSE
} from './types';
import { userSignedOut } from './authActions';
import history from '../history';
import { showNotification } from '../actions/notifyActions';
import store from '../store/store';

/**
 * agGrid set checked rows in store
 * @param {*} nodes
 */
const setAnalyzeGridChecked = nodes => dispatch => {
  dispatch({ type: ANALYZE_GRID_CHECKED, payload: { checkedRows: nodes } });
};

/**
 * Play Pause Status of Track
 * @param {*} nodes
 */
const setPlayPause = () => dispatch => {
  dispatch({ type: TRACK_PLAY_PAUSE });
};

/**
 * Action -> Stop Real Time Report Generation
 * @param {*} queryid
 */
const stopRealTimeReportGen = queryid => dispatch => {
  fetchAxios({
    url: `slprealtimecriteria/cancelrequest/${queryid}`,
    method: 'get'
  })
    .then(res => {
      let currstate = store.getState();
      if (currstate.analyze.analyseRealTimePool.id !== null) {
        clearInterval(currstate.analyze.analyseRealTimePool.id);
      }
      if (currstate.analyze.analyseRealTimePool.timerid !== null) {
        clearInterval(currstate.analyze.analyseRealTimePool.timerid);
      }
      dispatch({
        type: REALTIME_POOL_ID,
        payload: { id: null, timerid: null }
      });
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      dispatch(
        showNotification({
          notifyType: 'success',
          notifyProps: {
            title: 'Success',
            message: 'Succefully stopped report generation!'
          }
        })
      );
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error &&
        err.response.data.error === 'invalid_token'
      ) {
        dispatch(userSignedOut());
        history.push('/');
        return;
      }
      dispatch(
        showNotification({
          notifyType: 'error',
          notifyProps: {
            title: 'Error',
            message: `Error while stopping report generation! [${err.message}]`
          }
        })
      );
    });
};

/**
 * Action -> Track Submit
 * @param {*} trackJson
 */
const trackSubscriber = (queryid, callback, source) => dispatch => {
  fetchAxios({
    url: `slptracking/slpgettrackingdetails/${queryid}/tracking`,
    method: 'get',
    cancelToken: source.token
  })
    .then(res => {
      dispatch({ type: LOAD_TRACKLIST, payload: res.data });
      callback();
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      let message = `Fetched 'Track' details Successfully!`;
      if (res.data.length === 0) {
        message = `There is no data to track`;
      }
      dispatch(
        showNotification({
          notifyType: 'success',
          notifyProps: {
            title: 'Success',
            message: message
          }
        })
      );
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Tracking!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error while tracking subscriber! [${err.message}]`
            }
          })
        );
      }
    });
};
/**
 * Action -> Track Submit
 * @param {*} trackJson
 */
const getHeatMapData = (queryid, source) => dispatch => {
  fetchAxios({
    url: `slptraveldetection/subscriberheatmap/${queryid}`,
    method: 'get',
    cancelToken: source.token
  })
    .then(res => {
      if (res.data.heatMapResults !== null) {
        dispatch({ type: LOAD_HEATMAPLIST, payload: res.data.heatMapResults });
      }
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      let message = `Fetched 'HeatMap' details Successfully!`;
      if (res.data.heatMapResults === null) {
        message = `There is no data to show using heatmap`;
      }
      dispatch(
        showNotification({
          notifyType: 'success',
          notifyProps: {
            title: 'Success',
            message: message
          }
        })
      );
    })
    .catch(err => {
      dispatch({ type: EXEC_SPINNING, payload: { bool: false, tip: '' } });
      if (axios.isCancel(err)) {
        dispatch(
          showNotification({
            notifyType: 'warn',
            notifyProps: {
              title: 'Notification',
              message: `Aborted Fetching Heatmap!`
            }
          })
        );
      } else {
        if (
          err.response &&
          err.response.status === 401 &&
          err.response.data.error &&
          err.response.data.error === 'invalid_token'
        ) {
          dispatch(userSignedOut());
          history.push('/');
          return;
        }
        dispatch(
          showNotification({
            notifyType: 'error',
            notifyProps: {
              title: 'Error',
              message: `Error while getting the data! [${err.message}]`
            }
          })
        );
      }
    });
};

const setRealTimePoolId = val => dispatch => {
  dispatch({ type: REALTIME_POOL_ID, payload: val });
};

const clearAnalyzeInits = () => dispatch => {
  dispatch({ type: CLEAR_ANALYZEDATA });
};

export {
  setAnalyzeGridChecked,
  trackSubscriber,
  clearAnalyzeInits,
  setRealTimePoolId,
  stopRealTimeReportGen,
  setPlayPause,
  getHeatMapData
};
