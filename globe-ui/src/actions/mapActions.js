import {
  ADDON_SEARCH,
  ADDON_DRAWCONTROL,
  CLEAR_ON_EXITMAP,
  INIT_MAP_PARAMS,
  INIT_BASEMAP,
  SELECTED_POI,
  LOAD_GEOFENCE_MAP_RESPONSE
} from './types';
import axios from 'axios';
export const { RETRO_URL } = window;

/**
 * Action -> Include/Remove ADDON's
 */
const mapAddonSearch = bool => dispatch => {
  dispatch({ type: ADDON_SEARCH, payload: { hasSearch: bool } });
};

const mapAddonDrawControl = bool => dispatch => {
  dispatch({ type: ADDON_DRAWCONTROL, payload: { hasDrawControl: bool } });
};

const setMapPosition = mapconfig => dispatch => {
  dispatch({
    type: INIT_MAP_PARAMS,
    payload: {
      mapBaseLayer: mapconfig.mapBaseLayer,
      overlayPOI: mapconfig.overlayPOI || '',
      mapPosition: {
        zoom: mapconfig.mapPosition.zoom,
        center: mapconfig.mapPosition.center
      }
    }
  });
};

const setBaseMap = (name, mapconfig) => dispatch => {
  let selmap = !name
    ? mapconfig.mapBaseLayer && mapconfig.mapBaseLayer.length
      ? mapconfig.mapBaseLayer[0].layerName
      : null
    : name;
  dispatch({ type: INIT_BASEMAP, payload: selmap });
};

const setMapPOI = poi => dispatch => {
  dispatch({ type: SELECTED_POI, payload: poi });
};

const clearMapInits = () => dispatch => {
  dispatch({ type: CLEAR_ON_EXITMAP });
};

// Geofence Actions
const getGeofenceMapResponse = (query, currentQueryId) => dispatch => {
  const receivedQuery = query.filter((o) => o.queryId === currentQueryId);
  let queryCriteria = receivedQuery[0] && JSON.parse(receivedQuery[0].queryString);
  let queryCriteriaWithExpression = queryCriteria && queryCriteria.expression;
  if (queryCriteriaWithExpression) {
    delete queryCriteriaWithExpression.startRow;
    delete queryCriteriaWithExpression.endRow;
  }
  axios({
      method: 'post',
      url: `${RETRO_URL}/retrospective-query/map-search`,
      data: queryCriteriaWithExpression
    })
    .then((res) => {
      dispatch({
        type: LOAD_GEOFENCE_MAP_RESPONSE,
        payload: res.data.data
      });
    })
}

export {
  mapAddonSearch,
  mapAddonDrawControl,
  clearMapInits,
  setMapPosition,
  setBaseMap,
  setMapPOI,
  getGeofenceMapResponse
};
