import { CURRENT_MENU } from './types';
/**
 * Action -> Change Menu Selection
 */
const setSelectedMenu = id => dispatch => {
  dispatch({ type: CURRENT_MENU, payload: { selectedmenu: [id] } });
};

export { setSelectedMenu };
