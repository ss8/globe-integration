import {
  AUTH_LOGIN,
  AUTH_LOGOUT,
  RESET_ADMIN,
  RESET_ANALYSE,
  RESET_AREA,
  RESET_MAP,
  RESET_MODAL,
  RESET_NOTIFICATION,
  RESET_QUERY,
  RESET_NAV
} from './types';
/**
 * Action -> Successful Login
 */
const userSignedIn = (user, token, roles) => dispatch => {
  dispatch({
    type: AUTH_LOGIN,
    payload: { status: true, username: user, token: token, roles: roles }
  });
};
/**
 * Action -> Login Failure
 */
const userSignedOut = () => dispatch => {
  dispatch({ type: AUTH_LOGOUT, payload: false });
  /**
   * RESET STORE on Signout
   */
  dispatch({ type: RESET_ADMIN });
  dispatch({ type: RESET_ANALYSE });
  dispatch({ type: RESET_AREA });
  dispatch({ type: RESET_MAP });
  dispatch({ type: RESET_MODAL });
  dispatch({ type: RESET_NOTIFICATION });
  dispatch({ type: RESET_QUERY });
  dispatch({ type: RESET_NAV });
};

export { userSignedIn, userSignedOut };
