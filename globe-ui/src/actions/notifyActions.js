import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from './types';
/**
 * Action -> Show/Hide Notification Message
 */
const showNotification = ({ notifyType, notifyProps }) => dispatch => {
  dispatch({
    type: SHOW_NOTIFICATION,
    payload: { notifyType, notifyProps }
  });
};

const hideNotification = () => dispatch => {
  dispatch({
    type: HIDE_NOTIFICATION
  });
};

export { showNotification, hideNotification };
